#ifndef ROBINNPFRAGSPY_H
#define ROBINNPFRAGSPY_H

#include "RobinNPROIB.h"

namespace ROS{

class RobinNPFragSpy: public RobinNPROIB{

public:

	RobinNPFragSpy(unsigned int robinNPSlot,unsigned int rolMask, bool generateData, unsigned int crcInterval, unsigned int pageSize,unsigned int bufferSize);

	std::vector<std::pair<unsigned int, unsigned int>> m_fragStatusMap;
	std::vector<std::pair<unsigned int, std::string>> m_fragStatusMapDebug;
	std::vector<tbb::concurrent_bounded_queue<ROIBOutputElement>*> channelFragmentStorage;
	int testCounter;
	int m_bufferSize;
	std::string m_fileDumpPath = ".";

	void initStatusMaps();
	bool checkFragError(ROIBOutputElement &fragment);
	std::string parseErrorBits(unsigned int errorBits);
	void dumpFragmentQueueToFile(ROIBOutputElement &errorFragment);
	void storeFragment(ROIBOutputElement &fragment);

};

}

#endif

#ifndef ROBINNPREGISTERSCOMMON_H
#define ROBINNPREGISTERSCOMMON_H


/* These structures will be mapped directly on to the hardware registers */
/* Be careful when adding registers as it may move the allignment       */

typedef struct {
	Dummy64Struct                                   Dummy0;
	DesignIDStruct                                  DesignID;
	GlobalStatusRegisterStruct						GlobalStatusRegister;
	Dummy64Struct                                   Dummy2;
	GlobalControlRegisterStruct                     GlobalControlRegister;
	TemperatureComparatorsStruct                    TemperatureComparatorsRegister;
	InterruptStatusRegisterStruct                   InterruptStatusRegister;
	InterruptMaskRegisterStruct                     InterruptMaskRegister;
	InterruptControlRegisterStruct                  InterruptControlRegister;
	InterruptTestRegisterStruct                     InterruptTestRegister;
	InterruptMaskRegisterStruct                     InterruptPendingRegister;
	MemoryReadInvalidateControlRegisterStruct		MemoryReadInvalidateControlRegister;
	I2CRegisterStruct								I2CRegister;
} BAR0CommonStruct;

typedef struct {
	PrimaryDMAQueueFIFOStruct                       MemoryReadQueueFIFO;
	PrimaryDMAQueueFIFOStruct                       CaptureDMAQueueFIFO;
	PrimaryDMAStatusRegisterStruct                  PrimaryDMAStatusRegister;
	PrimaryDMAControlRegisterStruct                 PrimaryDMAControlRegister;
	PrimaryDMADoneFIFOStruct                        PrimaryDMADoneFIFO;
	SubRobStatusStruct                              SubRobStatus;
	Dummy64Struct                                   Dummy0;
	Dummy64Struct                                   Dummy1;
	PrimaryDMADoneFIFODuplicatorStruct              PrimaryDMADoneFIFODuplicator;
	SubRobControlStruct                             SubRobControl;
	CommonUPFBottomStruct                           CommonUPFBottom;
	CommonUPFTopStruct                              CommonUPFTop;
	CommonUPFBottomInvalidateStruct                 CommonUPFBottomInvalidate;
	CommonUPFTopInvalidateStruct                    CommonUPFTopInvalidate;
	Dummy64Struct                                   Dummy2;
	Dummy64Struct                                   Dummy3;
	Dummy64Struct                                   Dummy4;
	Dummy64Struct                                   Dummy5;
	UPFDuplicateStartLocationRegisterStruct         UPFDuplicateStartLocationRegister;
	UPFDuplicateWriteIndexRegisterStruct            UPFDuplicateWriteIndexRegister;
	UPFDuplicateWriteIndexLocationRegisterStruct    UPFDuplicateWriteIndexLocationRegister;
	UPFDuplicateReadIndexRegisterStruct             UPFDuplicateReadIndexRegister;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_0;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_1;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_2;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_3;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_0_Invalidate;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_1_Invalidate;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_2_Invalidate;
	MemoryOutputFIFOStruct							MemoryOutputSecondFIFO_3_Invalidate;
	Dummy64Struct                                   Dummy6;
	Dummy64Struct                                   Dummy7;
	MemoryControllerStatusStruct                    MemoryControllerStatus;
	MemoryControllerControlStruct                   MemoryControllerControl;
	MemoryTestInputDataStruct                       MemoryTestInputData;
	MemoryTestInputDataStruct                       MemoryTestInputEOF;
	MemoryTestInputAddressStruct                    MemoryTestInputAddress;
} BAR0SubRobStruct;

typedef struct {
	Dummy64Struct                                   Dummy0;
	Dummy64Struct                                   Dummy1;
	Dummy64Struct                                   Dummy2;
	Dummy64Struct                                   Dummy3;
	Dummy64Struct                                   Dummy4;
	Dummy64Struct                                   Dummy5;
	Dummy64Struct                                   Dummy6;
	Dummy64Struct                                   Dummy7;
	Dummy64Struct                                   Dummy8;
	Dummy64Struct                                   Dummy9;
	BufferManagerControlRegisterStruct              BufferManagerControlRegister;
	BufferManagerStatusRegisterStruct               BufferManagerStatusRegister;
	BufferManagerUsedFIFOBottomStruct               BufferManagerUsedFIFOBottom;
	BufferManagerUsedFIFOTopStruct                  BufferManagerUsedFIFOTop;
	BufferManagerUsedFIFOTopStruct                  BufferManagerUsedFIFOTopInvalidate;
	DataGeneratorControlRegisterStruct              DataGeneratorControlRegister;
	BufferManagerFreeFIFOStruct                     BufferManagerFreeFIFO;
	HOLABypassStatusStruct                          HOLABypassStatus;
	HOLABypassRXStruct                              HOLABypassRX;
	HOLABypassTXWriteStruct                         HOLABypassTXWrite;
	HOLABypassTXControlStruct                       HOLABypassTXControl;
	ChannelControlRegisterStruct                    ChannelControlRegister;
	ChannelStatusRegisterStruct                     ChannelStatusRegister;
} BAR0ChannelStruct;

#endif //ROBINNPREGISTERSCOMMON_H

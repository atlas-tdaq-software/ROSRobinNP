#ifndef ROBINNPCONFIG_H
#define ROBINNPCONFIG_H

#include <iostream>
#include <map>
#include "ROSRobinNP/RobinNPHardware.h"

class RobinNPROLConfig{

public:

	RobinNPROLConfig();

	unsigned int getTestSize() const{
		return m_testSize;
	}
	unsigned int getRobId() const{
		return m_robId;
	}
	bool getRolDataGen() const{
		return m_rolDataGen;
	}
	bool getRolEnabled() const{
		return m_rolEnabled;
	}
	bool getDiscardMode() const{
		return m_discardMode;
	}
	bool isDefault() const{
		return m_default;
	}
	bool getDGEarlyWrap() const{
		return m_dataGenEarlyWrap;
	}
	void setTestSize(unsigned int testSize){
		m_testSize = testSize;
	}
	void setRobId(unsigned int robId){
		m_robId = robId;
	}
	void setRolDataGen(bool rolDataGen){
		m_rolDataGen = rolDataGen;
	}
	void setRolEnabled(bool rolEnabled){
		m_rolEnabled = rolEnabled;
	}
	void setDiscardMode(bool discardMode){
		m_discardMode = discardMode;
	}
	void setDefault(bool defaultFlag){
		m_default = defaultFlag;
	}
	void setDGEarlyWrap(bool dataGenEarlyWrap){
		m_dataGenEarlyWrap = dataGenEarlyWrap;
		}

	void dump() const;

private:

	unsigned int m_testSize;   // default emulation fragment size
	bool m_rolDataGen;     	   // use data generator for S-Link input
	bool m_rolEnabled;         // if 0, channel is disabled
	unsigned int m_robId; 	   // channel source id
	bool m_discardMode;        // Discard input fragments
	bool m_default;
	bool m_dataGenEarlyWrap;   // Enable early wrap point for data generator
};

class RobinNPConfig{

public:

	RobinNPConfig();

	void configureRol(RobinNPROLConfig config, unsigned int m_rolId);

	void applyConfig(const RobinNPConfig &incoming);

	RobinNPROLConfig* getRolConfig(unsigned int m_rolId){
		return &m_rolConfig[m_rolId];
	}

	unsigned int getPageSize() const{
		return m_pageSize;
	}
	unsigned int getMaxRxPages() const{
		return m_maxRxPages;
	}
	bool getMemEmulationState() const{
		return m_emulateMem;
	}
	unsigned int getMemWriteTimeout() const{
		return m_memWriteTimeout;
	}
	unsigned int getMemReadTimeout() const{
		return m_memReadTimeout;
	}
	unsigned int getMemfifoFill() const{
		return m_memFifoFill;
	}
	unsigned int getCRCCheckInterval() const{
		return m_crcCheckInterval;
	}
	std::string getDumpFilePath() const{
		return m_dumpFilePath;
	}
	int getFragDumpLimit() const{
		return m_fragDumpLimit;
	}
	int getErrorDumpLimit() const{
		return m_errorDumpLimit;
	}
	unsigned int getUPFDelaySteps() const{
		return m_upfDelaySteps;
	}
	unsigned int getNumPages() const{
		return m_numPages;
	}

	void setPageSize(unsigned int pageSize){
		m_pageSize = pageSize;
	}
	void setMaxRxPages(unsigned int maxRxPages){
		m_maxRxPages = maxRxPages;
	}
	void setMemEmulationState(bool emulate){
		m_emulateMem = emulate;
	}
	void setMemWriteTimeout(unsigned int timeout){
		m_memWriteTimeout = timeout;
	}
	void setMemReadTimeout(unsigned int timeout){
		m_memReadTimeout = timeout;
	}
	void setMemfifoFill(unsigned int fill){
		m_memFifoFill = fill;
	}
	void setCRCCheckInterval(unsigned int interval){
		m_crcCheckInterval = interval;
	}
	void setDumpFilePath(std::string dumpFilePath){
		m_dumpFilePath = dumpFilePath;
	}
	void setFragDumpLimit(int fragDumpLimit){
		m_fragDumpLimit = fragDumpLimit;
	}
	void setErrorDumpLimit(int errorDumpLimit){
		m_errorDumpLimit = errorDumpLimit;
	}
	void setUPFDelaySteps(int upfDelaySteps){
		m_upfDelaySteps = upfDelaySteps;
	}
	void setNumPages(unsigned int numPages){
		m_numPages = numPages;
	}
	void dumpGlobal() const;
	void dumpAll() const;
	void dumpRol(unsigned int rolId) const;


private:

	unsigned int m_maxRxPages; // truncate incoming fragments after so many pages
	unsigned int m_pageSize;   // buffer manager page size
	bool m_emulateMem;
	unsigned int m_memWriteTimeout;
	unsigned int m_memReadTimeout;
	unsigned int m_memFifoFill;
	unsigned int m_crcCheckInterval;
	std::string m_dumpFilePath;
	int m_fragDumpLimit;
	int m_errorDumpLimit;
	unsigned int m_upfDelaySteps;
	RobinNPROLConfig m_rolConfig[s_maxRols];
	//std::map<unsigned int,RobinNPROLConfig> m_rolConfig;
	unsigned int m_numPages;    // Allow fixed number of pages for specialised operational modes

};

#endif

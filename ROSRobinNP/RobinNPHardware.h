/*
 Main RobinNP hardware header file, defines basic data types and structures
 */

#ifndef ROBINNPHARDWARE_H
#define ROBINNPHARDWARE_H

//RobinNP headers
#include "RobinNP_common.h"

#ifndef NO_MONITORING
#define NO_MONITORING
#endif

/*
For compatibility with Win32, GCC supports a set of #pragma directives which change the
maximum alignment of members of structures (other than zero-width bitfields), unions,
and classes subsequently defined. The n value below always is required to be a
small power of two and specifies the new alignment in bytes.

Alternatively:
-fpack-struct[=n]
    Without a value specified, pack all structure members together without holes.
	When a value is specified (which must be a small power of two),
	pack structure members according to this value, representing the
	maximum alignment (that is, objects with default alignment requirements larger
	than this will be output potentially unaligned at the next fitting location.
 */

// make sure structures are packed
//#pragma pack(1)

#include <string>

/// for tests we want to have the event log enabled
#define EVENT_LOG

// set a fixed number in absence of real run number
#define FIXED_RUN_NUMBER  //When do we turn this off? WPV

//Hardware specific parameters
static const unsigned int s_4k					   = 0x1000;
static const unsigned int s_maxSubRobs             = 2;             // information available from RobinNP registers, hard coding until multi-threading implementation complete
static const unsigned int s_robFormatVersion 	   = 0x05000000;    // rob format version 5.0
static const unsigned int s_rodFormatVersion 	   = 0x03010000;
static const unsigned int s_fpfSize 			   = 2049;
static const unsigned int s_upfSize 			   = 28;
static const unsigned int s_commonUpfSize		   = 514;
static const unsigned int s_mediaSizeMask		   = 0x1fff;	    // only bits 0..12 can be used, bit 13 is used to mark "partial" packets
static const unsigned int s_maxRols				   = 12;
static const unsigned int s_numUPFRegs			   = 4;
static const unsigned int s_eventLogSize		   = 256;            // formerly arbitrary value, now matching total possible number of ECRs for new Data driven trigger
// management parameters, values in words
static const unsigned int s_mgmtNumHashBits        = 20;//18;//16;
static const unsigned int s_mgmtMinPageSize        = 256;           // MIN_PAGE_BYTE_SIZE/sizeof(unsigned int),
static const unsigned int s_mgmtMaxPageSize        = 4096;          // MAX_PAGE_BYTE_SIZE/sizeof(unsigned int),
static const unsigned int s_mgmtMaxBufSize         = 1024*1024*1024;//1024*1024*1024;//16*1024*1024;//32*1024*1024;//16*1024*1024; // MAX_BUFFER_BYTE_SIZE/sizeof(unsigned int),
static const unsigned int s_mgmtRejectedSize       = 100;	        // rejected page storage
static const unsigned int s_mgmtMaxNumPages        = 0x100000;
static const unsigned int s_numHashLists 		   = 1 << s_mgmtNumHashBits;
static const unsigned int s_hashMask 			   = (~(0xffffffff << s_mgmtNumHashBits));
//The format of the ROB-fragment is defined in ATL-DAQ-98-129. <br>
static const unsigned int s_robHdrStatusWords      = 2;
static const unsigned int s_robHdrCrcType          = 0x01;		    // Robin used 16 bit CRC
static const unsigned int s_robHdrCrcTypeNone      = 0x00;		    // No CRC on truncated fragments
static const unsigned int s_robHdrSpecificWords    = 0;
//Header markers and IDs
static const unsigned int s_hdrMarkerRod           = 0xee1234ee;
static const unsigned int s_hdrMarkerRob           = 0xdd1234dd;
static const unsigned int s_fpfFillShift           = 0;             // shift by this value, then apply mask
static const unsigned int s_fpfFillMask            = 0xfff;
//Buffer Manager
static const unsigned int s_bmMaxFragPages         = 512;	        // allow 1MB fragments at 2kB typical page size
//Run control state definition - Valid run control states are: START, STOP. States are defined PER ROL
static const unsigned int s_runCtlStop             = 0;
static const unsigned int s_runCtlStart            = 1;
//static const unsigned long s_dmaDmaDoneEntry64 = 0x40000000;
//static const unsigned long s_dmaOffsetNoTWord64 = 0x80000000;
//DMA Control - not including flag to allow termination word dma with no offset, as this would allow overwriting of data, even though board is capable of it
static const unsigned int s_dmaOffsetTWord = 0xc0000000;                        // Mask to enable a DMA using both the defined offset and writing a termination word
static const unsigned int s_dmaOffsetNoTWord = 0x80000000;                      // Mask to enable a DMA using the defined offset but writing no termination word
static const unsigned int s_dmaNoOffsetNoTWord = 0x0;                           // Mask to enable a DMA using neither the defined offset nor writing a termination word
static const unsigned int s_dmaDmaDoneEntry = 0x20000000;
static const unsigned long s_dmaDmaDoneEntry64 = 0x2000000000000000;
static const unsigned long s_dmaOffsetNoTWord64 = 0x8000000000000000;
static const unsigned int s_dmaOffsetNoDmaDone = 0x4000;
static const unsigned int s_dmaNoOffsetDmaDone = 0x8000;
static const unsigned int s_dmaOffsetDmaDone = 0xC000;
//static const unsigned int s_DMAQueueFIFOSize = 258;
static const unsigned int s_DMAQueueFIFOSize = 2050;
static const unsigned int s_captureDMAQueueFIFOSize = 16; //Actually 18, but only using 16 bits for done fifo communication
static const unsigned int s_captureCountResetCtrlValue = 4050;
static const unsigned int s_captureIndexOffset = 4000;
static const unsigned int s_fragMinThreshold = 0x02000000;  // min threshold for pending range
static const unsigned int s_fragMaxThreshold = 0xfe000000;   // max threshold for pending range
static const unsigned int s_threadTimeoutInterval = 1000;   // in seconds
static const unsigned int c_order = 16;
static const unsigned int s_DMAInterruptCodes[s_maxSubRobs] = {5,6};
static const unsigned int s_UPFInterruptCodes[s_maxSubRobs] = {3,4};
static const unsigned int s_FIFOFillOffset = 2; //Some RobinNP FIFO's report a fill of 2 when empty, use offset to correct
static const unsigned int s_numStatusWordBits = 32;
static const unsigned int s_mgmtMinDelaySize = 256;

//Authors
static const unsigned int s_numAuthors = 7;
static std::string authorNamesList[s_numAuthors + 2] = {"Unknown","B. Green","G. Kieft","A. Kugel","M. Mueller","Krause","N. Schroer","A. Borga","Unknown"};
static std::string hardwareList[2] = {"PLDA XpressV6","ALICE C-RORC"};

// -------------------------------------------------------------------------------
/// Status values for RobFragment (in ROB header)
/**
Zero: Fragment OK <br>
Non-Zero: Fragment corrupted, further details application specific.
 */

enum FragStatusIndicator{
	fragStatusOk            = 0,           // fragment OK
	// specific errors
	// lower 8 bits mapped from UPF error bits
	// TTsync removed, bit unused
	fragStatusSizeError	    = 0x00020000,  // UPF: fragment size error
	fragStatusDataError	    = 0x00040000,  // UPF: data block error
	fragStatusCtlError		= 0x00080000,  // UPF: ctl word error
	fragStatusBofError		= 0x00100000,  // UPF: missing BOF
	fragStatusEofError		= 0x00200000,  // UPF: missing EOF
	fragStatusMarkerError	= 0x00400000,  // UPF: invalid header marker
	fragStatusFormatError	= 0x00800000,  // UPF: Major format version mismatch
	// upper 8 bits: higher-level indicators
	fragStatusDuplicate	    = 0x01000000,  // duplicate fragment. BC-ID of this fragment needs to be verified
	fragStatusSeqError		= 0x02000000,  // out-of-sequence fragment
	fragStatusTxError		= 0x04000000,  // transmission error. Set on any S-Link or fragment format error condition
	fragStatusTrunc		    = 0x08000000,  // Fragment truncation
	fragStatusShort		    = 0x10000000,  // ROD size smaller than header + trailer
	fragStatusLost			= 0x20000000,  // fragment lost. No data is available for the requested L1ID
	fragStatusPending		= 0x40000000,  // fragment pending. No data available yet, but might still arrive
	fragStatusDiscard		= 0x80000000,  // Indicates discard mode
	// generic errors
	fragGenStatusTime		= 0x00000004,  // timeout error: fragment lost or pending
	fragGenStatusData		= 0x00000008,  // data error: corrupted fragment
	fragGenStatusBuffer	    = 0x00000010,  // buffer error: truncation
};

// -------------------------------------------------------------------------------
// Monitoring and error structures

/// Fragment statistics
enum FragStat{
	fragsReceived = 0,
	fragsAvail,
	fragsNotAvail,
	fragsPending,
	fragsAdded,
	fragsDeleted,
	fragsTruncated,
	fragsCorrupted,
	fragsReplaced,
	fragsOutOfSync,
	fragsMissOnClear,
	fragsDiscarded,
	fragsShort,
	fragsLong,
	fragsTxError,
	fragsFormatError,
	fragsMarkerError,
	fragsFramingError,
	fragsCtrlWordError,
	fragsDataWordError,
	fragsSizeMismatch,
	fragsRejected,
	LAST_FRAGSTAT
};


/// Page statistics
enum PageStat{
	pagesReceived = 0,
	pagesAdded,
	pagesDeleted,
	pagesProvided,			// written to free page FIFO
	pagesSupressed,			// truncation
	pagesInvalid,
	LAST_PAGESTAT
};

enum RunMode{
	runModeLegacy = 0,
	runModeGen2ROS,
	runModeROIB
};

/// Fragment info received from FPGA buffer manager
// make sure that a 128 byte is a multiple of this size for UPFDMA
struct UpfInfo{
	unsigned int status;		// has 2 entries: Byte lastPage, fragmentStatus (24 Bit)
	unsigned int eventId;		// has 2 entries: Byte ECR, L1ID (24 Bit)
	unsigned int pageNum;		// embedded trigger type in upper byte
	unsigned short pageLen;
	unsigned int runNum;		// run number
};


/// Fragment info received from FPGA buffer manager
struct FragInfo{
	UpfInfo upfInfo;
	unsigned int trigType;
	unsigned int prevId;
	unsigned int RolId;
} ;

/// event identification via l1id and run number
struct EventTag{
	unsigned int eventId;
	unsigned int runNumber;
};

// -------------------------------------------------------------------------------
/// Management pointer needs to hold indices of elements
/** size must be sufficiently large, e.g. Word for 16 bit hash key and 64k pages
 */
typedef unsigned int MgmtPtr;


/// Management elements for fragment lists
/**
A management entry is composed of two word pointers (previous, next),
the FragInfo field, a status field and a fragment size field (total size in words)<br>
In addition, there is a flag to indicate completion (i.e. all pages of a multi-page
fragment received.<br>
 */
struct MgmtEntry
{
	MgmtPtr prev;
	MgmtPtr next;
	FragInfo info;
	unsigned int status;
	// unsigned int size;
	unsigned short size;
	bool isComplete;
	bool isErrorFragment;
	bool toDelete;
};

/// DMA Info Token
struct DMAInfo{
	unsigned long physicalAddressDMA;
	MgmtEntry* memoryPage;
	unsigned int rolId;
	bool useHeader;
} ;

/// Management elements for rejected pages
/**
Keeps UPF info block and MRI
 */
struct RejectedEntry
{
	unsigned int mri;		// most recent (valid) L1ID
	FragInfo info;
};

// -------------------------------------------------------------------------------
/// struct RobMsg defines generic ROB Fragment message


struct RobMsgHdr{
	// generic
	unsigned int marker;
	unsigned int totalSize;
	unsigned int hdrSize;
	unsigned int version;
	unsigned int srcId;
	unsigned int statCnt;			// 2 status words here
	unsigned int fragStat;
	unsigned int mrEvntId;         // most recent event id from S-Link
	unsigned int crc;				// crc added in event format version 4
	// specific: none
	// unsigned int specCnt;			// spec count removed in event format version 4
};

// -------------------------------------------------------------------------------
/**
The format of the ROD-fragment is defined in ATL-DAQ-98-129. <br>
Header:<br>
0	Start of header marker
1	Header size
2	Format version number
3	Source identifier
4	Run number
5	Extended Level 1 ID
6	Bunch crossing ID
7	Level 1 trigger type
8	Detector event type
Trailer:<br>
0	Status word
1	Number of status words
2	Number of data elements
3	Status block position (1: status after data)
 */
///Header struct for ROD fragment
struct RodHeader{
	unsigned int hdrMarker;
	unsigned int hdrSize;
	unsigned int eventFormat;
	unsigned int srcId;
	unsigned int runId;
	unsigned int eventId;
	unsigned int bxId;
	unsigned int trigType;
	unsigned int eventType;

};

///Trailer struct for ROD fragment
struct RodTrailer{
	unsigned int status;
	unsigned int numStatusWords;
	unsigned int numDataWords;
	unsigned int statusPosition;
};

///Overall struct for ROD fragment
struct RodFragment{
	RodHeader header;
	// the data section is in between here
	RodTrailer trailer;
};

// -------------------------------------------------------------------------------
/// DMA command structures

struct DmaDesc
{
	int hdrInfo;		// consists of VLAN-tag (if enable), VLAN Enable Flag, Incomplete-header flag, size of header in 32 bit words
	int dataSize;		// size of data block in 32 bit words, if to be taken from buffer. Else 0
	int dataAddr;		// start address of data block in buffer. Upper 4 bits used for ROL number
};

enum UpfFlags{
	upfValid       = 0x800000,          // valid flag: bit 23
	upfEndMask     = 0x400000,          // last page marker: bit 22
	upfEndMarker   = 0x400000,                 // last page marker. otherwise value is 0x40000000
	upfMoreMarker  = 0x400000,          // non-last page marker
	upfChannel     = 0xF0000,          // channel number: bits 19 to 16

	//Currently disabled conditions that were externally mapped in original ROBIN
	//upfMissingBofErrorFlag // not used, fragments dumped - may need to re-implement

	// Ensure up to date flag information available on twiki
	upfFlagCount			= 22,		  // number of flags

	upfDataWordTXErr	  		  =  (1 << 0),
	upfCtrlWordTXErr	      	  =  (1 << 1),
	//2 dummy bits
	upfMissingRODHdrMarker 		  =  (1 << 4),
	upfWrongRODHdrVersion  		  =  (1 << 5),
	upfFragTrailerLengthMismatch  =  (1 << 6),
	upfBOFPresent         	      =  (1 << 7),
	upfFragmentTooLong      	  =  (1 << 8),
	upfFragmentTooShort     	  =  (1 << 9),
	upfRunNumberPresent     	  =  (1 << 10),
	upfTriggerTypePresent  	      =  (1 << 11),
	upfL1IDPresent 		  	      =  (1 << 12),
	//3 dummy bits
	upfLDErr		         	  =  (1 << 15), //lderr asserted by HOLA core
	//4 bits for duplicator channel
	upfFramingError       		  =  (1 << 20), //didn't end on EOF
	upfEOFPresent         		  =  (1 << 21),


	// error combinations
	upfControlErrorFlag	          = (upfLDErr | upfCtrlWordTXErr),
	upfDataErrorFlag		      = (upfLDErr | upfDataWordTXErr),

	upfErrorMask = upfFramingError | upfControlErrorFlag | upfDataErrorFlag | upfFragmentTooShort | upfFragmentTooLong | upfFragTrailerLengthMismatch | upfWrongRODHdrVersion | upfMissingRODHdrMarker,
	upfHardErrorMask = upfFragmentTooShort, //currently any fragment shorter than header size will be a hard error, even if there are enough words for a L1 ID

	// UPF 1: event id
	upfEcrMask   = 0xff000000,             // ECR mask
	upfEcrShift  = 24,			           // ECR right shift

	// UPF 2: trigger type and address
	upfAddrMask       = 0x3fffffff,        // lower 30 bits used for buffer address // was 3f at end instead of 1
	upfTtMask	      = 0xff000000,	       // upper 8 bits used for TT
	upfTtShift	      = 24,		           // TT right shift
	upfStatusMask     = 0xFFFFFF          // lower 24 bits used for page status

};

enum BufMgrCtl{
	bmCtlXoff      = (1 << 31),            // Xoff bit
	bmCtlKeepCtl   = (1 << 30),            // control word stripping
	bmCtlReset     = (1 << 8),             // BufMan and UPF/FPF FIFO reset
	// finish conditions
	finish_on_format_ver    = (1 << 13),
	finish_on_frag_size     = (1 << 14),
	finish_on_hdr_mark      = (1 << 15),
	finish_on_CNTR          = (1 << 16),
	finish_on_BOF           = (1 << 17),
	finish_on_EOF           = (1 << 18),
	finish_on_GeneralError  = (1 << 19),
	finish_on_TriggerType   = (1 << 20),
	finish_on_RunNumber     = (1 << 21),
	// finish_on_Double_BOF    = (1 << 22),
	// finish_on_Double_EOF    = (1 << 23),
	finish_on_Missing_BOF   = (1 << 24),
	finish_on_Missing_EOF   = (1 << 25),
	finish_on_IncomplHeader = (1 << 26),
	finish_on_NoHeader      = (1 << 27),
	finish_on_CTL_Error     = (1 << 28),
	finish_on_Data_Error    = (1 << 29),
	//
	bmCtlSizeMask  = 0x0f,
	bmCtlSize1k    = 0,
	bmCtlSize2k    = 1,
	bmCtlSize4k    = 2,
	bmCtlSize8k    = 3,
	bmCtlSize16k   = 4,
	bmCtlInitVal = bmCtlXoff | bmCtlKeepCtl | finish_on_EOF | finish_on_Missing_EOF

};

#endif // ROBINNPHARDWARE_H

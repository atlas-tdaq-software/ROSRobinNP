#ifndef ROSROBINNP_ROLNP_H
#define ROSROBINNP_ROLNP_H

//C++ headers
#include <vector>
#include <string>

//RobinNP headers
#include "RobinNP.h"

namespace ROS 
{

struct ECRStatisticsBlock
{
	unsigned int mostRecentId;
	unsigned int overflow;
	unsigned int necrs;
	unsigned int ecr[25];
};

struct KeptPage
{
	RejectedEntry rentry;
	unsigned int pagedata_size;
	unsigned int pagedata[s_mgmtMaxPageSize];
};

class RobinNP;
class RolNP
{
public:

	RolNP(RobinNP & robinNPBoard, unsigned int rolId);
	virtual ~RolNP();
	RolNP(const RolNP&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RolNP& operator=(const RolNP&) = delete; //!< Assignment Operator - NON-COPYABLE

	void requestFragment(unsigned int eventId, unsigned char *replyAddress, unsigned long physicalAddress, unsigned int ticket);
	unsigned int *getFragment(void *responseAddress, unsigned int ticket);
	bool releaseFragment(const std::vector <unsigned int> *eventIds);
	bool releaseFragmentAll(const std::vector <unsigned int> *eventIds);
	int getTemperature();
	RobinNPROLStats *getStatistics();
	ECRStatisticsBlock getECR();
	KeptPage getRejectedPage(unsigned int pageno);

	void setRolEnabled(bool status);
	void reset();
	void clearStatistics();
	void clearRejectedPages();
	void configureDiscardMode(unsigned int mode);
	void setCRCInterval(int crc_interval);
	unsigned short crctablefast16(unsigned short* p, unsigned long len);
	unsigned short crctablefastwithskip16(unsigned short* p, unsigned long len);

	void startThreads();
	unsigned int getRolId();
	unsigned int getRolState();
private:
	RobinNP & m_robinNP;
	unsigned int m_rolId;
	unsigned int m_crc_interval;

	//CRC stuff
	unsigned long crcinit_direct;
	unsigned long crctab16[65536];
	unsigned long crcmask;
	static const unsigned int c_order;
};

/*! \class RolNP
 *  \brief RobinNP Readout Link Adapter Class.
 *
 *  This class provides an interface for the Readout system to core RobinNP ROL functions\n
 *
 */

}
#endif //ROSROBINNP_ROLNP_H

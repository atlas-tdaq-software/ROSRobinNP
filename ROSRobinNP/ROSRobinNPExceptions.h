

#ifndef ROSROBINNP_ROSROBINNPEXCEPTIONS_H
#define ROSROBINNP_ROSROBINNPEXCEPTIONS_H

//C++ headers
#include <string>
#include <iostream>

//TDAQ headers
#include "DFExceptions/ROSException.h"

namespace ROS
{

class ROSRobinNPExceptions : public ROSException
{
public:

	enum ErrorCode
	{
		MUNMAP,
		MMAP,
		OPEN,
		CLOSE,
		IOCTL,
		NROLS,
		WRONGREPLY,
		BADCRC,
		BADDELETE,
		UNIMPLEMENTED,
		ROLCOUNT,
		LOCK,
		SHARED,
		UNSHARED,
		THREAD,
		FRAGLOST,
		NOPAGES,
		NOHARDWARE,
		MISSINGPAGE,
		FRAGDUP,
		INTERRUPTFAIL,
		INTERRUPTINIT,
		BADID,
		INIT,
		RXPAGE,
		BADREQUEST,
		REQDISCARD,
		REJECTEDFRAGMENT,
		FRAGMENTERROR,
		FRAGPEND,
		LINKRESET,
		RXDMA,
		MAXFRAG,
		RUNMODE,
		CONFIGURE,
		CONDWAITERR
	};

	ROSRobinNPExceptions(ErrorCode errorCode);
	ROSRobinNPExceptions(ErrorCode errorCode, std::string description);
	ROSRobinNPExceptions(ErrorCode errorCode, const ers::Context& context);
	ROSRobinNPExceptions(ErrorCode errorCode, std::string description, const ers::Context& context);
	ROSRobinNPExceptions(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

	virtual ~ROSRobinNPExceptions() throw() { }

protected:
	virtual std::string getErrorString(unsigned int errorId) const;
	virtual ers::Issue * clone() const { return new ROSRobinNPExceptions( *this ); }
	static const char * get_uid() {return "ROS::ROSRobinNPExceptions";}
	virtual const char* get_class_name() const {return get_uid();}

};

/*! \class ROSRobinNPExceptions
 *  \brief RobinNP Exception Handling Class.
 *
 *  This class provides an interface for the RobinNP to the DataFlow exception handling system\n
 *
 */

}
#endif //ROSROBINNP_ROSROBINNPEXCEPTIONS_H

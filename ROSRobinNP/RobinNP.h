/*******************************************************/
/*                                                     */
/* File RobinNP.h                                      */
/*                                                     */
/*******************************************************/


#ifndef ROSROBINNP_ROBINNP_H
#define ROSROBINNP_ROBINNP_H

//C++ headers
#include <iostream>
#include <vector>
#include <queue>
#include <atomic>
//System headers
#include <stdint.h>
#include <pthread.h>
#include <semaphore.h>
#include <chrono>
#include <ctime>

#include "tbb/concurrent_queue.h"


//TDAQ headers
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"

//RobinNP headers
#include "RobinNPRegisters.h"
#include "RobinNPRegistersCommon.h"
#include "RobinNPHardware.h"
#include "RobinNPMonitoring.h"
#include "RobinNPConfig.h"
#include "RobinNPStats.h"
#include "ROSRobinNP/ROSRobinNPExceptions.h"

#include "ROSDescriptorNP/NPRequestDescriptor.h"

unsigned int decimalToBinary(unsigned int decimal);

namespace ROS
{
class RolNP;
class RobinNPMonitoring;

struct ROIBOutputElement{
	DataPage* page; 				// Location of data
	unsigned int rolId; 			// ROL ID associated with data
	unsigned int dataSize; 			// Size of DMA payload
	unsigned int fragmentStatus;	// Status of fragment error checking (s-link, L1 ID and CRC checks)
  unsigned int robinNPPageNumber;
};

class RobinNP
{

	friend class RobinNPBIST;

public:

	/**
	 * \defgroup Initialisation Startup and Initialisation Methods
	 * @{
	 */
	/*************************************************************************/
	/*          Construction / Destruction                                   */
	/*************************************************************************/

	RobinNP(unsigned int slotNumber, unsigned int runMode = 0, tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue = 0, std::string sharedMemoryName = "/sharedMemRobinNP_"); //!< Default Constructor.

	virtual ~RobinNP(); //!< Destructor.

	RobinNP(const RobinNP&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNP& operator=(const RobinNP&) = delete; //!< Assignment Operator - NON-COPYABLE

	/*************************************************************************/
	/*                         Board initialisation stuff                    */
	/*************************************************************************/

	void init(); //!< Run RobinNP initialisation sequence (HW and SW).

	/**@}*/

	/**
	 * \defgroup Legacy Methods for use with old-style readout modules
	 * @{
	 */

	/*************************************************************************/
	/*                         Legacy Methods                                */
	/*************************************************************************/

	unsigned char** getVirtArray(); //!< Return array of virtual addresses for DMA landing pages.
	unsigned long* getPhysArray(); //!< Return an array of physical addresses for DMA landing pages.

	bool receiveDMA(unsigned int rol, unsigned int ticket); //!< Wait for DMA completion (legacy mode).

	RolNP *getRolVector(int rolId); //!< Return vector of all readout links associated with the RobinNP.
	RolNP & getRol(unsigned int rolId); //!< Return a particular readout link.

	void requestFragment(unsigned int rolId, unsigned char *replyAddress, unsigned long physicalAddress, unsigned int eventId, unsigned int ticket); //!< Request the transfer of an event via DMA (legacy mode).

	bool getLegacyStatus(); //!< Return true if RobinNP running in legacy mode.

	/**@}*/

	/**
	 * \defgroup newinit New ROS Initialisation Methods (not for use with legacy code)
	 * @{
	 */

	/*************************************************************************/
	/*                         ROS Initialisation                            */
	/*************************************************************************/

	unsigned int getHeaderSize(); //!< Return size of ROB header.

	void initHeader(std::vector<unsigned int> * headerPage, unsigned int atlasID); //!< Initialise ROB header container, filling in known constants.

	void registerChannelIndex(unsigned int rolId,unsigned int channelIndex); //!< Associate a readout link with a ROS-wide ID.
	void registerSubRobIndex(unsigned int subRob,unsigned int subRobIndex); //!< Associate a subRob with a ROS-wide ID.

	void setCollectorQueue(tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue); //!< Store the location of the return queue for interaction with data collection system.
	void setupDirectOutputQueue(std::vector<tbb::concurrent_bounded_queue<ROIBOutputElement>*> *directOutputQueue);
	std::pair<unsigned long, unsigned char*> getMemPage(); //!< Take a DMA landing page from the available stack.

	void recycleRobinNPPage(unsigned int rolId, unsigned int pagenum);
	void recycleMemPage(unsigned long physicalAddress, unsigned char* virtualAddress); //!< Return a DMA landing page to the available stack.

	void initialisePageQueues(std::vector<tbb::concurrent_bounded_queue<DataPage*>*>& queues); //!< Fill Mk2 ROS Page Queues from RobinNP stack.

	int getRolState(unsigned int rolId);

	/**@}*/

	/**
	 * \defgroup newreq New ROS Initialisation Data Access Methods
	 * @{
	 */


	/*************************************************************************/
	/*                        New ROS Dataflow                               */
	/*************************************************************************/

	void requestFragment(unsigned int subRob, std::vector<unsigned int> *channelList,NPRequestDescriptor* descriptor);
	bool clearRequest(const std::vector<unsigned int> *l1IDs, int rolId = -1); //!< Delete given events from index for a given readout link (all by default).

	/**@}*/

	/**
	 * \defgroup misc Miscellaneous access methods
	 * @{
	 */

	/*************************************************************************/
	/*                        Misc                                           */
	/*************************************************************************/

	unsigned int getNumRols(); //!< Return number of readout links associated with the RobinNP.

	unsigned int getNumDMAPages(); //!< Return number of DMA pages currently managed by RobinNP.

	unsigned int getIdentity(); //!< Return PCI slot number of RobinNP.

	unsigned int getParentSubRob(unsigned int rolId); //!< Return the subRob (ROBgroup) to which a readout link belongs.

	unsigned int getNumSubRobs(); //!< Return the number of subRobs (ROBgroups) associated with the RobinNP.

	unsigned int getNumChannelsPerSubRob(); //!< Return the number of readout links associated with each subRob (ROBgroup).

	unsigned int getSubRob(unsigned int rolId); //!< Return the subRob (ROBgroup) associated with a given readout link.

	void testProbe(); //!< Temporary developer function (ignore).

	void linkReset(unsigned int rolId);

	void memoryErrorCatcher(unsigned int rolId, unsigned long virtualAddress, unsigned int eventID, unsigned int memAddress);

	void directFragmentDMA(FragInfo *theFragment,unsigned int rolId);

	void resetDumpFileCounter();

	void resetErrorDumpFileCounter();

	void requestErrorFragmentDMA(MgmtEntry* entry,unsigned int rolId,std::string& errorDescription);

	/**@}*/

	/**
	 * \defgroup util Dataflow utilities
	 * @{
	 */

	/*************************************************************************/
	/*                        Dataflow Utils                                 */
	/*************************************************************************/

	unsigned int* getECR(int rolId); //!< Return event counter reset.

	int restartRol(unsigned int rolId); //!< Reset a readout link.

	int requestTempVal(); //!< Return current FPGA Core temperature (degrees celcius).

	RejectedEntry *requestKeptPage(unsigned int rolId, int pageIndex); //!< Request a page from the 'jail' for a given readout link.

	int requestClearKeptPages(unsigned int rolId); //!< Empty the 'jail' for a given readout link.

	void requestEnterDiscardMode(unsigned int rolId); //!< Place a given readout link into discard mode.

	void requestLeaveDiscardMode(unsigned int rolId); //!< Take a given readout link out of discard mode.

	bool checkCRC(unsigned int *fragmentLocation,std::vector<unsigned int>* robHeader, unsigned int rolId); //!< Calculate CRC for a given fragment and check against version in data.
	bool checkCRC(unsigned int *fragmentLocation,RobMsgHdr* robHeader, unsigned int rolId);
	bool checkCRC(unsigned int *fragmentLocation,unsigned int fragmentSize, unsigned int rolId);

	bool compareCRC(unsigned int* ptr,unsigned short crc1, unsigned short crc2,unsigned long asize, unsigned int l1Id,unsigned int rolId);

	bool* getROLEnableStatus(unsigned int rolId); //!< Get pointer to ROL discard mode status

	void setRolEnabled(bool status, unsigned int rolId);

	void toggleBufferManagerXOFF(unsigned int rolId, bool xoff);

	/**@}*/

	/**
	 * \defgroup stats Statistics control methods
	 * @{
	 */

	/*************************************************************************/
	/*                        Statistics                                     */
	/*************************************************************************/

	RobinNPStats* getGeneralStatistics();

	RobinNPROLStats* getROLStatistics(unsigned int rolId); //!< Return statistics block for a given readout link.

	void initStatistics(int rolId);
	/**@}*/

	/**
	 * \defgroup config Configuration control methods
	 * @{
	 */

	/*************************************************************************/
	/*                        Configuration                                  */
	/*************************************************************************/

	void configure();
	void configure(RobinNPConfig& config);

	void initialiseLegacyControl(unsigned int numTickets); //!< Set RobinNP to legacy mode (old DMA interface).
	void closeLegacyControl(); //!< Delete legacy mode structures.

	/**@}*/


	/**
	 * \defgroup threading Thread control and status methods
	 * @{
	 */

	/*************************************************************************/
	/*                        Threads                                        */
	/*************************************************************************/

	void *freePageManagerMain(unsigned int subRob); //!< Top level method for Free Page Manager Thread.

	void *indexerMain(unsigned int subRob); //!< Top level method for Indexer Thread.

	void *requestManagerMain(unsigned int subRob); //!< Top level method for Request Manager Thread.

	void *dmaMonitorMain(unsigned int subRob); //!< Top level method for DMA Monitor Thread.

	void *directDMAManagerMain(unsigned int subRob); //!< Top level method for direct DMA Thread.

	void startThreads(); //!< Start all RobinNP threads.

	bool stopThreads(); //!< Stop all RobinNP threads.

	bool getThreadState(); //!< Return current state (running or not) of RobinNP threads.

	unsigned int getFirmwareVersion(); //!< Return RobinNP firmware version number.

	void resetSubRobChannels();
	unsigned int getAtlasID(unsigned int rolId);

	/**@}*/

protected:

	virtual RolNP* newRol(u_int number);

private:

	/****************/
	/* Data Members */
	/****************/

	//Main Indexer Thread
	pthread_t *m_indexer_thread;

	//Main Request Manager Thread
	pthread_t *m_requestManager_thread;

	//Main DMA Monitor Thread
	pthread_t *m_dmaMonitor_thread;

	//Main Free Page Manager Thread
	pthread_t *m_freePageManager_thread;

	//Main Free Page Manager Thread
	pthread_t *m_directDMAManager_thread;


	//Flag indicating run state of indexer thread, one one can run per Robin object
	bool m_threadsRunning;

	//Internal Indexer control flag
	volatile bool m_indexerRunState[s_maxSubRobs];

	//Internal DMA Monitor control flag
	volatile bool m_dmaMonitorRunState[s_maxSubRobs];

	//Internal Request Manager Control flag
	volatile bool m_requestManagerRunState[s_maxSubRobs];

	//Internal Free Page Manager control flag
	volatile bool m_freePageManagerRunState[s_maxSubRobs];

	//Internal Direct DMA Manager control flag
	volatile bool m_directDMAManagerRunState[s_maxSubRobs];

	// The ROLs
	RolNP **m_rolVector;

	// Management Variables
	unsigned int m_slotNumber;
	int m_devHandle;
	unsigned int m_numRols;

	std::string m_sharedMemoryName;

	//Data Channel Access Members
	BAR0CommonStruct *m_Bar0Common;
	BAR0ChannelStruct **m_Bar0Channel;
	BAR0SubRobStruct **m_Bar0SubRob;

	//Functional Module (SubRob) components
	unsigned int m_numChannelsPerSubRob;
	unsigned int m_numSubRobs;
	unsigned int m_numPagesPerRol;


	//Memory Pool Page Pointers
	unsigned char **m_virtualArrayPointer;
	unsigned long *m_physicalArrayPointer;

	/*
	 * Struct describing elements for use with the main DMA request queue
	 */
	struct RequestQueueElement{
		unsigned int rolId;
		unsigned long virtualAddress;
		unsigned long physicalAddress;
		unsigned long l1Id;
		NPRequestDescriptor *descriptor;
		std::vector<unsigned int> channels;
		unsigned int subRob;
		unsigned int ticket;
		DataPage* page;
		unsigned int robinnppagenum;
		bool terminate;
	};

	/*
	 * Struct describing elements for use with the active DMA queue
	 */
	struct DMATrackingQueueElement{

		DMATrackingQueueElement(){
			descriptor = 0;
			numDMAs = -1;
			rolID  = -1;
			ticket = -1;
			memAddress = 0;
			virt = 0;
			eventID = -1;
			transferSize = -1;
			phys = 0;
			index = -1;
			page = 0;
			robinnppagenum = -1;
			fragStatus = -1;
			fragIndexPtr = 0;
			text = "";
		}

		NPRequestDescriptor *descriptor;
		unsigned int numDMAs;
		unsigned int rolID;
		unsigned int ticket;
		unsigned long memAddress;
		unsigned long virt;
		unsigned int eventID;
		unsigned int transferSize;
		unsigned int phys;
		unsigned int index;
		DataPage* page;
		unsigned int robinnppagenum;
		unsigned int fragStatus;
		MgmtEntry* fragIndexPtr;
		std::string text;
	};

	/*
	 * Struct containing FIFO duplicator read and write indices
	 */
	struct Duplicator{
		volatile unsigned int *buffer;
		unsigned int readIndexCopy;
		unsigned int *readIndex;
		volatile unsigned int *writeIndexCopy;
	};

	Duplicator *m_duplicateDMADone;
	Duplicator *m_duplicateUPF;

	unsigned int m_upfMaskVal;


	tbb::concurrent_bounded_queue<RequestQueueElement>** m_concurrentRequestQueue;

	volatile unsigned int *m_DMADoneActiveQueueReadIndex;
	volatile unsigned int *m_DMADoneActiveQueueWriteIndex;
	DMATrackingQueueElement **m_DMADoneActiveQueue;

	volatile unsigned int *m_DMADoneActiveCaptureQueueReadIndex;
	volatile unsigned int *m_DMADoneActiveCaptureQueueWriteIndex;
	DMATrackingQueueElement **m_DMADoneActiveCaptureQueue;

	std::vector<DataPage*>** m_captureQueue;

	/*
	 * Struct grouping together all elements associated with individual readout links
	 */
	struct rolMembers{

		MgmtPtr *m_hashArray;			   // primary hash array
		MgmtPtr *m_hashArrayPtr;		   // pointer to primary hash array
		MgmtEntry *m_itemList;			   // item list
		MgmtEntry *m_itemListPtr;
		MgmtPtr *m_freeList;			   // free entry stack
		RejectedEntry *m_rejectedStore;

		unsigned int m_freeTos;

		int m_usedCounter;
		bool m_fragmentComplete;		   // current fragment status
		int m_ecrJump;				       // ECR jump

		MgmtPtr m_fragLastPage;
		MgmtPtr m_fragStartPage;
		unsigned int m_runCtlState;

		unsigned int subRobMapping;

		unsigned int m_fragPages;    // store actual number of pages per fragment
		bool m_inhibitFragment;	// prevent storing of erroneous fragments

		unsigned int m_pciCurrentAddr;

		pthread_spinlock_t m_pageStackSpinlock;
		pthread_spinlock_t *m_hashSpinlock;

		pthread_mutex_t m_pageStackMutex;
		pthread_mutex_t *m_hashMutex;
		pthread_mutex_t m_ecrMutex;

		MgmtEntry **m_fragmentToProcess;
		unsigned long *m_fragmentToProcessAddress;

		unsigned int atlasID;
		unsigned int channelIndex;

		std::atomic<unsigned int> pagesInIndex;

		pthread_cond_t *m_ticketCond;
		pthread_mutex_t* m_ticketCondMutex;
		unsigned int* m_ticketIndex;
		bool linkEnabled;

		std::atomic<unsigned int> dataVolumeTransferred;
		std::chrono::time_point<std::chrono::system_clock> m_startOutputTime, m_endOutputTime;

	};

	unsigned char* m_sharedMemory;
	RobinNPMonitoring* m_sharedRobinNPData;

	rolMembers *m_rolArray;
	//subRobMembers *m_subRobArray;

	std::vector<std::pair<unsigned int, unsigned int>> m_fragStatusMap;
	std::vector<std::pair<unsigned int, std::string>> m_fragStatusMapDebug;
	std::vector<std::pair<unsigned int, unsigned int>> m_fragErrorCombinations;

	//Direct Board Access Pointers
	volatile unsigned int *m_bufPtr;
	unsigned long m_bar0Addr,m_bar1Addr;

	//Hardware parameters container
	card_params_t m_cardData;

	//Shared Memory
	unsigned int m_memSize;
	unsigned char *m_sharedMemFreePtr;

	pthread_mutex_t m_screenPrintMutex;

	unsigned int m_runMode;

	tbb::concurrent_bounded_queue<NPRequestDescriptor*>* m_outputQueue;

	//RobinNP device lock file
	std::string m_lockfilename;
	int m_lockfd;

	volatile unsigned int m_numDMAsPending[s_maxSubRobs];
	volatile unsigned int m_numCaptureDMAsPending[s_maxSubRobs];

	bool m_emptyStack[s_maxSubRobs];
	bool m_fpfsFull[s_maxSubRobs];

	sem_t m_dmaCompletionSemaphore[s_maxSubRobs]; //legacy support

	pthread_mutex_t m_fileDumpMutex;

	pthread_mutex_t m_stackCondMutex[s_maxSubRobs];
	pthread_cond_t m_stackCond[s_maxSubRobs];
	timespec m_stackWait[s_maxSubRobs];

	pthread_mutex_t m_fpfCondMutex[s_maxSubRobs];
	pthread_cond_t m_fpfCond[s_maxSubRobs];
	timespec m_fpfWait[s_maxSubRobs];

	pthread_mutex_t m_dmaSlotCondMutex[s_maxSubRobs];
	bool m_dmaFull[s_maxSubRobs];
	pthread_cond_t m_dmaSlotCond[s_maxSubRobs];

	pthread_mutex_t m_captureDMASlotCondMutex[s_maxSubRobs];
	bool m_captureDMAFull[s_maxSubRobs];
	pthread_cond_t m_captureDMASlotCond[s_maxSubRobs];

	unsigned int m_subRobsAvailable;

	bool m_subRobsActive[s_maxSubRobs];

	volatile unsigned int* m_writeIndexBase;

	std::vector<std::pair<unsigned long,unsigned char*>> m_pageVector;

	unsigned int *m_hdrContainer[s_maxSubRobs];

	unsigned int m_dmaQueueFIFOLimit;

	unsigned int m_pageSizeOffset;

	unsigned int m_numTicketsPerRol;

	bool m_legacyMode;

	std::vector<DataPage *> m_dataPageVector;
	std::vector<tbb::concurrent_bounded_queue<DataPage*>*> *m_pageQueues;
	std::vector<tbb::concurrent_bounded_queue<ROIBOutputElement>*> *m_directOutputQueue;

	bool m_queuesReady;

	unsigned int m_subRobIndex[s_maxSubRobs];

	unsigned int m_designVersion;

	//CRC stuff
	unsigned int m_crc_interval;
	unsigned long m_crcinit_direct;
	unsigned long m_crctab16[65536];
	unsigned long m_crcmask;

	//Configuration and Statistics Containers
	RobinNPConfig* m_configuration;
	RobinNPStats* m_statistics;

	DMAInfo* m_DMAProcessContainer[s_maxSubRobs];
	DMAInfo* m_captureDMAProcessContainer[s_maxSubRobs];

	int m_filesDumped;
	int m_errorsDumped;

	std::string m_fileDumpPath;
	int m_fileDumpLimit;
	int m_errorDumpLimit;

	bool m_deviceAcquired;

	/*******************/
	/* Private Methods */
	/*******************/

	/*************************************************************************/
	/*                       Startup and Shutdown                            */
	/*************************************************************************/

	//Shared Memory Control
	void allocateSharedMem();
	void deallocateSharedMem();

	//Initialisation Methods
	void initSequence();
	int initRol(int rolId);
	int initPageManagement(int rolId);
	int initFpgaBufferManager(int rolId);
	void initDataGenerator(int rolId);
	void initConfigurationParameters();
	void initFragStatusMap();

	void acquireDevice();
	void initDevice();

	void reset();

	//FIFO Duplicator Startup/Shutdown Methods
	void initFifoDuplicators();
	void unmapFifoDuplicator();

	//ROL Startup/Shutdown Methods
	void enableRol(int rolId);
	void disableRol(int rolId);


	/*************************************************************************/
	/*                        Indexer                                        */
	/*************************************************************************/

	//In-line FIFO Duplicator Access Methods
	/*inline int checkCombinedUPFWriteIndex(unsigned int subRob){
		if(*(m_duplicateUPF[subRob].writeIndexCopy) == m_duplicateUPF[subRob].readIndexCopy) {
			return -99;
		}
		else {
			return m_duplicateUPF[subRob].readIndexCopy;
		}
	}*/

	inline bool checkCombinedUPFWriteIndex(unsigned int subRob, unsigned int& readIndex){
		if(*(m_duplicateUPF[subRob].writeIndexCopy) == m_duplicateUPF[subRob].readIndexCopy) {
			return false;
		}
		else {
			readIndex = m_duplicateUPF[subRob].readIndexCopy;
			return true;
		}
	}

	inline unsigned int readCombinedUPFDuplicator(int index, int element, unsigned int subRob){

		return *(m_duplicateUPF[subRob].buffer+4*index+element);

	}

	inline void updateCombinedUPFReadIndex(unsigned int subRob){
		m_duplicateUPF[subRob].readIndexCopy = (m_duplicateUPF[subRob].readIndexCopy + 0x1) & m_upfMaskVal; //mask off upper bits so at 255 index will loop back to 0
		*(m_duplicateUPF[subRob].readIndex) = m_duplicateUPF[subRob].readIndexCopy;
	}

	inline int checkDMADoneWriteIndex(unsigned int subRob){
		if(*(m_duplicateDMADone[subRob].writeIndexCopy) == m_duplicateDMADone[subRob].readIndexCopy) {
			return -99;
		}
		else {
			return m_duplicateDMADone[subRob].readIndexCopy;
		}
	}

	inline unsigned int readDMADoneDuplicator(int index, int element, unsigned int subRob){

		return *(m_duplicateDMADone[subRob].buffer+4*index+element);

	}

	inline void updateDMADoneReadIndex(unsigned int subRob){
		m_duplicateDMADone[subRob].readIndexCopy = (m_duplicateDMADone[subRob].readIndexCopy + 0x1) & 0xff; //mask off upper bits so at 255 index will loop back to 0
		*(m_duplicateDMADone[subRob].readIndex) = m_duplicateDMADone[subRob].readIndexCopy;
	}

	//Inline FIFO stats methods
	inline unsigned int getFPFItems(unsigned int rolId){
		return ((m_Bar0Channel[rolId]->BufferManagerStatusRegister.Register[0] >> s_fpfFillShift) & s_fpfFillMask);
	}


	//Inline stack control methods
	inline void addFreePageToStack(unsigned int rolId, MgmtPtr pageNum){
		pthread_mutex_lock(&m_rolArray[rolId].m_pageStackMutex);
		/*m_rolArray[rolId].m_itemListPtr[pageNum].info.upfInfo.eventId = check;
		pageStore1[rolId]->at(pageNum)--;
		m_rolArray[rolId].m_itemListPtr[pageNum].next = -1;
		m_rolArray[rolId].m_itemListPtr[pageNum].prev = -1;*/
		m_rolArray[rolId].m_freeTos++;
		m_rolArray[rolId].m_freeList[m_rolArray[rolId].m_freeTos] = pageNum;
		//std::cout << "recycle rol " << rolId << " page " << pageNum << " TOS " << m_rolArray[rolId].m_freeTos << std::endl;

		pthread_mutex_unlock(&m_rolArray[rolId].m_pageStackMutex);

	}

	inline MgmtPtr takeFreePageFromStack(unsigned int rolId){

		pthread_mutex_lock(&m_rolArray[rolId].m_pageStackMutex);

		MgmtPtr nextFpfEntry = m_rolArray[rolId].m_freeList[m_rolArray[rolId].m_freeTos];		// this should be written to FPGA
		m_rolArray[rolId].m_freeTos--;

		pthread_mutex_unlock(&m_rolArray[rolId].m_pageStackMutex);

		return nextFpfEntry;
	}

	inline void signalPagesOnStack(unsigned int subRob){

		if(m_emptyStack[subRob]){
			pthread_cond_signal(&m_stackCond[subRob]);
		}
	}

	inline void awaitPagesOnStack(unsigned int subRob){

		if(m_emptyStack[subRob]){
			pthread_mutex_lock(&m_stackCondMutex[subRob]);
			clock_gettime(CLOCK_REALTIME, &m_stackWait[subRob]);
			m_stackWait[subRob].tv_nsec = m_stackWait[subRob].tv_nsec + 1000000;
			if (m_stackWait[subRob].tv_nsec >= 1000000000){
				m_stackWait[subRob].tv_sec += 1;
				m_stackWait[subRob].tv_nsec -= 1000000000;
			}
			int ret = pthread_cond_timedwait(&m_stackCond[subRob],&m_stackCondMutex[subRob],&m_stackWait[subRob]);
			pthread_mutex_unlock(&m_stackCondMutex[subRob]);

			if(ret != 0 && ret != ETIMEDOUT){
				CREATE_ROS_EXCEPTION(ex_robinnp_awaitstackpage, ROSRobinNPExceptions, CONDWAITERR, "Stack page pthread condition wait returns: " << strerror(ret) << " for RobinNP " << m_slotNumber);
				throw(ex_robinnp_awaitstackpage);

			}

		}

	}

	inline void awaitFreeFPFSlots(unsigned int subRob){

		if(m_fpfsFull[subRob]){
			pthread_mutex_lock(&m_fpfCondMutex[subRob]);
			clock_gettime(CLOCK_REALTIME, &m_fpfWait[subRob]);
			m_fpfWait[subRob].tv_nsec = m_fpfWait[subRob].tv_nsec + 1000000;
			if (m_fpfWait[subRob].tv_nsec >= 1000000000){
				m_fpfWait[subRob].tv_sec += 1;
				m_fpfWait[subRob].tv_nsec -= 1000000000;
			}
			int ret = pthread_cond_timedwait(&m_fpfCond[subRob],&m_fpfCondMutex[subRob],&m_fpfWait[subRob]);
			pthread_mutex_unlock(&m_fpfCondMutex[subRob]);
			if(ret != 0 && ret != ETIMEDOUT){
				CREATE_ROS_EXCEPTION(ex_robinnp_awaitfpfslot, ROSRobinNPExceptions, CONDWAITERR, "FPF slot pthread condition wait returns: " << strerror(ret) << " for RobinNP " << m_slotNumber);
				throw(ex_robinnp_awaitfpfslot);
			}
		}

	}


	inline void signalFreeFPFSlots(unsigned int subRob){

		if(m_fpfsFull[subRob]){
			pthread_cond_signal(&m_fpfCond[subRob]);
		}
	}


	inline void signalFreeDMASlots(unsigned int subRob){

		pthread_mutex_lock(&m_dmaSlotCondMutex[subRob]);
		if(m_numDMAsPending[subRob] < m_dmaQueueFIFOLimit){
			if(m_dmaFull[subRob]){
				m_dmaFull[subRob] = false;
				pthread_mutex_unlock(&m_dmaSlotCondMutex[subRob]);
				pthread_cond_signal(&m_dmaSlotCond[subRob]);
			}
			else{
				pthread_mutex_unlock(&m_dmaSlotCondMutex[subRob]);
			}
		}
		else{
			pthread_mutex_unlock(&m_dmaSlotCondMutex[subRob]);
		}

	}

	inline void awaitFreeDMASlots(unsigned int subRob){

		pthread_mutex_lock(&m_dmaSlotCondMutex[subRob]);
		if(m_numDMAsPending[subRob] >= m_dmaQueueFIFOLimit){
			m_dmaFull[subRob] = true;
			pthread_cond_wait(&m_dmaSlotCond[subRob],&m_dmaSlotCondMutex[subRob]);
		}
		pthread_mutex_unlock(&m_dmaSlotCondMutex[subRob]);

	}

	inline void signalFreeCaptureDMASlots(unsigned int subRob){

		pthread_mutex_lock(&m_captureDMASlotCondMutex[subRob]);
		if(m_numCaptureDMAsPending[subRob] < s_captureDMAQueueFIFOSize){
			if(m_captureDMAFull[subRob]){
				m_captureDMAFull[subRob] = false;
				pthread_mutex_unlock(&m_captureDMASlotCondMutex[subRob]);
				pthread_cond_signal(&m_captureDMASlotCond[subRob]);
			}
			else{
				pthread_mutex_unlock(&m_captureDMASlotCondMutex[subRob]);
			}
		}
		else{
			pthread_mutex_unlock(&m_captureDMASlotCondMutex[subRob]);
		}

	}

	inline void awaitFreeCaptureDMASlots(unsigned int subRob){

		pthread_mutex_lock(&m_captureDMASlotCondMutex[subRob]);
		if(m_numCaptureDMAsPending[subRob] >= s_captureDMAQueueFIFOSize){
			m_captureDMAFull[subRob] = true;
			pthread_cond_wait(&m_captureDMASlotCond[subRob],&m_captureDMASlotCondMutex[subRob]);
		}
		pthread_mutex_unlock(&m_captureDMASlotCondMutex[subRob]);

	}

	//Fragment Receipt and Indexing Methods
	bool processUPFDuplicatorPage(int readIndex, unsigned int subRob, FragInfo &fragment);
	void processIncomingFragment(FragInfo *theFragment, int rolId);
	void processIncomingFragmentDirectDMA(FragInfo *theFragment, int rolId);

	MgmtPtr findIndexedFragment(EventTag *eventTag, int rolId, unsigned int *lastID, bool doLocking = true);
	void indexFragmentPage(FragInfo *fragment, int rolId);
	bool deleteIndexedFragment(EventTag *eventTag, int rolId, bool isIndexer = false, bool doLocking = true);

	//Misc Indexing Methods
	bool addRejectedPage(FragInfo *theFragment, unsigned int mostRecentId, int rolId);
	bool checkSeqError(unsigned int oldId, unsigned int newId);
	bool checkID(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId);

	/*************************************************************************/
	/*                        Misc.                                          */
	/*************************************************************************/

	unsigned int getFpfFillStatus(unsigned int rolId);
	unsigned int getUpfFillStatus(unsigned int rolId);

	void calcPageCount(int rolId);

	std::string intToString(unsigned int num);

	inline unsigned int calcXmin(unsigned int x, unsigned int y){
		return (x) < (y) ? (x):(y);
	} //!< Return smallest of two numbers.

	void screenPrint(std::string toPrint); //!< Print to screen via mutex arbitrated mechanism, use to avoid thread printout collisions.
	DataPage* getDMAPage(unsigned int subRob);
	unsigned short crctablefastwithskip16(unsigned short* p, unsigned long len);
	void initCRCTable();

	void initDriverDataContainer();
	void dumpCRCErrorFragment(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId, unsigned int totalSize, unsigned int memAddress = 0);
	void dumpInputErrorFragment(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId, unsigned int totalSize,std::string& errorDescription);
	unsigned int getDebugMemPage(unsigned int eventId, unsigned int rolId);
	int checkROIBFragment(FragInfo *theFragment, int rolId);
	/*************************************************************************/
	/*                        Requests                                       */
	/*************************************************************************/

	void queueRequest(const RequestQueueElement &element);
	void processFragmentRequest(RequestQueueElement &element);

	void prepareData(MgmtEntry *item, bool useHeader, RequestQueueElement *responseAddress, int rolId, bool dmaDone = true, unsigned int numDMAs = 0);
	void sendData(DmaDesc *dmaDescriptor, unsigned int* dataHeader, RequestQueueElement *responseAddress, unsigned int eventID, unsigned int rolId, bool dmaDone = true, unsigned int numDMAs = 0, bool lostOrPend = false);
	void queueActiveDMA(const DMATrackingQueueElement &element, unsigned int subRob);
	void queueActiveCaptureDMA(const DMATrackingQueueElement &element, unsigned int subRob);
	bool isLostFragment(unsigned int lastID, unsigned int l1Id);
};

/*! \class RobinNP
 *  \brief Core RobinNP class.
 *
 *  This class provides the primary interface to all RobinNP functionality.
 *
 */

struct ThreadParams{
	RobinNP *robin;
	unsigned int subRob;
};
/*! \struct ThreadParams
 *  \brief Thread Parameters Structure.
 *
 *  This structure contains all the parameters to be passed to a RobinNP indexer/requester thread on startup.
 *
 */
} 
#endif //ROSROBINNP_ROBINNP_H


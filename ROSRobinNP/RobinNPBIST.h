#ifndef ROBINNPBIST_H
#define ROBINNPHIST_H

//C++ headers
#include <string>

//RobinNP headers
#include "RobinNP.h"

typedef struct {
	/** Register 135 Pin Mapping */
	u_int n1;
	u_int hs_div;
	uint64_t rfreq_int;
	double rfreq_float;
	double fdco;
	double fxtal;
} refclk_cfg_t;

refclk_cfg_t cfg;


namespace ROS{

class RobinNPBIST;

struct InterrupterThreadParams{
	RobinNPBIST *bist;
	unsigned int index;
	unsigned int numInterrupts;
	float timeout;
};

class RobinNPBIST{

public:

	//Data Members
	RobinNP *m_testCandidate; //! RobinNP object under testing.

	//Constructors and Destructor
	RobinNPBIST(unsigned int slotNumber, bool isVerbose = false); //!< Standalone constructor (new RobinNP object created).
	RobinNPBIST(RobinNP* testCandidate, bool isVerbose = false); //!< Internal constructor (passed existing RobinNP object).
	~RobinNPBIST(); //!< Destructor.
	RobinNPBIST(const RobinNPBIST&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNPBIST& operator=(const RobinNPBIST&) = delete; //!< Assignment Operator - NON-COPYABLE

	//BIST config
	void setVerboseMode(bool isVerbose){
		m_verboseMode = isVerbose;
	}

	//All available tests
	bool pageflowTest(unsigned int rolId); //!< Monitor and verify dataflow through hardware control FIFOs.
	bool pageflowMemTest(unsigned int rolId); //!< Verify full data flow chain from source through buffer memory and out to host.
	bool pageflowSpeedTest(unsigned int rolId,unsigned int dataGenSize,bool interruptTest=false); //!< Benchmark dataflow mechanism.
	bool registerTest(); //!< Verify RobinNP control and status register values.
	bool memoryExpertTest(unsigned int subRob,unsigned int numReads = 1, unsigned int readLength = 10, unsigned int numWrites = 4, unsigned int holdWrites = 0, /*bool burstMode,*/ bool doReset = true, unsigned int numWordsPerWrite = 6, unsigned int addrIncrement = 0, bool doMemoryInit = false, unsigned int writeStartAddress = 0); //!< Expert memory test tools (experts only).
	bool memoryReadWriteTest(unsigned int subRob, double memSize = 1.0); //!< Write and verify a user defined volume of data to the memory (walking 1's test).
	bool primaryDMATest(unsigned int subRob, bool captureTest = false); //!< Verify functionality of DMA engine in multiple configurations.
	bool primaryDMASpeedTest(unsigned int subRob, unsigned int numWords = 25); //!< Benchmark DMA engine.
	bool dualDMASpeedTest(unsigned int numWords = 25); //!< Benchmark Dual DMA engine configuration (2 subRobs (ROBgroups) only).
	unsigned int designTest(); //!< Verify firmware version.
	bool interrupterTest(unsigned int interrupt); //!< Benchmark interrupter functionality (single thread).
	void resetFifoDuplicators(); //!< Reset FIFO Duplicator mechanisms.
	unsigned int getNumChannelsPerSubRob(); //!< Return number of channels associated with a subRob (ROBgroup).
	unsigned int getNumSubRobs(); //!< Return number of subRobs (ROBgroups) associated with RobinNP.
	unsigned int getNumRols(); //!< Return number of readout links associated with RobinNP.
	bool interrupterMultiTest(unsigned int interruptCount); //!< Benchmark interrupter functionality (multithreaded).
	void sLinkTest(unsigned int rolId, bool resetGTX, bool resetHOLA, bool uReset); //!< S-link status and configuration tests (with reset).
	void configureI2CClock(float newFreq, float defaultFreq = REFCLK_DEFAULT_FOUT); //!< Modify s-link clock frequency.
	int getHardwareVersion(); //!< Return hardware version for which firmware built.
	bool printSystemMonitorInfo(); //!< Print FPGA Core temperature and DNA code (more information pending).
	void *interrupterThreadProcess(InterrupterThreadParams *params); //!< Interrupter thread control process.
	void configureMemoryParams(unsigned int writeTimeout, unsigned int readTimeout, unsigned int fifoFill); //!< Modify memory firmware configuration.
	void reset(unsigned int memWriteTimeout, unsigned int memReadTimeout, unsigned int memFifoMax);

private:

	//Data Members
	bool m_remoteCall;
	bool m_verboseMode;
	pthread_spinlock_t m_interruptAcknowledgeLock;

	//Internal Methods
	int* processUPFDuplicatorPageSingleChannel(int readIndex,unsigned int rolId,unsigned int pageSize);
	bool initPageFlow(unsigned int pageSize, unsigned int rolId);
	void startDataGenerator(unsigned int fragmentSize,unsigned int rolId);
	void cleanupRol(unsigned int rolId);
	int* loadTestPageToMemory(unsigned int rolId, unsigned int fragmentSize);
	bool enableCommonPageFlow(unsigned int subRob);
	bool validateIdlePageFlow(unsigned int rolId, unsigned int pageTestAmount);
	bool validateActivePageFlow(unsigned int rolId, unsigned int pageTestAmount);
	void configureInterrupter(unsigned int interrupt);
	void readPageFromMemory(unsigned int subRob, int* results);
	bool initialiseMemory(unsigned int subRob, double size);
	bool receiveAndValidatePages(unsigned int rolId, unsigned int pageTestAmount, unsigned int pageSize, bool readMemory);
	void populateFreePageFifo(unsigned int rolId, unsigned int pageTestAmount, unsigned int pageSize, bool verbose = false);
	bool getDMAResult(unsigned int subRob,int& readIndex, bool verbose = false);
	void readQSFPdata();
	void initialiseOscillator();
	void computeClockConfig(float newFreq);
	void applyClockConfig();
	int i2cReadWrite(u_int chain, u_int slaveAddress, u_int memAddress, unsigned int readwriteSelect, u_int data = 0);
	int waitI2CCompletion();
	int writeI2CConfig(u_int chainSelect, u_int slaveAddress, u_int command, u_int mode, u_int byteEnable);
	void getClockConfig(float fout = REFCLK_DEFAULT_FOUT);
	void waitForClockClearance(uint8_t flag);
	int i2cDualWrite(u_int chain, u_int slaveAddress, u_int memAddress0, u_int memAddress1, u_int data0, u_int data1);
	void printI2CConfig();
	void readMemorySPD();
	std::string readMemoryPartNumber(unsigned int slaveAddr);
	unsigned int readMemoryCapacity(unsigned int slaveAddr);
	unsigned int readMemoryVendorID(unsigned int slaveAddr);
	std::string readMemoryManufactureDate(unsigned int slaveAddr);
	unsigned int readMemorySerialNumber(unsigned int slaveAddr);
	void readMemorySpec(unsigned int slaveAddr);
	int readQSFPTemp(unsigned int QSFP);
	double readQSFPVoltage(unsigned int QSFP);
	std::string readQSFPVendor(unsigned int QSFP);
	std::string readQSFPPartNumber(unsigned int QSFP);
	std::string readQSFPSerialNumber(unsigned int QSFP);
	std::string readQSFPRevisionNumber(unsigned int QSFP);
	double readQSFPTxBias(unsigned int QSFP, unsigned int channel);
	double readQSFPRxPower(unsigned int QSFP, unsigned int channel);
	double readQSFPWavelength(unsigned int QSFP);
	double readQSFPWavelengthTolerance(unsigned int QSFP);
	int readQSFPFaultMap(unsigned int QSFP);
	std::string readQSFPPowerClass(unsigned int QSFP);
	std::string readQSFPCDR(unsigned int QSFP);
	std::string readQSFPTypeIdentifier(unsigned int QSFP);
	std::string readQSFPConnectorType(unsigned int QSFP);
	std::string readQSFPModuleEncoding(unsigned int QSFP);
	void readQSFPSpecCompliance(unsigned int QSFP);
	std::string readQSFPRateSelectSupport(unsigned int QSFP);
	void readQSFPDeviceTechnologyDescription(unsigned int QSFP);
	void readQSFPInfinibandCompliance(unsigned int QSFP);
	std::string readQSFPMaxCaseTemp(unsigned int QSFP);
	std::string readQSFPVendorDateCode(unsigned int QSFP);
};

/*! \class RobinNPBIST
 *  \brief A class for implementing all RobinNP BIST functionality.
 *
 *  This class, which will be accessible both via a standalone application and via the RobinNP object within other applications\n
 *	provides a location for all future RobinNP BIST tests as they are developed.\n
 *	\n
 *	Current tests:\n
 *	\n
 *	1) Design ID Tests: Check RobinNP design information\n
 *	2) RegisterTest: Check the values of all RobinNP registers are as expected and perform write tests to those which can be written to\n
 *	3) PageFlowTest: Test Fill of the RobinNP Free Page FIFO follow by activation of data generator and monitoring of dataflow through the ROL Used Page FIFO, Combined Used Page FIFO and FIFO Duplicator verifying pages as they arrive in back in host memory\n
 *	4) DmaTest: Test RobinNP DMA engine in various configurations\n
 *	5) DmaSpeedTest: Benchmark RobinNP DMA engine\n
 *	6) PageFlowSpeedTest: Benchmark RobinNP page flow mechanism\n
 *	7) PageFlowMemTest: Verify page flow from source, through buffer memory and out via direct fifo readout and DMA\n
 *	8) MemoryTest: Read/Write user defined volume of data to buffer memory and verify\n
 *	9) MemoryExpertTest: Suite of expert buffer memory tests for developers\n
 *	10) S-Link Test: Verify S-link configuration and reset (associated ability to modify clock frequency)\n
 *	11) System Monitor Information: Print FPGA Core Temperature and DNA Code (further tests pending)\n
 *	12) InterrupterTest: Benchmark interrupter in single and multi-threaded mode
 */
}


#endif //ROBINNPHIST_H

#ifndef ROBINNPSTATS_H
#define ROBINNPSTATS_H
#include <stdint.h>
#include "ROSRobinNP/RobinNPHardware.h"
#include <iostream>

namespace ROS{

/// Event logging
struct EventLog{
	int logEnabled;             // if we keep a history
	int logSize;                // number of log entries
	int current;                // points to current index
	int overflow;               // set after first roll-over
	unsigned int eventArray[s_eventLogSize];   // pointer to log entries
};

class RobinNPROLStats{

public:


	RobinNPROLStats();

	void reset();

	void dump();

	unsigned int m_rolId;
	unsigned int m_robId;
	int	         m_pagesFree;                      	// number of free RobinNP memory pages available for data storage (true value can be larger by a few pages)
	int	         m_pagesInUse;	  			     	// number of RobinNP memory pages currently in use for data storage (see comment above)
	int          m_fragStatSize;	                // ROB: number of fragment statistic entries, see #FragStat
	int          m_pageStatSize;	                // ROB: number of page statistic entries, see #PageStat
	unsigned int m_mostRecentId;                   	// most recent L1ID
	int		     m_savedRejectedPages;    	     	// number of saved rejected pages
	EventLog     m_eventLog;                       	// event log
	bool         m_bufferFull;                     	// Current status of buffer
	bool         m_rolXoffStat;                    	// Current status of XOFF signal
	bool         m_rolDownStat;                    	// Current status of LDOWN
	unsigned int m_memreadcorruptions;			 	// Number of fragments corrupted on DMA read (can be re-requested)
	uint64_t     m_fragStat[LAST_FRAGSTAT];        	// fragment statistics, see #FragStat
	uint64_t     m_pageStat[LAST_PAGESTAT];        	// page statistics, see #PageStat
	double       m_xoffpercentage;                  // percentage of time in past 5 seconds that channel dataflow has been in xoff state
	double       m_rolInputBandwidth;               // input bandwidth to readout link in MB/s
	double       m_rolOutputBandwidth;              // output bandwidth to readout link in MB/s (estimated from DMA requests)
	double		 m_readoutFraction;					// instantaneous readout fraction (ratio of output/input bandwidth for same time window)
	unsigned int m_fragsIncomplete;                 // number of multipage fragments missing a page
	unsigned int m_multipageIndexError;             // number of times indexer found unexpected event ID in stored multipage fragment
	unsigned int m_xoffcount;                       // number of XOFFs asserted for ROL (up to make of 64k)
	uint8_t      m_ldowncount;                      // number of link down events experienced by channel
	unsigned int m_statusErrors[s_numStatusWordBits]; // number of times each fragment status error bit has been asserted

	// Fragment statistics
	static const char *m_robinFragStatStrings[LAST_FRAGSTAT];

	// Page statistics
	static const char *m_robinPageStatStrings[LAST_PAGESTAT];

	static const char* m_robinNPErrorStatStrings[s_numStatusWordBits];

};

class RobinNPStats{

public:

	RobinNPStats(unsigned int slotNumber);

	RobinNPROLStats* getROLStats(unsigned int rolId){
		return &m_rolStats[rolId];
	}

	void dump();

	unsigned int m_slotNumber;
	unsigned int m_firmwareVersion;	           	// ROB: FPGA firmware version
	unsigned int m_fragFormat;                 	// ROB: ROD format
	unsigned int m_bufByteSize;                 // ROB: buffer size. Used to set up host kernel memory structures
	unsigned int m_numRols;                    	// ROB: number of channels
	RobinNPROLStats m_rolStats[s_maxRols];
	bool m_running;								// Run Control State (RUNNING = 1, all others = 0)

};
}

#endif

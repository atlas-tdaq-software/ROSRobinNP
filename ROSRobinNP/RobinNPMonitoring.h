#ifndef ROBINNPMONITORING_H
#define ROBINNPMONITORING_H

//RobinNP headers
#include "RobinNPHardware.h"
#include "RobinNPConfig.h"
#include "RobinNPStats.h"

namespace ROS
{
class RobinNPMonitoring{

public:

	unsigned int m_numRols;

	RobinNPMonitoring(unsigned int numRols);
	~RobinNPMonitoring();
	RobinNPMonitoring(const RobinNPMonitoring&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNPMonitoring& operator=(const RobinNPMonitoring&) = delete; //!< Assignment Operator - NON-COPYABLE

	MgmtPtr* getHashArrayPtr(unsigned int Rol);
	MgmtEntry* getItemListPtr(unsigned int Rol);
	RobinNPStats* getStatsPtr();
	RobinNPConfig* getConfigPtr();
	//void initConfig();

	static unsigned int calculateSharedMemSize(unsigned int numRols);

private:

	unsigned char* getRolOffset(unsigned int Rol);

};

/*! \class RobinNPMonitoring
 *  \brief RobinNP shared memory interface class.
 *
 *  This class provides mappings to RobinNP shared memory for expert tools such as robinpscope.\n
 *	\n
 *	Shared Memory Contents (per ROL):\n
 *	\n
 *	1) Index item list\n
 *	2) Index hash array\n
 *	3) Statistics block\n
 *	4) Configuration structure\n
 *	5) Event log array (for statistics block)\n
 *
 */

}

#endif // ROBINNPMONITORING_H


#ifndef ROBINNP_COMMON_H
#define ROBINNP_COMMON_H


/*====== From robin driver =======*/
#ifndef COMPILE_DRIVER
#include <stdint.h>
#include <sys/types.h>
#include <asm/ioctl.h>
#endif

#define MAXCARDS				5   // Max. number of ROBIN cards

//For the proc file interface
#define PROC_MAX_CHARS			0x10000 //The max. length of the output of /proc/robin

//For the IRQ handling
#define NO_EVENT                0
#define GUEST_EVENT             1
#define INMATE_EVENT			2

#ifdef __cplusplus
namespace ROS {
#endif
typedef struct
{
	unsigned int ncards;
	unsigned int ch0_id[MAXCARDS];
	unsigned int ch0_type[MAXCARDS];
	unsigned int ch1_id[MAXCARDS];
	unsigned int ch1_type[MAXCARDS];
	unsigned int ch2_id[MAXCARDS];
	unsigned int ch2_type[MAXCARDS];
} T_irq_info;
#ifdef __cplusplus
}
#endif

/*======== robin driver end ==========*/

#define MAXCHAN 16
#define MAX_SUBROB 2
#define MAXDMABUF 60000

#ifdef __cplusplus
namespace ROS {
#endif
typedef struct
{
	struct pci_dev *pciDevice;
	unsigned int slot;
	unsigned int registerBaseAddress;
	unsigned int registerSize;
	unsigned int bufferBaseAddress;
	unsigned int bufferSize;
	unsigned int nChannels;
	unsigned int nSubRobs;
	void* upfDuplicateBuffer[MAX_SUBROB];
	uint64_t upfDuplicateHandle[MAX_SUBROB];
	void* dmaDoneDuplicateBuffer[MAX_SUBROB];
	uint64_t dmaDoneDuplicateHandle[MAX_SUBROB];
	void* writeIndexBuffer;
	uint64_t writeIndexHandle;
	int nDmaPages; //number of pre-allocated DMA buffers - can be modified when running insmod - defaults to MAXDMABUF
	int dmaPageSize;
	void* primaryDmaBuffer[MAXDMABUF];
	uint64_t primaryDmaHandle[MAXDMABUF];
	int oscfreq;
} card_params_t;

typedef struct {
	int card;
	int channel;
	int size;
	unsigned int buffer[4096];
} dmaDebug_t;

/* ioctl "switch" flags */
#define ROBINNP_MAGIC 'x'

#define GETCARDS    _IOW(ROBINNP_MAGIC, 1, int)
#define SETLINK     _IOR(ROBINNP_MAGIC, 2, card_params_t*)
#define READ_DMABUF _IOR(ROBINNP_MAGIC, 3, int)
#define WAIT_IRQ    _IOW(ROBINNP_MAGIC, 4, int)
#define CSTEST      _IOWR(ROBINNP_MAGIC, 5, int)
#define FAKE_IRQ    _IOWR(ROBINNP_MAGIC, 6, int)
#define ALLOC_DMABUF _IOWR(ROBINNP_MAGIC, 7, int)
#define CANCEL_IRQ_WAIT _IO(ROBINNP_MAGIC, 8)
#define INIT_IRQ _IO(ROBINNP_MAGIC, 9)

typedef struct {
	u_int n1;
	u_int hs_div;
	uint64_t rfreq_int;
	double rfreq_float;
	double fdco;
	double fxtal;
} refclk_cfg_t;


/** Register 135 Pin Mapping */
#define M_RECALL (1<<0)
#define M_NEWFREQ (1<<6)

#define I2C_CHAIN_REFCLK 6
#define I2C_CHAIN_FMC 5
#define I2C_CHAIN_DDR 4
#define I2C_CHAIN_QSFP 0

#define I2C_SLAVE_REFCLK 0x55
#define I2C_SLAVE_FMC 0x4A
#define I2C_SLAVE_QSFP0 0x50

#define I2C_CTRL_EN  (1<<7)
#define I2C_CTRL_IEN (1<<6)

#define I2C_CMD_STA  (1<<7)
#define I2C_CMD_STO  (1<<6)
#define I2C_CMD_RD   (1<<5)
#define I2C_CMD_WR   (1<<4)
#define I2C_CMD_NACK (1<<3)
#define I2C_CMD_IACK (1<<0)

#define I2C_STATUS_ACK  (1<<7)
#define I2C_STATUS_BUSY (1<<6)
#define I2C_STATUS_AL   (1<<5)
#define I2C_STATUS_TIP  (1<<1)
#define I2C_STATUS_IF   (1<<0)

#define I2C_READ                (1<<1)
#define I2C_WRITE               (1<<2)

#define FREEZE_DCO (1<<4)

#define REFCLK_DEFAULT_FOUT  (212.5)

enum
{
	LINK = 1,
	LINK_IRQ = 5,
	GETPROC = 6
};

/********************/
/*driver error codes*/
/********************/
enum
{
	RD_OK = 0,
	RD_KMALLOC,        // 1
	RD_CTU,            // 2
	RD_CFU,            // 3
	RD_ALREADYDONE,    // 4
	RD_FIFOEMPTY,      // 5
	RD_EIO,            // 6
	RD_EIO2,           // 7
	RD_PROC,           // 8
	RD_NOMEMORY,       // 9
	RD_NOSLOT,         // 10
	RD_ILLHANDLE,      // 11
	RD_TOOMANYCARDS,   // 12
	RD_REMAP,          // 13
	RD_BAR,            // 14
	RD_NOCARD,         // 15
	RD_MMAP,           // 16
	RD_NOSHARE,        // 17
	RD_RPR,            // 18
	RD_REQIRQ,         // 19
	RD_INTBYSIGNAL     // 20
};

#ifdef IOCTL_STRINGS
static char ioctl_strings[21][200] = {
		"robin driver(RD_OK): No error",
		"robin driver(RD_KMALLOC): Failed to execute the kmalloc() function",
		"robin driver(RD_CTU): Failed to execute the copy_to_user() function",
		"robin driver(RD_CFU): Failed to execute the copy_from_user() function",
		"robin driver(RD_ALREADYDONE): Robin::reserveMsgResources has already been called one",
		"robin driver(RD_FIFOEMPTY): You have requested more entries from the message passing FIFO of a Robin than are currently available",
		"robin driver(RD_EIO): Failed to execute the pci_register_driver() function",
		"robin driver(RD_EIO2): Failed to execute the register_chrdev() function",
		"robin driver(RD_PROC): Failed to execute the create_proc_entry() function",
		"robin driver(RD_NOMEMORY): You have requested more DPM or DMA memory from a Robin than is currently available (Hint: check if robinconfig was run successfully)",
		"robin driver(RD_NOSLOT): The bookkeeping tables for the DPM or DMA memory are full",
		"robin driver(RD_ILLHANDLE): You are trying to return resources (DPM or DMA memor, FIFO entries) but the handle is invalid",
		"robin driver(RD_TOOMANYCARDS): There are more Robincards installed than the driver can handle",
		"robin driver(RD_REMAP): Failed to execute the ioremap() function",
		"robin driver(RD_BAR): The BAR0 of something that looks like a Robin is not a PCI-MEM resource",
		"robin driver(RD_NOCARD): You are trying to execute an ioctl function on a non-existing Robin. Check your physicalAddress parameter",
		"robin driver(RD_MMAP): It is not possible to mmap an unaligned address",
		"robin driver(RD_NOSHARE): mmap failed because shared flag is missing",
		"robin driver(RD_RPR): The robin driver failed to execute the remap_page_range() function",
		"robin driver(RD_REQIRQ): The robin driver failed to register the interrupt handler",
		"robin driver(RD_INTBYSIGNAL): When waiting for an interrupt the driver got a signal"
};
#endif
#ifdef __cplusplus
}
#endif
#endif //ROBINNP_COMMON_H

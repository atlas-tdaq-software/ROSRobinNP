#ifndef ROBINNPREGISTERS_H
#define ROBINNPREGISTERS_H

/* These structures will be mapped directly on to the hardware registers */
/* Be careful when adding registers as it may move the alignment       */
/* Structs declared as C style for compatibility with driver compilation*/
/*-----------------------------------*/
/* LSB SHOULD BE LISTED AT THE TOP   */
/*-----------------------------------*/
typedef struct{
	volatile uint32_t DesignMinorRevision       : 11;
	volatile uint32_t RobinExpress              :  1;
	volatile uint32_t Author                    :  4;
	volatile uint32_t DesignMajorRevision       :  8;
	volatile uint32_t DesignVersion             :  8;
	volatile uint32_t HardwareVersion           :  7;
	volatile uint32_t OscFreq250		        :  1;
	volatile uint32_t NumberOfChannelsPerSubRob :  8;
	volatile uint32_t NumberOfSubRobs           :  8;
	volatile uint32_t NumberOfChannels          :  8;
} DesignIDStruct;

typedef struct{
	volatile uint32_t MSIAllowed                   :  1;
	volatile uint32_t NumberOfMSIsRequested        :  3; /* need more decoding 2^ */
	volatile uint32_t NumberOfMSIsAllocated        :  3; /* need more decoding 2^ */
	volatile uint32_t SixtyFourBitCapable          :  1;
	volatile uint32_t PerVectorMaskingCapable      :  1;
	volatile uint32_t Reserved                     :  7;
	volatile uint32_t InIdleState                  :  1;
	volatile uint32_t InWaitingForACKState         :  1;
	volatile uint32_t InWaitingForEnableResetState :  1;
	volatile uint32_t filler                       : 13;
 	volatile unsigned int commonInterrupterStatus  :  5;
   	volatile unsigned int filler2                  :  3;
   	volatile unsigned int IndividualInterrupterStatus0 : 2;
   	volatile unsigned int IndividualInterrupterStatus1 : 2;
   	volatile unsigned int IndividualInterrupterStatus2 : 2;
   	volatile unsigned int IndividualInterrupterStatus3 : 2;
   	volatile unsigned int IndividualInterrupterStatus4 : 2;
   	volatile unsigned int IndividualInterrupterStatus5 : 2;
   	volatile unsigned int IndividualInterrupterStatus6 : 2;
   	volatile unsigned int IndividualInterrupterStatus7 : 2;
	volatile uint32_t InterruptCount               :  8;
} InterruptStatusRegisterComponentStruct;

typedef enum {
   CIS_IDLE0,   CIS_IDLE1,   CIS_IDLE2,   CIS_IDLE3,
   CIS_IDLE4,   CIS_IDLE5,   CIS_IDLE6,   CIS_IDLE7,
   CIS_WAIT_ACK0, CIS_FINISH0,
   CIS_WAIT_ACK1, CIS_FINISH1,
   CIS_WAIT_ACK2, CIS_FINISH2,
   CIS_WAIT_ACK3, CIS_FINISH3,
   CIS_WAIT_ACK4, CIS_FINISH4,
   CIS_WAIT_ACK5, CIS_FINISH5,
   CIS_WAIT_ACK6, CIS_FINISH6,
   CIS_WAIT_ACK7, CIS_FINISH7,
} CommonInteruperStatusEnum;

typedef enum {
   IIS_IDLE=1,
   IIS_MAKE_REQUEST,
   IIS_WAIT_REENABLE
} IndividualInterrupterStatusenum;

typedef union{
	InterruptStatusRegisterComponentStruct Component;
	uint32_t Register[2];
	uint64_t reg64;
} InterruptStatusRegisterStruct;

typedef struct{
	volatile uint32_t PrimaryDMADoneNotEmpty          		:  1;
	volatile uint32_t PrimaryDMADoneDuplicateNotEmpty 		:  1;
	volatile uint32_t CommonUsedPageFIFONotEmpty            :  1;
	volatile uint32_t CommonUsedPageFIFODuplicateNotEmpty   :  1;
	volatile uint32_t Dummy                           		: 28;
	volatile uint32_t Top;
} InterruptMaskRegisterComponentStruct;

typedef union{
	InterruptMaskRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} InterruptMaskRegisterStruct;

typedef struct{
	volatile uint32_t acknowledge 	:  8;
	uint32_t Others					: 24;
	uint32_t Top               			;
} InterruptControlRegisterStruct;

typedef struct{
	volatile uint32_t Trigger :  8;
	volatile uint32_t Bottom  : 24;
	volatile uint32_t Top;
} InterruptTestRegisterStruct;


typedef struct{
	volatile uint32_t Bottom;
	volatile uint32_t Top;
} MemoryReadInvalidateControlRegisterStruct;


typedef struct{
	uint32_t dummy[2];
} Dummy64Struct;

typedef struct{
	volatile uint32_t Reset                     :  1;
	volatile uint32_t Enable                    :  1;
	volatile uint32_t TerminationWordDMAEnable  :  1;
	volatile uint32_t PrimaryDMAFIFOCountIgnore :  1;
	volatile uint32_t PrimaryDMARAMModeEnable   :  1;
	volatile uint32_t IgnoreDMADoneFull		    :  1;
	volatile uint32_t AbortCurrentDMAs		    :  1;
	volatile uint32_t DMAQueueIgnore            :  1;
	volatile uint32_t MemoryReadIgnore          :  1;
	volatile uint32_t dummy0                    : 23;
	volatile uint32_t dummy1                    : 16;
	volatile uint32_t DestinationOffset         : 16;
} PrimaryDMAControlRegisterComponentStruct;

typedef union{
	PrimaryDMAControlRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} PrimaryDMAControlRegisterStruct;

typedef struct{
	volatile uint32_t Status                 :  4;
	volatile uint32_t QueueFIFOFillError     :  1;
	volatile uint32_t DoneFIFOFill           :  8;
	volatile uint32_t DoneFIFOEmpty          :  1;
	volatile uint32_t DoneFIFOFull           :  1;
	volatile uint32_t QueueFIFODataAvailable :  1;
	volatile uint32_t QueueFIFOFill          :  8;
	volatile uint32_t Dummy                  :  1;
	volatile uint32_t QueueFIFOFull          :  1;
	volatile uint32_t StateIsIdle            :  1;
	volatile uint32_t StateIsWaitingForStart :  1;
	volatile uint32_t StateIsWaitingForEnd   :  1;
	volatile uint32_t dummy1                 :  3;
	volatile uint32_t dummy2                 : 32;
} PrimaryDMAStatusRegisterComponentStruct;

typedef union{
	PrimaryDMAStatusRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} PrimaryDMAStatusRegisterStruct;

typedef struct{
	volatile uint32_t pciAddressLower;
	volatile uint32_t pciAddressUpper; //bit 30/31 = terminationWordEnable, bit 31/31 = offsetEnable
	volatile uint32_t transferSize;
	volatile uint32_t localAddress;
} PrimaryDMAQueueFIFOComponentStruct;

typedef union{
	PrimaryDMAQueueFIFOComponentStruct Component;
	volatile uint64_t Register[2];
} PrimaryDMAQueueFIFOStruct;

typedef struct{
	volatile uint32_t pciAddressLower;
	volatile uint32_t pciAddressUpper;
	volatile uint32_t transferSize;
	volatile uint32_t localAddress;
	volatile uint32_t transferSizeInvalidate;
	volatile uint32_t localAddressInvalidate;
} PrimaryDMADoneFIFOStruct;

typedef struct{
	volatile uint32_t ReadIndexBottom;
	volatile uint32_t ReadIndexTop;
	volatile uint32_t WriteIndexBottom;
	volatile uint32_t WriteIndexTop;
	volatile uint32_t RingBufferLocationBottom;
	volatile uint32_t RingBufferLocationTop;
	volatile uint32_t TimeBottom;
	volatile uint32_t TimeTop;
	volatile uint32_t WriteIndexCopyLocationBottom;
	volatile uint32_t WriteIndexCopyLocationTop;
} PrimaryDMADoneFIFODuplicatorStruct;

typedef struct{
	volatile uint32_t RingBufferLocationBottom;
	volatile uint32_t RingBufferLocationTop;
	volatile uint32_t WriteIndexCopyLocationBottom;
	volatile uint32_t WriteIndexCopyLocationTop;
	volatile uint32_t ReadIndexBottom;
	volatile uint32_t ReadIndexTop;
	volatile uint32_t WriteIndexBottom;
	volatile uint32_t WriteIndexTop;
} UPFFIFODuplicatorStruct;

typedef struct{
	volatile uint64_t DNA      	   	   	  : 36;
	//volatile uint32_t DNA2      	   	  :  4;
	volatile uint32_t numUPFDelayBlocks   :  2;
	volatile uint32_t dummy		   		  :  2;
	volatile uint32_t FanSpeed		   	  :  8;
	volatile uint32_t Temperature      	  : 10;
	volatile uint32_t QSFPPresent      	  :  3;
	volatile uint32_t QSFPInterrupt    	  :  3;
} GlobalStatusRegisterStruct;

typedef struct{
	volatile uint32_t GlobalReset      					:  1;
	volatile uint32_t InterrupterReset 					:  1;
	volatile uint32_t dummy0           					: 10;
	volatile uint32_t I2CSelect		   					:  4;
	volatile uint32_t dummy1           					: 12;
	volatile uint32_t QSFPModSel       					:  2;
	volatile uint32_t dummy2	       					:  2;
	volatile uint16_t dummy3           					:  8;
	volatile uint16_t FDDestinationLengthElementsLarge  :  8;

	volatile unsigned int FDInternalBusWidthBitBytes 	:  4;
	volatile unsigned int FDDestinationLengthElements 	:  4;
	volatile unsigned int FDSourceLengthElements 		:  4;
	volatile unsigned int FDSourceWidthBits 			:  4;
} GlobalControlRegisterStruct;


typedef struct{
        unsigned int threshold0 : 10;
        unsigned int dummy0     :  6;
        unsigned int threshold1 : 10;
        unsigned int dummy1     :  4;
        unsigned int flag0      :  1;
        unsigned int flag1      :  1;
        unsigned int dummy2     : 32;
} TemperatureComparatorsStruct;

typedef struct{
	volatile uint32_t dummy0;
	volatile uint32_t dummy1;
} HOLABypassStatusStruct;

typedef struct{
	volatile uint32_t dummy0;
	volatile uint32_t dummy1;
} HOLABypassRXStruct;

typedef struct{
	volatile uint32_t dummy0;
	volatile uint32_t dummy1;
} HOLABypassTXWriteStruct;

typedef struct{
	volatile uint32_t dummy0;
	volatile uint32_t dummy1;
} HOLABypassTXControlStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} UPFDuplicateStartLocationRegisterStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} UPFDuplicateWriteIndexRegisterStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} UPFDuplicateWriteIndexLocationRegisterStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} UPFDuplicateReadIndexRegisterStruct;

typedef struct{
	volatile uint32_t upper;
	volatile uint32_t lower;
} UPFStruct;

/*
typedef struct{
	uint32_t  dummy0             :  1;
	uint32_t  FragSizeError      :  1;
	uint32_t  CNTR               :  1;
	uint32_t  EOFBit             :  1;
	uint32_t  BOFBit             :  1;
	uint32_t  DataError          :  1;
	uint32_t  CTLError           :  1;
	uint32_t  NoHeader           :  1;
	uint32_t  IncomplHeader      :  1;
	uint32_t  MissingEOF         :  1;
	uint32_t  MissingBOF         :  1;
	uint32_t  HdrMarkerError     :  1;
	uint32_t  FormatVersionError :  1;
	uint32_t  RunNumber          :  1;
	uint32_t  TriggerTypePresent :  1;
	uint32_t  EventID            :  1;
	uint32_t  DuplicatorChannel  :  4;
	uint32_t  dummy1             :  2;
	uint32_t  LastPageMark       :  1;
	uint32_t  Valid              :  1;
	uint32_t  TriggerType        :  8;
	uint32_t  L1ID               : 24;
	uint32_t  ECR                :  8;
} UPFBottomCopyStruct;
*/
/*
typedef struct{
	uint32_t  LDErrCounter       : 15;
	uint32_t  LDErr		         :  1;
	uint32_t  DuplicatorChannel  :  4;
	uint32_t  GeneralError       :  1;
	uint32_t  FinishedOnEOF      :  1;
	uint32_t  LastPageMark       :  1;
	uint32_t  Valid              :  1;
	uint32_t  TriggerType        :  8;
	uint32_t  L1ID               : 24;
	uint32_t  ECR                :  8;
} UPFBottomCopyStruct;
*/

typedef struct{
	uint32_t  PrevTXBlockErr	  		 :  1;
	uint32_t  CtrlWordTXErr	      		 :  1;
	uint32_t  dummy     	      	     :  2;
	uint32_t  MissingRODHdrMarker 		 :  1;
	uint32_t  WrongRODHdrVersion  		 :  1;
	uint32_t  FragTrailerLengthMismatch  :  1;
	uint32_t  BOFPresent         	     :  1;
	uint32_t  FragmentTooLong      	     :  1;
	uint32_t  FragmentTooShort     	     :  1;
	uint32_t  RunNumberPresent     	     :  1;
	uint32_t  TriggerTypePresent  	     :  1;
	uint32_t  dummy1       				 :  3;
	uint32_t  LDErr		         		 :  1; //lderr asserted by HOLA core
	uint32_t  DuplicatorChannel  		 :  4;
	uint32_t  FramingError       		 :  1; //didn't end on EOF
	uint32_t  EOFPresent         		 :  1;
	uint32_t  LastPageMark       		 :  1;
	uint32_t  Valid              		 :  1;
	uint32_t  TriggerType        		 :  8;
	uint32_t  L1ID               		 : 24;
	uint32_t  ECR                		 :  8;
} UPFBottomCopyStruct;


typedef struct{
	uint32_t  Address     : 30;
	uint32_t  dummy0	  :  2;
	uint32_t  RunNumber   : 32;
} UPFTopCopyStruct;


/* Per Channel register structures */
/* test for common structures */ 

typedef struct{
	volatile uint32_t BufferPageSize           : 4;
	volatile uint32_t Dummy0                   : 3;
	volatile uint32_t IgnoreMemoryFlowControl  : 1;
	volatile uint32_t ResetBit                 : 1;
	volatile uint32_t Dummy1                   : 4;
	volatile uint32_t FinishFormatVersionError : 1;
	volatile uint32_t FinishFragSizeError      : 1;
	volatile uint32_t FinishHdrMarkerError     : 1;
	volatile uint32_t FinishCNTR               : 1;
	volatile uint32_t FinishBOF                : 1;
	volatile uint32_t FinishEOF                : 1;
	volatile uint32_t FinishGeneralError       : 1;
	volatile uint32_t FinishTriggerType        : 1;
	volatile uint32_t FinishRunNumber          : 1;
	volatile uint32_t Dummy2                   : 2;
	volatile uint32_t FinishMissingBOF         : 1;
	volatile uint32_t FinishMissingEOF         : 1;
	volatile uint32_t FinishIncomplHeader      : 1;
	volatile uint32_t FinishNoHeader           : 1;
	volatile uint32_t FinishCTLError           : 1;
	volatile uint32_t FinishDataError          : 1;
	volatile uint32_t SuppressCNTLWrdStorage   : 1;
	volatile uint32_t XOFF                     : 1;
	volatile uint32_t Dummy3;
} BufferManagerControlRegisterComponentStruct;

typedef union{
	BufferManagerControlRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} BufferManagerControlRegisterStruct;

typedef struct{
	volatile uint32_t FreeFIFOFill          : 12;
	volatile uint32_t dummy0                :  4;
	volatile uint32_t UsedFIFOFill          : 10;
	volatile uint32_t ChannelNumber         :  4;
	volatile uint32_t OneInTheBreech        :  1;
	volatile uint32_t UsedFIFODataAvailable :  1;
	volatile uint32_t dummy2                : 32;
} BufferManagerStatusRegisterComponentStruct;

typedef union{
	BufferManagerStatusRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} BufferManagerStatusRegisterStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} BufferManagerUsedFIFOBottomStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} BufferManagerUsedFIFOTopStruct;

typedef struct{
	volatile uint32_t FragmentSize                : 16;
	volatile uint32_t FragHeaderRunNumLower16Bits : 16;
	volatile uint32_t upper;
} DataGeneratorControlRegisterStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} BufferManagerFreeFIFOStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} EtMisFIFOStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} CommonUPFBottomStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} CommonUPFTopStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} CommonUPFBottomInvalidateStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} CommonUPFTopInvalidateStruct;

typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} MemoryOutputFIFOStruct;

typedef struct{
	volatile uint32_t MemoryReadQueueFIFOFill      : 12;
	volatile uint32_t MemoryReadQueueFIFOFull      :  1;
	volatile uint32_t MemoryReadQueueFIFOEmpty     :  1;
	volatile uint32_t MemoryOutputFirstFIFOFill    : 13;
	volatile uint32_t MemoryOutputFirstFIFOFull    :  1;
	volatile uint32_t MemoryOutputFirstFIFOEmpty   :  1;
	volatile uint32_t MemoryTestInputFIFOFull      :  1;
	volatile uint32_t MemoryTestInputFIFOEmpty     :  1;
	volatile uint32_t dummy0                       :  1;
	volatile uint32_t CommonUPFFill                : 12;
	volatile uint32_t CommonUPFFull                :  1;
	volatile uint32_t CommonUPFEmpty               :  1;
	volatile uint32_t MemoryReadQueueFIFODataValid :  1;
	volatile uint32_t MemoryOutputSecondFIFOEmpty  :  1;
	volatile uint32_t MemoryPLLLocked              :  1;
	volatile uint32_t MemoryPhysicalInitDone       :  1;
	volatile uint32_t MemoryOutputSecondFIFOFill   :  9;
	volatile uint32_t MemoryOutputSecondFIFOFull   :  1;
	volatile uint32_t SubrobNumber                 :  4;
} SubRobStatusComponentStruct;

typedef union{
	SubRobStatusComponentStruct Component;
	volatile uint32_t Register[2];
} SubRobStatusStruct;

typedef struct{
	volatile uint32_t ChannelReset                		:  1;
	volatile uint32_t FIFOMuxReset                		:  1;
	volatile uint32_t FIFOMuxEnable               		:  1;
	volatile uint32_t PrimaryDMAReset             		:  1;
	volatile uint32_t MemoryTestInputFIFOHold     		:  1;
	volatile uint32_t CommonUPFDuplicatorEnable   		:  1;
	volatile uint32_t CommonUPFDuplicatorReset    		:  1;
	volatile uint32_t CommonUPFReset              		:  1;
	volatile uint32_t MemoryOutputFIFOCommsHold   		:  1;
	volatile uint32_t UPFDelayStepsBitMask        		:  2;
	volatile uint32_t DMADoneFIFODuplicatorEnable 		:  1;
	volatile uint32_t DMADoneFIFODuplicatorReset  		:  1;
	volatile uint32_t MemoryReadQueueReset        		:  1;
	volatile uint32_t MemoryOutputSecondFIFOReset 		:  1;
	volatile uint32_t dummy1					  		: 17;
	volatile uint32_t CommonUPFDuplicateSpaceAvailable	: 23;
	volatile uint32_t dummy2                      		:  9;
} SubRobControlComponentStruct;

typedef union{
	SubRobControlComponentStruct Component;
	volatile uint32_t Register[2];
} SubRobControlStruct;

typedef struct{
	volatile uint32_t GTX_Reset          	  :  1;
	volatile uint32_t HOLA_Reset         	  :  1;
	volatile uint32_t BufferManagerReset 	  :  1;
	volatile uint32_t CRCEnable    		 	  :  1;
	volatile uint32_t EnableEarlyWrap 	 	  :  1;
	volatile uint32_t StartDataGen        	  :  1;
	volatile uint32_t SelectDataGen      	  :  1;
	volatile uint32_t UReset           		  :  1;
	volatile uint32_t DroppedWordCounterReset :  1;
	volatile uint32_t DiscardModeEnable       :  1;
	volatile uint32_t ZeroLinkDownCounter     :  1;
	volatile uint32_t ZeroXoffCounter         :  1;
	volatile uint32_t DataGeneratorReset   	  :  1;
	volatile uint32_t ClearRXLostSyncLatched  :  1;
	volatile uint32_t InternalFIFOReset       :  1;
	volatile uint32_t dummy2             	  : 17;
	volatile uint32_t dummy3             	  : 32;	
} ChannelControlRegisterComponentStruct;

typedef union{
	ChannelControlRegisterComponentStruct Component;
	volatile uint64_t Register;
} ChannelControlRegisterStruct;

/*typedef struct{
	volatile uint32_t ChannelNumber                  :  8;
	volatile uint32_t HOLA_UXOFF_n 			         :  1;
	volatile uint32_t ROLHandler_LDCEmulationEnabled :  1;
	volatile uint32_t ROL_LinkDataError              :  1;
	volatile uint32_t ROL_LinkUp                     :  1;
	volatile uint32_t ROL_FlowControl                :  1;
	volatile uint32_t ROL_Activity                   :  1;
	volatile uint32_t ROL_Test                       :  1;
	volatile uint32_t GTX_ResetDone                  :  1;
    volatile uint32_t TX_PLLLK                       :  1;
	volatile uint32_t RX_PLLLK                       :  1;
	volatile uint32_t dummy0                         :  6;
	volatile uint32_t DroppedWordCount               :  8;
	volatile uint32_t LinkDownCount                  : 16;
	volatile uint32_t XoffCount                      : 16;

} ChannelStatusRegisterComponentStruct;
*/

typedef struct{
	volatile uint32_t ChannelNumber                  :  4;
	volatile uint32_t TX_PLLLK                       :  1;
	volatile uint32_t RX_PLLLK                       :  1;
	volatile uint32_t RXSynchStatus                  :  1;
	volatile uint32_t RXSynchStatusLatched           :  1;
	volatile uint32_t HOLA_UXOFF_n 			         :  1;
	volatile uint32_t ROLHandler_LDCEmulationEnabled :  1;
	volatile uint32_t ROL_LinkDataError              :  1;
	volatile uint32_t ROL_LinkUp                     :  1;
	volatile uint32_t ROL_FlowControl                :  1;
	volatile uint32_t ROL_Activity                   :  1;
	volatile uint32_t ROL_Test                       :  1;
	volatile uint32_t GTX_ResetDone                  :  1;
	volatile uint32_t UsageRatio	                 :  8;
	volatile uint32_t DroppedWordCount               :  8;
	volatile uint32_t LinkDownCount                  :  8;
	volatile uint32_t XoffRatio                      :  8;
	volatile uint32_t XoffCount                      : 16;

} ChannelStatusRegisterComponentStruct;

typedef union{
	ChannelStatusRegisterComponentStruct Component;
	volatile uint32_t Register[2];
} ChannelStatusRegisterStruct;

typedef struct{
	volatile uint32_t MemoryPLLLocked            :  1;
	volatile uint32_t MemoryPhysicalInitDone     :  1;
	volatile uint32_t dummy0                     :  2;
	volatile uint32_t MemoryMonitorFSMArbiter    :  4;
	volatile uint32_t MemoryMonitorFSMInputCtrl  :  4;
	volatile uint32_t MemoryMonitorFSMOutputCtrl :  4;
	volatile uint32_t lower                      : 16;
	volatile uint32_t upper;
} MemoryControllerStatusStruct;


/*typedef struct{
	volatile uint32_t MIGAsyncReset            :  1;
	volatile uint32_t dummy                    :  3;
	volatile uint32_t MemoryArbitrationTimeout : 16;
	volatile uint32_t dummy1                   : 12;
	volatile uint32_t InputFIFOFullThreshold   : 10;
	volatile uint32_t dummy2				   : 22;
} MemoryControllerControlComponentStruct;
*/

typedef struct{
	volatile uint32_t MIGAsyncReset            		:  1;
	volatile uint32_t dummy                    		:  7;
	volatile uint32_t MemoryArbitrationWriteTimeout :  9;
	volatile uint32_t dummy1                   		: 15;
	volatile uint32_t InputFIFOFullThreshold   		:  9;
	volatile uint32_t dummy2                   		:  7;
	volatile uint32_t MemoryArbitrationReadTimeout 	:  9;
	volatile uint32_t dummy3				   		:  7;
} MemoryControllerControlComponentStruct;

typedef union{
	MemoryControllerControlComponentStruct Component;
	volatile uint32_t Register[2];
} MemoryControllerControlStruct;

typedef struct{
	volatile uint32_t word;
	volatile uint32_t dummy;
} MemoryTestInputDataStruct;

typedef struct{
	volatile uint32_t lower;
} MemoryTestInputAddressStruct;
/*
typedef struct{
	volatile uint32_t lower;
	volatile uint32_t upper;
} I2CRegisterStruct;
*/

typedef struct{
	volatile uint32_t status;
	volatile uint32_t config;
} I2CRegisterStruct;


#endif //ROBINNPREGISTERS_H

#ifndef ROBINNPROIB_H
#define ROBINNPROIB_H

#include "ROSRobinNP/RobinNP.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "tbb/concurrent_queue.h"
#include "ROSDescriptorNP/DataPage.h"

namespace ROS{

class RobinNPROIB{
public:
	RobinNPROIB(unsigned int robinNPSlot,unsigned int rolMask, bool generateData, unsigned int crcInterval = 0, unsigned int pageSize = 256, unsigned int numPages = 0, bool earlyDGWrap = false);

	~RobinNPROIB();

	void start();
	void abort();
	void stop();

	ROIBOutputElement getFragment(unsigned int subRob){
		ROIBOutputElement receivedFragment;
		try{
			directOutputQueue[subRob]->pop(receivedFragment);
		}
		catch (...) {
			receivedFragment.page = 0;
		}
		return receivedFragment;
	}

	void recyclePage(ROIBOutputElement &fragment){
		unsigned int subRob = m_robIn->getSubRob(fragment.rolId);
		queues[subRob]->push(fragment.page);
		m_robIn->recycleRobinNPPage(fragment.rolId,fragment.robinNPPageNumber);
	}

	RobinNPROLStats* getRolStatistics(unsigned int rolId);


private:
	std::vector<tbb::concurrent_bounded_queue<DataPage*>*> queues;
	std::vector<tbb::concurrent_bounded_queue<ROIBOutputElement>*> directOutputQueue;
	unsigned int m_rolMask;
	RobinNP* m_robIn;
};
}

#endif

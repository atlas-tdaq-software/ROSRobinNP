#include <iostream>
#include <fstream>
#include <iomanip>

#include "ROSRobinNP/RobinNPFragSpy.h"

namespace ROS{


RobinNPFragSpy::RobinNPFragSpy(unsigned int robinNPSlot,unsigned int rolMask, bool generateData, unsigned int crcInterval, unsigned int pageSize,unsigned int bufferSize)
:RobinNPROIB(robinNPSlot,rolMask,generateData,crcInterval,pageSize), m_bufferSize(bufferSize){

	testCounter = 0;
	initStatusMaps();
	
	for(unsigned int channel = 0; channel < s_maxRols; ++channel){
		channelFragmentStorage.push_back(new tbb::concurrent_bounded_queue<ROIBOutputElement>);
	}

}


void RobinNPFragSpy::initStatusMaps(){

	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusFormatError,"Fragment header format incorrect"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusMarkerError,"Missing ROD header marker"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusEofError,"Missing end-of-fragment word"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusCtlError,"Link data error asserted during fragment control word(s)"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusDataError,"Link data error asserted during fragment data word(s)"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusSizeError,"Number of words in fragment does not match ROD trailer"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusSeqError,"Fragment L1ID out of sequence"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusDuplicate,"Fragment L1ID duplicates previous received fragment for this link"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusShort,"Fragment shorter than length of ROD header + trailer"));

}

bool RobinNPFragSpy::checkFragError(ROIBOutputElement &fragment){
  /*testCounter++;
	if(testCounter == 500000){
		fragment.fragmentStatus |= fragStatusMarkerError;
		testCounter = 0;
		}*/
  //std::cout << "status " << fragment.fragmentStatus << std::endl;
	if(fragment.fragmentStatus != 0){
		return true;
	}
	return false;
}

std::string RobinNPFragSpy::parseErrorBits(unsigned int errorBits){
	std::stringstream errors;
	//std::cout << "bits " << errorBits << std::endl;
	for (auto statusPair : m_fragStatusMapDebug) {
		if ((errorBits & statusPair.first) == statusPair.first ){
			errors << statusPair.second << " ";
		}
	}
	return errors.str();
}

void RobinNPFragSpy::dumpFragmentQueueToFile(ROIBOutputElement &errorFragment){

	unsigned int rolId = errorFragment.rolId;
	unsigned int fragsDumped = 1;
	std::time_t time = std::time(NULL);
	std::stringstream ss;
	testCounter++;
	ss << m_fileDumpPath << "/RobinNPSpyErrorDump_ROL_" << rolId << "_" << time << "_dump_" <<  testCounter << ".log";

	std::string filename = ss.str();
	//std::cout << filename.c_str() << std::endl;
	std::ofstream dumpFile(filename.c_str());

	ROIBOutputElement fragmentToDump;
	while(channelFragmentStorage[rolId]->try_pop(fragmentToDump)){
		//std::cout << "dumping " << fragsDumped << std::endl;
		unsigned int* fragmentLocation = fragmentToDump.page->virtualAddress();
		dumpFile << "Fragment Dump for ROL " << rolId << std::hex << " L1 ID " << *(fragmentLocation + 5) << ", error description " << parseErrorBits(fragmentToDump.fragmentStatus) << "- fragment size 0x" << fragmentToDump.dataSize << " words" << std::endl;
		for(unsigned int dataWord = 0; dataWord < fragmentToDump.dataSize ; ++dataWord){
			dumpFile << std::hex << std::setfill('0') << std::setw(8) << *(fragmentLocation + dataWord) << " ";
			if((dataWord + 1) % 8 == 0){
				dumpFile << std::endl;
			}
		}
		dumpFile << "END OF FRAGMENT" << std::endl;
		fragsDumped++;
		recyclePage(fragmentToDump);
	}

	unsigned int* fragmentLocation = errorFragment.page->virtualAddress();
	dumpFile << "Error Fragment Dump for ROL " << rolId << std::hex << " L1 ID " << *(fragmentLocation + 5) << ", error description " << parseErrorBits(errorFragment.fragmentStatus) << "- fragment size 0x" << fragmentToDump.dataSize << " words" << std::endl;
	for(unsigned int dataWord = 0; dataWord < fragmentToDump.dataSize ; ++dataWord){
		dumpFile << std::hex << std::setfill('0') << std::setw(8) << *(fragmentLocation + dataWord) << " ";
		if((dataWord + 1) % 8 == 0){
			dumpFile << std::endl;
		}
	}
	dumpFile << "END OF DUMP FILE" << std::endl;

	dumpFile << std::hex << std::endl;
	dumpFile.close();
	recyclePage(errorFragment);
	std::cout << "Fragment Queue Dumped for ROL " << rolId << " containing " << fragsDumped << " fragments to " << filename.c_str() << std::endl;

}

void RobinNPFragSpy::storeFragment(ROIBOutputElement &fragment){

	if(checkFragError(fragment)){
		dumpFragmentQueueToFile(fragment);
	}
	else{
		if(channelFragmentStorage[fragment.rolId]->size() >= m_bufferSize){
			//Recycle fragment DMA landing area to allow new fragments to take its place, ensure all data has been copied/processed
			ROIBOutputElement fragmentToRecycle;
			channelFragmentStorage[fragment.rolId]->pop(fragmentToRecycle);
			recyclePage(fragmentToRecycle);
		}
		channelFragmentStorage[fragment.rolId]->push(fragment);
	}
}


}

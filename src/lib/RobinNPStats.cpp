#include "ROSRobinNP/RobinNPStats.h"
#include <iostream>
#include <iomanip>

using namespace ROS;

void drawLine(unsigned int elements){
	while(elements != 0){
		std::cout << "-";
		elements--;
	}
	std::cout << std::endl;
}

RobinNPROLStats::RobinNPROLStats(): m_rolId(0), m_robId(0){
	reset();
}

void RobinNPROLStats::reset(){
	m_pagesFree = 0;
	m_pagesInUse = 0;
	m_fragStatSize = 0;
	m_pageStatSize = 0;
	m_mostRecentId = 0;
	m_savedRejectedPages = 0;
	m_eventLog.current = 0;
	m_eventLog.overflow = 0;
	m_eventLog.logEnabled = 1;
	m_eventLog.logSize = s_eventLogSize;

	for(int logEntry = 0; logEntry < m_eventLog.logSize; ++logEntry){
		m_eventLog.eventArray[logEntry] = 0;
	}

	m_bufferFull = 0;
	m_rolXoffStat = 0;
	m_rolDownStat = 0;
	m_memreadcorruptions = 0;

	for(unsigned int statCounter = 0; statCounter < LAST_FRAGSTAT; ++statCounter){
		m_fragStat[statCounter] = 0;
	}

	for(unsigned int statCounter = 0; statCounter < LAST_PAGESTAT; ++statCounter){
		m_pageStat[statCounter] = 0;
	}

	m_xoffpercentage = 0;
	m_rolInputBandwidth = 0;
	m_rolOutputBandwidth = 0;
	m_readoutFraction = 0;
	m_fragsIncomplete = 0;
	m_multipageIndexError = 0;
	m_xoffcount = 0;
	m_ldowncount = 0;

	for(unsigned int statCounter = 0; statCounter < s_numStatusWordBits; ++statCounter){
		m_statusErrors[statCounter] = 0;
	}
}

// Fragment statistics
const char *RobinNPROLStats::m_robinFragStatStrings[LAST_FRAGSTAT] = { \
		"Frags received", \
		"Frags available", \
		"Frags not avail", \
		"Frags pending", \
		"Frags added", \
		"Frags deleted", \
		"Frags truncated", \
		"Frags corrupted", \
		"Frags replaced", \
		"Frags out of sync", \
		"Frags missing on delete", \
		"Frags req. in discard mode", \
		"Frags too short (no L1ID)", \
		"Frags longer than max (128k)", \
		"Frags TX Error", \
		"Frags format error", \
		"Frags marker error", \
		"Frags framing error", \
		"Frags ctrl word s-link error", \
		"Frags data word s-link error", \
		"Frags size mismatch in trailer", \
		"Frags rejected"};

// Page statistics
const char *RobinNPROLStats::m_robinPageStatStrings[LAST_PAGESTAT] = { \
		"Pages received", \
		"Pages added", \
		"Pages deleted", \
		"Pages provided", \
		"Pages suppressed", \
		"Pages invalid"};

const char *RobinNPROLStats::m_robinNPErrorStatStrings[s_numStatusWordBits] = {
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"RESERVED NON-ROS", \
		"Frags req. in discard mode", \
		"Frags pending", \
		"Frags lost", \
		"Frags too short (no L1ID)", \
		"Frags truncated", \
		"Frags with TX Error", \
		"Frags out of sequence", \
		"Frags duplicate", \
		"Frags format error", \
		"Frags invalid header marker", \
		"Frags missing EOF", \
		"UNUSED", \
		"Frags s-link ctrl word error", \
		"Frags s-link data word error", \
		"Frags size error", \
		"UNUSED"};


void RobinNPROLStats::dump(){
	drawLine(52);
	std::cout << "Print Statistics for ROL " << m_rolId << std::endl;
	drawLine(52);
	std::cout << "ROB ID \t\t\t\t\t|0x" << std::hex << m_robId << std::dec << std::endl;
	std::cout << "Number of free pages \t\t\t|" << m_pagesFree << std::endl;
	if(m_pagesInUse < 0){ // temporary workaround for firmware fifo size reporting issue
		std::cout << "Number of pages in use \t\t\t|" << 0 << std::endl;
	}
	else{
		std::cout << "Number of pages in use \t\t\t|" << m_pagesInUse << std::endl;
	}
	std::cout << "Number of fragment statistic types \t|" << m_fragStatSize << std::endl;
	std::cout << "Number of page statistic types \t\t|" << m_pageStatSize << std::endl;
	std::cout << "Most recently received L1 ID \t\t|0x" << std::hex << m_mostRecentId << std::dec << std::endl;
	std::cout << "Number of saved rejected pages \t\t|" << m_savedRejectedPages << std::endl;
	std::cout << "Memory buffer fill state \t\t|" << m_bufferFull << std::endl;
	std::cout << "ROL XOFF State \t\t\t\t|" << m_rolXoffStat << std::endl;
	std::cout << "ROL link down State \t\t\t|" << m_rolDownStat << std::endl;
	std::cout << "Frags failing DMA integrity test \t|" << m_memreadcorruptions << std::endl;
	std::cout << "XOFF percentage \t\t\t|" << m_xoffpercentage << std::endl;
	std::cout << "ROL input bandwidth (MB/s) \t\t|" << m_rolInputBandwidth << std::endl;
	std::cout << "Frags missing pages when requested \t|" << m_fragsIncomplete << std::endl;
	std::cout << "Frags with inconsistent pages in index \t|" << m_multipageIndexError << std::endl;
	std::cout << "Number of XOFF assertions \t\t|" << m_xoffcount << std::endl;
	std::cout << "Number of link down events \t\t|" << (unsigned int)m_ldowncount << std::endl;
	std::cout << std::endl;
	drawLine(52);
	std::cout << "Event Log Data" << std::endl;
	drawLine(52);
	std::cout << "Event Log Latest ID \t\t\t|" << m_eventLog.current << std::endl;
	std::cout << "Event Log Enabled State \t\t|" << m_eventLog.logEnabled << std::endl;
	std::cout << "Event Log Size \t\t\t\t|" << m_eventLog.logSize << std::endl;
	std::cout << "Event Log Overflow flag \t\t|" << m_eventLog.overflow << std::endl;
	drawLine(52);
	std::cout << "Dump of Log Data (Hex)" << std::endl;
	drawLine(52);
	for(int logEntry = 0; logEntry < m_eventLog.logSize; ++logEntry){
		std::cout << "Event Log Entry " << logEntry << ": \t" << std::hex << m_eventLog.eventArray[logEntry] << std::dec << std::endl;
	}
	drawLine(45);
	std::cout << "Fragment Statistics (Decimal Counters)" << std::endl;
	drawLine(45);

	for(unsigned int statCounter = 0; statCounter < LAST_FRAGSTAT; ++statCounter){
		std::cout << std::setw(31) << std::left << m_robinFragStatStrings[statCounter] << "|" << std::setw(10)  << m_fragStat[statCounter] << std::endl;
	}
	drawLine(45);
	std::cout << "Page Statistics (Decimal Counters)" << std::endl;
	drawLine(45);
	for(unsigned int statCounter = 0; statCounter < LAST_PAGESTAT; ++statCounter){
		std::cout << std::setw(31) << std::left << m_robinPageStatStrings[statCounter] << "|" << std::setw(10)  << m_pageStat[statCounter] << std::endl;
	}
/*	drawLine(55);
	std::cout << "ROB Status Bits (Decimal Counters)" << std::endl;
	drawLine(55);
	for(unsigned int statCounter = s_numStatusWordBits/2; statCounter < s_numStatusWordBits; ++statCounter){
		std::cout << "Bit: " << statCounter << " " << std::setw(35) << std::left << m_robinNPErrorStatStrings[statCounter] << "|" << std::setw(10)  << m_statusErrors[statCounter] << std::endl;
	}
*/
}


RobinNPStats::RobinNPStats(unsigned int slotNumber): m_slotNumber(slotNumber){
	m_firmwareVersion = 0;
	m_fragFormat = 0;
	m_bufByteSize = 0;
	m_numRols = 0;
	m_running = false;

	for(unsigned int rolId = 0; rolId < s_maxRols; ++rolId){
		m_rolStats[rolId].m_rolId = rolId;
	}
}

void RobinNPStats::dump(){
	std::cout << "Print Card Level Information for RobinNP " << m_slotNumber << std::endl;
	drawLine(52);

	std::cout << "Card Number \t\t\t\t|" << m_slotNumber << std::endl;
	std::cout << "Firmware version \t\t\t|0x" << std::hex << m_firmwareVersion << std::dec << std::endl;
	std::cout << "Fragment format \t\t\t|0x" << std::hex << m_fragFormat << std::dec << std::endl;
	std::cout << "Buffer byte size \t\t\t|0x" << std::hex << m_bufByteSize << std::dec << std::endl;
	std::cout << "Number of ROLs \t\t\t\t|" << m_numRols << std::endl;
	std::cout << "Run Status \t\t\t\t|" << m_running << std::endl;
	std::cout << std::endl;
}

#include "ROSRobinNP/RobinNPROIB.h"

namespace ROS{

RobinNPROIB::RobinNPROIB(unsigned int robinNPSlot,unsigned int rolMask, bool generateData, unsigned int crcInterval, unsigned int pageSize, unsigned int numPages, bool earlyDGWrap): m_rolMask(rolMask){



	m_robIn = new RobinNP(robinNPSlot, runModeROIB);
	m_robIn->init();

	RobinNPConfig configuration;
	configuration.setMaxRxPages(1);
	configuration.setPageSize(pageSize);
	configuration.setMemEmulationState(false);
	configuration.setMemWriteTimeout(50);
	configuration.setMemReadTimeout(50);
	configuration.setMemfifoFill(50);
	configuration.setCRCCheckInterval(crcInterval);
	configuration.setNumPages(numPages);

	unsigned int numRols = 0;
	for(unsigned int rolId = 0; rolId < s_maxRols; ++rolId){
		if((1 << rolId) & m_rolMask){
			RobinNPROLConfig rolConfig;
			rolConfig.setDiscardMode(false); //check DB entry for this
			rolConfig.setRobId(rolId);
			rolConfig.setRolDataGen(generateData);
			rolConfig.setRolEnabled(false);
			rolConfig.setTestSize(100);
			rolConfig.setDGEarlyWrap(earlyDGWrap);
			configuration.configureRol(rolConfig,rolId);
			numRols++;
		}
	}

	std::cout << "Total number of ROLs active: " << numRols << std::endl;

	m_robIn->configure(configuration);

	for(unsigned int subRob = 0; subRob < 2; ++subRob){
		m_robIn->registerSubRobIndex(subRob,subRob);
	}

	m_robIn->initialisePageQueues(queues); //get queues and then use to repopulate pages

	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){
		directOutputQueue.push_back(new tbb::concurrent_bounded_queue<ROIBOutputElement>());
	}
	m_robIn->setupDirectOutputQueue(&directOutputQueue);


}


RobinNPROIB::~RobinNPROIB(){
	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){
		delete directOutputQueue[subRob];
	}
	delete m_robIn;
}

void RobinNPROIB::start(){

	m_robIn->resetSubRobChannels();

	for (u_int chan = 0; chan < s_maxRols; ++chan){
		if((1 << chan) & m_rolMask){
			m_robIn->setRolEnabled(true, chan);
			m_robIn->restartRol(chan);
			m_robIn->initStatistics(chan);
			m_robIn->toggleBufferManagerXOFF(chan,true);
		}
	}

	if(!m_robIn->getThreadState()) {
		std::cout << "Starting Threads!" << std::endl;
		m_robIn->startThreads();
	}
}

void RobinNPROIB::abort(){
	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){
		directOutputQueue[subRob]->abort();
	}
}

void RobinNPROIB::stop(){
	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){
		while(!directOutputQueue[subRob]->empty()){
			ROIBOutputElement pageToClear;
			directOutputQueue[subRob]->pop(pageToClear);
			recyclePage(pageToClear);
		}
	}
	for (u_int chan = 0; chan < s_maxRols; ++chan){
		if((1 << chan) & m_rolMask){
			m_robIn->setRolEnabled(false, chan);
		}
	}
	m_robIn->stopThreads();
}

RobinNPROLStats* RobinNPROIB::getRolStatistics(unsigned int rolId){
	return m_robIn->getROLStatistics(rolId);
}


}

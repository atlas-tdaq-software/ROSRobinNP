//RobinNP headers
#include "ROSRobinNP/ROSRobinNPExceptions.h"

using namespace ROS;

/*****************************************************************************/
ROSRobinNPExceptions::ROSRobinNPExceptions(ErrorCode errorCode)
/*****************************************************************************/
:
		ROSException("ROSRobinNP", errorCode, getErrorString(errorCode)) {
}

/******************************************************************************************************/
ROSRobinNPExceptions::ROSRobinNPExceptions(ErrorCode errorCode,
		std::string description)
/******************************************************************************************************/
:
		ROSException("ROSRobinNP", errorCode, getErrorString(errorCode),
				description) {
}

/*****************************************************************************/
ROSRobinNPExceptions::ROSRobinNPExceptions(ErrorCode errorCode,
		const ers::Context& context)
/*****************************************************************************/
:
		ROSException("ROSRobinNP", errorCode, getErrorString(errorCode), context) {
}

/******************************************************************************************************/
ROSRobinNPExceptions::ROSRobinNPExceptions(ErrorCode errorCode,
		std::string description, const ers::Context& context)
/******************************************************************************************************/
:
		ROSException("ROSRobinNP", errorCode, getErrorString(errorCode),
				description, context) {
}

ROSRobinNPExceptions::ROSRobinNPExceptions(const std::exception& cause,
		ErrorCode error, std::string description, const ers::Context& context) :
		ROSException(cause, "ROSRobinNP", error, getErrorString(error),
				description, context) {
}

/************************************************************************/
std::string ROSRobinNPExceptions::getErrorString(unsigned int errorId) const
/************************************************************************/
{
	std::string rc;
	switch (errorId) {
	/*******************************/
	/*Exceptions of the Robin class*/
	/*******************************/
	case IOCTL:
		rc = "Error from call to ROBIN driver";
		break;
	case MUNMAP:
		rc = "Error from call to munmap()";
		break;
	case MMAP:
		rc = "Error from call to mmap()";
		break;
	case OPEN:
		rc = "Failed to open /dev/robinnp. Check if the file exists and if the read and write flags for user have been set";
		break;
	case CLOSE:
		rc = "Failed to close /dev/robinnp";
		break;
	case NROLS:
		rc = "Real and requested number of ROLs does not match";
		break;
	case BADCRC:
		rc = "The CRC of the ROD fragment does not match";
		break;
	case UNIMPLEMENTED:
		rc = "This feature has yet to be implemented for the RobinNP";
		break;
	case ROLCOUNT:
		rc = "Requested number of ROLs greater than available amount";
		break;
	case LOCK:
		rc = "Unable to lock RobinNP. Verify another process isn't using the same hardware.";
		break;
	case SHARED:
		rc = "Error Allocating Shared Memory";
		break;
	case UNSHARED:
		rc = "Error De-Allocating Shared Memory";
		break;
	case THREAD:
		rc = "Thread Control Error";
		break;
	case FRAGLOST:
		rc = "Lost Fragment";
		break;
	case NOPAGES:
		rc = "No more DMA landing pages";
		break;
	case NOHARDWARE:
		rc = "RobinNP not detected in requested slot";
		break;
	case MISSINGPAGE:
		rc = "Incomplete fragment in index";
		break;
	case FRAGDUP:
		rc = "Duplicate fragment";
		break;
	case INTERRUPTFAIL:
		rc = "Interrupt handling reported failure";
		break;
	case INTERRUPTINIT:
		rc = "Unable to initialise interrupts";
		break;
	case BADID:
		rc = "Level1 ID check error";
		break;
	case INIT:
		rc = "Attempt to open RobinNP failed in requested slot";
		break;
	case RXPAGE:
		rc = "Invalid max RX pages";
		break;
	case BADREQUEST:
		rc = "Invalid fragment request";
		break;
	case REQDISCARD:
		rc = "Fragment requested from discarded ROL";
		break;
	case REJECTEDFRAGMENT:
		rc = "Rejected page";
		break;
	case FRAGMENTERROR:
		rc = "Fragment error";
		break;
	case FRAGPEND:
		rc = "Pending Fragment";
		break;
	case LINKRESET:
		rc = "S-link reset problem";
		break;
	case RXDMA:
		rc = "Invalid max RX pages";
		break;
	case MAXFRAG:
		rc = "Fragments too large";
		break;
	case RUNMODE:
		rc = "Invalid Run Mode";
		break;
	case CONFIGURE:
		rc = "Failure to Configure RobinNP";
		break;
	case CONDWAITERR:
		rc = "Thread condition wait returns error code";
		break;
		/*****************************/
		/*Exceptions of the RolNP class*/
		/*****************************/
	case WRONGREPLY:
		rc = "The reply message from the ROBIN contains invalid data";
		break;
	default:
		rc = "";
		break;
	}
	return rc;
}

/*
 *
 * robinnpbist - self test module for RobinNP
 *
 * author: William Panduro Vazquez (j.panduro.vazquez@cern.ch) - 2012
 *
 */

//C++ headers
#include <iostream>
#include <iomanip>
#include <map>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <chrono>

//System headers
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <time.h>

//RobinNP headers
#include "ROSRobinNP/RobinNPBIST.h"
#include "rcc_time_stamp/tstamp.h"

using namespace ROS;


class Timer {

private:

	unsigned long m_startTime;
	unsigned long m_timeLimit;
	bool m_clockStarted;

public:

	Timer(unsigned long timeout):m_startTime(0),m_timeLimit(timeout),m_clockStarted(false){
	}

	void start() {
		m_startTime = clock();
		m_clockStarted = true;
	}

	double elapsedTime() {
		if(m_clockStarted){
			return ((double) clock() - m_startTime) / CLOCKS_PER_SEC;
		}
		else{
			std::cout << "Timing: Timer not started!" << std::endl;
			return 0;
		}
	}

	bool isTimeout() {
		return m_timeLimit <= elapsedTime();
	}

	void reset(){
		m_startTime = clock();
	}
};

/*************************************************************************/
/*************************************************************************/
/*  					Public Methods									 */
/*************************************************************************/
/*************************************************************************/



/*************************************************************************/
/*          		Construction / Destruction                           */
/*************************************************************************/

//Standalone Constructor
RobinNPBIST::RobinNPBIST(unsigned int slotNumber, bool isVerbose){
	std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
	std::ofstream   fout("/dev/null");
	try{


		if(!isVerbose){
			std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
		}

		m_testCandidate = new RobinNP(slotNumber,runModeLegacy,0,"/sharedMemRobinNPBIST_");
		m_testCandidate->init();
		m_remoteCall = false;
		m_verboseMode = isVerbose;
		m_interruptAcknowledgeLock = 0;
	}
	catch(ers::Issue& ex){
		std::cout.rdbuf(cout_sbuf); // restore the original stream buffer
		std::stringstream exception;
		exception << "Constructor Exception: " << ex.message() << std::endl;
		throw(exception.str());
	}
	catch(std::exception &ex){
		std::cout.rdbuf(cout_sbuf); // restore the original stream buffer
		std::stringstream exception;
		exception << "Constructor Std Exception: " << ex.what() << std::endl;
		throw(exception.str());

	}
	catch(...){
		std::cout.rdbuf(cout_sbuf); // restore the original stream buffer
		std::string exception =  "Unhandled Constructor Exception ";
		throw(exception);
	}
	std::cout.rdbuf(cout_sbuf); // restore the original stream buffer

}

//Constructor for calls from RobinNP object
RobinNPBIST::RobinNPBIST(RobinNP* testCandidate, bool isVerbose):m_testCandidate(testCandidate){
	m_remoteCall = true;
	m_verboseMode = isVerbose;
	m_interruptAcknowledgeLock = 0;
}

//Destructor
RobinNPBIST::~RobinNPBIST(){
	if(!m_remoteCall){
		delete m_testCandidate;
	}
}

/*************************************************************************/
/*                         Public Tests                                  */
/*************************************************************************/

/*
 *
 * Place 1000 Pages on the Free Page Fifo and activate the data generator for a specific ROL
 * Verify that the pages arrive in the fifo duplicator space in the correct order with the correct
 * event IDs and that none are lost
 *
 */

bool RobinNPBIST::pageflowTest(unsigned int rolId){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Page Flow Test for ROL " << rolId << "               *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;

	const unsigned int pageTestAmount = s_fpfSize;
	unsigned int pageSize = 1024;
	unsigned int dataGenSize = 450;

	std::cout << std::endl;
	std::cout << "BIST PageflowTest: Filling FPF up to " << pageTestAmount << " from stack for ROL " << rolId << std::endl;

	if(!initPageFlow(pageSize,rolId)){
		std::cout << "BIST PageflowTest FAIL: Unable to initialise pageflow" << std::endl;
		return false;
	}

	populateFreePageFifo(rolId,pageTestAmount,pageSize);

	if(!validateIdlePageFlow(rolId,pageTestAmount)){
		std::cout << "BIST PageflowTest FAIL: PageFlow Problem Detected" << std::endl;
		return false;
	}

	startDataGenerator(dataGenSize,rolId);

	std::cout << "BIST PageflowTest: Wait for 5 seconds to allow pages to flow through FIFOs" << std::endl;
	sleep(5);
	std::cout << "BIST PageflowTest: Continuing..." << std::endl;

	if(!validateActivePageFlow(rolId,pageTestAmount)){
		std::cout << "BIST PageflowTest FAIL: PageFlow Problem Detected" << std::endl;
		return false;
	}

	std::cout << "BIST PageflowTest: Wait for 5 seconds to allow pages to flow through FIFOs" << std::endl;
	sleep(5);
	std::cout << "BIST PageflowTest: Continuing..." << std::endl;

	if(!receiveAndValidatePages(rolId,pageTestAmount,pageSize,false)){
		std::cout << "BIST PageflowTest: FAIL: Did not receive requested fragments" << std::endl;
		return false;
	}

	std::cout << "BIST PageflowTest: Successfully received " << pageTestAmount << " fragments from duplicator for ROL " << rolId << std::endl;

	std::cout << std::endl;
	cleanupRol(rolId);
	std::cout << "BIST PageflowTest PASS: Page Flow Test Passed For ROL " << rolId << std::endl;
	std::cout << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Page Flow Test for ROL " << rolId << "             *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return(true);
}


bool RobinNPBIST::pageflowMemTest(unsigned int rolId){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Page Flow Memory Test for ROL " << rolId << "        *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;

	const unsigned int pageTestAmount = 10;
	const unsigned int dataGenSize = 450;
	const unsigned int pageSize = 512;

	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;

	if(!initialiseMemory(subRob,0.1)){
		std::cout << "BIST PageMemSpeedTest FAIL: Unable to initialise memory" << std::endl;
		return false;
	}

	if(!initPageFlow(pageSize,rolId)){
		std::cout << "BIST PageMemSpeedTest FAIL: Unable to initialise pageflow" << std::endl;
		return false;
	}

	std::cout << std::endl;

	populateFreePageFifo(rolId,pageTestAmount,pageSize,true);

	startDataGenerator(dataGenSize,rolId);

	if(!enableCommonPageFlow(subRob)){
		std::cout << "BIST PageflowMemTest FAIL: Unable to activate Common Pageflow" << std::endl;
		cleanupRol(rolId);
		return false;
	}

	if(!receiveAndValidatePages(rolId,pageTestAmount,pageSize,true)){
		std::cout << "BIST PageflowMemTest: FAIL: Did not receive requested fragments" << std::endl;
		return false;
	}

	std::cout << "BIST PageflowMemTest: Successfully received " << pageTestAmount << " fragments from duplicator for ROL " << rolId << std::endl;

	std::cout << std::endl;
	cleanupRol(rolId);
	std::cout << "BIST PageflowMemTest PASS: Page Flow Test Passed For ROL " << rolId << std::endl;
	std::cout << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Page Flow Memory Test for ROL " << rolId << "      *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return(true);
}


bool RobinNPBIST::pageflowSpeedTest(unsigned int rolId, unsigned int dataGenSize, bool interruptTest){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Page Flow Speed Test for ROL " << rolId << "         *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;

	const unsigned int pageSize = 1024;
	unsigned int testTotalRequested = 1000000;
	int tempStack = testTotalRequested;
	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;

	unsigned int interrupt = s_UPFInterruptCodes[subRob];

	if(interruptTest){
		std::cout << "BIST PageflowSpeedTest: Running interrupt based test" << std::endl;
		configureInterrupter(interrupt);
	}

	std::cout << std::endl;
	std::cout << "BIST PageflowSpeedTest: Stack for ROL " << rolId << " initialised with " << testTotalRequested << " available entries" << std::endl;

	if(!initPageFlow(pageSize,rolId)){
		std::cout << "BIST PageflowSpeedTest FAIL: Unable to initialise pageflow" << std::endl;
		return false;
	}

	std::cout << std::endl;

	startDataGenerator(dataGenSize,rolId);

	if(!enableCommonPageFlow(subRob)){
		std::cout << "BIST PageflowSpeedTest: FAIL: Unable to enable pageflow" << std::endl;
		cleanupRol(rolId);
		return false;
	}

	int* results;
	unsigned int totalReceived = 0;
	bool timeout = false;
	double timeResult = 0;
	Timer timeControl(20);
	timeControl.start();
	unsigned int interruptsReceived = 0;
	unsigned int mostRecentId = -1;
	std::cout << "BIST PageflowSpeedTest: Waiting for fragments..." << std::endl;

	while(totalReceived < testTotalRequested){

		if(timeControl.isTimeout()){
			timeResult = timeControl.elapsedTime();
			timeout = true;
			break;
		}

		if(!interruptTest){
			unsigned int readIndex = 0;
			while(m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
				results = processUPFDuplicatorPageSingleChannel(readIndex,rolId,pageSize);
				if(results[0] != -99){
					if(m_testCandidate->checkSeqError(mostRecentId,results[0])){
						std::cout << "BIST PageflowSpeedTest FAIL: Fragments out of sequence: Previous L1Id 0x" << std::hex << mostRecentId << " latest 0x" << results[0] << std::dec << " Total Received " << totalReceived << std::endl;
					}
					totalReceived++;
					mostRecentId = results[0];
				}
				delete[] results;

			}
		}

		int fpfFreeCount = s_fpfSize - m_testCandidate->getFPFItems(rolId);

		while ((fpfFreeCount > 0) && (tempStack > 0)){

			m_testCandidate->m_Bar0Channel[rolId]->BufferManagerFreeFIFO.lower = tempStack * pageSize;
			tempStack--;
			fpfFreeCount --;
		}

		if(interruptTest){
			bool interrupted = false;
			unsigned int readIndex = 0;
			if(!m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
				int ret = ioctl(m_testCandidate->m_devHandle, WAIT_IRQ, &interrupt);
				if (ret)
				{
					std::cout << "BIST PageflowSpeedTest FAIL: Interrupt IOCTL failed to return success" << std::endl;
					return false;
				}
				interruptsReceived++;
				interrupted = true;
			}
			else{
				results = processUPFDuplicatorPageSingleChannel(readIndex,rolId,pageSize);
				if(results[0] != -99){
					if(m_testCandidate->checkSeqError(mostRecentId,results[0])){
						std::cout << "BIST PageflowSpeedTest FAIL: Fragments out of sequence: Previous L1Id 0x" << std::hex << mostRecentId << " latest 0x" << results[0] << std::dec << " Total Received " << totalReceived << std::endl;
					}

					totalReceived++;
					mostRecentId = results[0];
				}
				delete[] results;
			}
			while(m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
				results = processUPFDuplicatorPageSingleChannel(readIndex,rolId,pageSize);

				if(results[0] != -99){
					if(m_testCandidate->checkSeqError(mostRecentId,results[0])){
						std::cout << "BIST PageflowSpeedTest FAIL: Fragments out of sequence: Previous L1Id 0x" << std::hex << mostRecentId << " latest 0x" << results[0] << std::dec << " Total Received " << totalReceived << std::endl;
					}

					totalReceived++;
					mostRecentId = results[0];
				}
				delete[] results;
			}
			if(interrupted){
				m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
				interrupted = false;
			}
		}
	}

	timeResult = timeControl.elapsedTime();

	if(timeout){
		std::cout << "BIST PageflowSpeedTest FAIL: Timeout when waiting for " << testTotalRequested << " fragments from duplicator for ROL " << rolId << "." << std:: endl;
		std::cout << "BIST PageflowSpeedTest FAIL: Time taken = " << timeResult << " seconds." << std::endl;
		std::cout << "BIST PageflowSpeedTest FAIL: Total pages received = " << totalReceived << std::endl;
		std::cout << "BIST PageflowSpeedTest FAIL: Page Rate = " << totalReceived/timeResult << " pages per second." << std::endl;
	}
	else{
		timeResult = timeControl.elapsedTime();
		std::cout << "BIST PageflowSpeedTest: Successfully received " << testTotalRequested << " fragments from duplicator for ROL " << rolId << std::endl;
		std::cout << "BIST PageflowSpeedTest: Fragment Size " << dataGenSize << " words, Time taken " << timeResult << " seconds. Page Rate = " << testTotalRequested/timeResult << " pages per second." << std::endl;
	}
	std::cout << "BIST PageflowSpeedTest: Last Received L1Id 0x" << std::hex << mostRecentId << std::dec << std::endl;
	if(interruptTest){
		std::cout << "BIST PageflowSpeedTest: Total Interrupts Received = " << interruptsReceived << std::endl;
	}


	std::cout << std::endl;
	cleanupRol(rolId);
	std::cout << "BIST PageflowSpeedTest PASS: Page Flow Test Passed For ROL " << rolId << std::endl;
	std::cout << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Page Flow Speed Test for ROL " << rolId << "       *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return(true);
}

/*
 *
 * Attempt to read and write to all RobinNP registers (where R/W are permitted)
 * Confirm readback on required registers and confirm control registers take on specified values after initialisation
 *
 */

// Define some macros to be used in the Register Test
#define TEST_INIT(DESC) std::cout << "BIST Register Test: Testing 1/0 toggle of " << DESC << std::endl;

#define TEST_ZERO(X,DESC,RES) \
		if(X != 0){ \
			std::cout << "BIST Register Test FAIL: " << DESC << " has non-zero value, actual value = 0x" << std::hex << X << std::dec << std::endl; \
			RES = false; \
		}

#define TEST_VALUE(X,VALUE,DESC,RES) \
		X = VALUE; \
		if(X != VALUE){ \
			std::cout << "BIST Register Test FAIL: Wrote 0x" << std::hex << VALUE << " to " << DESC << " but reading back different value, actual value = 0x" << X << std::dec << std::endl; \
			RES = false; \
		}

#define TEST_SUCC(DESC,RES) if (RES) std::cout << "BIST Register Test: Successfully tested toggle of " << DESC << std::endl;

#define TEST_BIT(X,DESC) { bool macroLocalResult = true; \
		TEST_INIT(DESC) \
		TEST_ZERO(X, DESC, macroLocalResult) \
		TEST_VALUE(X, 1, DESC, macroLocalResult) \
		TEST_VALUE(X, 0, DESC, macroLocalResult) \
		TEST_SUCC(DESC,macroLocalResult) \
		result &= macroLocalResult; \
}

bool RobinNPBIST::registerTest(){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Register Test                          *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;

	const unsigned int bufferManagerStatusRegisterReferenceOffset = 0x2;

	unsigned int comparisonValue = bufferManagerStatusRegisterReferenceOffset;
	unsigned int testReference = 0;
	const unsigned int bufferManagerControlRegisterReferenceValue = 0x800C0005;
	const unsigned int bufferManagerStatusRegisterReferenceValue = 0x4000000;
	const unsigned int subRobControlRegisterUpperReferenceValue = 0x7FF8;
	const unsigned int subRobControlRegisterLowerReferenceValue = 0x0;

	bool result = true;

	std::cout << std::endl;
	std::cout << "BIST Register Test: BAR0 of board mapped to 0x" << std::hex << m_testCandidate->m_bar0Addr << std::dec << std::endl;

	//Test Common Struct Mapping
	if((unsigned long)m_testCandidate->m_Bar0Common != m_testCandidate->m_bar0Addr){
		std::cout << "BIST Register Test FAIL: Common Struct not mapped to base of BAR0, actual value = " << std::hex << m_testCandidate->m_Bar0Common << std::dec << std::endl;
		result = false;
	}
	else
	{
		std::cout << "BIST Register Test: Common Struct correctly mapped to base of BAR0, actual value = " << std::hex << m_testCandidate->m_Bar0Common << std::dec << std::endl;

	}

	std::cout << std::endl;
	//Test Channel Struct Mapping
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		if((unsigned long)m_testCandidate->m_Bar0Channel[rol] != (m_testCandidate->m_bar0Addr + 0x1000 + (rol * 0x100))){
			std::cout << "BIST Register Test FAIL: ROL " << rol << " Channel Struct not correctly mapped to BAR0." << std::endl;
			std::cout << "BIST Register Test FAIL: Value should be BAR + 0x1000 + (rol number X 0x100), i.e. BAR0 + 0x" << std::hex << 0x1000 + (rol * 0x100) << " = 0x" << m_testCandidate->m_bar0Addr + 0x1000 + (rol * 0x100) << ", actual value = " << m_testCandidate->m_Bar0Channel[rol] << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: ROL " << rol << " Channel Struct correctly mapped to BAR0." << std::endl;
			std::cout << "BIST Register Test: Value should be BAR + 0x1000 + (rol number X 0x100), i.e. BAR0 + 0x" << std::hex << 0x1000 + (rol * 0x100) << " = 0x" <<  m_testCandidate->m_bar0Addr + 0x1000 + (rol * 0x100) << ", actual value = " << m_testCandidate->m_Bar0Channel[rol] << std::dec << std::endl;

		}
	}

	std::cout << std::endl;
	//Test SubRob Struct Mapping
	for(unsigned int subRob = 0; subRob < m_testCandidate->m_numSubRobs; ++subRob){
		if((unsigned long)m_testCandidate->m_Bar0SubRob[subRob] != (m_testCandidate->m_bar0Addr + 0x4000 + (subRob * 0x200))){
			std::cout << "BIST Register Test FAIL: SubRob " << subRob << " Channel Struct not correctly mapped to BAR0." << std::endl;
			std::cout << "BIST Register Test FAIL: Value should be BAR + 0x4000 + (subRob number X 0x200), i.e. BAR0 + 0x" << std::hex << 0x4000 + (subRob * 0x200) << " = 0x" << m_testCandidate->m_bar0Addr + 0x4000 + (subRob * 0x200) << ", actual value = " << m_testCandidate->m_Bar0SubRob[subRob] << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: SubRob " << subRob << " Channel Struct correctly mapped to BAR0." << std::endl;
			std::cout << "BIST Register Test: Value should be BAR + 0x4000 + (subRob number X 0x200), i.e. BAR0 + 0x" << std::hex << 0x4000 + (subRob * 0x200) << " = 0x" << m_testCandidate->m_bar0Addr + 0x4000 + (subRob * 0x200) << ", actual value = " << m_testCandidate->m_Bar0SubRob[subRob] << std::dec << std::endl;

		}
	}

	std::cout << std::endl;

	//Test Global Reset
	TEST_BIT(m_testCandidate->m_Bar0Common->GlobalControlRegister.GlobalReset, "Global Control Register Reset Bit");
	std::cout << std::endl;

	//Test Control Register Values
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		if(m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] != bufferManagerControlRegisterReferenceValue){
			std::cout << "BIST Register Test FAIL: ROL " << rol << " Buffer Manager Control Register Value not at expected value (0x" << std::hex << bufferManagerControlRegisterReferenceValue << "), actual value = 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: ROL " << rol << " Buffer Manager Control Register Value at expected value (0x" << std::hex << bufferManagerControlRegisterReferenceValue << "), actual value = 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
		}
	}

	std::cout << std::endl;
	//Test Status Register Values
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		testReference = m_testCandidate->m_Bar0Channel[rol]->BufferManagerStatusRegister.Register[0];
		if(testReference != comparisonValue){
			std::cout << "BIST Register Test FAIL: ROL " << rol << " Buffer Manager Status Register Value not at expected value (0x" << std::hex << comparisonValue << "), actual value = 0x" << testReference << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: ROL " << rol << " Buffer Manager Status Register Value at expected value (0x" << std::hex << comparisonValue << "), actual value = 0x" << testReference << std::dec << std::endl;
		}
		comparisonValue+=bufferManagerStatusRegisterReferenceValue;
	}
	comparisonValue = 0;
	testReference = 0;

	std::cout << std::endl;
	std::cout << "BIST Register Test: Reset Buffer Managers for each ROL using Control Registers." << std::endl;
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){

		unsigned int controlReg = m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0];
		unsigned int controlRegCopy = m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] | (1 << 8);

		std::cout << "BIST Register Test: Initial Value for ROL " << rol << " Control Register = 0x" << std::hex << controlReg << std::dec << std::endl;
		std::cout << "BIST Register Test: Value to be written to ROL " << rol << " Control Register with Reset Bit flipped = 0x" << std::hex << controlRegCopy << std::dec << std::endl;
		std::cout << "BIST Register Test: Attempt to write flipped bit version to control register and then return to original value" << std::endl;

		m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] = controlRegCopy;
		if(m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] != controlRegCopy){
			std::cout << "BIST Register Test FAIL: Incorrect read-back on Buffer Manager Control Register for ROL " << rol << ". Expected 0x" << std::hex << controlRegCopy << " but read 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: Correct read-back on Buffer Manager Control Register for ROL " << rol << ". Expected 0x" << std::hex << controlRegCopy << " and read 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
		}

		m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] = controlReg;
		if(m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] != controlReg){
			std::cout << "BIST Register Test FAIL: Incorrect read-back on Buffer Manager Control Register for ROL " << rol << ". Expected 0x" << std::hex << controlReg << " but read 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: Correct read-back on Buffer Manager Control Register for ROL " << rol << ". Expected 0x" << std::hex << controlReg << " and read 0x" << m_testCandidate->m_Bar0Channel[rol]->BufferManagerControlRegister.Register[0] << std::dec << std::endl;
		}

	}

	comparisonValue = bufferManagerStatusRegisterReferenceOffset;

	std::cout << std::endl;
	//Re-Test Status Register Values
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		testReference = m_testCandidate->m_Bar0Channel[rol]->BufferManagerStatusRegister.Register[0];
		if(testReference != comparisonValue){
			std::cout << "BIST Register Test FAIL: (Re-test after reset) ROL " << rol << " Buffer Manager Status Register Value not at expected value 0x(" << std::hex << comparisonValue << "), actual value = 0x" << testReference << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: (Re-test after reset) ROL " << rol << " Buffer Manager Status Register Value at expected value 0x(" << std::hex << comparisonValue << "), actual value = 0x" << testReference << std::dec << std::endl;
		}
		comparisonValue+=bufferManagerStatusRegisterReferenceValue;
	}
	comparisonValue = 0;
	testReference = 0;

	std::cout << std::endl;
	//Test Channel IDs
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		if(m_testCandidate->m_Bar0Channel[rol]->BufferManagerStatusRegister.Component.ChannelNumber != rol){
			std::cout << "BIST Register Test FAIL: ROL " << rol << " not identified correctly in on-board registers, actual value = " << std::hex << m_testCandidate->m_Bar0Channel[rol]->BufferManagerStatusRegister.Component.ChannelNumber << std::dec << std::endl;
			result = false;
		}
		else{
			std::cout << "BIST Register Test: ROL " << rol << " identified correctly in on-board registers, actual value = " << std::hex << m_testCandidate->m_Bar0Channel[rol]->BufferManagerStatusRegister.Component.ChannelNumber << std::dec << std::endl;
		}
	}

	std::cout << std::endl;
	//Test Channel Specific Resets
	for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
		std::cout << "BIST Register Test: Testing Control Register Resets for Channel " << rol << std::endl;

		//Reset Buffer Manager Through Control Register
		TEST_BIT(m_testCandidate->m_Bar0Channel[rol]->ChannelControlRegister.Component.BufferManagerReset,"Channel Control Buffer Manager Reset Bit");

		std::cout << std::endl;

	}


	//Reset Interrupter

	for(unsigned int subRob = 0; subRob < m_testCandidate->m_numSubRobs; ++subRob){
		std::cout << "BIST Register Test: Testing Control Register for SubRob " << subRob << std::endl;
		std::cout << std::endl;

		std::cout << "SubRob " << subRob << " Control Register Address = " << std::hex << &(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl) << std::dec << std::endl;

		std::cout << "SubRob " << subRob << " Control Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[0] << std::dec << std::endl;
		std::cout << "SubRob " << subRob << " Control Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[1] << std::dec << std::endl;
		if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[0] != subRobControlRegisterLowerReferenceValue){
			std::cout << "BIST Register Test FAIL: SubRob " << subRob << " Control Register Lower Bits at expected value 0x" << std::hex << subRobControlRegisterLowerReferenceValue << ", actual value = 0x" <<  m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[0] << std::dec << std::endl;
			result = false;
		}
		if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[1] != subRobControlRegisterUpperReferenceValue){
			std::cout << "BIST Register Test FAIL: SubRob " << subRob << " Control Register Upper Bits at expected value 0x" << std::hex << subRobControlRegisterUpperReferenceValue << ", actual value = 0x" <<  m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Register[1] << std::dec << std::endl;
			result = false;
		}

		std::cout << std::endl;
		//Reset Channels Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.ChannelReset,"SubRob Control Channel Reset Bit");

		std::cout << std::endl;
		//Reset Common UPF Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFReset,"Common UPF Reset Bit");

		std::cout << std::endl;
		//Enable Common UPF Multiplexer Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable,"Common UPF Multiplexer Enable Bit");

		std::cout << std::endl;
		//Reset Common UPF Multiplexer Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxReset,"Common UPF Multiplexer Reset Bit");

		std::cout << std::endl;
		//Enable Common UPF Duplicator Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable,"SubRob Control Register Common UPF Duplicator Enable Bit");

		std::cout << std::endl;
		//Reset Common UPF Duplicator Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorReset,"SubRob Control Register Common UPF Duplicator Reset Bit");

		std::cout << std::endl;

		//std::cout << std::endl;

		//Reset DMA Done FIFO Duplicator Through SubRob Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.DMADoneFIFODuplicatorReset,"SubRob Control Register DMADone FIFO Duplicator Reset Bit");

		std::cout << std::endl;
		//Reset Primary DMA Through SubRobControl Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.PrimaryDMAReset,"SubRob Control Register Primary DMA Reset Bit");

		std::cout << std::endl;
		//Enable Primary DMA Through Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable,"Primary DMA Control Register Enable Bit");

		std::cout << std::endl;
		//Reset Primary DMA Through Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Reset,"Primary DMA Control Register Reset Bit");

		std::cout << std::endl;
		//Enable Primary DMA Termination Word Write Through Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.TerminationWordDMAEnable,"Primary DMA Control Register Termination Word Write Enable Bit");

		std::cout << std::endl;
		//Write Primary DMA Destination Offset Through Control Register
		TEST_BIT(m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DestinationOffset,"Primary DMA Control Register Destination Offset");

	}
	std::cout << std::endl;

	if(m_verboseMode){
		std::cout << "Verbose Mode: Print Remaining Status Registers." << std::endl;

		std::cout << "Global Control Register FDInternalBusWidthBitBytes = 0x" << std::hex << m_testCandidate->m_Bar0Common->GlobalControlRegister.FDInternalBusWidthBitBytes << std::endl;
		std::cout << "Global Control Register FDDestinationLengthElements = 0x" << m_testCandidate->m_Bar0Common->GlobalControlRegister.FDDestinationLengthElements << std::endl;
		std::cout << "Global Control Register FDSourceLengthElements = 0x" << m_testCandidate->m_Bar0Common->GlobalControlRegister.FDSourceLengthElements << std::endl;
		std::cout << "Global Control Register FDSourceWidthBits = 0x" << m_testCandidate->m_Bar0Common->GlobalControlRegister.FDSourceWidthBits << std::dec << std::endl;
		std::cout << "Global Control Register FDDestinationLengthElementsLarge = 0x" << m_testCandidate->m_Bar0Common->GlobalControlRegister.FDDestinationLengthElementsLarge << std::endl;

		std::cout << "Global Control Register - Calculated UPF Duplicator Size = " << (1 << (m_testCandidate->m_Bar0Common->GlobalControlRegister.FDSourceWidthBits + m_testCandidate->m_Bar0Common->GlobalControlRegister.FDDestinationLengthElementsLarge)) << std::endl;

		for(unsigned int rol = 0; rol < m_testCandidate->m_numRols; ++rol){
			std::cout << "Channel Status Register for ROL " << rol << " = 0x" << std::hex << *((unsigned int*)&m_testCandidate->m_Bar0Channel[rol]->ChannelStatusRegister) << std::dec << std::endl;
		}

		std::cout << std::endl;
		for(unsigned int subRob = 0; subRob < m_testCandidate->m_numSubRobs; ++subRob){
			std::cout << "SubRob Status Register[0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Register[0] << std::dec << std::endl;
			std::cout << "SubRob Status Register[1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Register[1] << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryReadQueueFIFOFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFill << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryReadQueueFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFull << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryReadQueueFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOEmpty << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputFirstFIFOFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFill << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputFirstFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFull << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputFirstFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOEmpty << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryTestInputFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryTestInputFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOEmpty << std::dec << std::endl;
			std::cout << "SubRob Status Register CommonUPFFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFFill << std::dec << std::endl;
			std::cout << "SubRob Status Register CommonUPFFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFFull << std::dec << std::endl;
			std::cout << "SubRob Status Register CommonUPFEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFEmpty << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryReadQueueFIFODataValid for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFODataValid << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputSecondFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryPLLLocked for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryPLLLocked << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryPhysicalInitDone for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryPhysicalInitDone << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputSecondFIFOFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFill << std::dec << std::endl;
			std::cout << "SubRob Status Register MemoryOutputSecondFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFull << std::dec << std::endl;
			std::cout << "SubRob Status Register SubrobNumber for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.SubrobNumber << std::dec << std::endl;
			std::cout << std::endl;
			std::cout << "Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
			std::cout << "Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
			std::cout << "Primary DMA Status Register Status for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.Status << std::dec << std::endl;
			std::cout << "Primary DMA Status Register QueueFIFOFillError for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFillError << std::dec << std::endl;
			std::cout << "Primary DMA Status Register DoneFIFOFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFillError << std::dec << std::endl;
			std::cout << "Primary DMA Status Register DoneFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.DoneFIFOEmpty << std::dec << std::endl;
			std::cout << "Primary DMA Status Register DoneFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.DoneFIFOFull << std::dec << std::endl;
			std::cout << "Primary DMA Status Register QueueFIFODataAvailable for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFODataAvailable << std::dec << std::endl;
			std::cout << "Primary DMA Status Register QueueFIFOFill for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFill << std::dec << std::endl;
			std::cout << "Primary DMA Status Register QueueFIFOFull for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFull << std::dec << std::endl;
			std::cout << "Primary DMA Status Register StateIsIdle for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsIdle << std::dec << std::endl;
			std::cout << "Primary DMA Status Register StateIsWaitingForStart for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsWaitingForStart << std::dec << std::endl;
			std::cout << "Primary DMA Status Register StateIsWaitingForEnd for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsWaitingForEnd << std::dec << std::endl;
			std::cout << std::endl;

			std::cout << "Primary DMA Done FIFO pciAddressLower for SubRob " << subRob << " = 0x" << std::hex <<m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFO.pciAddressLower << std::dec << std::endl;
			std::cout << "Primary DMA Done FIFO pciAddressUpper for SubRob " << subRob << " = 0x" << std::hex <<m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFO.pciAddressUpper << std::dec << std::endl;
			std::cout << "Primary DMA Done FIFO transferSize for SubRob " << subRob << " = 0x" << std::hex <<m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFO.transferSize << std::dec << std::endl;
			std::cout << "Primary DMA Done FIFO localAddress for SubRob " << subRob << " = 0x" << std::hex <<m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFO.localAddress << std::dec << std::endl;

		}
		std::cout << std::endl;
		std::cout << "Interrupt Status Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[0] << std::dec << std::endl;
		std::cout << "Interrupt Status Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[1] << std::dec << std::endl;
		std::cout << "Interrupt Status Register MSIAllowed = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.MSIAllowed << std::dec << std::endl;
		std::cout << "Interrupt Status Register NumberOfMSIsRequested = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsRequested << std::dec << std::endl;
		std::cout << "Interrupt Status Register NumberOfMSIsAllocated = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsAllocated << std::dec << std::endl;
		std::cout << "Interrupt Status Register SixtyFourBitCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
		std::cout << "Interrupt Status Register PerVectorMaskingCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
		std::cout << "Interrupt Status Register Reserved = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.Reserved << std::dec << std::endl;
		std::cout << "Interrupt Status Register InIdleState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
		std::cout << "Interrupt Status Register InWaitingForACKState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
		std::cout << "Interrupt Status Register InWaitingForEnableResetState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InWaitingForEnableResetState << std::dec << std::endl;
		std::cout << "Interrupt Status Register filler = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler << std::dec << std::endl;
		std::cout << "Interrupt Status Register commonInterrupterStatus = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.commonInterrupterStatus << std::dec << std::endl;
		std::cout << "Interrupt Status Register filler2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler2 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus0 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus0 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus1 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus1 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus2 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus3 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus3 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus4 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus4 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus5 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus5 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus6 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus6 << std::dec << std::endl;
		std::cout << "Interrupt Status Register IndividualInterrupterStatus7 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus7 << std::dec << std::endl;
		std::cout << "Interrupt Status Register InterruptCount = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InterruptCount << std::dec << std::endl;
		std::cout << std::endl;

		std::cout << "Interrupt Status Register Location = " << std::hex << &m_testCandidate->m_Bar0Common->InterruptStatusRegister << std::dec << std::endl;

		std::cout << std::endl;
		std::cout << "Interrupt Mask Register = 0x" << std::hex << *((unsigned int*)&m_testCandidate->m_Bar0Common->InterruptMaskRegister) << std::dec << std::endl;
		std::cout << "Interrupt Mask Register PrimaryDMADoneNotEmpty = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Component.PrimaryDMADoneNotEmpty << std::dec << std::endl;
		std::cout << "Interrupt Mask Register PrimaryDMADoneDuplicateNotEmpty = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Component.PrimaryDMADoneDuplicateNotEmpty << std::dec << std::endl;
		std::cout << "Interrupt Mask Register CommonUsedPageFIFONotEmpty = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Component.CommonUsedPageFIFONotEmpty << std::dec << std::endl;
		std::cout << "Interrupt Mask Register CommonUsedPageFIFODuplicateNotEmpty = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Component.CommonUsedPageFIFODuplicateNotEmpty << std::dec << std::endl;
		std::cout << std::endl;

		std::cout << "Interrupt Mask Register Location = " << std::hex << &m_testCandidate->m_Bar0Common->InterruptMaskRegister << std::dec << std::endl;
		std::cout << std::endl;

		std::cout << "Interrupt Control Register Location = 0x" << std::hex << &m_testCandidate->m_Bar0Common->InterruptControlRegister << std::dec << std::endl;
		std::cout << std::endl;
		std::cout << "Interrupt Test Register acknowledge = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger << std::dec << std::endl;
		std::cout << "Interrupt Test Register Bottom = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptTestRegister.Bottom << std::dec << std::endl;
		std::cout << "Interrupt Test Register Top = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptTestRegister.Top << std::dec << std::endl;
		std::cout << std::endl;

		std::cout << "Interrupt Test Register Location = " << std::hex << &m_testCandidate->m_Bar0Common->InterruptTestRegister << std::dec << std::endl;
		std::cout << std::endl;
	}

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Register Test                        *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return result;

}

void printbar(unsigned long counter, unsigned long total, unsigned int barWidth = 50){
	static long pos=0;
	char cursor[4]={'/','-','\\','|'};
	pos = (pos+1) % 4;
	float ratio = counter/(float)total;
	unsigned long progress = ratio * barWidth;
	std::cout << std::setw(3) <<  cursor[pos] << " " << (long)(ratio*100)  << "% [";
	for (unsigned long barPosition=0; barPosition<progress; barPosition++) std::cout << "=";
	for (unsigned long barPosition=progress; barPosition<barWidth; barPosition++) std::cout << " ";
	std::cout << "]\r " << std::flush;

}

void loadbar(unsigned long counter, unsigned long total, unsigned long numRefreshes = 1000000)
{
	static long cursorCounter = 0;
	static long lastcount = 0;
	static long lasttotal = 0;
	if ( (counter != total) && (counter % (total/numRefreshes) != 0) ){
		cursorCounter = (cursorCounter + 1) % 100;
		if(cursorCounter == 1){
			printbar(lastcount,lasttotal);
		}
		return;
	}
	lastcount = counter;
	lasttotal = total;
	printbar(lastcount,lasttotal);
}

/*
 *
 * Verify that RobinNP can dma data to desired locations in host memory, including multipage fragments
 *
 */
bool RobinNPBIST::primaryDMATest(unsigned int subRob, bool captureTest){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Primary DMA Test for SubRob " << subRob << "          *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Primary DMA Test: Test features of Primary DMA engine without pipelining." << std::endl;
	std::cout << "BIST Primary DMA Test: Use Main DMA landing memory pools for landing areas." << std::endl;
	std::cout << std::endl;

	std::ios::fmtflags flagSave(std::cout.flags());

	//Initial Configuration

	volatile unsigned int *dmaPtr = (volatile unsigned int*)m_testCandidate->m_virtualArrayPointer[0];
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.IgnoreDMADoneFull = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable = 1;

	std::cout << "BIST Primary DMA Test: Mapped location of Primary DMA Status Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister << std::dec << std::endl;
	std::cout << "BIST Primary DMA Test: Mapped location of Primary DMA Control Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister << std::dec << std::endl;
	std::cout << std::endl;
	std::cout << "DMA Queue Fifo Fill " << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFill << std::endl;
	std::cout << "DMA Queue Fifo Data Available " << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFODataAvailable << std::endl;
	std::cout << "DMA Queue Fifo Full " << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFull << std::endl;

	std::cout << "BIST Primary DMA Test: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Primary DMA Test: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;

	int rolId = subRob * m_testCandidate->getNumChannelsPerSubRob();
	std::cout << "BIST Primary DMA Test: Using rol " << rolId << " to provide a test page from the data generator" << std::endl;
	int* results = loadTestPageToMemory(rolId,100);


	unsigned int interrupt = s_DMAInterruptCodes[subRob];

	configureInterrupter(interrupt);

	std::cout << "BIST Primary DMA Test: Create Experimental DMA (" << results[3] << " words) with zero offset" << std::endl;

	//Initialise + Print DMA Landing Area
	for(int location = 0; location < results[3]; ++location){
		*(dmaPtr + location) = 0x99999999;
	}
	std::cout << std::hex << "BIST Primary DMA Test: DMA Landing Area Post-Initialisation" <<  std::endl;
	std::cout << std::endl;

	for(int location = 0; location < results[3]; ++location){
		std::cout << std::setw(8) << *(dmaPtr + location) << " ";
		if((location + 1) % 16 == 0){
			std::cout << std::endl;
		}
	}
	std::cout << std::dec << std::endl;
	std::cout << std::endl;

	//Request DMA
	if(captureTest){
		std::cout << "BIST Primary DMA Test: Using Capture DMA" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->CaptureDMAQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
		m_testCandidate->m_Bar0SubRob[subRob]->CaptureDMAQueueFIFO.Register[1] = (uint16_t)((results[3]) * 4) | s_dmaNoOffsetDmaDone | ((uint64_t)(results[2] * 4)) << 32;

	}
	else{
		std::cout << "BIST Primary DMA Test: Using Memory Read Queue DMA" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)((results[3]) * 4) | s_dmaNoOffsetDmaDone | ((uint64_t)(results[2] * 4)) << 32;
	}


	std::cout << "BIST Primary DMA Test: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Primary DMA Test: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
	std::cout << std::endl;

	int readIndex = 0;
	if(!getDMAResult(subRob,readIndex,true)){
		delete[] results;
		return false;
	}

	std::cout << "BIST Primary DMA Test: Final DMA Landing Area Contents" << std::hex << std::endl;
	std::cout << std::endl;

	//Final Print of DMA landing area
	std::cout << std::hex << std::endl;
	for(int location = 0; location < results[3]; ++location){
		std::cout <<  std::setfill('0') << std::setw(8) << *(dmaPtr + location) << " ";
		if((location + 1) % 16 == 0){
			std::cout << std::endl;
		}
	}
	std::cout << std::dec << std::endl;
	std::cout << std::endl;

	delete[] results;

	if(*dmaPtr != s_hdrMarkerRod){
		std::cout << "BIST Primary DMA Test: FAIL - First word of DMA (" << *dmaPtr << ") does not match expected control word (" << std::hex << s_hdrMarkerRod << ")" << std::dec << std::endl;
	}
	else{
		std::cout << "BIST Primary DMA Test: PASS - DMA Completed Successfully" << std::endl;
	}
	//Exit Test
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Primary DMA Test for SubRob " << subRob << "        *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	std::cout.flags(flagSave);

	return true;
}

bool RobinNPBIST::primaryDMASpeedTest(unsigned int subRob, unsigned int numWords){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Primary DMA Speed Test for SubRob " << subRob << "    *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Primary DMA Speed Test: Test features of Primary DMA engine without pipelining." << std::endl;
	std::cout << "BIST Primary DMA Speed Test: Use Main DMA landing memory pools for landing areas." << std::endl;
	std::cout << std::endl;

	//Initial Configuration
	unsigned int numTests = 1000000;
	unsigned int dmaStackSize = 10;
	volatile unsigned int **dmaPtr = new volatile unsigned int*[dmaStackSize];

	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.IgnoreDMADoneFull = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable = 1;

	std::cout << "BIST Primary DMA Speed Test: Mapped location of Primary DMA Status Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister << std::dec << std::endl;
	std::cout << "BIST Primary DMA Speed Test: Mapped location of Primary DMA Control Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister << std::dec << std::endl;
	std::cout << std::endl;

	std::cout << "BIST Primary DMA Speed Test: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Primary DMA Speed Test: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
	/*
	unsigned int interrupt = s_DMAInterruptCodes[subRob];

	configureInterrupter(interrupt);
	 */

	int rolId = subRob * m_testCandidate->getNumChannelsPerSubRob();
	std::cout << "BIST Primary DMA Test: Using rol " << rolId << " to provide a test page from the data generator" << std::endl;
	int* results = loadTestPageToMemory(rolId,numWords);

	std::cout << "BIST Primary DMA Speed Test: Create " << numTests << " Experimental DMA (" << results[3] << " words) requests and measure timing" << std::endl;
	for(unsigned int entry = 0; entry < dmaStackSize; ++entry){
		dmaPtr[entry] = (volatile unsigned int*)m_testCandidate->m_virtualArrayPointer[entry];
	}

	Timer globalCount(100);
	globalCount.start();
	bool globalTimeout = false;
	unsigned int successCount = 0;

	double totalTime = 0;

	unsigned int completed[10];
	unsigned int completeCount = 0;

	//Initialise DMA Control Words
	for(unsigned int location = 0; location < dmaStackSize; ++location){
		*(dmaPtr[location] + results[3] - 1) = 0x99999999;
	}

	//Fill DMA request stack
	for(unsigned int entry = 0; entry < dmaStackSize; ++entry){
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[entry]; //Physical Address of Response Area
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)((results[3]) * 4) | s_dmaNoOffsetDmaDone | ((uint64_t)(results[2] * 4)) << 32;
	}

	while(successCount < numTests){

		globalTimeout = globalCount.isTimeout();
		if(globalTimeout){
			totalTime = globalCount.elapsedTime();
			break;
		}
		//Wait for DMA Completion
		for(unsigned int entry = 0; entry < dmaStackSize; ++entry){

			while(*(dmaPtr[entry] + results[3] - 1) == 0x99999999);
			/*
				int readIndex = 0;
			if(!getDMAResult(subRob,readIndex)){
				delete[] results;
				return false;
			}
			 */

			if(*dmaPtr[entry] != s_hdrMarkerRod){
				std::cout << "BIST Primary DMA Test: FAIL - " << successCount << " First word of DMA (" << *dmaPtr << ") does not match expected control word (" << std::hex << s_hdrMarkerRod << ")" << std::dec << std::endl;
				delete[] dmaPtr;
				delete[] results;
				return false;
			}

			*(dmaPtr[entry] + results[3] - 1) = 0x99999999;

			completed[completeCount] = entry;
			completeCount++;
			if(completeCount == 10){
				for(unsigned int index = 0; index < completeCount; ++index){
					//Request new DMA for freed up landing area
					m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[completed[index]]; //Physical Address of Response Area
					m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)((results[3]) * 4) | s_dmaNoOffsetDmaDone | ((uint64_t)(results[2] * 4)) << 32;
				}
				completeCount = 0;
			}
			successCount++;
		}
		totalTime = globalCount.elapsedTime();
	}

	if(globalTimeout){
		std::cout << "BIST Primary DMA Speed Test FAIL: Timeout when waiting for " << numTests << " DMA's to complete" << std::endl;
		std::cout << "BIST Primary DMA Speed Test FAIL: Time taken = " << totalTime << " seconds" << std::endl;
		std::cout << "BIST Primary DMA Speed Test FAIL: Total successful DMA's = " << successCount << "." << std::endl;
		std::cout << "BIST Primary DMA Speed Test FAIL: DMA rate = " << successCount/totalTime << " DMA per second." << std::endl;
		std::cout << "BIST Primary DMA Speed Test FAIL: Requested DMA size =  " << results[3] << " words. " << std::endl;
		std::cout << "BIST Primary DMA Speed Test FAIL: Estimated bandwidth = " << ((double)successCount*4*results[3])/(totalTime*1024*1024) << " MB/s." << std::endl;
	}
	else{
		std::cout << "BIST Primary DMA Speed Test: Successfully completed " << numTests << " DMA's" << std::endl;
		std::cout << "BIST Primary DMA Speed Test: Time taken = " << totalTime << " seconds" << std::endl;
		std::cout << "BIST Primary DMA Speed Test: DMA rate = " << numTests/totalTime << " DMA per second." << std::endl;
		std::cout << "BIST Primary DMA Speed Test: Requested DMA size =  " << results[3] << " words. Estimated bandwidth = " << ((double)numTests*4*results[3])/(totalTime*1024*1024) << " MB/s." << std::endl;
	}

	std::cout << "BIST Primary DMA Speed Test: Cleanup - Waiting for DMA Queue to empty" << std::endl;
	while(m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFill != 0){
	}

	delete[] dmaPtr;
	delete[] results;
	results = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 0;

	//Exit Test
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Primary DMA Speed Test for SubRob " << subRob << "  *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return true;
}

bool RobinNPBIST::dualDMASpeedTest(unsigned int numWords){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Dual DMA Speed Test                    *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Dual DMA Speed Test: Test Speed of Both Primary and Pipeline DMA's combined." << std::endl;
	std::cout << "BIST Dual DMA Speed Test: Use Main DMA landing memory pools for landing areas." << std::endl;
	std::cout << std::endl;

	//Initial Configuration
	unsigned int numTests = 1000000;
	unsigned int dmaStackSize = 10;
	volatile unsigned int **dmaPtr = new volatile unsigned int*[dmaStackSize];

	for(unsigned int subRob = 0; subRob < 2; ++subRob){
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.IgnoreDMADoneFull = 1;
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 0;
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 0;
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable = 1;

		std::cout << "BIST Dual DMA Speed Test: Mapped location of Primary DMA Status Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister << std::dec << std::endl;
		std::cout << "BIST Dual DMA Speed Test: Mapped location of Primary DMA Control Register for SubRob " << subRob << " = " << std::hex << &m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister << std::dec << std::endl;
		std::cout << std::endl;

		std::cout << "BIST Dual DMA Speed Test: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
		std::cout << "BIST Dual DMA Speed Test: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
		std::cout << std::endl;
	}

	std::cout << "BIST Primary DMA Test: Using rols 0 and " << m_testCandidate->getNumChannelsPerSubRob() << " to provide a test page from the data generator for each subRob" << std::endl;
	int* results = loadTestPageToMemory(0,numWords);
	int* results1 = loadTestPageToMemory(m_testCandidate->getNumChannelsPerSubRob(),numWords);
	std::cout << "BIST Dual DMA Speed Test: Create " << numTests << " Experimental DMA (" << results[3] << " words) requests and measure timing" << std::endl;
	for(unsigned int entry = 0; entry < dmaStackSize; ++entry){
		dmaPtr[entry] = (volatile unsigned int*)m_testCandidate->m_virtualArrayPointer[entry];
	}

	//Initialise DMA Control Words
	for(unsigned int location = 0; location < dmaStackSize; ++location){
		*(dmaPtr[location] + results[3] - 1) = 0x99999999;
	}

	Timer globalCount(100);
	globalCount.start();
	bool globalTimeout = false;
	unsigned int successCount = 0;

	double totalTime = 0;

	unsigned int completed[10];
	unsigned int completeCount = 0;

	//Fill DMA request stack
	for(unsigned int entry = 0; entry < dmaStackSize; ++entry){
		if(entry % 2 == 0){
			m_testCandidate->m_Bar0SubRob[0]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[entry]; //Physical Address of Response Area
			m_testCandidate->m_Bar0SubRob[0]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*results[3]) | s_dmaNoOffsetDmaDone;
		}
		else{
			m_testCandidate->m_Bar0SubRob[1]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[entry]; //Physical Address of Response Area
			m_testCandidate->m_Bar0SubRob[1]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*results[3]) | s_dmaNoOffsetDmaDone;
		}
	}

	while(successCount < numTests){

		globalTimeout = globalCount.isTimeout();
		if(globalTimeout){
			totalTime = globalCount.elapsedTime();
			break;
		}
		//Wait for DMA Completion
		for(unsigned int entry = 0; entry < dmaStackSize; ++entry){

			while(*(dmaPtr[entry] + results[3] - 1) == 0x99999999){
			}
			if(*dmaPtr[entry] != s_hdrMarkerRod){
				std::cout << "BIST Primary DMA Test: FAIL - " << successCount << " First word of DMA (" << *dmaPtr << ") does not match expected control word (" << std::hex << s_hdrMarkerRod << ")" << std::dec << std::endl;
				delete[] dmaPtr;
				delete[] results;
				delete[] results1;
				return false;
			}
			*(dmaPtr[entry] + results[3] - 1) = 0x99999999;

			completed[completeCount] = entry;
			completeCount++;
			if(completeCount == 10){
				for(unsigned int index = 0; index < completeCount; ++index){
					//Request new DMA for freed up landing area
					if(completed[index] %2 == 0){
						m_testCandidate->m_Bar0SubRob[0]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[completed[index]]; //Physical Address of Response Area
						m_testCandidate->m_Bar0SubRob[0]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*results[3]) | s_dmaNoOffsetDmaDone;
					}
					else{
						m_testCandidate->m_Bar0SubRob[1]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[completed[index]]; //Physical Address of Response Area
						m_testCandidate->m_Bar0SubRob[1]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*results[3]) | s_dmaNoOffsetDmaDone;
					}
				}
				completeCount = 0;
			}
			successCount++;
		}
		totalTime = globalCount.elapsedTime();
	}

	if(globalTimeout){
		std::cout << "BIST Dual DMA Speed Test FAIL: Timeout when waiting for " << numTests << " DMA's to complete" << std::endl;
		std::cout << "BIST Dual DMA Speed Test FAIL: Time taken = " << totalTime << " seconds" << std::endl;
		std::cout << "BIST Dual DMA Speed Test FAIL: Total successful DMA's = " << successCount << "." << std::endl;
		std::cout << "BIST Dual DMA Speed Test FAIL: DMA rate = " << successCount/totalTime << " DMA per second." << std::endl;
		std::cout << "BIST Dual DMA Speed Test FAIL: Requested DMA size =  " << results[3] << " words. " << std::endl;
		std::cout << "BIST Dual DMA Speed Test FAIL: Estimated bandwidth = " << ((double)successCount*4*results[3])/(totalTime*1024*1024) << " MB/s." << std::endl;
	}
	else{
		std::cout << "BIST Dual DMA Speed Test: Successfully completed " << numTests << " DMA's" << std::endl;
		std::cout << "BIST Dual DMA Speed Test: Time taken = " << totalTime << " seconds" << std::endl;
		std::cout << "BIST Dual DMA Speed Test: DMA rate = " << numTests/totalTime << " DMA per second." << std::endl;
		std::cout << "BIST Dual DMA Speed Test: Requested DMA size =  " << results[3] << " words. Estimated bandwidth = " << (((double)numTests*4*results[3])/(totalTime*1024*1024)) << " MB/s." << std::endl;
	}

	delete[] dmaPtr;
	delete[] results;
	delete[] results1;

	//Exit Test
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Dual DMA Speed Test for              *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return true;
}

bool RobinNPBIST::memoryReadWriteTest(unsigned int subRob, double memSize){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Memory Read/Write Test for SubRob " << subRob << "    *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;

	std::cout << "BIST Memory Read/Write Test up to " << memSize << " Gibibytes" << std::endl;

	tstamp startTime, writeTime, readTime;

	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryOutputFIFOCommsHold = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 1;

	if(memSize <= 0){
		std::cout << "Illegal size requested, must be greater than zero" << std::endl;
		return false;
	}


	if(memSize > 4){
		std::cout << "Requested write size of exceeds memory capacity" << std::endl;
		return false;
	}

	unsigned int numWrites = (unsigned int)ceil(memSize * 1024*1024*1024/32); //divide through by 4 as each input fifo element is 4 bytes

	if(m_verboseMode){
		std::cout << "BIST Memory Read/Write Test: Maximum number of 256bit rows to be written to fill " << memSize << "GiB of memory  = " << numWrites << std::endl;
	}else{
		std::cout << "Writing Test Data" << std::endl;
	}

	m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputAddress.lower = 0;

	unsigned long finalWord = 0;
	ts_clock(&startTime);

	Timer writeTimer(20);
	writeTimer.start();

	for(unsigned int counter = 0; counter < numWrites; ++counter){
		for(unsigned int intcounter = 1; intcounter < 9; ++intcounter){
			if(counter == numWrites - 1 && intcounter == 8){
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputEOF.word = counter*8 + intcounter;
				finalWord = counter*8 + intcounter; // write final word as e0f
			}
			else{
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputData.word = counter*8 + intcounter;
			}

		}
		loadbar(counter,numWrites,1000);
		while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull == 1){
			if(writeTimer.isTimeout()){
				std::cout << "Test input fifo persistently full" << std::endl;
				return false;
			}
		}
	}

	ts_clock(&writeTime);
	loadbar(numWrites,numWrites,1000);
	std::cout << std::endl;
	if(m_verboseMode){
		std::cout << "BIST Memory Read/Write Test: Final Read Word Expected: " << std::hex << finalWord << std::dec << std::endl;
	}
	unsigned long readLength = ((unsigned long)numWrites)*32;
	unsigned long readLimit = 16000;
	bool readPass = true;
	unsigned long lastRead = 0;
	unsigned long readCounter = 1;
	if(m_verboseMode){
		std::cout << "BIST Memory Read/Write Test: Total required read length = " << readLength << " bytes" << std::endl;
	}
	else{
		std::cout << "Reading back and testing data" << std::endl;
	}
	for(unsigned long readChunk = 0; readChunk < readLength; readChunk+=readLimit){

		unsigned long toRead = 0;
		if(readChunk + readLimit < readLength){
			toRead = readLimit;
		}
		else{
			toRead = readLength - readChunk;
		}

		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressLower = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
#if __x86_64__
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressUpper = 0 | s_dmaOffsetTWord;
#else
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressUpper = 0;
#endif
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.transferSize = toRead;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.localAddress = readChunk;

		usleep(1);

		while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower != readCounter){
				readPass = false;
			}
			readCounter++;
			if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper != readCounter){
				readPass = false;
			}
			lastRead = m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper;
			readCounter++;
			m_testCandidate->m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;

		}
		if(!readPass){
			std::cout << "BIST Memory Read/Write Test: Read Check Failed in Read Chunk from address: " << readChunk << " of length " << toRead << " bytes" << std::endl;
		}
		loadbar(readChunk,readLength);
	}
	loadbar(readLength,readLength);
	ts_clock(&readTime);
	float writeDuration = ts_duration(startTime, writeTime);
	float readDuration = ts_duration(writeTime, readTime);
	std::cout << std::endl;
	if(readPass){
		std::cout << "Read Check Completed Successfully" << std::endl;
	}
	else{
		std::cout << "Read Check Failed" << std::endl;
	}
	if(m_verboseMode){
		std::cout << "Time for write phase: " << writeDuration << " seconds, time for read phase: " << readDuration << " seconds, total " << writeDuration + readDuration << " seconds" << std::endl;
		std::cout << "Last Read = " << std::hex << lastRead << std::dec << std::endl;
	}
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Memory Read/Write Test for SubRob " << subRob << "  *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;

	return readPass;
}


/*
 *
 * Verify that RobinNP can pipeline dma data to desired locations in host memory, including multipage fragments
 *
 */
bool RobinNPBIST::memoryExpertTest(unsigned int subRob,unsigned int numReads, unsigned int readLength, unsigned int numWrites, unsigned int holdWrites, /*bool burstMode,*/ bool doReset, unsigned int numWordsPerWrite, unsigned int addrIncrement, bool doMemoryInit, unsigned int writeStartAddress){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Memory Expert Test for SubRob " << subRob << "        *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Memory Expert Test: Test placement of request in Memory Read Queue FIFO and check processing through Memory Output FIFO and DMA." << std::endl;
	std::cout << "BIST Memory Expert Test: Use Main DMA landing memory pools for landing areas." << std::endl;
	std::cout << std::endl;
	std::cout << "Test Options:" << std::endl;
	std::cout << "Num Writes: " << numWrites << std::endl;
	std::cout << "Hold Writes: " <<   holdWrites << std::endl;
	std::cout << "Num Reads: " << numReads << std::endl;
	//std::cout << "Burst Mode: " << burstMode << std::endl;
	std::cout << "Words Per Frame: " << numWordsPerWrite << std::endl;
	std::cout << "Read Length: " << readLength << std::endl;
	std::cout << "Reset: " << doReset << std::endl;
	std::cout << "Write Address Step Size: " << addrIncrement << std::endl;
	std::cout << "Write Start Address Requested: " << writeStartAddress << " - x4 for true location" << std::endl;
	std::cout << "Initialise Memory with dummy data: " << doMemoryInit << std::endl;
	bool dobytewiseRead = false;
	/*
	 *
	 * Sequence:
	 *
	 * 1) Initialise Read/Write On-board R/W functions
	 *
	 * 2) Place data onto write input FIFO - follow data through into memory stepwise
	 *
	 * 3) Please read request onto read queue FIFO - follow data through stepwise into output fifo stepwise (do not invalidate when reading)
	 *
	 * 4) Request DMA of data and verify on receipt
	 *
	 *
	 */

	//1) Initial Configuration
	unsigned int numWords = 0;
	if(readLength < 4){
		numWords = 1;
	}
	else {
		numWords = (int)(ceil(readLength / 4.0));
	}
	bool result = true;
	unsigned int *dmaPtr = (unsigned int*)m_testCandidate->m_virtualArrayPointer[0];
	unsigned int memSize = 4096;

	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DestinationOffset = 0x0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.PrimaryDMAFIFOCountIgnore = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.PrimaryDMARAMModeEnable = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.IgnoreDMADoneFull = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 1;

	std::map<unsigned int, std::string> memControlArbiterMap;
	memControlArbiterMap.insert(std::make_pair(0, "Write Channel 0"));
	memControlArbiterMap.insert(std::make_pair(1, "Write Channel 1"));
	memControlArbiterMap.insert(std::make_pair(2, "Write Channel 2"));
	memControlArbiterMap.insert(std::make_pair(3, "Write Channel 3"));
	memControlArbiterMap.insert(std::make_pair(4, "Write Channel 4"));
	memControlArbiterMap.insert(std::make_pair(5, "Write Channel 5"));
	memControlArbiterMap.insert(std::make_pair(6, "Read DMA"));
	memControlArbiterMap.insert(std::make_pair(7, "Idle"));

	std::map<unsigned int, std::string> memInputControlMap;
	memInputControlMap.insert(std::make_pair(1, "Read Init"));
	memInputControlMap.insert(std::make_pair(2, "Read Request"));
	memInputControlMap.insert(std::make_pair(3, "Read Transfer"));
	memInputControlMap.insert(std::make_pair(7, "Idle"));

	std::map<unsigned int, std::string> memOutputControlMap;
	memOutputControlMap.insert(std::make_pair(1, "Read Request"));
	memOutputControlMap.insert(std::make_pair(2, "Read Data First"));
	memOutputControlMap.insert(std::make_pair(3, "Read Data"));
	memOutputControlMap.insert(std::make_pair(7, "Idle"));

	// 1) Read Side Setup

	std::cout << "BIST Memory Expert Test: Read Side Setup: Check New Status of Buffer Memory Phase Locked Loop: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPLLLocked << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check New Status of Buffer Memory Physical Init: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPhysicalInitDone << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of lower part of Buffer Memory Control Register " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of upper part of Buffer Memory Control Register " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[1] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of Buffer Memory Control Register Reset Bit " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MIGAsyncReset << std::dec << std::endl;
	std::cout << std::endl;


	std::map<unsigned int, std::string>::iterator FSMMapResult;
	unsigned int tempResultArb = -1;
	unsigned int tempResultIn = -1;
	unsigned int tempResultOut = -1;

	tempResultArb = (unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMArbiter;
	tempResultIn = (unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMInputCtrl;
	tempResultOut = (unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMOutputCtrl;

	FSMMapResult = memControlArbiterMap.find(tempResultArb);
	if(FSMMapResult != memControlArbiterMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Control Arbiter: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Control Arbiter returns unrecognised value: " << tempResultArb << std::endl;
	}

	FSMMapResult = memInputControlMap.find(tempResultIn);
	if(FSMMapResult != memInputControlMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Input Control: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Input Control returns unrecognised value: " << tempResultIn << std::endl;
	}

	FSMMapResult = memOutputControlMap.find(tempResultOut);
	if(FSMMapResult != memOutputControlMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Output Control: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Output Control returns unrecognised value: " << tempResultOut << std::endl;
	}
	//	}
	std::cout << std::endl;

	if(doReset){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Set Buffer Memory Control Register Reset Bit to 1" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MIGAsyncReset = 1;
		std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of Buffer Memory Control Register Reset Bit " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MIGAsyncReset << std::dec << std::endl;
		std::cout << "BIST Memory Expert Test: Read Side Setup: Set Buffer Memory Control Register Reset Bit to 0" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MIGAsyncReset = 0;
		sleep(1);
		std::cout << "BIST Memory Expert Test: Read Side Setup: Read state machines after reset" << std::endl;
	}
	FSMMapResult = memControlArbiterMap.find((unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMArbiter);
	if(FSMMapResult != memControlArbiterMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Control Arbiter: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Control Arbiter returns unrecognised value: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMArbiter << std::endl;
	}

	FSMMapResult = memInputControlMap.find((unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMInputCtrl);
	if(FSMMapResult != memInputControlMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Input Control: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Input Control returns unrecognised value: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMInputCtrl << std::endl;
	}

	FSMMapResult = memOutputControlMap.find((unsigned int)m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMOutputCtrl);
	if(FSMMapResult != memOutputControlMap.end()){
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Output Control: " << FSMMapResult->second << std::endl;
	}
	else{
		std::cout << "BIST Memory Expert Test: Read Side Setup: Memory Output Control returns unrecognised value: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryMonitorFSMOutputCtrl << std::endl;
	}

	std::cout << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of Buffer Memory Control Register Reset Bit " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MIGAsyncReset << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check New Status of Buffer Memory Phase Locked Loop: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPLLLocked << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check New Status of Buffer Memory Physical Init: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPhysicalInitDone << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of lower part of Buffer Memory Control Register " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: Check Value of upper part of Buffer Memory Control Register " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[1] << std::dec << std::endl;
	std::cout << std::endl;

	std::cout << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryReadQueueFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFill << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryReadQueueFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFull << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryReadQueueFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOEmpty << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryReadQueueFIFODataValid = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFODataValid << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputFirstFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFill << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputFirstFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFull << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputFirstFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOEmpty << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputSecondFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputSecondFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFill << std::endl;
	std::cout << "BIST Memory Expert Test: Read Side Setup: SubRob " << subRob << " MemoryOutputSecondFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFull << std::dec << std::endl;
	std::cout << std::endl;

	//1) Write Side Setup

	//Set Memory Arbitrator Timeout
	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register[0] " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register[1] " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[1] << std::dec << std::endl;

	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register Arbitrator Write Timeout " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationWriteTimeout << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register Arbitrator Read Timeout " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationReadTimeout << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: Setting Value of Buffer Memory Control Register Arbitrator Timeouts (Read and Write) to 128" << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationWriteTimeout = 128;
	m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationReadTimeout = 128;

	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register Arbitrator Write Timeout " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationWriteTimeout << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register Arbitrator Read Timeout " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationReadTimeout << std::dec << std::endl;

	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register[0] " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: Check Value of Buffer Memory Control Register[1] " << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerControl.Register[1] << std::dec << std::endl;

	//Hold Writes to enable FIFO fill check

	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryOutputFIFOCommsHold = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 1;
	std::cout << "BIST Memory Expert Test: Write Side Setup: SubRob " << subRob << " MemoryTestInputFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOEmpty << std::endl;
	std::cout << "BIST Memory Expert Test: Write Side Setup: SubRob " << subRob << " MemoryTestInputFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;

	//1) DMA Setup

	//1) Initialise DMA Landing Area
	std::cout << "BIST Memory Expert Test: Initialise DMA Landing Area" << std::endl;
	std::cout << std::endl;
	for(unsigned int location = 0; location < memSize; ++location){
		*(dmaPtr + location) = 0x99999999;
	}

	//1) Print Status & DMA Landing Area
	std::cout << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Address of Primary DMA Status Register [0] for SubRob " << subRob << " = " << std::hex << (void *)&m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Address of Primary DMA Status Register [1] for SubRob " << subRob << " = " << std::hex << (void *)&m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Address of Primary DMA Control Register [0] for SubRob " << subRob << " = " << std::hex << (void *)&m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Address of Primary DMA Control Register [1] for SubRob " << subRob << " = " << std::hex << (void *)&m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[1] << std::dec << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Primary DMA Control Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST Memory Expert Test: DMA Setup: Primary DMA Control Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[1] << std::dec << std::endl;
	std::cout << std::endl;

	std::cout << std::hex << "BIST Memory Expert Test: DMA Setup: DMA Landing Area Post-Initialisation" <<  std::endl;
	std::cout << std::endl;
	for(unsigned int location = 0; location < numWords; ++location){
		std::cout << std::setw(8) << *(dmaPtr + location) << " ";
		if((location + 1) % 16 == 0){
			std::cout << std::endl;
		}
	}
	std::cout << std::endl;
	//2) Request Writes
	if(doMemoryInit){
		std::cout << "Initialising Memory" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 0;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputAddress.lower = 0;
		unsigned int testCount = 0;
		for(unsigned int counter = 0; counter < 4096; ++counter){
			for(unsigned int intcounter = 0; intcounter < 8; ++intcounter){
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputData.word = 0x77777777;
			}
			while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull == 1){
				testCount++;
				if(testCount > 10000000){
					std::cout << "Test input fifo persistently full" << std::endl;
					return false;
				}
			}
		}
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputEOF.word = 0x77777777;
		std::cout << "Test Input FIFO Full Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;
	}
	sleep(1);

	std::cout << "Test Input FIFO Full Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;
	std::cout << "Test Input FIFO Empty Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOEmpty << std::endl;
	if(holdWrites){
		std::cout << "Holding Writes" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 1;
	}
	else{
		std::cout << "No Write Hold Selected" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 0;
	}

	if(addrIncrement == 0){
		std::cout << "No Address Increments Selected" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputAddress.lower = writeStartAddress*4;
	}
	unsigned int fifoSize = 0;
	std::cout << std::dec << "Number of Writes = " << numWrites << ", starting at address " << writeStartAddress*4 << std::endl;

	while((fifoSize < numWrites)){

		if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull == 1){
			std::cout << "ERROR - Memory Test Input FIFO Full!" << std::endl;
			break;
		}

		if(addrIncrement != 0){
			m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputAddress.lower = 4*(writeStartAddress + fifoSize*addrIncrement);
			std::cout << "Writing to Memory Address " << 4*(writeStartAddress + fifoSize*addrIncrement) << std::endl;
		}

		fifoSize++;

		std::cout << "Write " << std::setfill(' ') << std::setw(6) << 1 << ": " << std::hex;

		for(unsigned int counter = 1; counter < numWordsPerWrite; ++counter){

			m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputData.word = (counter<<16) | fifoSize;

			std::cout << " " << std::setfill('0') << std::setw(8) << ((counter<<16) | fifoSize);
		}

		m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputEOF.word = (numWordsPerWrite<<16) | fifoSize;

		std::cout << " EOF:" << std::setfill('0') << std::setw(8) << ((numWordsPerWrite<<16) | fifoSize) << std::dec << std::endl;

	}
	std::cout << std::endl;


	if(holdWrites){
		std::cout << "Test Input FIFO Full Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;
		std::cout << "Test Input FIFO Empty Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOEmpty << std::endl;

		std::cout << "Releasing Write Hold" << std::endl;
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 0;
	}
	sleep(1);

	std::cout << "Test Input FIFO Full Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;
	std::cout << "Test Input FIFO Empty Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOEmpty << std::endl;

	//Release Output FIFO Hold before reading
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryOutputFIFOCommsHold = 0;

	//3) Request Reads
	if(dobytewiseRead){
		unsigned int readCounter;
		std::cout << "Running byte at a time read test, readlength set to 1" << std::endl;
		for(unsigned int testCounter = 0; testCounter < numReads; ++ testCounter){

			std::cout << "Reading Address " << testCounter * 8 << std::endl;
			m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
			m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(1) | ((uint64_t)(testCounter * 32)) << 32;

			usleep(1);
			readCounter = 1;
			while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
				std::cout << "Read " << std::setfill(' ') << std::setw(6) << ((readCounter -1) *4) << ": " << std::hex
						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower
						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper

						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower
						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper

						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower
						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper

						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower
						<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper

						<< std::dec << std::endl;
				m_testCandidate->m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;
				readCounter++;

			}
		}
	}
	else{
		unsigned int maxReads = 2;

		for(unsigned int maincount = 0; maincount < maxReads; ++maincount){
			std::cout << "Read Length = " << readLength << std::endl;
			std::cout << "Number of Reads = " << numReads << std::endl;
			unsigned int readCounter;
			for(unsigned int testCounter = 0; testCounter < numReads; ++ testCounter){

				m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = readLength;
			}
			sleep(1);
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFill << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFull << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOEmpty << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFODataValid = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFODataValid << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFill << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFull << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOEmpty << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFill << std::endl;
			std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFull << std::endl;
			std::cout << std::endl;

			if(maincount < maxReads - 1){

				std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " Attempt direct invalidate read of MemoryOutputSecondFIFO" << std::endl;
				std::cout << "Guide: Bottom lower - Bottom upper - Top lower - Top upper" << std::endl;
				readCounter = 1;
				//int invalidate;
				while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
					std::cout << "Read " << std::setfill(' ') << std::setw(6) << ((readCounter -1) *4) << ": " << std::hex
							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower
							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper

							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower
							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper

							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower
							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper

							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower
							<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper

							<< std::dec << std::endl;
					m_testCandidate->m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;
					readCounter++;
				}
			}
			else{
				std::cout << "BIST Memory Expert Test: Final Loop - Read Request: SubRob " << subRob << " Send final extra read for use with Primary DMA test" << std::endl;

			}

		}
		std::cout << std::endl;

		//3) Print Status & attempt direct read of memory output fifo
		sleep(1);
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFill << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOFull << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFOEmpty << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryReadQueueFIFODataValid = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryReadQueueFIFODataValid << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFill << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOFull << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputFirstFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputFirstFIFOEmpty << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOEmpty = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOFill = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFill << std::endl;
		std::cout << "BIST Memory Expert Test: Read Request: SubRob " << subRob << " MemoryOutputSecondFIFOFull = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOFull << std::dec << std::endl;

		std::cout << "BIST Memory Expert Test: Read Request: Request " << numWords << " words from the Memory Queue" << std::endl;
		std::cout << std::endl;

		//4) Request DMA
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 0;

		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
		m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = readLength;

		std::cout << "BIST Memory Expert Test: DMA Request: Primary DMA Status Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[0] << std::dec << std::endl;
		std::cout << "BIST Memory Expert Test: DMA Request: Primary DMA Status Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Register[1] << std::dec << std::endl;
		std::cout << "BIST Memory Expert Test: DMA Request: Primary DMA Control Register [0] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[0] << std::dec << std::endl;
		std::cout << "BIST Memory Expert Test: DMA Request: Primary DMA Control Register [1] for SubRob " << subRob << " = 0x" << std::hex << m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Register[1] << std::dec << std::endl;
		std::cout << std::endl;

		//4) Wait for DMA
		int counter = 0;
		while(*dmaPtr == 0x99999999 && counter < 1000){
			counter++;
		}

		std::cout << std::endl;
		std::cout << std::hex << "BIST Memory Expert Test: DMA Request: DMA Landing Area Post-DMA request" <<  std::endl;
		std::cout << std::endl;

		if(result){
			//4) Validate DMA results
			std::map<unsigned int, bool> dmaMap;
			dmaMap.insert(std::make_pair(0x9abcdef0, true));
			dmaMap.insert(std::make_pair(0x87654321, true));
			dmaMap.insert(std::make_pair(0x0fedcba9, true));
			dmaMap.insert(std::make_pair(0x12345678, true));
			dmaMap.insert(std::make_pair(0x0, true));

			unsigned int dmaWordCount = 0;
			for(unsigned int location = 0; location < numWords; ++location){
				std::cout << std::setw(8) << *(dmaPtr + location) << " ";
				if(dmaMap.find(*(dmaPtr + location))->second){
					dmaWordCount++;
				}
				if((location + 1) % 16 == 0){
					std::cout << std::endl;
				}
			}
			std::cout << std::dec << std::endl;
			if(dmaWordCount != numWords){
				std::cout << "BIST Memory Expert Test FAIL: DMA did not deliver expected number of words. Requested " << numWords << " and received " << dmaWordCount << std::endl;
				result = false;
			}
			else{
				std::cout << "BIST Memory Expert Test: DMA Request: DMA delivered expected number of words. Requested " << numWords << " and received " << dmaWordCount << std::endl;
			}

		}
		std::cout << std::endl;
		//4) Final Print of DMA landing area
		std::cout << std::hex << std::endl;
		for(unsigned int location = 0; location < numWords; ++location){
			std::cout << std::setw(8) << *(dmaPtr + location) << " ";
			if((location + 1) % 16 == 0){
				std::cout << std::endl;
			}
		}
		std::cout << std::dec << std::endl;
	}
	//Exit Test
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Memory Expert Test for SubRob " << subRob << "  *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return result;
}

/*
 *
 * Design test - check design ID, authors etc as expected
 *
 */
unsigned int RobinNPBIST::designTest(){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Design Test                            *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;

	const int designOffset = 0x8;
	const int designVersion = 1;
	const int designMajorRevision = 6;
	const int designMinorRevision = 4;

	std::cout << std::endl;
	std::cout << "BIST Design Test: Check RobinNP design and version details." << std::endl;
	std::cout << std::endl;

	std::cout << "BIST Design Test: Check Design Struct Mapping:" << std::endl;

	if((unsigned long)((void *)&m_testCandidate->m_Bar0Common->DesignID) != m_testCandidate->m_bar0Addr + designOffset){
		std::cout << "BIST Design Test: Error. Design Struct Incorrectly Mapped. Expecting BAR0 + 0x" << std::hex << designOffset << " = 0x" << m_testCandidate->m_bar0Addr + designOffset << ", actual value = " << &m_testCandidate->m_Bar0Common->DesignID << std::dec << std::endl;
		return 1;
	}
	else{
		std::cout << "BIST Design Test: Design Struct Correctly Mapped. Expecting BAR0 + 0x" << std::hex << designOffset << " = 0x" << m_testCandidate->m_bar0Addr + designOffset << ", actual value = " << &m_testCandidate->m_Bar0Common->DesignID << std::dec << std::endl;
	}
	std::cout << std::endl;
	std::cout << "BIST Design Test: Check Versions and Authors:" << std::endl;
	unsigned int result = 0;
	if(m_testCandidate->m_Bar0Common->DesignID.DesignVersion != designVersion){
		std::cout << "BIST Design Test: Warning. Design Version does not match expected value " << designVersion << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignVersion << std::endl;
		result =  2;
	}
	else{
		std::cout << "BIST Design Test: Design Version matches expected value " << designVersion << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignVersion << std::endl;
	}

	if(m_testCandidate->m_Bar0Common->DesignID.DesignMajorRevision != designMajorRevision){
		std::cout << "BIST Design Test: Warning. Major Design Revision does not match expected value " << designMajorRevision << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignMajorRevision << std::endl;
		result =  2;
	}
	else{
		std::cout << "BIST Design Test: Major Design Revision matches expected value " << designMajorRevision << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignMajorRevision << std::endl;
	}

	if(m_testCandidate->m_Bar0Common->DesignID.DesignMinorRevision != designMinorRevision){
		std::cout << "BIST Design Test: Warning.  Minor Design Revision does not match expected value " << designMinorRevision << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignMinorRevision << std::endl;
		result =  2;
	}
	else{
		std::cout << "BIST Design Test: Minor Design Revision matches expected value " << designMinorRevision << ", actual value = " << m_testCandidate->m_Bar0Common->DesignID.DesignMinorRevision << std::endl;
	}
	std::cout << std::endl;

	std::cout << "BIST Design Test: RobinNP reports expected hardware version: " << hardwareList[m_testCandidate->m_Bar0Common->DesignID.HardwareVersion] << std::endl;
	std::cout << std::endl;
	std::cout << "BIST Design Test: RobinNP reports " << m_testCandidate->m_Bar0Common->DesignID.NumberOfSubRobs << " SubRobs in total." << std::endl;
	std::cout << "BIST Design Test: RobinNP reports " << m_testCandidate->m_Bar0Common->DesignID.NumberOfChannelsPerSubRob << " Channels per SubRob." << std::endl;
	std::cout << "BIST Design Test: RobinNP reports " << m_testCandidate->m_Bar0Common->DesignID.NumberOfChannels << " Channels in total." << std::endl;
	if(m_testCandidate->m_Bar0Common->DesignID.NumberOfChannelsPerSubRob * m_testCandidate->m_Bar0Common->DesignID.NumberOfSubRobs != m_testCandidate->m_Bar0Common->DesignID.NumberOfChannels){
		std::cout << "BIST Design Test: FAIL. Total number of channels does not match expected amount based on number of SubRobs and Channels per SubRob"  << std::endl;
		result +=1;
	}
	std::cout << std::endl;

	if(authorNamesList[m_testCandidate->m_Bar0Common->DesignID.Author] == "Unknown"){
		std::cout << "BIST Design Test FAIL: RobinNP reports unknown author!" << std::endl;
		result +=1;
	}
	else{
		std::cout << "BIST Design Test: RobinNP reports author of this firmware revision is " << authorNamesList[m_testCandidate->m_Bar0Common->DesignID.Author] << "." << std::endl;
	}

	std::cout << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Design Test                          *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	return result;
}

bool RobinNPBIST::interrupterTest(unsigned int interrupt){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Interrupter Test                       *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;

	int interruptsToTest = 1000000;

	std::cout << "BIST Interrupter Test: Trigger Interrupt " << interrupt << " then wait on IOCTL. Testing with " << interruptsToTest << " interrupts" << std::endl;

	configureInterrupter(interrupt);

	m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 1 << interrupt;

	std::cout << std::endl;
	std::cout << "Interrupt Status Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "Interrupt Status Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[1] << std::dec << std::endl;
	std::cout << "Interrupt Status Register MSIAllowed = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.MSIAllowed << std::dec << std::endl;
	std::cout << "Interrupt Status Register NumberOfMSIsRequested = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsRequested << std::dec << std::endl;
	std::cout << "Interrupt Status Register NumberOfMSIsAllocated = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsAllocated << std::dec << std::endl;
	std::cout << "Interrupt Status Register SixtyFourBitCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
	std::cout << "Interrupt Status Register PerVectorMaskingCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
	std::cout << "Interrupt Status Register Reserved = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.Reserved << std::dec << std::endl;
	std::cout << "Interrupt Status Register InIdleState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
	std::cout << "Interrupt Status Register InWaitingForACKState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
	std::cout << "Interrupt Status Register InWaitingForEnableResetState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InWaitingForEnableResetState << std::dec << std::endl;
	std::cout << "Interrupt Status Register filler = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler << std::dec << std::endl;
	std::cout << "Interrupt Status Register commonInterrupterStatus = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.commonInterrupterStatus << std::dec << std::endl;
	std::cout << "Interrupt Status Register filler2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler2 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus0 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus0 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus1 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus1 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus2 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus3 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus3 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus4 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus4 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus5 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus5 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus6 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus6 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus7 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus7 << std::dec << std::endl;
	std::cout << "Interrupt Status Register InterruptCount = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InterruptCount << std::dec << std::endl;
	std::cout << std::endl;
	std::cout << "Trigger " << m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger << std::endl;
	std::cout << std::endl;

	int interruptsTested = 0;
	float totalTime = 0;

	Timer interruptTimer(20);
	interruptTimer.start();

	while(interruptsTested < interruptsToTest){
		if(interruptTimer.isTimeout()){
			totalTime = interruptTimer.elapsedTime();
			std::cout << "BIST Interrupter Test FAIL: Timeout - total of " << interruptsTested << " interrupts received out of " << interruptsToTest << " in " << totalTime << " seconds" << std::endl;
			std::cout << "BIST Interrupter Test: Time per interrupt = " << totalTime/interruptsTested << " seconds, for a rate of " << interruptsTested/totalTime << " interrupts/second" << std::endl;

			m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 0;
			m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
			return false;
		}

		int ret = ioctl(m_testCandidate->m_devHandle, WAIT_IRQ, &interrupt);
		if (ret)
		{
			std::cout << "BIST Interrupter Test FAIL: Interrupt IOCTL failed to return success" << std::endl;
			return false;
		}

		m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1<< interrupt;// 0xffffffffffffffff;

		interruptsTested++;
	}

	totalTime = interruptTimer.elapsedTime();
	std::cout << "BIST Interrupter Test: Success - Total of " << interruptsTested << " interrupts received out of " << interruptsToTest << " in " << totalTime << " seconds" << std::endl;
	if(totalTime != 0 && interruptsTested != 0){
		std::cout << "BIST Interrupter Test: Time per interrupt = " << totalTime/interruptsTested << " seconds, for a rate of " << interruptsTested/totalTime << " interrupts/second" << std::endl;
	}
	std::cout << "BIST Interrupter Test: Re-set interrupter" << std::endl;
	m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 0;
	m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
	std::cout << "BIST Interrupter Test: Successfully completed" << std::endl;

	return true;
}



void *interruptThread(void* params){
	InterrupterThreadParams *thread = static_cast<InterrupterThreadParams*>(params);
	void* res = thread->bist->interrupterThreadProcess(thread);
	return res;
}

void *RobinNPBIST::interrupterThreadProcess(InterrupterThreadParams *params){

	unsigned int interruptsTested = 0;
	unsigned int interruptsToTest = params->numInterrupts;
	float totalTime = 0;
	unsigned int interrupt = params->index;
	std::stringstream ss1;
	ss1 << "Initiating Interrupter Test Thread " << interrupt;
	params->bist->m_testCandidate->screenPrint(ss1.str());

	Timer interruptMultiTimer(params->timeout);
	interruptMultiTimer.start();

	while(interruptsTested < interruptsToTest){

		if(interruptMultiTimer.isTimeout()){
			totalTime = interruptMultiTimer.elapsedTime();
			std::stringstream ss2;

			ss2 << "BIST Interrupter Test FAIL: Timeout - total of " << interruptsTested << " interrupts received out of " << interruptsToTest << " in " << totalTime << " seconds" << std::endl;
			if(totalTime != 0 && interruptsTested != 0){
				ss2 << "BIST Interrupter Test: Time per interrupt = " << totalTime/interruptsTested << " seconds, for a rate of " << interruptsTested/totalTime << " interrupts/second" << std::endl;
			}
			params->bist->m_testCandidate->screenPrint(ss2.str());

			m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 0;
			m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
			return 0;
		}

		int ret = ioctl(m_testCandidate->m_devHandle, WAIT_IRQ, &interrupt);
		if (ret)
		{
			std::cout << "BIST Interrupter Test FAIL: Interrupt IOCTL failed to return success" << std::endl;
			return 0;
		}

		m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;

		interruptsTested++;
	}

	totalTime = interruptMultiTimer.elapsedTime();
	std::stringstream ss3;

	ss3 << "BIST Interrupter Test Thread " << interrupt << ": Success - Total of " << interruptsTested << " interrupts received out of " << interruptsToTest << " in " << totalTime << " seconds\n"
			<< "BIST Interrupter Test Thread " << interrupt << ": Time per interrupt = " << totalTime/interruptsTested << " seconds, for a rate of " << interruptsTested/totalTime << " interrupts/second";
	params->bist->m_testCandidate->screenPrint(ss3.str());

	return 0;

}

bool RobinNPBIST::interrupterMultiTest(unsigned int interruptCount){

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Running Multithreaded Interrupter Test         *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;

	pthread_t interrupterTestThread[8];
	float timeout = 120;
	unsigned int numInterrupts = 1000000;
	InterrupterThreadParams *params = new InterrupterThreadParams[8];
	pthread_spin_init(&m_interruptAcknowledgeLock, PTHREAD_PROCESS_PRIVATE);
	std::cout << "BIST Interrupter Test: Multithreaded Interrupt Test for " << interruptCount << " MSI-X Signals, " << numInterrupts << " interrupts each" << std::endl;

	m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 0;

	configureInterrupter((1 << interruptCount) - 1);

	std::cout << "Trigger " << m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger << std::endl;
	std::cout << std::endl;


	for(unsigned int interruptIndex = 0; interruptIndex < interruptCount; ++interruptIndex){
		params[interruptIndex].bist = this;
		params[interruptIndex].index = interruptIndex;
		params[interruptIndex].timeout = timeout;
		params[interruptIndex].numInterrupts = numInterrupts;
		int res = pthread_create(&interrupterTestThread[interruptIndex],0,interruptThread,(void*)&params[interruptIndex]);
		if(res){
			std::cout << "ERROR Creating thread " << interruptIndex << std::endl;
		}
	}

	sleep(1);
	m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = (1 << interruptCount) - 1;
	std::cout << std::endl;
	std::cout << "Interrupt Status Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[0] << std::dec << std::endl;
	std::cout << "Interrupt Status Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Register[1] << std::dec << std::endl;
	std::cout << "Interrupt Status Register MSIAllowed = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.MSIAllowed << std::dec << std::endl;
	std::cout << "Interrupt Status Register NumberOfMSIsRequested = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsRequested << std::dec << std::endl;
	std::cout << "Interrupt Status Register NumberOfMSIsAllocated = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.NumberOfMSIsAllocated << std::dec << std::endl;
	std::cout << "Interrupt Status Register SixtyFourBitCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
	std::cout << "Interrupt Status Register PerVectorMaskingCapable = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.SixtyFourBitCapable << std::dec << std::endl;
	std::cout << "Interrupt Status Register Reserved = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.Reserved << std::dec << std::endl;
	std::cout << "Interrupt Status Register InIdleState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
	std::cout << "Interrupt Status Register InWaitingForACKState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InIdleState << std::dec << std::endl;
	std::cout << "Interrupt Status Register InWaitingForEnableResetState = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InWaitingForEnableResetState << std::dec << std::endl;
	std::cout << "Interrupt Status Register filler = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler << std::dec << std::endl;
	std::cout << "Interrupt Status Register commonInterrupterStatus = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.commonInterrupterStatus << std::dec << std::endl;
	std::cout << "Interrupt Status Register filler2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.filler2 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus0 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus0 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus1 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus1 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus2 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus2 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus3 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus3 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus4 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus4 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus5 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus5 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus6 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus6 << std::dec << std::endl;
	std::cout << "Interrupt Status Register IndividualInterrupterStatus7 = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.IndividualInterrupterStatus7 << std::dec << std::endl;
	std::cout << "Interrupt Status Register InterruptCount = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptStatusRegister.Component.InterruptCount << std::dec << std::endl;
	std::cout << std::endl;

	std::cout << "Trigger " << m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger << std::endl;

	void *result;
	for(unsigned int interruptIndex = 0; interruptIndex < interruptCount; ++interruptIndex){
		int res = pthread_join(interrupterTestThread[interruptIndex],&result);
		if(res){
			std::cout << "ERROR Joining thread " << interruptIndex << std::endl;
		}
		std::cout << "Successfully completed thread for interrupt " << interruptIndex << std::endl;
	}

	delete[] params;
	m_testCandidate->m_Bar0Common->InterruptTestRegister.Trigger = 0;
	m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = (1 << interruptCount) - 1;
	std::cout << "BIST Interrupter Multi Test: Re-set interrupter" << std::endl;
	std::cout << "BIST Interrupter Multi Test: Successfully completed" << std::endl;
	pthread_spin_destroy(&m_interruptAcknowledgeLock);
	return true;
}

unsigned int RobinNPBIST::getNumChannelsPerSubRob(){
	return m_testCandidate->m_Bar0Common->DesignID.NumberOfChannelsPerSubRob;
}

unsigned int RobinNPBIST::getNumSubRobs(){
	return m_testCandidate->getNumSubRobs();
}

unsigned int RobinNPBIST::getNumRols(){
	return m_testCandidate->getNumRols();
}

/*************************************************************************/
/*************************************************************************/
/* 						Private Methods								     */
/*************************************************************************/
/*************************************************************************/

/******************************************************/
int* RobinNPBIST::processUPFDuplicatorPageSingleChannel(int readIndex,unsigned int rolId,unsigned int pageSize)
/******************************************************/
{
	//acquire new fragment page from used page FIFO duplicator.
	//return 0 if fragment invalid or channel inactive

	int *results = new int[4];
	unsigned int pageSizeOffset = log10(pageSize)/log10(2);
	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;
	unsigned int status = m_testCandidate->readCombinedUPFDuplicator(readIndex, 0, subRob);

	unsigned int channel_number = subRob*(m_testCandidate->getNumChannelsPerSubRob()) + ((status & upfChannel) >> 16);
	unsigned int MemorySize = s_4k;
	unsigned int loopCounter = MemorySize/sizeof(unsigned int);

	if (upfValid != (status & upfValid)){
		std::cout << "BIST PageflowTest FAIL: UPF returned invalid page, status = 0x" << std::hex << status << ", read index = 0x" << readIndex << std::dec << std::endl;
		results[0] = -99;

		std::cout << "BIST PageflowTest: Display duplicator buffer" << std::hex << std::endl;

		for(unsigned int loop = 0; loop < loopCounter; loop+=4){
			for(int tempLoop = 3; tempLoop > -1; --tempLoop){
				std::cout << std::setfill('0') << std::setw(8) << *(m_testCandidate->m_duplicateUPF[subRob].buffer + loop + tempLoop) << " ";
			}
			std::cout << std::endl;
		}

		return results;
	}

	// process requested channels only
	if (channel_number != rolId){
		std::cout << "BIST PageflowTest FAIL: Channel Number in duplicator does not correspond to requested ROL - received " << channel_number << " expected " << rolId << std::endl;
		results[0] = -99;

		std::cout << "BIST PageflowTest: Display duplicator buffer" << std::hex << std::endl;

		for(unsigned int loop = 0; loop < loopCounter; loop+=4){
			for(int tempLoop = 3; tempLoop > -1; --tempLoop){
				std::cout << std::setfill('0') << std::setw(8) << *(m_testCandidate->m_duplicateUPF[subRob].buffer + loop + tempLoop) << " ";
			}
			std::cout << std::endl;
		}

		std::cout << std::dec << std::endl;
		return results;
	}

	int eventId = m_testCandidate->readCombinedUPFDuplicator(readIndex, 1, subRob);
	unsigned int pageInfo = m_testCandidate->readCombinedUPFDuplicator(readIndex, 2, subRob);
	//unsigned int runNum = m_testCandidate->readCombinedUPFDuplicator(readIndex, 3, subRob);

	// mask address bits
	pageInfo &= upfAddrMask;    // mask off end marker bits

	int pageNum = pageInfo >> pageSizeOffset;

	unsigned int pageLen =  pageInfo & (pageSize - 1);

	results[0] = eventId;
	results[1] = pageNum;
	results[2] = ((pageNum * pageSize) & 0xFFFFFFF);
	results[3] = pageLen;

	m_testCandidate->updateCombinedUPFReadIndex(subRob);

	return results;
}

/******************************/
void RobinNPBIST::resetFifoDuplicators()
/******************************/
{
	std::cout << "BIST PageflowTest: Resetting FIFO Duplicators: Called" << std::endl;

	for(unsigned int subRob = 0; subRob < m_testCandidate->m_numSubRobs; ++subRob){

		//Disable DMA Done Duplicator
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.DMADoneFIFODuplicatorEnable = 0;

		//Store local memory locations for ring buffer and write index on board
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationBottom = m_testCandidate->m_cardData.dmaDoneDuplicateHandle[subRob]& 0xffffffff;
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationTop = (m_testCandidate->m_cardData.dmaDoneDuplicateHandle[subRob]>>32)& 0xffffffff;

		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationBottom = (volatile unsigned int)((m_testCandidate->m_cardData.writeIndexHandle + 0x8*subRob)& 0xffffffff);
		m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationTop = (volatile unsigned int)(((m_testCandidate->m_cardData.writeIndexHandle + 0x8*subRob)>>32)& 0xffffffff);

		//Initialise Read and Write indices
		m_testCandidate->m_duplicateDMADone[subRob].readIndexCopy = 0x0;
		*(m_testCandidate->m_duplicateDMADone[subRob].readIndex) = 0x0;
		*(m_testCandidate->m_duplicateDMADone[subRob].writeIndexCopy) = 0x0;

		//Initialise Ring Buffer Memory Space
		for(int counter = 0; counter < 256; ++counter){
			*(m_testCandidate->m_duplicateDMADone[subRob].buffer+4*counter+0) = 0x9999;
			*(m_testCandidate->m_duplicateDMADone[subRob].buffer+4*counter+1) = 0x8888;
			*(m_testCandidate->m_duplicateDMADone[subRob].buffer+4*counter+2) = 0x7777;
			*(m_testCandidate->m_duplicateDMADone[subRob].buffer+4*counter+3) = 0x6666;
		}

		//Set Enable Bit for DMA Done Duplicator
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.DMADoneFIFODuplicatorEnable = 1;

		//Disable Common UPF Duplicator
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 0;

		//Store local memory locations for ring buffer and write index on board
		m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.lower = m_testCandidate->m_cardData.upfDuplicateHandle[subRob]& 0xffffffff;
		m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.upper = (m_testCandidate->m_cardData.upfDuplicateHandle[subRob]>>32)& 0xffffffff;

		m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.lower = (m_testCandidate->m_cardData.writeIndexHandle + 0x8*(subRob + 2))& 0xffffffff;
		m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.upper = ((m_testCandidate->m_cardData.writeIndexHandle + 0x8*(subRob + 2))>>32)& 0xffffffff;

		//Initialise Read and Write indices
		m_testCandidate->m_duplicateUPF[subRob].readIndexCopy = 0x0;
		*(m_testCandidate->m_duplicateUPF[subRob].readIndex) = 0x0;
		*(m_testCandidate->m_duplicateUPF[subRob].writeIndexCopy) = 0x0;

		//Initialise Ring Buffer Memory Space
		for(unsigned int counter = 0; counter < m_testCandidate->m_upfMaskVal + 1; ++counter){
			*(m_testCandidate->m_duplicateUPF[subRob].buffer+sizeof(int)*counter+0) = 0x9999;
			*(m_testCandidate->m_duplicateUPF[subRob].buffer+sizeof(int)*counter+1) = 0x8888;
			*(m_testCandidate->m_duplicateUPF[subRob].buffer+sizeof(int)*counter+2) = 0x7777;
			*(m_testCandidate->m_duplicateUPF[subRob].buffer+sizeof(int)*counter+3) = 0x6666;
		}

		//Set Enable Bit for Common UPF Duplicator
		m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 1;
	}
	std::cout << "BIST PageflowTest: Resetting FIFO Duplicators: Done" << std::endl;

}



void RobinNPBIST::printI2CConfig()
{
	printf("I2C NEW CFG  : 0x%x\n", m_testCandidate->m_Bar0Common->I2CRegister.config);
	printf("  Selection:   0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 24) & 0xFF);
	printf("  Slave:       0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 8) & 0x7F);
	printf("  Error0:      %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 6) & 0x1);
	printf("  Error1:      %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 7) & 0x1);
	printf("  Byte Enable: %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 3) & 0x3);
	printf("  WE:          %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 2) & 0x1);
	printf("  RE:          %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 1) & 0x1);
	printf("  Ready:       %d\n", (m_testCandidate->m_Bar0Common->I2CRegister.config >> 0) & 0x1);

	printf("I2C NEW STS  : 0x%x\n", m_testCandidate->m_Bar0Common->I2CRegister.status);
	printf("  Byte 0 addr: 0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.status >>  0) & 0xFF);
	printf("  Byte 0 rx:   0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.status >>  8) & 0xFF);
	printf("  Byte 1 addr: 0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.status >> 16) & 0xFF);
	printf("  Byte 1 rx:   0x%x\n", (m_testCandidate->m_Bar0Common->I2CRegister.status >> 24) & 0xFF);
}

int RobinNPBIST::writeI2CConfig(u_int chainSelect, u_int slaveAddress, u_int command, u_int mode, u_int byteEnable)
{
	/** Chain mapping:
	 * 0: QSFP - use mode select to choose which transciever to access
	 * 4: DDR3
	 * 5: FMC
	 * 6: RefClkGen
	 *
	 * bytes_enable:
	 * 0x1: lower byte
	 * 0x2: upper byte
	 * 0x3: both bytes
	 *
	 * mode:
	 * 0: 100 kHz
	 * 1: 400 kHz
	 * */

	if ( (chainSelect > 7) || (byteEnable > 3) || (command!=I2C_READ && command!=I2C_WRITE) || (mode>1) )
	{
		std::cout << "ERROR, Invalid Configuration" << std::endl;
		return 1;
	}

	if(m_testCandidate->m_designVersion > 0x1060002){

		unsigned int selector = 0;
		switch(chainSelect){
		case 0:
			selector = 8;
			break;
		case 1:
			selector = 9;
			break;
		case 2:
			selector = 0xA;
			break;
		case 4:
			selector = 2;
			break;
		case 5:
			selector = 3;
			break;
		case 6:
			selector = 1;
			break;
		default:
			selector = 0;
			break;
		}
		//std::cout << "I2C Select: " << selector << std::endl;
		m_testCandidate->m_Bar0Common->GlobalControlRegister.I2CSelect = selector;
		chainSelect = 0;
	}
	//usleep(100);
	u_int cfgcmd = (1 << chainSelect << 24) | (slaveAddress << 8) | (mode<<5) | (byteEnable<<3) | command;
	//std::cout << "Writing config: " << std::hex << cfgcmd << std::dec << std::endl;
	m_testCandidate->m_Bar0Common->I2CRegister.config = cfgcmd;

	return 0;
}


int RobinNPBIST::waitI2CCompletion()
{
	while( (m_testCandidate->m_Bar0Common->I2CRegister.config & 0x1)==0 )
	{
		usleep(100);
	}

	if ( m_testCandidate->m_Bar0Common->I2CRegister.config & (0xc0) ) /** error flags: bits 6,7 */
	{
		std::cout << "Wait ERROR" << std::endl;
		return 1;
	}

	return 0;
}

int RobinNPBIST::i2cDualWrite(u_int chain, u_int slaveAddress, u_int memAddress0, u_int memAddress1, u_int data0, u_int data1)
{
	// We have to read out the cfg register before.
	// This might be related to the reset of WE/RE in the firmware.
	//  volatile u_int cfg = comm->i2c_new_cfg;

	m_testCandidate->m_Bar0Common->I2CRegister.status =  (data1<<24) | (memAddress1<<16) | (data0<<8) | (memAddress0);
	std::cout << "Dual Write Status check " << std::hex << m_testCandidate->m_Bar0Common->I2CRegister.status << " " << ((data1<<24) | (memAddress1<<16) | (data0<<8) | (memAddress0)) <<std::dec << std::endl;

	writeI2CConfig(chain,slaveAddress,I2C_WRITE,0/*mode*/,3/*byte enable*/);

	return waitI2CCompletion();
}




int RobinNPBIST::i2cReadWrite(u_int chain, u_int slaveAddress, u_int memAddress, unsigned int readwriteSelect, u_int data)
{
	// We have to read out the cfg register before.
	// This might be related to the reset of WE/RE in the firmware.
	//  volatile u_int cfg = comm->i2c_new_cfg;

	//std::cout << "Reg: " << (void*)busReg << std::endl;

	m_testCandidate->m_Bar0Common->I2CRegister.status = ((data<<8) | (memAddress));
	/*
	if(readwriteSelect == I2C_WRITE){
		std::cout << "Write Status check " << std::hex << m_testCandidate->m_Bar0Common->I2CRegister.status << " " << ((data<<8) | (memAddress)) <<std::dec << std::endl;
	}
	else{
		std::cout << "Read Status check " << std::hex << m_testCandidate->m_Bar0Common->I2CRegister.status << " " << ((data<<8) | (memAddress)) <<std::dec << std::endl;
	}*/
	writeI2CConfig(chain,slaveAddress,readwriteSelect,0/** mode */, 1/** byte_enable */);

	volatile u_int value = 0;

	unsigned int ret =  waitI2CCompletion();
	if(ret == 0){
		//std::cout << "Completion output: " << std::hex << ret << std::dec << std::endl;
		value = ((m_testCandidate->m_Bar0Common->I2CRegister.status)>>8) & 0xff;
	}
	return value;
}


void RobinNPBIST::waitForClockClearance(uint8_t flag)
{
	/** wait for flag to be cleared by device */
	uint8_t reg135 = flag;
	while ( reg135 & flag )
	{
		reg135 = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 135, I2C_READ);
	}
}


void RobinNPBIST::getClockConfig(float fout){

	std::cout << "Getting clock configuration assuming fout = " << fout << std::endl;
	int i;
	u_int value;
	uint64_t rfreq_int;
	u_int hs_div, n1;
	double fdco, fxtal, rfreq_float;

	/** addr 7: HS_DIV[2:0], N1[6:2] */
	value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 13, I2C_READ);
	std::cout << "Read Value register shift 13: " << std::hex << value << std::dec << std::endl;
	hs_div = ((value>>5) & 0x07) + 4;
	n1 = ((u_int)(value&0x1f))<<2;

	value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK,14, I2C_READ);
	std::cout << "Read Value register shift 14: " << std::hex << value << std::dec << std::endl;

	n1 += (value>>6)&0x03;
	n1++;

	rfreq_int = ((uint64_t)(value & 0x3f))<<((uint64_t)32);

	/** addr 15...18: RFREQ[31:0] */
	for(i=0; i<4; i++)
	{
		value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK,i+15, I2C_READ);
		std::cout << "Read Value register shift " << i + 15 << ": " << std::hex << value << std::dec << std::endl;

		rfreq_int |= ((uint64_t)(value)) << ((3-i)*8);
	}

	rfreq_float = (double)(rfreq_int/((double)(1<<28)));

	/** fOUT is known -> get accurate fDCO */
	if ( fout!=0 )
	{
		fdco = fout * n1 * hs_div;
		fxtal = fdco/rfreq_float;
	}
	else
		/** fOUT is unknown
		 * -> use default fXTAL to get approximate fDCO */
	{
		fdco = 114.285 * rfreq_float;
		fxtal = 114.285;
	}

	cfg.n1 = n1;
	cfg.hs_div = hs_div;
	cfg.rfreq_int = rfreq_int;
	cfg.rfreq_float = rfreq_float;
	cfg.fdco = fdco;
	cfg.fxtal = fxtal;

	printf("n1            : %4d\n", cfg.n1);
	printf("hs_div        : %4d\n", cfg.hs_div);
	printf("rfreq (int)   : 0x%4lx\n", cfg.rfreq_int);
	printf("rfreq (float) : %9.4f\n", cfg.rfreq_float);
	printf("fdco          : %9.4f Mhz\n", cfg.fdco);
	printf("fxtal         : %9.4f Mhz\n", cfg.fxtal);
	printf("fout          : %9.4f Mhz\n", cfg.fdco/(cfg.n1*cfg.hs_div));
	//	}
	//getchar();
}


std::string RobinNPBIST::readQSFPVendor(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);

	std::string readout = "";
	for(unsigned int byte = 148; byte < 164; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	return readout;

}

std::string RobinNPBIST::readQSFPPartNumber(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);

	std::string readout = "";
	for(unsigned int byte = 168; byte < 184; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	return readout;

}

std::string RobinNPBIST::readQSFPSerialNumber(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);

	std::string readout = "";
	for(unsigned int byte = 196; byte < 212; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	return readout;

}


std::string RobinNPBIST::readQSFPRevisionNumber(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);

	std::string readout = "";
	for(unsigned int byte = 184; byte < 186; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	return readout;

}


double RobinNPBIST::readQSFPTxBias(unsigned int QSFP, unsigned int channel){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);
	double bias = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 42+2*channel, I2C_READ)<<8 | i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 43+2*channel, I2C_READ))*0.002;
	return bias;
}


double RobinNPBIST::readQSFPRxPower(unsigned int QSFP, unsigned int channel){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);
	double power = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 34+2*channel, I2C_READ)<<8 | i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 35+2*channel, I2C_READ))/10000.0;
	return power;
}


double RobinNPBIST::readQSFPWavelength(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);
	double wavelength = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 186, I2C_READ)<<8 | i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 187, I2C_READ))*0.05;
	return wavelength;
}

double RobinNPBIST::readQSFPWavelengthTolerance(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);
	double wavelengthTolerance = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 188, I2C_READ)<<8 | i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 189, I2C_READ))*0.005;
	return wavelengthTolerance;
}


int RobinNPBIST::readQSFPFaultMap(unsigned int QSFP){
	i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_WRITE);
	int map = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 3, I2C_READ) & 0x3FF;
	return map;
}


std::string RobinNPBIST::readQSFPPowerClass(unsigned int QSFP){
	std::string classDescriptor[4] = {"Max 1.5W","Max 2.0W","Max 2.5W","Max 3.5W"};
	int powerclass = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 129, I2C_READ) >> 6) & 0x3;
	return classDescriptor[powerclass];
}

std::string RobinNPBIST::readQSFPCDR(unsigned int QSFP){
	std::string result = "Tx: ";
	result.append(((i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 129, I2C_READ) & 0x4) == 1)?"true":"false");
	result.append(" / Rx: ");
	result.append(((i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 129, I2C_READ) & 0x2) == 1)?"true":"false");
	return result;
}

void RobinNPBIST::readMemorySpec(unsigned int slaveAddr){
	std::string description[150] = {
			"DDR3-crc range, eeprom bytes, bytes used",
			"DDR3-spd revision",
			"DDR3-dram device type",
			"DDR3-module type (form factor)",
			"DDR3-sdram device density banks",
			"DDR3-sdram device row column count",
			"DDR3-module nominal vdd",
			"DDR3-module ranks device dq count",
			"DDR3-ecc tag module memory bus width",
			"DDR3-fine timebase dividend/divisor",
			"DDR3-medium timebase dividend",
			"DDR3-medium timebase divisor",
			"DDR3-min sdram cycle time (tckmin)",
			"DDR3-byte 13 reserved",
			"DDR3-cas latencies supported (cl4 => cl11)",
			"DDR3-cas latencies supported (cl12 => cl18)",
			"DDR3-min cas latency time (taamin)",
			"DDR3-min write recovery time (twrmin)",
			"DDR3-min ras# to cas# delay (trcdmin)",
			"DDR3-min row active to row active delay (trrdmin)",
			"DDR3-min row precharge delay (trpmin)",
			"DDR3-upper nibble for tras trc",
			"DDR3-min active to precharge delay (trasmin)",
			"DDR3-min active to active/refresh delay (trcmin)",
			"DDR3-min refresh recovery delay (trfcmin) lsb",
			"DDR3-min refresh recovery delay (trfcmin) msb",
			"DDR3-min internal write to read cmd delay (twtrmin)",
			"DDR3-min internal read to precharge cmd delay (trtpmin)",
			"DDR3-min four active window delay (tfawmin) msb",
			"DDR3-min four active window delay (tfawmin) lsb",
			"DDR3-sdram device output drivers supported",
			"DDR3-sdram device thermal refresh options",
			"DDR3-module thermal sensor",
			"DDR3-sdram device type",
			"DDR3-fine offset for tckmin",
			"DDR3-fine offset for taamin",
			"DDR3-fine offset for trcdmin",
			"DDR3-fine offset for trpmin",
			"DDR3-fine offset for trcmin",
			"RESERVED",
			"RESERVED",
			"DDR3-ptrr tmaw mac",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"DDR3-rc rev nom module height",
			"DDR3-module thickness (max)",
			"DDR3-reference raw card id",
			"DDR3 - address mapping/module attributes",
			"DDR3-heatspreader solution",
			"DDR3-register vendor id (lsb)",
			"DDR3-register vendor id (msb)",
			"DDR3-register revison number",
			"DDR3-register type",
			"DDR3-reg ctrl words 1 and zero",
			"DDR3-reg ctrl words 3 and 2",
			"DDR3-reg ctrl words 5 and 4",
			"DDR3-reg ctrl words 7 and 6",
			"DDR3-reg ctrl words 9 and 8",
			"DDR3-reg ctrl words 11 and 10",
			"DDR3-reg ctrl words 13 and 12",
			"DDR3-reg ctrl words 15 and 14",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"RESERVED",
			"DDR3-module mfr id (lsb)",
			"DDR3-module mfr id (msb)",
			"DDR3-module mfr location id",
			"DDR3-module mfr year",
			"DDR3-module mfr week",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"MULTIWORD",
			"DDR3-module die rev",
			"DDR3-module pcb rev",
			"DDR3-dram device mfr id (lsb)",
			"DDR3-dram device mfr (msb)",
	};
	for(unsigned int byte = 0; byte < 150; ++byte){
		if(description[byte] != "RESERVED" && description[byte] != "MULTIWORD"){
			u_int val = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, byte, I2C_READ);
			std::cout << description[byte].c_str() << ": " << std::hex << val << std::dec << std::endl;
		}
	}



}

std::string RobinNPBIST::readQSFPTypeIdentifier(unsigned int QSFP){
	std::string typeDescriptor[14] = {
			"Unknown",
			"GBIC",
			"Module/connector soldered to motherboard",
			"SFP",
			"33 pin XBI",
			"XENPAK",
			"XFP",
			"XFF",
			"XFP-E",
			"XPAK",
			"X2",
			"DWDM-SFP",
			"QSFP",
			"QSFP+"
	};

	int typeIdentifier = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 128, I2C_READ));
	if(typeIdentifier > 13){
		return std::string("Unknown");
	}
	else{
		return typeDescriptor[typeIdentifier];
	}
}


std::string RobinNPBIST::readQSFPConnectorType(unsigned int QSFP){
	std::string typeDescriptor[36] = {
			"Unknown",
			"SC",
			"FC Style 1 Copper Connector",
			"FC Style 2 Copper Connector",
			"BNC/TNC",
			"FC coax headers",
			"Fiberjack",
			"LC",
			"MT-RJ",
			"MU",
			"SG",
			"Optical Pigtail",
			"MPO",
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"Reserved"
			"HSSDC II"
			"Copper Pigtail"
			"RJ45"
			"No separable connector"
	};

	int typeIdentifier = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 130, I2C_READ));
	if(typeIdentifier > 35){
		return std::string("Unknown");
	}
	else{
		return typeDescriptor[typeIdentifier];
	}
}

std::string RobinNPBIST::readQSFPModuleEncoding(unsigned int QSFP){
	std::string encodingDescriptor[7] = {
			"Unspecified",
			"8B10B",
			"4B5B",
			"NRZ",
			"SONET Scrambled",
			"64B66B",
			"Manchester",
	};

	int encoding = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 139, I2C_READ));
	if(encoding > 6){
		return std::string("Unknown");
	}
	else{
		return encodingDescriptor[encoding];
	}
}


void RobinNPBIST::readQSFPSpecCompliance(unsigned int QSFP){

	std::cout << "Specification Complicance Report" << std::endl;
	std::string ethernetDescriptor[8] = {
			"40G Active Cable (XLPPI)",
			"40GBASE-LR4",
			"40GBASE-SR4",
			"40GBASE-CR4",
			"10GBASE-SR",
			"10GBASE-LR",
			"10GBASE-LRM",
			"Reserved"
	};

	int ethernetComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 131, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((ethernetComp & (1 << bit)) || (ethernetComp == 0 && bit == 0)){
			std::cout << "->10/40G Ethernet Compliance Code: " << ethernetDescriptor[bit] << std::endl;
		}
	}

	std::string sonetDescriptor[8] = {
			"OC 48 short reach",
			"OC 48 intermediate reach",
			"OC 48 long reach",
			"40G OTN (OTU3B/OTU3C)",
			"Reserved",
			"Reserved",
			"Reserved",
			"Reserved"
	};

	int sonetComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 132, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((sonetComp & (1 << bit)) || (ethernetComp == 0 && bit == 0)){
			std::cout << "->SONET Compliance Codes: " << sonetDescriptor[bit] << std::endl;
		}
	}

	std::string sassataDescriptor[8] = {
			"Reserved",
			"Reserved",
			"Reserved",
			"Reserved",
			"SAS 3.0G",
			"SAS 6.0G",
			"Reserved SAS",
			"Reserved SAS"
	};

	int sassataComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 133, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((sassataComp & (1 << bit)) || (sassataComp == 0 && bit == 0)){
			std::cout << "->SAS/SATA Compliance Codes: " << sassataDescriptor[bit] << std::endl;
		}
	}

	std::string gbeDescriptor[8] = {
			"1000BASE-SX",
			"1000BASE-LX",
			"1000BASE-CX",
			"1000BASE-T",
			"Reserved",
			"Reserved",
			"Reserved",
			"Reserved"
	};

	int gbeComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 134, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((gbeComp & (1 << bit)) || (gbeComp == 0 && bit == 0)){
			std::cout << "->Gigabit Ethernet Compliance Codes: " << gbeDescriptor[bit] << std::endl;
		}
	}

	std::string linklengthDescriptor[8] = {
			"Electrical inter-enclosure (EL)",
			"Longwave laser (LC)",
			"Reserved",
			"Medium(M)",
			"Long distance (L)",
			"Intermediate distance (I)",
			"Short distance (S)",
			"Very long distance (V)"
	};

	int linklengthComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 135, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((linklengthComp & (1 << bit)) || (linklengthComp == 0 && bit == 0)){
			std::cout << "->Fibre Channel Link Length: " << linklengthDescriptor[bit] << std::endl;
		}
	}

	std::string transmittertechDescriptor[8] = {
			"Reserved",
			"Reserved",
			"Reserved",
			"Reserved",
			"Longwave laser (LL)",
			"Shortwave laser w OFC (SL)",
			"Shortwave laser w/o OFC (SN)",
			"Electrical intra-enclosure"
	};

	int transmittertechComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 136, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((transmittertechComp & (1 << bit)) || (transmittertechComp == 0 && bit == 0)){
			std::cout << "->Transmitter Technology: " << transmittertechDescriptor[bit] << std::endl;
		}
	}


	std::string fibremediaDescriptor[8] = {
			"Single Mode (SM)",
			"Multi-mode 50um (OM3)",
			"Multi-mode 50m (M5)",
			"Multi-mode 62.5m (M6)",
			"Video Coax (TV)",
			"Miniature Coax (MI)",
			"Shielded Twister Pair (TP)",
			"Twin Axial Pair (TW)"
	};

	int fibremediaComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 137, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((fibremediaComp & (1 << bit)) || (ethernetComp == 0 && bit == 0)){
			std::cout << "->Fibre Channel transmission media: " << fibremediaDescriptor[bit] << std::endl;
		}
	}

	std::string fibreSpeedDescriptor[8] = {
			"100 MB/s",
			"Reserved",
			"200 MB/s",
			"Reserved",
			"400 MB/s",
			"1600 MB/s",
			"800 MB/s",
			"1200 MB/s"
	};

	int fibreSpeedComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 138, I2C_READ));
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((fibreSpeedComp & (1 << bit)) || (fibreSpeedComp == 0 && bit == 0)){
			std::cout << "->Fibre Speed: " << fibreSpeedDescriptor[bit] << std::endl;
		}
	}

}

std::string RobinNPBIST::readQSFPRateSelectSupport(unsigned int QSFP){


	int reg141 = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 141, I2C_READ));
	int reg221_b2 = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 221, I2C_READ)) >> 2 & (0x1);
	int reg221_b3 = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 221, I2C_READ)) >> 3 & (0x1);

	int reg195_b5 = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 195, I2C_READ)) >> 5 & 0x1;

	if(reg221_b2 == 0 && reg221_b3 == 0 && reg195_b5 == 0){
		return std::string("None");
	}
	if(reg221_b2 == 0 && reg221_b3 == 1 && reg141 != 0){
		return std::string("Extended Rate Selection");
	}
	if(reg221_b2 == 1 && reg221_b3 == 0){
		return std::string("Application Select Tables Selection");
	}
	return std::string("Unknown");

}

void RobinNPBIST::readQSFPDeviceTechnologyDescription(unsigned int QSFP){

	std::string devTechDescriptor[16] = {
			"850 nm VCSEL",
			"1310 nm VCSEL",
			"1550 nm VCSEL",
			"1310 nm FP",
			"1310 nm DFB",
			"1550 nm DFB",
			"1310 nm EML",
			"1550 nm EML",
			"Others",
			"1490 nm DFB",
			"Copper Cable Unequalised",
			"Copper Cable, Passive Equalised",
			"Copper Cable, near and far end limiting active equalisers",
			"Copper Cable, far end limiting active equalisers",
			"Copper Cable, near end limiting active equalisers",
			"Copper Cable, linear active equalisers",
	};


	std::cout << "Device Technology Description:" << std::endl;

	int descriptionLower = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 147, I2C_READ)) & 0xF;
	int descriptionUpper = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 147, I2C_READ)) & 0xF0;

	for(unsigned int bit = 0; bit < 4; ++bit){
		if(descriptionLower & (1 << bit)){
			switch(bit){
			case 0:
				std::cout << "->Transmitter tunable" << std::endl;
				break;
			case 1:
				std::cout << "->APD Detector" << std::endl;
				break;
			case 2:
				std::cout << "->Cooled Transmitter" << std::endl;
				break;
			case 3:
				std::cout << "->Active Wavelength Control" << std::endl;
				break;
			default:
				std::cout << "->Unexpected Value!" << std::endl;
				break;
			}
		}
		else{
			switch(bit){
			case 0:
				std::cout << "->Transmitter not tunable" << std::endl;
				break;
			case 1:
				std::cout << "->Pin Detector" << std::endl;
				break;
			case 2:
				std::cout << "->Uncooled Transmitter" << std::endl;
				break;
			case 3:
				std::cout << "->No Wavelength Control" << std::endl;
				break;
			default:
				std::cout << "->Unexpected Value!" << std::endl;
				break;
			}
		}
	}

	std::cout << "->Transmitter Technology breakdown: " << devTechDescriptor[descriptionUpper] << std::endl;

}

void RobinNPBIST::readQSFPInfinibandCompliance(unsigned int QSFP){
	std::string infinibandDescriptor[8] = {
			"SDR",
			"DDR",
			"QDR",
			"Reserved for FDR",
			"Reserved for EDR",
			"Reserved",
			"Reserved",
			"Reserved",
	};

	int infinibandComp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 164, I2C_READ));
	std::cout << "Infiniband Interfaces Supported:" << std::endl;
	for(unsigned int bit = 0; bit < 8; ++bit){
		if((infinibandComp & (1 << bit)) || (infinibandComp == 0 && bit == 0)){
			std::cout << "-> " << infinibandDescriptor[bit] << std::endl;
		}
	}
}

std::string RobinNPBIST::readQSFPMaxCaseTemp(unsigned int QSFP){
	std::string result;
	int maxTemp = (i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 190, I2C_READ));
	if(maxTemp == 0){
		result.append("70 deg C (Default)");
		return result;
	}
	else{
		result.append(std::to_string(maxTemp));
		result.append(" deg C");
	}
	return result;
}

std::string RobinNPBIST::readQSFPVendorDateCode(unsigned int QSFP){

	std::string readout = "";
	for(unsigned int byte = 216; byte < 218; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	readout += "/";
	for(unsigned int byte = 214; byte < 216; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	readout += "/20";
	for(unsigned int byte = 212; byte < 214; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		readout.append(1, data_r);
	}
	std::string lot;
	for(unsigned int byte = 218; byte < 220; ++byte){
		char data_r = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, byte, I2C_READ);
		lot.append(1, data_r);
	}
	lot.erase(remove_if(lot.begin(), lot.end(), isspace), lot.end());
	if(!lot.empty()){
		readout += " - lot: ";
		readout += lot;
	}
	return readout;

}

int RobinNPBIST::readQSFPTemp(unsigned int QSFP){
	int temp = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 22, I2C_READ);
	return temp;
}


double RobinNPBIST::readQSFPVoltage(unsigned int QSFP){
	int voltage = i2cReadWrite(QSFP, I2C_SLAVE_QSFP0, 26, I2C_READ);
	return (voltage << 8) * 0.0001;

}


void RobinNPBIST::readQSFPdata(){

	std::cout << "------------------------" << std::endl;
	std::cout << "Reading QSFP Information" << std::endl;
	std::cout << "------------------------\n" << std::endl;

	std::cout << "QSFP | Vendor          | Part number      | Rev. | Serial number   | Date code (D/M/Y) | Temperature | Voltage | Wavelength |" << std::endl;
	for(unsigned int QSFP = 0; QSFP < 3; ++QSFP){
		if(((1 << QSFP) & m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPPresent) == 0){
			std::cout << "No QSFP Present in slot " << QSFP << " - Ignoring" << std::endl;
		} else{
			std::cout << QSFP << "    | " << readQSFPVendor(QSFP).c_str() << "| " << readQSFPPartNumber(QSFP).c_str() << " | " << readQSFPRevisionNumber(QSFP).c_str() << "   | " <<
					readQSFPSerialNumber(QSFP).c_str() << "| " << readQSFPVendorDateCode(QSFP).c_str() << "        | " <<  readQSFPTemp(QSFP) << " deg C    | " <<  readQSFPVoltage(QSFP) << "V | " <<
					readQSFPWavelength(QSFP) << "nm      |" << std::endl;
		}
	}

	std::streamsize defaultPrecision = std::cout.precision();
	std::streamsize defaultWidth = std::cout.width();
	std::cout << "\nQSFP | channel | Rx power  | Tx bias  | TX Err. | Loss of Signal Tx/Rx |" << std::endl;
	for(unsigned int QSFP = 0; QSFP < 3; ++QSFP){
		if(((1 << QSFP) & m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPPresent) == 0){
			std::cout << "No QSFP Present in slot " << QSFP << " - Ignoring" << std::endl;
		} else{
			int map = readQSFPFaultMap(QSFP);
			for(unsigned int channel = 0; channel < 4; ++channel){
				std::cout << QSFP << "    | " << channel << "       | " << std::fixed << std::setprecision(4) << readQSFPRxPower(QSFP,channel) << " mW | " << std::setprecision(3) << readQSFPTxBias(QSFP,channel) << " mA |    " <<
						std::setprecision(defaultPrecision) << bool(map & (1 << (channel + 8))) << "    |     " << bool(map & (1 << (channel + 4))) << "    /    " << bool(map & (1 << channel)) << "      |" << std::setw(defaultWidth) << std::endl;
			}
		}
	}

	std::cout << std::endl;


	if(m_verboseMode){
		std::cout << "Reading Extra QSFP Information\n" << std::endl;

		std::cout << "QSFP | Power Class | Transceiver Type | Connector Type  | Module Encoding | Clock Data Recovery Availbility | Rate Selection Support | Max Case Temperature | Wavelength Tolerance |" << std::endl;

		for(unsigned int QSFP = 0; QSFP < 3; ++QSFP){
			if(((1 << QSFP) & m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPPresent) == 0){
				std::cout << "No QSFP Present in slot " << QSFP << " - Ignoring" << std::endl;
			}
			else{
				std::cout << QSFP << "    | " <<  readQSFPPowerClass(QSFP) << "    |      " << readQSFPTypeIdentifier(QSFP)<< "       |       " << readQSFPConnectorType(QSFP) << "       |     " << readQSFPModuleEncoding(QSFP) << "      |      " << readQSFPCDR(QSFP) << "      |          " << readQSFPRateSelectSupport(QSFP) << "          |       " << readQSFPMaxCaseTemp(QSFP) << "       |     " << readQSFPWavelengthTolerance(QSFP) << "nm      |"<< std::endl;
			}
		}
		std::cout << std::endl;

		for(unsigned int QSFP = 0; QSFP < 3; ++QSFP){
			std::cout << "-----------------------------------------------------------------------------" << std::endl;
			if(((1 << QSFP) & m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPPresent) == 0){
				std::cout << "No QSFP Present in slot " << QSFP << " - No Technology Report" << std::endl;
			}
			else{
				std::cout << "QSFP " << QSFP << " Technology Report" << std::endl;
				std::cout << std::endl;
				readQSFPInfinibandCompliance(QSFP);
				std::cout << std::endl;
				readQSFPSpecCompliance(QSFP);
				std::cout << std::endl;
				readQSFPDeviceTechnologyDescription(QSFP);
				std::cout << std::endl;
			}
		}
	}

}

void RobinNPBIST::computeClockConfig(float newFrequency){

	// find the lowest value of N1 with the highest value of HS_DIV
	// to get fDCO in the range of 4.85...5.67 GHz
	int vco_found = 0;
	double fDCO_new;
	int n, h;
	double lastfDCO = 5670.0;

	//cfg->fxtal = fxtal;

	for (n=1; n<=128; n++)
	{
		/** N1 can be 1 or any even number up to 128 */
		if (n!=1 && (n & 0x1))
			continue;

		for (h=11; h>3; h--)
		{
			/** valid values for HSDIV are 4, 5, 6, 7, 9 or 11 */
			if (h==8 || h==10)
				continue;

			fDCO_new =  newFrequency * h * n;
			if ( fDCO_new >= 4850.0 && fDCO_new <= 5670.0 )
			{
				vco_found = 1;
				/** find lowest possible fDCO for this configuration */
				if (fDCO_new<lastfDCO)
				{
					cfg.hs_div = h;
					cfg.n1 =  n;
					cfg.fdco = fDCO_new;
					cfg.rfreq_float = fDCO_new / cfg.fxtal;
					cfg.rfreq_int = (uint64_t)(trunc(cfg.rfreq_float*(1<<28)));
					lastfDCO = fDCO_new;
				}
			}

		}
	}

	if (vco_found==0){
		std::cout << "VCO ERROR" << std::endl;
	}
	//return 0;


}

void RobinNPBIST::applyClockConfig(){

	uint8_t val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, I2C_READ);
	//std::cout << "Freeze Pre: " << val << std::endl;
	val |= FREEZE_DCO;
	//std::cout << "Freeze Post: " << val << std::endl;

	//std::cout << "Write Done" << std::endl;
	val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, I2C_WRITE, val);
	//std::cout << "Freeze Result: " << val << std::endl;

	// write new oscillator values
	uint8_t value = ((cfg.hs_div - 4)<<5) | ((cfg.n1 - 1)>>2);
	//std::cout << "New Oscillator Value Reg 13: " << value << std::endl;

	val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 13, I2C_WRITE, value);
	//std::cout << "New Oscillator Value Reg 13 Result: " << val << std::endl;

	value = (((cfg.n1 - 1) & 0x03)<<6) | (cfg.rfreq_int>>32);
	//std::cout << "New Oscillator Value Reg 14: " << value << std::endl;

	val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 14, I2C_WRITE, value);
	//std::cout << "New Oscillator Value Reg 14 Result: " << val << std::endl;

	// addr 15...18: RFREQ[31:0]
	for(uint8_t i=0; i<=3; i++)
	{
		value = ((cfg.rfreq_int>>((3-i)*8)) & 0xff);
		//std::cout << "New Oscillator Value Reg " << 15 + i << ": " << value << std::endl;
		u_int val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 15 + i, I2C_WRITE, value);
		std::cout << "New Oscillator Value Reg " << 15 + i << " Result: " << val << std::endl;

	}

	uint8_t freeze_val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, I2C_READ);

	//std::cout << "Unfreeze Pre: " << freeze_val << std::endl;
	// clear FREEZE_DCO bit
	freeze_val &= 0xef;
	//std::cout << "Unfreeze Post: " << freeze_val << std::endl;

	u_int res = i2cDualWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, 135,freeze_val, M_NEWFREQ);
	std::cout << "Unfreeze Result: " << res << std::endl;
	// wait for NewFreq to be deasserted
	waitForClockClearance(M_NEWFREQ);

}

void RobinNPBIST::initialiseOscillator(){
	//Reset Oscillator to default frequency (212.5 Mhz) using M_RECALL command
	u_int val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 135, I2C_WRITE, M_RECALL);
	std::cout << "Initialise result: " << std::hex << val << std::dec << std::endl;
	waitForClockClearance(M_RECALL);

}



void RobinNPBIST::configureI2CClock(float newFreq,float defaultFreq){

	initialiseOscillator();

	std::cout << "Get default clock config: " << defaultFreq << " Mhz " << std::endl;
	getClockConfig(defaultFreq);

	std::cout << "Get default clock config based on measured fxtal for desired frequency: " << newFreq << " Mhz " << std::endl;
	computeClockConfig(newFreq);

	std::cout << "Apply new config" << std::endl;
	applyClockConfig();

	std::cout << "Read back clock config for frequency " << newFreq << " Mhz" << std::endl;

	getClockConfig(newFreq);
}

void RobinNPBIST::sLinkTest(unsigned int rolId, bool resetGTX, bool resetHOLA, bool uReset){

	std::cout << "**********************************************" << std::endl;
	std::cout << "* S-Link Test for ROL " << rolId << std::endl;
	std::cout << "**********************************************" << std::endl;


	std::cout << std::hex << "BIST sLinkTest QSFP Check: " << m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPPresent << std::endl;
	std::cout << "BIST sLinkTest QSFP Interrupt: " << m_testCandidate->m_Bar0Common->GlobalStatusRegister.QSFPInterrupt << std::endl;

	std::cout << "Read I2C Register Address: " << &m_testCandidate->m_Bar0Common->I2CRegister << std::endl;

	std::cout << std::hex << "Read I2C Control Register: 0x" << m_testCandidate->m_Bar0Common->I2CRegister.config << std::endl;
	std::cout << "Read I2C Status Register: 0x" << m_testCandidate->m_Bar0Common->I2CRegister.status << std::dec << std::endl;

	std::cout << "BIST sLinkTest Called for ROL " << rolId << std::endl;

	std::cout << "Clear RX Synch Latch" << std::endl;

	std::cout << "BIST sLinkTest RXSynchStatusLatched Pre-Clear = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatusLatched << std::endl;

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched = 1;
	//std::cout << "Read back latch clear " << m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched << std::endl;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched = 0;

	std::cout << "BIST sLinkTest RXSynchStatusLatched Post-Clear = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatusLatched << std::endl;

	if(resetHOLA || resetGTX || uReset){
		std::cout << "Attempt s-link reset for ROL " << rolId << std::endl;
	}

	if(resetHOLA){
		std::cout << "Reset HOLA Core for ROL " << rolId << std::endl;
		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 1;
	}
	if(resetGTX){
		std::cout << "Reset GTX for ROL " << rolId << std::endl;

		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.GTX_Reset = 1;
		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.GTX_Reset = 0;

		while(m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.GTX_ResetDone == 0);
	}
	if(resetHOLA){
		std::cout << "Deassert Reset HOLA Core for ROL " << rolId << std::endl;
		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 0;
	}

	if(resetGTX || resetHOLA){

		auto tsStart=std::chrono::system_clock::now();

		while(m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp == 0){
			auto tsCurrent=std::chrono::system_clock::now();
			auto elapsed=tsCurrent - tsStart;
			if(((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) > 1){
				std::cout << "WARNING: Link Failed to Come Up after HOLA Reset" << std::endl;
				break;
			}
		}
	}

	if(uReset)
	{
		std::cout << "Toggle UReset for ROL " << rolId << std::endl;
		auto tsStart=std::chrono::system_clock::now();

		//m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 1;
		//m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 0;
		//sleep(1);

		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.UReset = 1;
		//std::cout << "Read back UReset: " << m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.UReset << std::endl;
		//std::cout << "Read back UReset: " << std::endl;

		while(m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp == 1){
			auto tsCurrent=std::chrono::system_clock::now();
			auto elapsed=tsCurrent - tsStart;
			if(((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) > 1){
				std::cout << "WARNING: Failed to detect link down after UReset" << std::endl;
				break;
			}
		}
		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.UReset = 0;

		tsStart=std::chrono::system_clock::now();

		while(m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp == 0){
			auto tsCurrent=std::chrono::system_clock::now();
			auto elapsed=tsCurrent - tsStart;
			if(((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) > 1){
				std::cout << "WARNING: Link Failed to Complete Startup" << std::endl;
				break;
			}
		}
	}
	std::cout << "BIST sLinkTest ChannelNumber = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ChannelNumber << std::endl;
	std::cout << "BIST sLinkTest ROLHandler_intUXOF = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.HOLA_UXOFF_n << std::endl;
	std::cout << "BIST sLinkTest ROLHandler_LDCEmulationEnabled = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROLHandler_LDCEmulationEnabled << std::endl;
	std::cout << "BIST sLinkTest ROL_LinkDataError = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkDataError << std::endl;
	std::cout << "BIST sLinkTest ROL_LinkUp = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp << std::endl;
	std::cout << "BIST sLinkTest ROL_FlowControl = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_FlowControl << std::endl;
	std::cout << "BIST sLinkTest ROL_Activity = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_Activity << std::endl;
	std::cout << "BIST sLinkTest ROL_Test = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_Test << std::endl;
	std::cout << "BIST sLinkTest GTX_ResetDone = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.GTX_ResetDone << std::endl;
	std::cout << "BIST sLinkTest TX_PLLLK = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.TX_PLLLK << std::endl;
	std::cout << "BIST sLinkTest RX_PLLLK = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RX_PLLLK << std::endl;
	std::cout << "BIST sLinkTest RXSynchStatus = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatus << std::endl;
	std::cout << "BIST sLinkTest RXSynchStatusLatched = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatusLatched << std::endl;
	std::cout << "BIST sLinkTest RXSynchStatus = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatus << std::endl;

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched = 1;
	std::cout << "Read back latch clear " << m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched << std::endl;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched = 0;
	std::cout << "Read back latch clear " << m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.ClearRXLostSyncLatched << std::endl;
	std::cout << "BIST sLinkTest RXSynchStatus = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatus << std::endl;
	std::cout << "BIST sLinkTest RXSynchStatusLatched = " << m_testCandidate->m_Bar0Channel[rolId]->ChannelStatusRegister.Component.RXSynchStatusLatched << std::endl;
}



bool RobinNPBIST::printSystemMonitorInfo(){
	std::cout << "*************************************************************" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*      BIST: Printing System Monitor Information            *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*                                                           *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	std::cout << std::endl;
	bool result = true;
	std::cout << "FPGA DNA Code (lowest 16 bits): " << std::hex << m_testCandidate->m_Bar0Common->GlobalStatusRegister.DNA << std::dec << std::endl;
	std::cout << std::endl;
	std::cout << "FPGA Core Temperature: " << ((m_testCandidate->m_Bar0Common->GlobalStatusRegister.Temperature * 503.975)/1024) - 273.15 << " degrees celsius"<< std::endl;
	std::cout << std::endl;
	std::cout << "Running Memory Status Check:" << std::endl;
	std::cout << std::endl;

	for(unsigned int subRob = 0; subRob < m_testCandidate->getNumSubRobs(); ++subRob){
		if(m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPhysicalInitDone != 1 || m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPLLLocked != 1){
			std::cout << "ERROR - SubRob " << subRob << " Buffer Memory Initialisation Problem" << std::endl;
			std::cout << "\tStatus of SubRob " << subRob << " Buffer Memory Physical Init: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPhysicalInitDone << std::endl;
			std::cout << "\tStatus of SubRob " << subRob << " Buffer Memory Phase Locked Loop: " << m_testCandidate->m_Bar0SubRob[subRob]->MemoryControllerStatus.MemoryPLLLocked << std::endl;
			result = false;
		}
		else{
			std::cout << "\tSubRob " << subRob << " Buffer Memory Successfully Initialised" << std::endl;
		}
		std::cout << std::endl;
	}
	readMemorySPD();
	readQSFPdata();

	std::cout << "*************************************************************" << std::endl;
	std::cout << "*      BIST: Completed Printing System Monitor Information  *" << std::endl;
	std::cout << "*************************************************************" << std::endl;
	return result;
}

#undef TEST_BIT

std::string RobinNPBIST::readMemoryPartNumber(unsigned int slaveAddr){
	std::string partNumber = "";
	for(unsigned int byte = 128; byte < 146; ++byte){
		char data_r = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, byte, I2C_READ);
		partNumber.append(1, data_r);
	}
	return partNumber;
}

unsigned int RobinNPBIST::readMemoryCapacity(unsigned int slaveAddr){
	unsigned int wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 7, I2C_READ);
	unsigned int numRanks = ((wordRead >> 3) & 0x07) + 1;
	wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 4, I2C_READ);
	unsigned int capacity = numRanks * (1 << ((wordRead & 0x0f) - 2)) * (1 << (wordRead >> 4));
	return capacity;
}

unsigned int RobinNPBIST::readMemoryVendorID(unsigned int slaveAddr){
	unsigned int wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 149, I2C_READ);
	unsigned int vendorID = wordRead << 8;
	wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 148, I2C_READ);
	vendorID |= wordRead & 0xff;
	return vendorID;
}

std::string RobinNPBIST::readMemoryManufactureDate(unsigned int slaveAddr){

	unsigned int wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 120, I2C_READ);
	unsigned int mfrYear = (wordRead >> 4) * 10 + (wordRead & 0x0f);

	wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 121, I2C_READ);
	unsigned int mfrWeek = (wordRead >> 4) * 10 + (wordRead & 0x0f);

	std::stringstream dateStream;
	dateStream << "Week " << std::setw(2) << mfrWeek << ", 20" << std::setfill('0') << std::setw(2) << mfrYear;
	return dateStream.str();
}

unsigned int RobinNPBIST::readMemorySerialNumber(unsigned int slaveAddr){
	unsigned int serialNumber = 0;
	for(unsigned int byte = 122; byte < 126; ++byte) {
		unsigned int wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, byte, I2C_READ);
		serialNumber = (serialNumber << 8) | (wordRead & 0xff);
	}
	return serialNumber;

}

void RobinNPBIST::readMemorySPD(){

	std::cout << "--------------------------" << std::endl;
	std::cout << "Reading Memory Information" << std::endl;
	std::cout << "--------------------------\n" << std::endl;

	std::cout << "Module | Part number        | Vendor | Serial Number | Manufactured  | Capacity |" << std::endl;

	for (unsigned int bank = 0; bank < 2 ; bank++) {
		unsigned int slaveAddr = 0x50 + bank;
		std::cout << bank << "      | " << readMemoryPartNumber(slaveAddr).c_str() << " | " << std::hex << readMemoryVendorID(slaveAddr) << "   | " <<  std::setw(8) << std::left << readMemorySerialNumber(slaveAddr) << std::dec << "      | " << readMemoryManufactureDate(slaveAddr).c_str() << " | " << readMemoryCapacity(slaveAddr) << "GB      |" << std::endl;
	}

	std::cout << std::endl;
	if(m_verboseMode){
		for (unsigned int bank = 0; bank < 2 ; bank++) {
			unsigned int slaveAddr = 0x50 + bank;

			readMemorySpec(slaveAddr);
			std::cout << std::endl;
		}
	}

}

int RobinNPBIST::getHardwareVersion(){
	return m_testCandidate->m_Bar0Common->DesignID.HardwareVersion;
}

void RobinNPBIST::reset(unsigned int memWriteTimeout, unsigned int memReadTimeout, unsigned int memFifoMax){
	std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
	std::ofstream   fout("/dev/null");

	if(!m_verboseMode){
		std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
	}

	m_testCandidate->reset();

	configureMemoryParams(memWriteTimeout,memReadTimeout,memFifoMax);

	for(unsigned int rolId = 0; rolId < m_testCandidate->getNumRols(); ++rolId){
		m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.CRCEnable = 1;
	}
	std::cout.rdbuf(cout_sbuf); // restore the original stream buffer

}

bool RobinNPBIST::initPageFlow(unsigned int pageSize, unsigned int rolId){


	std::cout << "BIST initPageFlow: Initialising page flow for rol " << rolId << ", page size " << pageSize << std::endl;

	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;

	std::cout << "BIST initPageFlow: Initialising Fifo Duplicator Buffer words to 0x99999999" << std::hex << std::endl;

	unsigned int MemorySize = s_4k;
	unsigned int loopCounter = (MemorySize/sizeof(unsigned int));

	for(unsigned int loop = 0; loop < loopCounter; ++loop){
		*(m_testCandidate->m_duplicateUPF[subRob].buffer + loop) = 0x99999999;
	}

	std::cout << "BIST initPageFlow: Disabling Common UPF Duplicator for UPF test" << std::endl;

	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 0;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable != 0)
	{
		std::cout << "BIST initPageFlow FAIL: Unable to de-activate Common UPF Duplicator for SubRob " << subRob << std::endl;
		return false;
	}

	std::cout << "BIST initPageFlow: Disabling Common UPF Multiplexer for UPF test" << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable = 0;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable != 0)
	{
		std::cout << "BIST initPageFlow FAIL: Unable to de-activate Common UPF Multiplexer" << std::endl;
		return false;
	}

	std::cout << "BIST initPageFlow: Initialising ROL " << rolId << std::endl;

	std::cout << "BIST initPageFlow: Disabling ROL " << rolId << " for configuration " << std::endl;

	m_testCandidate->disableRol(rolId);

	std::cout << "BIST initPageFlow: Disabling ROL " << rolId << " data generator " << std::endl;
	// stop data generator
	m_testCandidate->m_Bar0Channel[rolId]->DataGeneratorControlRegister.FragmentSize = 0;	// event size zero

	std::cout << "BIST initPageFlow: Disabling ROL " << rolId << " data generator - set s-link register " << std::endl;

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.StartDataGen = 0;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.SelectDataGen = 0;
	std::cout << "BIST initPageFlow: ROL " << rolId << " data generator stopped" << std::endl;

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.DataGeneratorReset = 1;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.DataGeneratorReset = 0;

	m_testCandidate->resetSubRobChannels();

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.InternalFIFOReset = 1;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.InternalFIFOReset = 0;

	// reset buffer manager via control register
	m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] = bmCtlInitVal | bmCtlReset;	// initialise and turn reset bit on
	std::cout << "BIST initPageFlow: Resetting ROL " << rolId << " Buffer manager, UPF + FPF" << std::endl;

	// update buffer control, de-assert reset bit
	unsigned int bufCtlVal = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] & ~bmCtlReset;
	m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] = bufCtlVal;

	bufCtlVal = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0];
	std::cout << "BIST initPageFlow: Initial value of BufMgrCtl[" << rolId << "] = 0x" << std::hex << bufCtlVal << std::dec << std::endl;
	bufCtlVal &= ~bmCtlSizeMask;

	switch (pageSize){
	case 256:
		bufCtlVal |= bmCtlSize1k;
		break;
	case 512:
		bufCtlVal |= bmCtlSize2k;
		break;
	case 1024:
		bufCtlVal |= bmCtlSize4k;
		break;
	case 2048:
		bufCtlVal |= bmCtlSize8k;
		break;
	case 4096:
		bufCtlVal |= bmCtlSize16k;
		break;
	default:
		std::cout << "BIST initPageFlow FAIL: Invalid page size " << pageSize << "requested, must be 2^n where 8 <= n <= 15";
		return false;
		break;
	}
	// enable/disable S-Link control word transmission
	bufCtlVal |= bmCtlKeepCtl;        // turn bit on to strip
	std::cout << "BIST initPageFlow: Suppressing S-Link control words" << std::endl;

	// update buffer control
	m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] = bufCtlVal;
	std::cout << "BIST initPageFlow: Value of BufMgrCtl[" << rolId << "] = 0x" << std::hex << bufCtlVal << std::dec << std::endl;
	unsigned int bufCtlVal_readback = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0];
	if(bufCtlVal_readback != bufCtlVal){
		std::cout << "BIST initPageFlow FAIL: Unable to set Buffer Manager Control Value - requested 0x" << std::hex << bufCtlVal << " but board responds 0x" << bufCtlVal_readback << std::dec << std::endl;
	}
	else{
		std::cout << "BIST initPageFlow: Buffer Manager for ROL " << rolId << " successfully initialised to 0x" << std::hex << bufCtlVal << std::dec << std::endl;
	}

	m_testCandidate->m_rolArray[rolId].m_runCtlState =  s_runCtlStart;
	std::cout << "BIST initPageFlow: Enabling ROL " << rolId << " for testing" << std::endl;
	m_testCandidate->enableRol(rolId);

	// process activated channels only
	if (s_runCtlStart == m_testCandidate->m_rolArray[rolId].m_runCtlState){

		if (0 == m_testCandidate->m_rolArray[rolId].m_freeTos){
			std::cout << "BIST initPageFlow FAIL: Page Stack Empty for Desired ROL " << rolId << ", check configuration" << std::endl;
			cleanupRol(rolId);
			return false;
		}


		if(m_testCandidate->m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.OneInTheBreech != 0){
			std::cout << "BIST initPageFlow FAIL: Buffer Manager claims to be holding a page, check reset state" << std::endl;
			cleanupRol(rolId);
			return false;
		}
	}
	else{
		std::cout << "BIST initPageFlow FAIL: ROL " << rolId << " inactive, check configuration" << std::endl;
		return false;
	}
	return true;
}

void RobinNPBIST::startDataGenerator(unsigned int fragmentSize, unsigned int rolId){

	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.SelectDataGen = 1;

	// start programmable datagen after S-Link reset
	std::cout << "BIST startDataGenerator: Starting RobinNP Data Generator for rol " << rolId << " with fragment size " << fragmentSize << " words" << std::endl;
	m_testCandidate->m_Bar0Channel[rolId]->DataGeneratorControlRegister.FragmentSize = fragmentSize;
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.StartDataGen = 1;

}

void RobinNPBIST::cleanupRol(unsigned int rolId){
	m_testCandidate->m_Bar0Channel[rolId]->ChannelControlRegister.Component.StartDataGen = 0;
	m_testCandidate->m_rolArray[rolId].m_runCtlState =  s_runCtlStop;
	std::cout << "BIST: Cleanup - Disabling ROL " << rolId << std::endl;
	m_testCandidate->disableRol(rolId);
}

int* RobinNPBIST::loadTestPageToMemory(unsigned int rolId,unsigned int fragmentSize){

	unsigned int pageSize = 1024;

	if(fragmentSize > pageSize){
		std::cout << "BIST: loadTestPageToMemory: Requested fragment larger than page size allowed for test (" << pageSize << ") words)" << std::endl;
		return 0;
	}

	if(!initPageFlow(pageSize,rolId)){
		std::cout << "BIST: loadTestPageToMemory: FAIL - unable to initialise pageflow" << std::endl;
	}

	m_testCandidate->m_Bar0Channel[rolId]->BufferManagerFreeFIFO.lower = 0;

	startDataGenerator(fragmentSize,rolId);

	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;
	if(!enableCommonPageFlow(subRob)){
		std::cout << "BIST: loadTestPageToMemory: FAIL - unable to activate pageflow" << std::endl;
	}

	unsigned int readIndex = 0;
	Timer upftimer(20);
	upftimer.start();
	while(!m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
		if(upftimer.isTimeout()){
			std::cout << "BIST: loadTestPageToMemory: FAIL - timed out waiting for page to return on UPF" << std::endl;
			return 0;
		}
	}

	cleanupRol(rolId);

	return processUPFDuplicatorPageSingleChannel(readIndex,rolId,pageSize);
}

bool RobinNPBIST::enableCommonPageFlow(unsigned int subRob){

	//Enable Combined UPF Multiplexer
	std::cout << std::endl;
	std::cout << "BIST enableCommonPageFlow: Activating Combined UPF Multiplexer" << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable = 1;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable != 1){
		std::cout << "BIST enableCommonPageFlow: Unable to Activate Combined UPF Multiplexer" << std::endl;
		return false;
	}
	std::cout << std::endl;

	//Enable Combined UPF Duplicator
	std::cout << "BIST enableCommonPageFlow: Activating Combined UPF Duplicator" << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 1;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable != 1){
		std::cout << "BIST enableCommonPageFlow: Unable to Activate Combined UPF Duplicator" << std::endl;
		return false;
	}

	return true;
}

void RobinNPBIST::configureMemoryParams(unsigned int writeTimeout, unsigned int readTimeout, unsigned int fifoFill){
	RobinNPConfig config;
	config.setMemWriteTimeout(writeTimeout);
	config.setMemReadTimeout(readTimeout);
	config.setMemfifoFill(fifoFill);
	m_testCandidate->configure(config);
}


bool RobinNPBIST::validateIdlePageFlow(unsigned int rolId, unsigned int pageTestAmount){
	//Validate the flow of pages up to the buffer manager in the absence of any dataflow

	unsigned int fpfFillStatus = m_testCandidate->getFpfFillStatus(rolId);
	unsigned int bufferManagerFill = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.OneInTheBreech;
	if(fpfFillStatus + bufferManagerFill != pageTestAmount){
		std::cout << "BIST validateIdlePageFlow FAIL: FPF Not Holding Required " << pageTestAmount << " Pages. Number of pages = " << fpfFillStatus << ", Buffer Manager Fill Status = " << bufferManagerFill << ".  Checking UPF Status" << std::endl;
		if(m_testCandidate->getUpfFillStatus(rolId) != 0){
			std::cout << "BIST validateIdlePageFlow FAIL: UPF Holding Pages. Dataflow should not be active, check data generator and s-link status" << std::endl;
		}
		else{
			if(fpfFillStatus != m_testCandidate->getFpfFillStatus(rolId)){
				std::cout << "BIST validateIdlePageFlow FAIL: UPF Empty but FPF Fill Still Decreasing, now " << m_testCandidate->getFpfFillStatus(rolId) << " - Pages Lost in DataFlow before UPF - check data generator and s-link status" << std::endl;
			}
			else{
				std::cout << "BIST validateIdlePageFlow FAIL: UPF Empty - Pages Lost in FPF Fill" << std::endl;
			}
		}

		cleanupRol(rolId);
		return false;
	}
	if(m_testCandidate->getUpfFillStatus(rolId) != 0){
		std::cout << "BIST validateIdlePageFlow FAIL: UPF Holding Pages but FPF Still Holding " << pageTestAmount << " -  Dataflow should not be active, check data generator and s-link status" << std::endl;
		cleanupRol(rolId);
		return false;
	}

	std::cout << "BIST validateIdlePageFlow: ROL " << rolId << " Passes FPF fill test - Fill Status = " << fpfFillStatus << ", buffer manager fill status =  " << bufferManagerFill << ", all pages accounted for" << std::endl;
	std::cout << std::endl;


	return true;
}

bool RobinNPBIST::validateActivePageFlow(unsigned int rolId, unsigned int pageTestAmount){
	//Validate the flow of pages through the buffer manager and into the UPF with active dataflow


	unsigned int fpfFillStatus = 0;
	unsigned int upfAlmostFullOffset = 9;

	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;
	unsigned int upfFillStatus = m_testCandidate->getUpfFillStatus(rolId);
	if(upfFillStatus != 0){
		std::cout << "BIST validateActivePageFlow: UPF for ROL " << rolId << " successfully filled to " << upfFillStatus << " entries" << std::endl;
	}
	else{
		std::cout << "BIST validateActivePageFlow FAIL: UPF for ROL " << rolId << " empty, Dataflow blockage, checking FPF" << std::endl;
		fpfFillStatus = m_testCandidate->getFpfFillStatus(rolId);
		if(fpfFillStatus != (pageTestAmount - 1)){
			std::cout << "BIST validateActivePageFlow FAIL: FPF for ROL " << rolId << " not full despite UPF empty - contains " << fpfFillStatus << " pages. Page loss suspected, check buffer configuration" << std::endl;
		}
		else{
			std::cout << "BIST validateActivePageFlow FAIL: FPF for ROL " << rolId << " still full - check data generator and s-link status" << std::endl;
		}
		cleanupRol(rolId);
		return false;
	}

	fpfFillStatus = m_testCandidate->getFpfFillStatus(rolId);
	unsigned int bufferManagerFill = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.OneInTheBreech;

	if(upfFillStatus + fpfFillStatus + bufferManagerFill != pageTestAmount){
		std::cout << "BIST validateActivePageFlow FAIL: Sum of pages in FPF, UPF and Buffer Manager do not equal total, pages lost for ROL " << rolId << std::endl;
		std::cout << "BIST validateActivePageFlow FAIL: FPF Fill " << fpfFillStatus << " entries. UPF Fill " << upfFillStatus << " entries. Buffer Manager Fill " << bufferManagerFill << " entry. Total pages = " << pageTestAmount << std::endl;
		cleanupRol(rolId);
		return false;
	}
	std::cout << std::endl;
	std::cout << "BIST validateActivePageFlow: Activating Combined UPF Multiplexer" << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable = 1;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable != 1)
	{
		std::cout << "BIST validateActivePageFlow FAIL: Unable to activate Common UPF Multiplexer" << std::endl;
		cleanupRol(rolId);
		return false;
	}

	std::cout << "BIST validateActivePageFlow: Wait for 5 seconds to allow pages to flow through FIFOs" << std::endl;
	sleep(5);
	std::cout << "BIST validateActivePageFlow: Continuing..." << std::endl;

	if(m_testCandidate->m_designVersion < 0x1060000){ //Tests below will fail with delay line

		if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFFill == 0){
			std::cout << "BIST validateActivePageFlow FAIL: Pages not moving from ROL " << rolId << " UPF to combined UPF" << std::endl;
			cleanupRol(rolId);
			return false;
		}
		else{
			unsigned int commonUpfFillStatus = m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFFill;
			fpfFillStatus = m_testCandidate->getFpfFillStatus(rolId);
			upfFillStatus = m_testCandidate->getUpfFillStatus(rolId);
			unsigned int bufferManagerFill = m_testCandidate->m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.OneInTheBreech;
			if((fpfFillStatus + upfFillStatus + commonUpfFillStatus + bufferManagerFill) != pageTestAmount){
				unsigned int pageTotal = commonUpfFillStatus + fpfFillStatus + upfFillStatus;
				if(pageTotal<pageTestAmount){
					std::cout << "BIST validateActivePageFlow FAIL: Sum of pages on FPF, UPF, Buffer Manager and Combined UPF do not equal total for ROL " << rolId << ". Pages Lost!" << std::endl;
				}
				if(pageTotal>pageTestAmount){
					std::cout << "BIST validateActivePageFlow FAIL: Sum of pages on FPF, UPF, Buffer Manager and Combined UPF do not equal total for ROL " << rolId << ". Pages Duplicated!" << std::endl;
				}
				std::cout << "BIST validateActivePageFlow FAIL: FPF Fill " << fpfFillStatus << " entries. UPF Fill " << upfFillStatus << " entries. Buffer Manager Fill " << bufferManagerFill << ". Combined UPF Fill " << commonUpfFillStatus << ". Total current pages = " << pageTotal <<  ". Total original pages = " << pageTestAmount << std::endl;
				cleanupRol(rolId);
				return false;
			}
			else if(upfFillStatus != s_upfSize || commonUpfFillStatus != (s_commonUpfSize - upfAlmostFullOffset)){
				unsigned int totalPageCount = upfFillStatus + commonUpfFillStatus + fpfFillStatus + bufferManagerFill;

				std::cout << "BIST validateActivePageFlow FAIL: UPFs do not contain the expected number of events for ROL " << rolId << std::endl;
				std::cout << "BIST validateActivePageFlow FAIL: UPF contains " << upfFillStatus << ", expecting " << s_upfSize << "." << std::endl;
				std::cout << "BIST validateActivePageFlow FAIL: Common UPF contains " << commonUpfFillStatus << ", expecting " << (s_commonUpfSize - upfAlmostFullOffset) << "." << std::endl;
				std::cout << "BIST validateActivePageFlow FAIL: FPF contains " << fpfFillStatus << std::endl;
				std::cout << "BIST validateActivePageFlow FAIL: Buffer Manager contains " << bufferManagerFill << std::endl;

				if(totalPageCount > pageTestAmount){
					std::cout << "BIST validateActivePageFlow FAIL: Total number of pages visible = " << totalPageCount << ", which is greater than expected " << pageTestAmount << ". Pages duplicated!" << std::endl;
				}
				else if(totalPageCount < pageTestAmount){
					std::cout << "BIST validateActivePageFlow FAIL: Total number of pages visible = " << totalPageCount << ", which is less than expected " << pageTestAmount << ". Pages lost!" << std::endl;
				}
				else{
					std::cout << "BIST validateActivePageFlow FAIL: Total number of pages visible = " << totalPageCount << ", which equals expected " << pageTestAmount << ". Pageflow blockage, check data generator setup" << std::endl;
				}
				cleanupRol(rolId);
				return false;
			}
			else{
				std::cout << "BIST validateActivePageFlow: Pages Successfully transferred to Combined UPF without loss, current fill state " << commonUpfFillStatus << std::endl;
			}
		}
	}

	std::cout << std::endl;

	unsigned int readIndex = 0;
	if(m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
		std::cout << "BIST validateActivePageFlow FAIL: FIFO Duplicator for SubRob " << subRob << " appears to be active despite being disabled" << std::endl;
		std::cout << "BIST validateActivePageFlow FAIL: Read Index = " << readIndex << std::endl;
		cleanupRol(rolId);
		return false;
	}
	else{
		std::cout << "BIST validateActivePageFlow: FIFO Duplicator for SubRob " << subRob << " inactive as expected, activating for next stage of test" << std::endl;
	}

	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 1;
	if(m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable != 1)
	{
		std::cout << "BIST validateActivePageFlow FAIL: Unable to activate Common UPF Duplicator" << std::endl;
		cleanupRol(rolId);
		return false;
	}

	return true;
}

void RobinNPBIST::configureInterrupter(unsigned int interrupt){
	std::cout << "BIST configureInterrupter: Interrupt Mask Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST configureInterrupter: Interrupt Mask Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[1] << std::dec << std::endl;
	std::cout << "BIST configureInterrupter: Toggle Common UPF Duplicator Interrupt Bit" << std::endl;
	m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[1] = 0x0;
	m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[0] = 1 << interrupt;
	std::cout << "BIST configureInterrupter: Interrupt Mask Register[0] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[0] << std::dec << std::endl;
	std::cout << "BIST configureInterrupter: Interrupt Mask Register[1] = 0x" << std::hex << m_testCandidate->m_Bar0Common->InterruptMaskRegister.Register[1] << std::dec << std::endl;
	m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
}

void RobinNPBIST::readPageFromMemory(unsigned int subRob, int* results){

	std::cout << "BIST readPageFromMemory: Attempt Read of Memory Buffer to Confirm Fill for page " << results[1] << " with address " << results[2] <<  " and length " << results[3] << std::endl;
	std::cout << std::endl;

	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryOutputFIFOCommsHold = 0;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 1;
	m_testCandidate->m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 0;

	m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0]= m_testCandidate->m_physicalArrayPointer[0]; //Physical Address of Response Area
	m_testCandidate->m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(results[3] *4) | ((uint64_t)(results[2] * 4)) << 32;
	sleep(1);

	unsigned int readCounter = 1;
	while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
		std::cout << "Read " << std::setfill(' ') << std::setw(6) << ((readCounter -1) *4) << ": " << std::hex
				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower
				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper

				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower
				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper

				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower
				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper

				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower
				<< " " << std::setfill('0') << std::setw(8) << m_testCandidate->m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper

				<< std::dec << std::endl;
		readCounter++;
		m_testCandidate->m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;
	}
	std::cout << std::endl;
}

bool RobinNPBIST::initialiseMemory(unsigned int subRob, double size){

	if(size == 0){
		std::cout << "BIST initialiseMemory: ERROR - Requested write size zero" << std::endl;
		return false;
	}

	if(size > 4){
		std::cout << "BIST initialiseMemory: ERROR - Requested size " << size << " GibiBytes exceed memory capacity (4GB)" << std::endl;
		return false;
	}

	std::cout << "BIST InitialiseMemory: Writing control pattern to first " << size << " GibiBytes of Memory for SubRob " << subRob << std::endl;
	m_testCandidate->m_Bar0SubRob[subRob]->SubRobControl.Component.MemoryTestInputFIFOHold = 0;

	unsigned int memSize = 0.1;
	unsigned int numWrites = (unsigned int)ceil(memSize * 1024*1024*1024/32);

	std::cout << "BIST InitialiseMemory: Maximum number of 256bit rows to be written to fill " << memSize << "GiB of memory  = " << numWrites << std::endl;


	m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputAddress.lower = 0;

	Timer readtimer(20);
	readtimer.start();

	unsigned int baseWord = 0x11111111;
	for(unsigned int counter = 0; counter < numWrites; ++counter){
		for(unsigned int intcounter = 1; intcounter < 9; ++intcounter){
			if(counter == numWrites - 1 && intcounter == 8){
				std::cout << "BIST InitialiseMemory: Writing Final Word as EOF" << std::endl;
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputEOF.word = baseWord*intcounter;
			}
			else{
				m_testCandidate->m_Bar0SubRob[subRob]->MemoryTestInputData.word = baseWord*intcounter;
			}

		}
		while(m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull == 1){
			if(readtimer.isTimeout()){
				std::cout << "BIST InitialiseMemory : Test input fifo persistently full" << std::endl;
				return false;
			}
		}
	}
	std::cout << "BIST InitialiseMemory: Test Input FIFO Full Flag = " << m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryTestInputFIFOFull << std::endl;

	return true;
}

bool RobinNPBIST::receiveAndValidatePages(unsigned int rolId, unsigned int pageTestAmount, unsigned int pageSize, bool readMemory){

	unsigned int subRob = m_testCandidate->m_rolArray[rolId].subRobMapping;

	unsigned int mostRecentId = 0;
	int eventId;
	unsigned int receivedPages = 0;
	Timer pageflowtimer(20);
	int lastPage = 0;
	int* results;
	bool indexChange = false;

	unsigned int duplicatorReadIndex = m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateReadIndexRegister.lower;
	unsigned int duplicatorWriteIndex = m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexRegister.lower;

	pageflowtimer.start();

	if(duplicatorReadIndex !=0){
		std::cout << "BIST receiveAndValidatePages FAIL: Initial Duplicator Read Index on RobinNP is non-zero, check reset state. Actual value = " << duplicatorReadIndex << std::endl;
	}
	std::cout << "Check read and write indices at start values: read " <<  duplicatorReadIndex << " write " << duplicatorWriteIndex << std::endl;
	while(receivedPages != pageTestAmount){
		if(duplicatorWriteIndex != m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexRegister.lower && !indexChange){
			indexChange = true;
		}
		if(pageflowtimer.isTimeout()){
			std::cout << "BIST receiveAndValidatePages FAIL: Duplicator timeout while waiting for " << pageTestAmount << " pages for ROL " << rolId << std::endl;
			std::cout << "BIST receiveAndValidatePages FAIL: Received " << receivedPages << " pages for ROL " << rolId << std::endl;
			if(receivedPages == 0 && indexChange){
				std::cout << "BIST receiveAndValidatePages FAIL: Duplicator Write Index on RobinNP changing despite no pages received, check mapping to host memory. Actual value = " << m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateReadIndexRegister.lower << std::endl;
			}
			int commonUpfFillStatus = m_testCandidate->m_Bar0SubRob[subRob]->SubRobStatus.Component.CommonUPFFill;
			unsigned int fpfFillStatus = m_testCandidate->getFpfFillStatus(rolId);
			unsigned int upfFillStatus = m_testCandidate->getUpfFillStatus(rolId);
			int pageTotal = commonUpfFillStatus + fpfFillStatus + upfFillStatus;
			std::cout << "BIST receiveAndValidatePages FAIL: FPF Fill " << fpfFillStatus << " entries. UPF Fill " << upfFillStatus << " entries. Combined UPF Fill " << commonUpfFillStatus << ". Total current pages = " << pageTotal << ". Total original pages = " << pageTestAmount << std::endl;
			cleanupRol(rolId);
			return(false);
		}
		unsigned int readIndex = 0;
		if(!m_testCandidate->checkCombinedUPFWriteIndex(subRob,readIndex)){
			continue;
		}

		pageflowtimer.reset();
		results = processUPFDuplicatorPageSingleChannel(readIndex,rolId,pageSize);
		eventId = results[0];
		if(eventId == -99){
			std::cout << "BIST receiveAndValidatePages FAIL: Fragment Processing Error " << rolId << std::endl;

			cleanupRol(rolId);
			delete[] results;
			return(false);
		}
/*		if(eventId == 0){
			std::cout << "Ring Buffer Address Bottom " << std::hex << (m_testCandidate->m_cardData.upfDuplicateHandle[subRob]& 0xffffffff) << std::endl;
			std::cout << "Ring Buffer Address Top " << ((m_testCandidate->m_cardData.upfDuplicateHandle[subRob]>>32)& 0xffffffff) << std::endl;

			std::cout << "Read back Ring Buffer Address Bottom " << m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.lower << std::endl;
			std::cout << "Read back Ring Buffer Address Top "  << m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.upper << std::endl;

			std::cout << "Write Index Address Bottom " <<  ((m_testCandidate->m_cardData.writeIndexHandle + 0x8*(subRob + 2))& 0xffffffff) << std::endl;
			std::cout << "Write Index Address Top " <<  (((m_testCandidate->m_cardData.writeIndexHandle + 0x8*(subRob + 2))>>32)& 0xffffffff) << std::endl;

			std::cout << "Read back Write Index Address Bottom " << m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.lower << std::endl;
			std::cout << "Read back Write Index Address Top " << m_testCandidate->m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.upper << std::dec << std::endl;


		}
		if(eventId < 1000){
			std::cout << "WI Host " << *(m_testCandidate->m_duplicateUPF[subRob].writeIndexCopy) << std::endl;
			std::cout << "RI Host " << m_testCandidate->m_duplicateUPF[subRob].readIndexCopy << std::endl;
			std::cout << "RI Board " << *(m_testCandidate->m_duplicateUPF[subRob].readIndex) << std::endl;

			std::cout << "Received Fragment " << eventId << " page number " << results[0] << std::endl;
		}*/
		if (m_testCandidate->checkSeqError(mostRecentId, eventId)){
			std::cout << "BIST receiveAndValidatePages FAIL: Fragments out of sync for ROL " << rolId << ". Most Recent ID " << mostRecentId << ", new ID " << eventId << std::endl;

			cleanupRol(rolId);
			delete[] results;
			return(false);
		}
		else{
			mostRecentId = eventId;
			receivedPages++;
		}
		if(lastPage != results[1]){
			std::cout << "BIST receiveAndValidatePages FAIL: Pages out of sync for ROL " << rolId << ". Expected " << lastPage - 1 << ", got " << results[1] << std::endl;

			cleanupRol(rolId);
			delete[] results;
			return(false);
		}
		else{
			lastPage++;
		}

		if(readMemory){
			readPageFromMemory(subRob,results);
		}

		delete[] results;
	}

	return true;
}

void RobinNPBIST::populateFreePageFifo(unsigned int rolId, unsigned int pageTestAmount, unsigned int pageSize, bool verbose){

	std::cout << "BIST populateFreePageFifo: Filling FPF up to " << pageTestAmount << " for ROL " << rolId << std::endl;

	for(unsigned int pageCount = 0; pageCount < pageTestAmount; ++pageCount){
		m_testCandidate->m_Bar0Channel[rolId]->BufferManagerFreeFIFO.lower = (pageCount * pageSize);
		if(verbose) {
			std::cout << "BIST populateFreePageFifo: Writing page " << pageCount << " with location " << pageCount * pageSize << " into FPF" << std::endl;
		}
	}

}

bool RobinNPBIST::getDMAResult(unsigned int subRob, int& readIndex, bool verbose){

	bool interrupted = false;
	unsigned int interrupt = s_DMAInterruptCodes[subRob];

	//Wait for DMA Completion
	readIndex = m_testCandidate->checkDMADoneWriteIndex(subRob);
	if(readIndex == -99){


		if(verbose){
			std::cout << "Wait for Interrupt" << std::endl;
		}

		int ret = ioctl(m_testCandidate->m_devHandle, WAIT_IRQ, &interrupt);
		if (ret)
		{
			std::cout << "BIST Primary DMA Test FAIL: Interrupt IOCTL failed to return success" << std::endl;
			return false;
		}

		if(verbose){
			std::cout << "Got Interrupt" << std::endl;
		}

		interrupted = true;
	}

	readIndex = m_testCandidate->checkDMADoneWriteIndex(subRob);

	if(verbose){
		std::cout << std::hex;
		for(unsigned int element = 0; element < 4; ++element){
			std::cout << "BIST Primary DMA Test: Duplicator element " << element << ": " << m_testCandidate->readDMADoneDuplicator(readIndex,element,subRob) << std::endl;
			std::cout << "BIST Primary DMA Test: Duplicator element shift" << element << ": " << ((m_testCandidate->readDMADoneDuplicator(readIndex,element,subRob) << 8) >> 16) << std::endl;
		}
		std::cout << std::dec << std::endl;
	}

	m_testCandidate->updateDMADoneReadIndex(subRob);

	if(interrupted){
		m_testCandidate->m_Bar0Common->InterruptControlRegister.acknowledge = 1 << interrupt;
	}
	return true;
}

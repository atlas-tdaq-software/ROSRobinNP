#include <string>

//RobinNP headers
#include "ROSRobinNP/RobinNPMonitoring.h"
#include "ROSRobinNP/RobinNPConfig.h"
#include "ROSRobinNP/RobinNPStats.h"

using namespace ROS;

unsigned int align(int size) {
	if ( (size & 15) != 0 )
		size += (16 - (size & 15));
	return size;
}

RobinNPMonitoring::RobinNPMonitoring(unsigned int numRols)
:m_numRols(numRols)
{
}

RobinNPMonitoring::~RobinNPMonitoring(){
}

MgmtPtr* RobinNPMonitoring::getHashArrayPtr(unsigned int Rol){
	return reinterpret_cast<MgmtPtr*>(getRolOffset(Rol)); // primary hash array
}

MgmtEntry* RobinNPMonitoring::getItemListPtr(unsigned int Rol){
	return reinterpret_cast<MgmtEntry*>(getRolOffset(Rol) + align(sizeof(MgmtPtr)*s_numHashLists)); // item list
}

RobinNPStats* RobinNPMonitoring::getStatsPtr(){
	return reinterpret_cast<RobinNPStats*>((unsigned char*)this + align(sizeof(RobinNPMonitoring)) + align(sizeof(RobinNPConfig)));
}

RobinNPConfig* RobinNPMonitoring::getConfigPtr(){
	return reinterpret_cast<RobinNPConfig*>((unsigned char*)this + align(sizeof(RobinNPMonitoring)));
}

unsigned char* RobinNPMonitoring::getRolOffset(unsigned int Rol){
	unsigned int ourSize = align(sizeof(RobinNPMonitoring));
	ourSize += align(sizeof(RobinNPConfig));
	ourSize += align(sizeof(RobinNPStats));

	unsigned int size = (align(sizeof(MgmtEntry)*s_mgmtMaxNumPages) + align(sizeof(MgmtPtr)*s_numHashLists));

	return ((unsigned char*)this + ourSize + (size*Rol));
}

unsigned int RobinNPMonitoring::calculateSharedMemSize(unsigned int numRols){
	unsigned int ourSize = align(sizeof(RobinNPMonitoring));
	ourSize += align(sizeof(RobinNPConfig));
	ourSize += align(sizeof(RobinNPStats));

	//Calculate size of shared memory to allocate for index and statistics block
	unsigned int size = (align(sizeof(MgmtEntry)*s_mgmtMaxNumPages) + align(sizeof(MgmtPtr)*s_numHashLists));

	return ourSize + (size*numRols);
}




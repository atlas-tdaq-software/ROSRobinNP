/*********************************************/
/* file: RobinNP.cpp					     */
/* author: William Panduro Vazquez      RHUL */
/*         (j.panduro.vazquez@cern.ch)       */
/*********************************************/

//C++ headers
#include <iostream>
#include <iomanip>
#include <new>           // This forces new to throw an exception if no memory could be allocated instead of returning 0
#include <fstream>

//System headers
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <unistd.h>
#include <sys/syscall.h>


//TDAQ headers
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"

//RobinNP headers
#include "ROSRobinNP/ROSRobinNPExceptions.h"
#include "ROSRobinNP/RobinNP.h"
#include "ROSRobinNP/RolNP.h"
#include "ROSRobinNP/RobinNPBIST.h"
//#define MEMDEBUG
using namespace ROS;
//#define SLINKERRBITS_TEST
/****************************/
/*  Threading Functions		*/
/****************************/
void* indexer(void *);

void* requestManager(void *);

void* dmaMonitor(void *);

void* freePageManager(void *);

void* directDMAManager(void *);

unsigned int decimalToBinary(unsigned int decimal)
{
	int i = 1, binary = 0;
	while (decimal != 0)
	{
		int remainder = decimal%2;
		//std::cout << " rem " << remainder << std::endl;
		decimal /= 2;
		binary+=remainder*i;
		i*=10;
	}
	return binary;
}

/*************************************************************************/
/*************************************************************************/
/*  					Public Methods									 */
/*************************************************************************/
/*************************************************************************/

/*************************************************************************/
/*          		Construction / Destruction                           */
/*************************************************************************/
#ifdef MEMDEBUG
std::vector<unsigned int> **megastore;
std::vector<bool> **isDeleted;
#endif
/*******************************************/
RobinNP::RobinNP(unsigned int slotNumber, unsigned int runMode, tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue, std::string sharedMemoryName)
/*******************************************/
: m_threadsRunning(false),
  m_slotNumber(slotNumber),
  m_devHandle(0),
  m_numRols(0),
  m_sharedMemoryName(sharedMemoryName+intToString(slotNumber)),
  m_numSubRobs(0),
  m_runMode(runMode),
  m_outputQueue(descriptorQueue),
  m_lockfilename("/var/lock/robinnp/robinnplock_"+intToString(slotNumber)),
  m_subRobsAvailable(0),
  m_writeIndexBase(0),
  m_dmaQueueFIFOLimit(0),
  m_pageSizeOffset(0),
  m_numTicketsPerRol(0),
  m_legacyMode(false),
  m_queuesReady(false),
  m_designVersion(0),
  m_crc_interval(0),
  m_crcinit_direct(0),
  m_crcmask(0),
  m_configuration(0),
  m_filesDumped(0),
  m_fileDumpLimit(0),
  m_deviceAcquired(false)
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP Constructor: Called for card " << slotNumber);
}

void RobinNP::init()
{
	if(m_runMode != runModeGen2ROS && m_runMode != runModeLegacy && m_runMode != runModeROIB){
		CREATE_ROS_EXCEPTION(ex_robinnp_destaa, ROSRobinNPExceptions, RUNMODE, "RobinNP " << m_slotNumber << " running in unknown mode, please select 0 for legacy, 1 for gen2ROS and 2 for ROIB");
		throw(ex_robinnp_destaa);
	}
	initDriverDataContainer();

	acquireDevice();

	m_memSize = RobinNPMonitoring::calculateSharedMemSize(s_maxRols);
	//std::cout << "Shared Memory Size = " << HEX(m_memSize) << std::endl;
	//std::cout << "Max hashlists = " << s_maxHashLists << std::endl;
#define SHAREDMEM
#ifdef SHAREDMEM
	//Allocate shared memory for index and statistics block
	allocateSharedMem();
	m_sharedRobinNPData =  new (m_sharedMemory) RobinNPMonitoring(s_maxRols);
	m_configuration = new(m_sharedRobinNPData->getConfigPtr()) RobinNPConfig();
	m_statistics = new(m_sharedRobinNPData->getStatsPtr()) RobinNPStats(m_slotNumber);
#else
	m_configuration = new RobinNPConfig(s_maxRols);
	m_statistics = new RobinNPStats(m_slotNumber);
#endif

	m_rolVector = new RolNP*[s_maxRols];
	m_Bar0Channel = new BAR0ChannelStruct*[s_maxRols];
	m_Bar0SubRob = new BAR0SubRobStruct*[s_maxSubRobs];

#ifdef MEMDEBUG
	megastore = new std::vector<unsigned int>*[s_maxRols];
	isDeleted = new std::vector<bool>*[s_maxRols];
#endif

	m_rolArray = new rolMembers[s_maxRols];
	for(unsigned int rol = 0; rol < s_maxRols; ++rol){

		m_rolArray[rol].m_freeList = new MgmtPtr[s_mgmtMaxNumPages]; // free entry stack
		m_rolArray[rol].m_rejectedStore = new RejectedEntry[s_mgmtRejectedSize];
		m_rolArray[rol].m_fragPages = 0; // store actual number of pages per fragment
		m_rolArray[rol].pagesInIndex = 0;
		m_rolArray[rol].linkEnabled = true;
		m_rolArray[rol].dataVolumeTransferred= 0;
		m_rolArray[rol].m_startOutputTime = std::chrono::system_clock::now();

#ifdef SHAREDMEM
		m_rolArray[rol].m_hashArray = m_sharedRobinNPData->getHashArrayPtr(rol); // primary hash array
		m_rolArray[rol].m_itemList = m_sharedRobinNPData->getItemListPtr(rol); // item list
#else
		m_rolArray[rol].m_itemList = new MgmtEntry[s_mgmtMaxNumPages]; // item list
		m_rolArray[rol].m_hashArray = new MgmtPtr[s_maxHashLists];
#endif

		m_rolArray[rol].m_hashSpinlock = new pthread_spinlock_t[s_numHashLists];
		m_rolArray[rol].m_hashMutex = new pthread_mutex_t[s_numHashLists];

		m_rolArray[rol].m_fragmentToProcess = new MgmtEntry*[s_bmMaxFragPages];
		m_rolArray[rol].m_fragmentToProcessAddress = new unsigned long[s_bmMaxFragPages];

		//Initialise legacy mode rolVector
		m_rolVector[rol] = newRol(rol);

		pthread_spin_init(&m_rolArray[rol].m_pageStackSpinlock, PTHREAD_PROCESS_PRIVATE);
		pthread_mutex_init(&m_rolArray[rol].m_pageStackMutex, NULL);
		pthread_mutex_init(&m_rolArray[rol].m_ecrMutex, NULL);
		for(unsigned int hashList = 0; hashList < s_numHashLists; ++hashList){
			pthread_spin_init(&m_rolArray[rol].m_hashSpinlock[hashList], PTHREAD_PROCESS_PRIVATE);
			pthread_mutex_init(&m_rolArray[rol].m_hashMutex[hashList], NULL);
		}
#ifdef MEMDEBUG
		CREATE_ROS_EXCEPTION(ex_robinnp_desta, ROSRobinNPExceptions, OPEN, "RobinNP running in memory debug mode");
		ers::warning(ex_robinnp_desta);
		megastore[rol] = new std::vector<unsigned int>(10000000);
		isDeleted[rol] = new std::vector<bool>(10000000);
#endif
	}

#ifdef MEMDEBUG
	CREATE_ROS_EXCEPTION(ex_robinnp_desta, ROSRobinNPExceptions, OPEN, "RobinNP running in memory debug mode");
	ers::warning(ex_robinnp_desta);
#endif

	//Open RobinNP and Map Initial Memory Location
	initDevice();


	//m_subRobArray = new subRobMembers[s_maxSubRobs];

	m_duplicateDMADone = new Duplicator[m_numSubRobs];
	m_duplicateUPF = new Duplicator[m_numSubRobs];

	m_DMADoneActiveQueueReadIndex = new volatile unsigned int[m_numSubRobs];
	m_DMADoneActiveQueueWriteIndex = new volatile unsigned int[m_numSubRobs];

	m_DMADoneActiveCaptureQueueReadIndex = new volatile unsigned int[m_numSubRobs];
	m_DMADoneActiveCaptureQueueWriteIndex = new volatile unsigned int[m_numSubRobs];

	m_pageSizeOffset= 9; // default for 512 word pages

	m_concurrentRequestQueue = new tbb::concurrent_bounded_queue<RequestQueueElement>*[m_numSubRobs];

	m_DMADoneActiveQueue = new DMATrackingQueueElement*[m_numSubRobs];
	m_DMADoneActiveCaptureQueue = new DMATrackingQueueElement*[m_numSubRobs];

	m_indexer_thread = new pthread_t[m_numSubRobs];
	m_requestManager_thread = new pthread_t[m_numSubRobs];
	m_dmaMonitor_thread = new pthread_t[m_numSubRobs];
	m_freePageManager_thread = new pthread_t[m_numSubRobs];
	m_directDMAManager_thread = new pthread_t[m_numSubRobs];
	m_captureQueue = new std::vector<DataPage*>*[m_numSubRobs];

	m_queuesReady = false;
	m_filesDumped = 0;
	m_errorsDumped = 0;

	//Initialise request queues
	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		m_DMADoneActiveQueueReadIndex[subRob] = 0;
		m_DMADoneActiveQueueWriteIndex[subRob] = 0;
		m_DMADoneActiveCaptureQueueReadIndex[subRob] = 0;
		m_DMADoneActiveCaptureQueueWriteIndex[subRob] = 0;
		m_numDMAsPending[subRob] = 0;
		m_numCaptureDMAsPending[subRob] = 0;
		m_requestManagerRunState[subRob] = false;
		m_indexerRunState[subRob] = false;
		m_dmaMonitorRunState[subRob] = false;
		m_freePageManagerRunState[subRob] = false;
		m_directDMAManagerRunState[subRob] = false;
		m_subRobsActive[subRob] = false;
		m_hdrContainer[subRob] = new unsigned int[(sizeof(RobMsgHdr) + sizeof(RodFragment) + sizeof(unsigned int))*s_maxRols]; // space for headers + appended CRC word
		m_concurrentRequestQueue[subRob] = new tbb::concurrent_bounded_queue<RequestQueueElement>;
		m_DMADoneActiveQueue[subRob] = new DMATrackingQueueElement[s_DMAQueueFIFOSize];
		m_DMADoneActiveCaptureQueue[subRob] = new DMATrackingQueueElement[s_captureDMAQueueFIFOSize];
		pthread_mutex_init(&m_stackCondMutex[subRob],NULL);
		pthread_mutex_init(&m_dmaSlotCondMutex[subRob],NULL);
		pthread_mutex_init(&m_fpfCondMutex[subRob],NULL);
		pthread_mutex_init(&m_captureDMASlotCondMutex[subRob],NULL);
		pthread_cond_init(&m_fpfCond[subRob], NULL);
		pthread_cond_init(&m_dmaSlotCond[subRob], NULL);
		pthread_cond_init(&m_stackCond[subRob], NULL);
		pthread_cond_init(&m_captureDMASlotCond[subRob], NULL);
		m_stackWait[subRob].tv_sec = 0;
		m_stackWait[subRob].tv_nsec = 0;
		m_fpfWait[subRob].tv_sec = 0;
		m_fpfWait[subRob].tv_nsec = 0;
		m_DMAProcessContainer[subRob] = new DMAInfo[s_DMAQueueFIFOSize];
		m_captureDMAProcessContainer[subRob] = new DMAInfo[s_captureDMAQueueFIFOSize];
		m_captureQueue[subRob] = new std::vector<DataPage*>;
	}

	//Reset Board to ensure all Registers in Initial State
	reset();

	//Run Main Initialisation
	initSequence();

	initCRCTable();

	pthread_mutex_init(&m_screenPrintMutex, NULL);
	pthread_mutex_init(&m_fileDumpMutex,NULL);

	initFragStatusMap();

	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::init: OK");

}

/*************/
RobinNP::~RobinNP()
/*************/
{

	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::destructor: Start of function");

	if (m_deviceAcquired) {

		if (m_threadsRunning) {
			stopThreads();
		}

		if (m_legacyMode) {
			closeLegacyControl();
		}

		//Deallocate process specific memory
		for (int page = 0; page < m_cardData.nDmaPages; ++page) {
			int ret = munmap((void *) m_virtualArrayPointer[page],
					m_cardData.dmaPageSize);
			if (ret) {
				CREATE_ROS_EXCEPTION(ex_robinnp_dest1, ROSRobinNPExceptions,
						MUNMAP,
						"A call to munmap for RobinNP " << m_slotNumber << " returns error: " << strerror(errno));
				ers::warning(ex_robinnp_dest1);
			}
		}

		delete[] m_virtualArrayPointer;
		delete[] m_physicalArrayPointer;

		delete[] m_indexer_thread;
		delete[] m_requestManager_thread;
		delete[] m_dmaMonitor_thread;
		delete[] m_freePageManager_thread;
		delete[] m_directDMAManager_thread;

		/*Unmap FIFO Duplicator Memory Structure*/
		unmapFifoDuplicator();

		delete[] m_duplicateDMADone;
		delete[] m_duplicateUPF;

		delete[] m_DMADoneActiveQueueReadIndex;
		delete[] m_DMADoneActiveQueueWriteIndex;

		delete[] m_DMADoneActiveCaptureQueueReadIndex;
		delete[] m_DMADoneActiveCaptureQueueWriteIndex;

		for (unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob) {
			delete[] m_hdrContainer[subRob];
			delete m_concurrentRequestQueue[subRob];
			delete[] m_DMADoneActiveQueue[subRob];
			delete[] m_DMADoneActiveCaptureQueue[subRob];
			pthread_mutex_destroy(&m_dmaSlotCondMutex[subRob]);
			pthread_mutex_destroy(&m_stackCondMutex[subRob]);
			pthread_mutex_destroy(&m_fpfCondMutex[subRob]);
			pthread_mutex_destroy(&m_captureDMASlotCondMutex[subRob]);
			pthread_cond_destroy(&m_dmaSlotCond[subRob]);
			pthread_cond_destroy(&m_stackCond[subRob]);
			pthread_cond_destroy(&m_fpfCond[subRob]);
			pthread_cond_destroy(&m_captureDMASlotCond[subRob]);

			while (!(m_captureQueue[subRob])->empty()) {
				DataPage* page = m_captureQueue[subRob]->back();
				delete page;
				m_captureQueue[subRob]->pop_back();
			}
			delete m_captureQueue[subRob];

			if (m_queuesReady) {
				while (!((*m_pageQueues)[m_subRobIndex[subRob]]->empty())) {
					DataPage* tempPtr;
					(*m_pageQueues)[m_subRobIndex[subRob]]->pop(tempPtr);
					delete tempPtr;
				}
				delete (*m_pageQueues)[m_subRobIndex[subRob]];

			}

			delete[] m_DMAProcessContainer[subRob];
			delete[] m_captureDMAProcessContainer[subRob];

		}

		delete[] m_concurrentRequestQueue;
		delete[] m_DMADoneActiveQueue;
		delete[] m_DMADoneActiveCaptureQueue;
		delete[] m_captureQueue;

		// Delete the ROL objects of this Robin.
		for (unsigned int rol = 0; rol < s_maxRols; rol++) {
			if (m_rolVector[rol] != 0) {
				DEBUG_TEXT(DFDB_ROSROBINNP, 20, "RobinNP::destructor: Deleting ROL " << rol);
				delete m_rolVector[rol];
				m_rolVector[rol] = 0;
			}
		}

		delete[] m_rolVector;
		delete[] m_Bar0Channel;
		delete[] m_Bar0SubRob;
		//delete[] m_fragStatusMap;

		for (unsigned int rol = 0; rol < s_maxRols; ++rol) {

			delete[] m_rolArray[rol].m_freeList;
			delete[] m_rolArray[rol].m_rejectedStore;

#ifndef SHAREDMEM
			delete[] m_rolArray[rol].m_itemList;
			delete[] m_rolArray[rol].m_hashArray;
#endif

			delete[] m_rolArray[rol].m_fragmentToProcessAddress;
			delete[] m_rolArray[rol].m_fragmentToProcess;

			pthread_spin_destroy(&m_rolArray[rol].m_pageStackSpinlock);
			pthread_mutex_destroy(&m_rolArray[rol].m_pageStackMutex);
			pthread_mutex_destroy(&m_rolArray[rol].m_ecrMutex);
			for (unsigned int hashList = 0; hashList < s_numHashLists;
					++hashList) {
				pthread_spin_destroy(&m_rolArray[rol].m_hashSpinlock[hashList]);
				pthread_mutex_destroy(&m_rolArray[rol].m_hashMutex[hashList]);
			}
			delete[] m_rolArray[rol].m_hashSpinlock;
			delete[] m_rolArray[rol].m_hashMutex;

			//	delete pageStore1[rol];
		}

		//delete pageStore1;
		delete[] m_rolArray;

		while (!m_dataPageVector.empty()) {
			delete m_dataPageVector.back();
			m_dataPageVector.pop_back();
		}

#ifdef SHAREDMEM
		//Have to explicitly call destructors due to use of placement new
		m_configuration->~RobinNPConfig();
		m_statistics->~RobinNPStats();
		m_sharedRobinNPData->~RobinNPMonitoring();
		deallocateSharedMem();
#else
		delete m_configuration;
#endif

		int ret = munmap((void *) m_bar0Addr, m_cardData.registerSize);
		if (ret) {
			CREATE_ROS_EXCEPTION(ex_robinnp_dest2, ROSRobinNPExceptions, MUNMAP,
					"A call to munmap for RobinNP " << m_slotNumber << " BAR0 returns error: " << strerror(errno));
			ers::warning(ex_robinnp_dest2);
		}

		pthread_mutex_destroy(&m_screenPrintMutex);
		pthread_mutex_destroy(&m_fileDumpMutex);
	}

	int ret = close(m_devHandle);
	if (ret < 0){
		CREATE_ROS_EXCEPTION(ex_robinnp_dest3, ROSRobinNPExceptions, CLOSE, "Error from call to close m_dev_handle for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_dest3);
	}



	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::destructor: End of function");

}

/*************************************************************************/
/*                         Board initialisation stuff                    */
/*************************************************************************/


/*****************/
void RobinNP::reset()
/*****************/
{
	/*std::cout << std::endl;
	std::cout << "RobinNP::reset - Performing global reset and setup" << std::endl;*/
	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::reset: Start of function");

	//Reset Board Using Global Control Registers
	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::reset: Reset Board using Global Control Register");
	m_Bar0Common->GlobalControlRegister.GlobalReset = 1;

	m_Bar0Common->GlobalControlRegister.GlobalReset = 0;

	sleep(1); //double delete workaround for inability to reset UPF delay, need to wait for pages to flow to common upf before resetting again

	m_Bar0Common->GlobalControlRegister.GlobalReset = 1;

	m_Bar0Common->GlobalControlRegister.GlobalReset = 0;

	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::reset: Set Board registers to required startup values");
	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){

		//Enable Primary DMA
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.Enable = 1;

		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.TerminationWordDMAEnable = 0;

		//Enable Common UPF Multiplexer
		m_Bar0SubRob[subRob]->SubRobControl.Component.FIFOMuxEnable = 1;

		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 1;
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 0;

		//Disable DMA RAM Mode
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.PrimaryDMARAMModeEnable = 0;

		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.IgnoreDMADoneFull = 0;


		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 0;

	}

	//Reset HOLA core on all links
	for(unsigned int rolId = 0; rolId < m_numSubRobs; ++rolId){

		m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 1;

		m_Bar0Channel[rolId]->ChannelControlRegister.Component.HOLA_Reset = 0;
	}


	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::reset: End of function");
}

/*************************************************************************/
/*                         Memory Access                                 */
/*************************************************************************/

/****************************/
unsigned char ** RobinNP::getVirtArray()
/****************************/
{
	return m_virtualArrayPointer;
}

/****************************/
unsigned long *RobinNP::getPhysArray()
/****************************/
{
	return m_physicalArrayPointer;
}

unsigned int RobinNP::getNumDMAPages(){
	return m_cardData.nDmaPages;
}
/*************************************************************************/
/*                         DMA Completion Adapter                        */
/*************************************************************************/

/************************************/
bool RobinNP::receiveDMA(unsigned int rol, unsigned int ticket)
/************************************/
{
	pthread_mutex_lock(&m_rolArray[rol].m_ticketCondMutex[ticket]);
	if(m_rolArray[rol].m_ticketIndex[ticket] == false){
		pthread_cond_wait(&m_rolArray[rol].m_ticketCond[ticket],&m_rolArray[rol].m_ticketCondMutex[ticket]);
	}
	pthread_mutex_unlock(&m_rolArray[rol].m_ticketCondMutex[ticket]);
	return true;
}

/*************************************************************************/
/*                         ROLs                                          */
/*************************************************************************/

/*****************************/
RolNP& RobinNP::getRol(unsigned int rolId)
/*****************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15, "RobinNP::getRol: called with rolId = " << rolId);

	if (rolId > m_numRols - 1)
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_getrol1, ROSRobinNPExceptions, NROLS, "Number of requested ROLs exceeded maximum! Requested ROL ID "
				<< rolId << " but RobinNP " << m_slotNumber << " only supports max ROL ID " << m_numRols - 1 << " (" << m_numRols << " ROLs in total)");
		throw (ex_robinnp_getrol1);
	}

	// Get the ROL object from the array
	RolNP *ret = m_rolVector[rolId];
	if (ret == 0)
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_getrol2, ROSRobinNPExceptions, NROLS, "Requested ROL is not available for RobinNP " << m_slotNumber);
		throw (ex_robinnp_getrol2);
	}

	return *ret;
}

/************************/
unsigned int RobinNP::getNumRols()
/************************/
{
	return m_numRols;
}

/************************/
unsigned int RobinNP::getNumSubRobs()
/************************/
{
	return m_numSubRobs;
}

/*************************************************************************/
/*                        Misc                                           */
/*************************************************************************/

/****************************/
unsigned int RobinNP::getIdentity()
/****************************/
{
	return(m_slotNumber);
}

void RobinNP::linkReset(unsigned int rolId){

	std::cout << "Toggle UReset for ROL " << rolId << std::endl;
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.UReset = 1;
	auto tsStart=std::chrono::system_clock::now();
	/*
	while(m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp == 1){
				auto tsCurrent=std::chrono::system_clock::now();
		auto elapsed=tsCurrent - tsStart;
		if(((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) > 1){
			CREATE_ROS_EXCEPTION(ex_robinnp_linkreset1, ROSRobinNPExceptions, LINKRESET, "RobinNP::linkReset Failed to detect s-link down after UReset for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_linkreset1);
			break;
		}
	}
	 */
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.UReset = 0;

	tsStart=std::chrono::system_clock::now();

	while(m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp == 0){
		auto tsCurrent=std::chrono::system_clock::now();
		auto elapsed=tsCurrent - tsStart;
		if(((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) > 1){
			CREATE_ROS_EXCEPTION(ex_robinnp_linkreset2, ROSRobinNPExceptions, LINKRESET, "RobinNP::linkReset ROL " << rolId << " s-link failed to come up after UReset for RobinNP " << m_slotNumber);
			ers::error(ex_robinnp_linkreset2);
			break;
		}
	}
}

/********************************/
int RobinNP::restartRol(unsigned int rolId)
/********************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::restartRol: Called for ROL " << rolId);
	initRol(rolId);
	// clear discard mode
	m_rolArray[rolId].linkEnabled = true;


	if(m_configuration->getRolConfig(rolId)->getRolEnabled()){
		m_rolArray[rolId].m_runCtlState = s_runCtlStart;
	}

	m_Bar0Channel[rolId]->ChannelControlRegister.Component.InternalFIFOReset = 1;
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.InternalFIFOReset = 0;

	//Reset XOFF counter here as InternalFIFOReset causes counter to increment as reset is processed
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroXoffCounter = 1;
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroXoffCounter = 0;

	if(m_configuration->getRolConfig(rolId)->getRolDataGen()){
		//std::cout << "Resetting Data Generator for ROL " << rolId << std::endl;
		m_Bar0Channel[rolId]->ChannelControlRegister.Component.DataGeneratorReset = 1;
		m_Bar0Channel[rolId]->ChannelControlRegister.Component.DataGeneratorReset = 0;
		initDataGenerator(rolId);
	}
	else {
		if (s_runCtlStart == m_rolArray[rolId].m_runCtlState){
			// toggle S-Link reset
			linkReset(rolId);
		}
	}

	if (s_runCtlStart == m_rolArray[rolId].m_runCtlState){
		enableRol(rolId);
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::restartRol: ROL " << rolId << " enabled");
	} else {
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::restartRol: ROL " << rolId << " disabled");
	}
	return 0;
}

/*****************************/
unsigned int* RobinNP::getECR(int rolId)
/*****************************/
{
	pthread_mutex_lock(&m_rolArray[rolId].m_ecrMutex);
	//Assemble event log
	int numLogEntries = m_statistics->m_rolStats[rolId].m_eventLog.current;
	unsigned int *logDest = new unsigned int[numLogEntries + 3];

	// copy configuration
	logDest[0] = m_statistics->m_rolStats[rolId].m_mostRecentId;
	logDest[1] = numLogEntries;
	logDest[2] = m_statistics->m_rolStats[rolId].m_eventLog.overflow;

	// copy event log entries
	for (int entry = 0; entry < numLogEntries; ++entry){
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::getECR: Current LOG ITEM is: 0x" << HEX(m_statistics->m_rolStats[rolId].m_eventLog.eventArray[entry]));
		logDest[entry + 3] = m_statistics->m_rolStats[rolId].m_eventLog.eventArray[entry];
	}

	// reset event log
	m_statistics->m_rolStats[rolId].m_eventLog.current = 0;
	m_statistics->m_rolStats[rolId].m_eventLog.overflow = 0;
	pthread_mutex_unlock(&m_rolArray[rolId].m_ecrMutex);

	return logDest;
}

/**************************************/
RolNP *RobinNP::getRolVector(int rolId){
	/**************************************/
	return m_rolVector[rolId];
}

/*************************************************************************/
/*                        Statistics                                     */
/*************************************************************************/

RobinNPStats* RobinNP::getGeneralStatistics(){
	return m_statistics;
}


/***********************************************/
RobinNPROLStats* RobinNP::getROLStatistics(unsigned int rolId)
/***********************************************/
{
	// update various monitoring information
	calcPageCount(rolId); // update used pages counter





	m_statistics->m_rolStats[rolId].m_rolDownStat = m_Bar0Channel[rolId]->ChannelStatusRegister.Component.ROL_LinkUp?0:1;
	m_statistics->m_rolStats[rolId].m_rolXoffStat = m_Bar0Channel[rolId]->ChannelStatusRegister.Component.HOLA_UXOFF_n?0:1;
	m_statistics->m_rolStats[rolId].m_ldowncount = (uint8_t)(m_Bar0Channel[rolId]->ChannelStatusRegister.Component.LinkDownCount);
	m_statistics->m_rolStats[rolId].m_xoffcount = m_Bar0Channel[rolId]->ChannelStatusRegister.Component.XoffCount;
	m_statistics->m_rolStats[rolId].m_xoffpercentage = (m_Bar0Channel[rolId]->ChannelStatusRegister.Component.XoffRatio/2.55);
	m_statistics->m_rolStats[rolId].m_rolInputBandwidth = (m_Bar0Channel[rolId]->ChannelStatusRegister.Component.UsageRatio*250.0/255);

	m_rolArray[rolId].m_endOutputTime = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = m_rolArray[rolId].m_endOutputTime - m_rolArray[rolId].m_startOutputTime;
	m_statistics->m_rolStats[rolId].m_rolOutputBandwidth = m_rolArray[rolId].dataVolumeTransferred/(1024*1024*elapsed_seconds.count());

	if(m_statistics->m_rolStats[rolId].m_rolInputBandwidth == 0){
		m_statistics->m_rolStats[rolId].m_readoutFraction = 0;
	}
	else{
		m_statistics->m_rolStats[rolId].m_readoutFraction = m_statistics->m_rolStats[rolId].m_rolOutputBandwidth/m_statistics->m_rolStats[rolId].m_rolInputBandwidth;
	}

	m_rolArray[rolId].dataVolumeTransferred = 0;
	m_rolArray[rolId].m_startOutputTime = m_rolArray[rolId].m_endOutputTime;
	return m_statistics->getROLStats(rolId);
}

/*************************************************************************/
/*                        Requests                                       */
/*************************************************************************/

/***********************************/
void *RobinNP::requestManagerMain(unsigned int subRob)
/***********************************/
{
	//Top level process for Request Manager Thread
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::requestManagerMain: Starting Request Manager Thread");

	m_requestManagerRunState[subRob] = true;
	m_dmaFull[subRob] = false;

	while(m_requestManagerRunState[subRob]){

		awaitFreeDMASlots(subRob);

		RequestQueueElement request;
		m_concurrentRequestQueue[subRob]->pop(request);
		if(!request.terminate){
			processFragmentRequest(request);
		}
		else{
			break;
		}
	}

	std::stringstream ss;
	ss << "RobinNP::requestManagerMain: Exiting Request Manager Thread for SubRob " << subRob << ".";
	screenPrint(ss.str());

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::requestManagerMain: Exiting Request Manager Thread");
	return 0;
}

void RobinNP::memoryErrorCatcher(unsigned int rolId, unsigned long virtualAddress, unsigned int eventID, unsigned int memAddress){

	unsigned int subRob = m_rolArray[rolId].subRobMapping;
	unsigned int *testPtr = (unsigned int*)virtualAddress;
	bool bypass = true;

	if(bypass && ((eventID != *(testPtr + 5)))){
		pthread_mutex_lock(&m_screenPrintMutex);
		std::cout << std::hex << "ROL " << rolId << ", expected " << eventID << " got " << *(testPtr + 5) <<  std::dec << std::endl;

		std::cout << std::hex << std::endl;
		for(unsigned int location = 0; location < (512); ++location){
			std::cout << std::setfill('0') << std::setw(8) << *(testPtr + location) << " ";
			if((location + 1) % 16 == 0){
				std::cout << std::endl;
			}
		}
		std::cout << std::endl;
		unsigned int rolReceived = (*(testPtr + 10)) >> 28;
		std::cout << "actual ROL received " << rolReceived  << std::endl;
		std::cout << "page address for requested page from ROL " << rolId << " = " << memAddress << std::endl;
		std::cout << "page byte address in hex: 0x" << std::hex << (memAddress * 4) << std::dec << std::endl;
		std::cout << "page byte address in hex / 2: 0x" << std::hex << (memAddress * 2) << std::dec << std::endl;
		std::cout << "page byte address in hex / 4: 0x" << std::hex << (memAddress) << std::dec << std::endl;
		std::cout << "page byte address in hex / 8: 0x" << std::hex << (memAddress / 2) << std::dec << std::endl;
		std::cout << "page byte address in hex / 16: 0x" << std::hex << (memAddress / 4) << std::dec << std::endl;
		std::cout << "page byte address in hex / 32: 0x" << std::hex << (memAddress / 8) << std::dec << std::endl;
		m_requestManagerRunState[subRob] = false;
		m_directDMAManagerRunState[subRob] = false;
		sleep(5);
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 1;
		sleep(1);
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.AbortCurrentDMAs = 0;
		sleep(5);
		m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DMAQueueIgnore = 1;
		std::cout << "Empty Fifos for test" << std::endl;
		int readCounter = 1;
		while(m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
			std::cout << "Read " << std::setfill(' ') << std::setw(6) << ((readCounter -1) *4) << ": " << std::hex
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper

					<< std::dec << std::endl;
			readCounter++;
			m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;

		}
		std::cout << std::endl;
		std::cout << "Primary DMA Status Register Status for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.Status << std::dec << std::endl;
		std::cout << "Primary DMA Status Register QueueFIFOFillError for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFillError << std::dec << std::endl;
		std::cout << "Primary DMA Status Register DoneFIFOFill for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFillError << std::dec << std::endl;
		std::cout << "Primary DMA Status Register DoneFIFOEmpty for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.DoneFIFOEmpty << std::dec << std::endl;
		std::cout << "Primary DMA Status Register DoneFIFOFull for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.DoneFIFOFull << std::dec << std::endl;
		std::cout << "Primary DMA Status Register QueueFIFODataAvailable for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFODataAvailable << std::dec << std::endl;
		std::cout << "Primary DMA Status Register QueueFIFOFill for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFill << std::dec << std::endl;
		std::cout << "Primary DMA Status Register QueueFIFOFull for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.QueueFIFOFull << std::dec << std::endl;
		std::cout << "Primary DMA Status Register StateIsIdle for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsIdle << std::dec << std::endl;
		std::cout << "Primary DMA Status Register StateIsWaitingForStart for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsWaitingForStart << std::dec << std::endl;
		std::cout << "Primary DMA Status Register StateIsWaitingForEnd for SubRob " << subRob << " = 0x" << std::hex <<m_Bar0SubRob[subRob]->PrimaryDMAStatusRegister.Component.StateIsWaitingForEnd << std::dec << std::endl;

		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressLower = 0x1; //Physical Address of Response Area
#if __x86_64__
		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressUpper = s_dmaOffsetTWord ;
#else
		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.pciAddressUpper = 0;
#endif
		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.transferSize = 512 * 4;
		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Component.localAddress = memAddress * 4;

		sleep(1);

		readCounter = 1;
		while(m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
			std::cout << "Read " << std::setfill(' ') << std::setw(6) << ((readCounter -1) *4) << ": " << std::hex
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_0.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_1.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_2.upper

					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.lower
					<< " " << std::setfill('0') << std::setw(8) << m_Bar0SubRob[subRob]->MemoryOutputSecondFIFO_3.upper

					<< std::dec << std::endl;
			readCounter++;
			m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;

		}

		pthread_mutex_unlock(&m_screenPrintMutex);

		exit(1);


	}
}


/***********************************/
void *RobinNP::dmaMonitorMain(unsigned int subRob)
/***********************************/
{
	//Top level process for DMA Monitor Thread
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::dmaMonitorMain: Starting DMA Monitor Thread");
	m_dmaMonitorRunState[subRob] = true;

	unsigned int interrupt = s_DMAInterruptCodes[subRob];

	int readIndex;
	bool interrupted = false;
	bool fatalError = false;

	while(m_dmaMonitorRunState[subRob]){

		readIndex = checkDMADoneWriteIndex(subRob);
		if(readIndex == -99){
			int ret = ioctl(m_devHandle, WAIT_IRQ,&interrupt);
			if(ret){
				CREATE_ROS_EXCEPTION(ex_robinnp_dmamonitormain1, ROSRobinNPExceptions, INTERRUPTFAIL, "RobinNP::dmaMonitorMain: Interrupt " << interrupt << " failure: " << strerror(errno) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_dmamonitormain1);
			}
			interrupted = true;
		}

		while(1){
			readIndex = checkDMADoneWriteIndex(subRob);

			if(readIndex == -99){
				break;
			}
			unsigned int descriptorInfo = (readDMADoneDuplicator(readIndex,0,subRob) & 0xFFFF);

			if((descriptorInfo & 0xFFF) >= s_captureIndexOffset){

				if((descriptorInfo & 0xFFF) != s_captureCountResetCtrlValue){
					unsigned int descriptor = ((descriptorInfo & 0xFFF) - s_captureIndexOffset);
					while(m_DMADoneActiveCaptureQueueReadIndex[subRob] == m_DMADoneActiveCaptureQueueWriteIndex[subRob]);
					unsigned int numDMAs = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].numDMAs;
					unsigned int index = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].index;
					unsigned int fragSize = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].transferSize;
					unsigned int *fragPtr = (unsigned int*)m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].virt;
					unsigned int rolId = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].rolID;
					unsigned int l1Id = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].eventID;

					std::string errorDescription = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].text;
					if((index != descriptor) && !fatalError){
					  std::stringstream ss;
					  ss << "RobinNP::dmaMonitorMain: Capture DMA Synchronisation Error: Index " << index << " but DMA reports ID " << descriptor << " Num DMAs completed this turn " << numDMAs << ". All subsequent transfers may be affected. Please restart ROS application!" << std::endl;
					  CREATE_ROS_EXCEPTION(ex_robinnp_dmasync1, ROSRobinNPExceptions, BADID, ss.str()); //Temporarily use BADID type to avoid header change
					  ers::fatal(ex_robinnp_dmasync1);
					  fatalError = true;
						/*screenPrint(ss.str());*/
					}

					dumpInputErrorFragment(fragPtr,l1Id,rolId,fragSize,errorDescription);

					MgmtEntry* entry = m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueReadIndex[subRob]].fragIndexPtr;
					EventTag eventTag;
					eventTag.eventId =  l1Id;
					eventTag.runNumber = 0;
					unsigned int hashKey = eventTag.eventId & s_hashMask;

					pthread_mutex_lock(&m_rolArray[rolId].m_hashMutex[hashKey]);
					if(entry->toDelete){
						deleteIndexedFragment(&eventTag, rolId, false, false);
					}
					else{
						if(entry->isErrorFragment == true){
							entry->isErrorFragment = false;
						}
					}
					pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);

					if ( m_DMADoneActiveCaptureQueueReadIndex[subRob] == s_captureDMAQueueFIFOSize - 1)
						m_DMADoneActiveCaptureQueueReadIndex[subRob] = 0;
					else
						m_DMADoneActiveCaptureQueueReadIndex[subRob]++;

					__sync_fetch_and_sub(&m_numCaptureDMAsPending[subRob], numDMAs);

				}
				else
				{
					__sync_fetch_and_sub(&m_numCaptureDMAsPending[subRob], 10);
				}
				signalFreeCaptureDMASlots(subRob);
				updateDMADoneReadIndex(subRob);

			}
			else{

				while(m_DMADoneActiveQueueReadIndex[subRob] == m_DMADoneActiveQueueWriteIndex[subRob]);

				unsigned int rolId = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].rolID;

				if(m_runMode == runModeGen2ROS){

					NPRequestDescriptor *descriptor =  m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].descriptor;

					unsigned int numDMAs = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].numDMAs;
					unsigned int index = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].index;

					if((index != descriptorInfo) && !fatalError){
					  std::stringstream ss;
					  ss << "RobinNP::dmaMonitorMain: DMA Synchronisation Error: Index " << index << " Descriptor ID " << descriptor->getId() << " Num DMAs completed this turn " << numDMAs << ", num currently pending " << m_numDMAsPending[subRob] << " for subRob " << subRob << ", info " << descriptorInfo << "All subsequent transfers may be affected. Please restart ROS application!" << std::endl;

						CREATE_ROS_EXCEPTION(ex_robinnp_dmasync2, ROSRobinNPExceptions, BADID, ss.str()); //Temporarily use BADID type to avoid header change
					  ers::fatal(ex_robinnp_dmasync2);
					  fatalError = true;
							       
						/*screenPrint(ss.str());*/
					}

					for(unsigned int rol = subRob *m_numChannelsPerSubRob; rol < (m_numChannelsPerSubRob*(subRob + 1)); ++rol){
						if (s_runCtlStart == m_rolArray[rol].m_runCtlState){
							if(descriptor->dataPage(m_rolArray[rol].channelIndex) != 0){

								bool hasStatusErrors = false;
								bool isCorruptFragment = false;
								if (descriptor->headerPage(m_rolArray[rol].channelIndex)->at(6) != 0)
								{
									DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RobinNP::dmaMonitorMain: Fragment has errors flagged in status bit - Will not perform CRC check");
									hasStatusErrors = true;
								}
								//hasStatusErrors = false;
								if(!hasStatusErrors){
									isCorruptFragment = checkID(descriptor->dataPage(m_rolArray[rol].channelIndex)->virtualAddress(),descriptor->level1Id(),rol);

									if (((m_crc_interval != 0) && (descriptor->level1Id() % m_crc_interval) == 0))
									{
										if(descriptor->headerPage(m_rolArray[rol].channelIndex)->at(8) == s_robHdrCrcType){
											isCorruptFragment |= checkCRC(descriptor->dataPage(m_rolArray[rol].channelIndex)->virtualAddress(),descriptor->headerPage(m_rolArray[rol].channelIndex),rol);
										}
									}
								}

								if(isCorruptFragment)
								{
									if(descriptor->status(m_rolArray[rol].channelIndex) == NPRequestDescriptor::RequestStatus::REQUEST_CORRUPT)
									{
										descriptor->headerPage(m_rolArray[rol].channelIndex)->at(6) |= fragGenStatusData;    // flag data error;
										descriptor->status(m_rolArray[rol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
										unsigned int totalSize = descriptor->headerPage(m_rolArray[rol].channelIndex)->at(1) - descriptor->headerPage(m_rolArray[rol].channelIndex)->at(2);
										unsigned int debugMemAddress = getDebugMemPage(descriptor->level1Id(),rol);
										dumpCRCErrorFragment(descriptor->dataPage(m_rolArray[rol].channelIndex)->virtualAddress(),descriptor->level1Id(),rol,totalSize,debugMemAddress);
									}
									else{

										descriptor->status(m_rolArray[rol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_CORRUPT);
										m_statistics->m_rolStats[rol].m_memreadcorruptions++;
									}
								}
								else{
									descriptor->status(m_rolArray[rol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
								}
							}
						}
					}

#ifdef MEMDEBUG
					for(unsigned int rol = subRob *m_numChannelsPerSubRob; rol < (m_numChannelsPerSubRob*(subRob + 1)); ++rol){
						memoryErrorCatcher(rol, (unsigned long)descriptor->dataPage(m_rolArray[rolId].channelIndex)->virtualAddress(), descriptor->level1Id(), descriptor->headerPage(m_rolArray[rol].channelIndex)->at(8));
					}
#endif

					/*std::stringstream ss;
				ss << "Completed " << numDMAs << " DMAs - pending " << m_numDMAsPending[subRob] << std::endl;
				screenPrint(ss.str());*/
					__sync_fetch_and_sub(&m_numDMAsPending[subRob], numDMAs);
					m_outputQueue->push(descriptor);

				}
				else if(m_runMode == runModeLegacy){
					unsigned int ticket =  m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].ticket;
					if(ticket != (descriptorInfo)){
						std::stringstream ss;
						ss << "DMA Synchronisation Error: Ticket " << ticket << " for subRob " << subRob << ", info " << descriptorInfo << std::endl;
						screenPrint(ss.str());
					}


					unsigned int memAddress = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].memAddress;
					unsigned int eventID = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].eventID;
					unsigned long virtAddress = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].virt;
					unsigned long numDMAs = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].numDMAs;

					unsigned int *dataPtr = (unsigned int*)virtAddress + sizeof(RobMsgHdr)/sizeof(unsigned int) + 1;
					RobMsgHdr *hdrPtr = (RobMsgHdr*)virtAddress;
					bool hasStatusErrors = false;
					bool isCorruptFragment = false;
					if (hdrPtr->fragStat != 0)
					{
						DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RobinNP::dmaMonitorMain: Fragment has errors flagged in status bit - Will not perform CRC check");
						hasStatusErrors = true;
					}
					//hasStatusErrors = false;
					if(!hasStatusErrors){
						isCorruptFragment = checkID(dataPtr,eventID,rolId);
						if (((m_crc_interval != 0) && (eventID % m_crc_interval) == 0))
						{
							if(((unsigned int*)virtAddress)[9] == s_robHdrCrcType){
								isCorruptFragment |= checkCRC(dataPtr,hdrPtr,rolId);
							}
						}
					}

					if(isCorruptFragment)
					{
						hdrPtr->fragStat |= fragGenStatusData;    // flag data error;
						unsigned int totalSize = hdrPtr->totalSize - hdrPtr->hdrSize;
						dumpCRCErrorFragment(dataPtr,eventID,rolId,totalSize,memAddress);
						m_statistics->m_rolStats[rolId].m_memreadcorruptions++;
					}

#ifdef MEMDEBUG
					memoryErrorCatcher(rolId, virtAddress, eventID, memAddress);
#endif

					pthread_mutex_lock(&m_rolArray[rolId].m_ticketCondMutex[ticket]);
					m_rolArray[rolId].m_ticketIndex[descriptorInfo] = true;
					pthread_cond_signal(&m_rolArray[rolId].m_ticketCond[descriptorInfo]);
					pthread_mutex_unlock(&m_rolArray[rolId].m_ticketCondMutex[ticket]);

					__sync_fetch_and_sub(&m_numDMAsPending[subRob], numDMAs);

				}
				else{
					unsigned int index = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].index;
					if(index != descriptorInfo){
						std::stringstream ss;
						ss << "DMA Synchronisation Error: Index " << index << " Num DMAs completed this turn " << 1 << ", num currently pending " << m_numDMAsPending[subRob] << " for subRob " << subRob << ", info " << descriptorInfo << std::endl;
						screenPrint(ss.str());
					}
					/*unsigned int eventId =  m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].eventID;
				if(eventId != *(m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].page->virtualAddress() + 5)){

					for(unsigned int dataWord = 0; dataWord < m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].transferSize; ++dataWord){
						std::cout << std::hex << std::setfill('0') << std::setw(8) << *(m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].page->virtualAddress() + dataWord) << " ";
						if((dataWord + 1) % 8 == 0){
							std::cout << std::endl;
						}
					}
					std::cout << std::endl;
					std::stringstream ss;
					ss << "Wrong L1Id for ROL " << rolId << " got " << std::hex << *(m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].page->virtualAddress() + 5) << " expected " << eventId << std::dec << std::endl;
					screenPrint(ss.str());
					memoryErrorCatcher(rolId, (unsigned long)m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].page->virtualAddress(), eventId, m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].memAddress);

					exit(0);
				}*/
					ROIBOutputElement element;
					element.page =  m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].page;
					element.rolId = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].rolID;
					element.dataSize = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].transferSize;
					element.fragmentStatus =  m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].fragStatus;
					element.robinNPPageNumber = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].robinnppagenum;
					unsigned int memAddress = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].memAddress;
					unsigned int eventID = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].eventID;
					unsigned long virtAddress = m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueReadIndex[subRob]].virt;
					unsigned int *dataPtr = (unsigned int*)virtAddress;
					bool hasStatusErrors = false;
					bool isCorruptFragment = false;
					if (element.fragmentStatus != 0)
					{
						DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RobinNP::dmaMonitorMain: Fragment has errors flagged in status bit - Will not perform CRC check");
						hasStatusErrors = true;
					}
					//hasStatusErrors = false;
					if(!hasStatusErrors){
						isCorruptFragment = checkID(dataPtr,eventID,rolId);

						if (((m_crc_interval != 0) && (eventID % m_crc_interval) == 0))
						{
							isCorruptFragment |= checkCRC(dataPtr,element.dataSize,rolId);
						}
					}
					//isCorruptFragment = true;
					if(isCorruptFragment)
					{
						element.fragmentStatus |= fragGenStatusData;    // flag data error;
						dumpCRCErrorFragment(dataPtr,eventID,rolId,element.dataSize,memAddress);
						m_statistics->m_rolStats[rolId].m_memreadcorruptions++;
					}



					__sync_fetch_and_sub(&m_numDMAsPending[subRob], 1);
					m_rolArray[rolId].pagesInIndex++;
					m_directOutputQueue->at(subRob)->push(element);
				}
				if ( m_DMADoneActiveQueueReadIndex[subRob] == s_DMAQueueFIFOSize - 1 )
					m_DMADoneActiveQueueReadIndex[subRob] = 0;
				else
					m_DMADoneActiveQueueReadIndex[subRob]++;

				signalFreeDMASlots(subRob);

				updateDMADoneReadIndex(subRob);
			}
		}

		if(interrupted){
			m_Bar0Common->InterruptControlRegister.acknowledge = (1 << interrupt);
			interrupted = false;
		}

	}

	std::stringstream ss;
	ss << "RobinNP::dmaMonitorMain: Exiting DMA Monitor Thread for SubRob " << subRob;
	screenPrint(ss.str());

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::dmaMonitorMain: Exiting DMA Monitor Thread");
	return 0;
}


/************************************/
int RobinNP::requestTempVal()
/************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::requestTempVal Called");
	return (((m_Bar0Common->GlobalStatusRegister.Temperature * 503.975)/1024) - 273.15);
}

/***************************************************************/
RejectedEntry *RobinNP::requestKeptPage(unsigned int /*rolId*/, int /*pageIndex*/)
/***************************************************************/
{
	CREATE_ROS_EXCEPTION(ex_robinnp_reqkeptpage1, ROSRobinNPExceptions, UNIMPLEMENTED, "RobinNP::requestKeptPage: Rejected Page DMA not yet implemented for RobinNP");
	ers::warning(ex_robinnp_reqkeptpage1);
	//return entry;
	return 0;
}

/*******************************************/
int RobinNP::requestClearKeptPages(unsigned int rolId)
/*******************************************/
{
	CREATE_ROS_EXCEPTION(ex_robinnp_reqclearkeptpage1, ROSRobinNPExceptions, UNIMPLEMENTED, "RobinNP::requestClearKeptPages: Clearing of Kept Pages Not Yet Implemented for RobinNP - Requested for ROL ID " << rolId);
	ers::warning(ex_robinnp_reqclearkeptpage1);
	return 1;
}

/*************************************************************************************************/
void RobinNP::requestFragment(unsigned int rolId, unsigned char *replyAddress, unsigned long physicalAddress, unsigned int eventId, unsigned int ticket)
/*************************************************************************************************/
{
	m_rolArray[rolId].m_ticketIndex[ticket] = false;

	RequestQueueElement element;
	element.rolId = rolId;
	element.virtualAddress = reinterpret_cast<unsigned long>(replyAddress);
	element.physicalAddress = physicalAddress;
	element.l1Id = eventId;
	element.descriptor = 0;
	element.subRob = m_rolArray[rolId].subRobMapping;
	element.ticket = ticket;
	element.terminate = false;
	queueRequest(element);
}

/*************************************************************************************************/
void RobinNP::requestFragment(unsigned int subRob,std::vector<unsigned int> *channelList,NPRequestDescriptor* descriptor)

/*************************************************************************************************/
{
	RequestQueueElement element;
	element.subRob = subRob;
	element.descriptor = descriptor;
	element.channels = *channelList;
	element.terminate = false;
	queueRequest(element);
}

/*********************************************/
void RobinNP::requestLeaveDiscardMode(unsigned int rolId)
/*********************************************/
{
	SEND_ROS_INFO("Deactivating discard mode for ROL 0x" << HEX(m_configuration->getRolConfig(rolId)->getRobId()));
	//std::cout << "Deactivating discard mode for ROL " << rolId << std::endl;
	setRolEnabled(true, rolId);
	m_rolArray[rolId].linkEnabled = true;
	restartRol(rolId);
}

/*********************************************/
void RobinNP::requestEnterDiscardMode(unsigned int rolId)
/*********************************************/
{
	SEND_ROS_INFO("Activating discard mode for ROL 0x" << HEX(m_configuration->getRolConfig(rolId)->getRobId()));
	//std::cout << "Activating discard mode for ROL " << rolId << std::endl;
	m_rolArray[rolId].m_runCtlState =  s_runCtlStop;
	m_rolArray[rolId].linkEnabled = false;
	setRolEnabled(false, rolId);
	initRol(rolId);
}

/*************************************************************************/
/*                        Indexer                                        */
/*************************************************************************/

/***********************************/
void *RobinNP::freePageManagerMain(unsigned int subRob)
/***********************************/
{
	//Top level process for Indexer Thread
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: Starting Free Page Manager Thread for SubRob " << subRob);

	m_freePageManagerRunState[subRob] = true;
	m_emptyStack[subRob] = false;
	m_fpfsFull[subRob] = false;
	bool startup = true; //hold dataflow for ROIB mode
	while(m_freePageManagerRunState[subRob]){

		awaitFreeFPFSlots(subRob);
		awaitPagesOnStack(subRob);

		m_emptyStack[subRob] = true;
		m_fpfsFull[subRob] = true;

		for (unsigned int channel=subRob*m_numChannelsPerSubRob; channel < (subRob + 1)*m_numChannelsPerSubRob; ++channel)
		{
			if (s_runCtlStart == m_rolArray[channel].m_runCtlState){

				pthread_mutex_lock(&m_rolArray[channel].m_pageStackMutex);

				if (m_rolArray[channel].m_freeTos == 0){
					pthread_mutex_unlock(&m_rolArray[channel].m_pageStackMutex);
					continue;              // check next ROL if we don't have pages
				}

				//int fpfFreeCount = s_fpfSize - /*getFpfFillStatus(channel);estimateFPFItems(channel);*/getFPFItems(channel);
				volatile unsigned int stackFill = m_rolArray[channel].m_freeTos;
				int fpfFreeCount = s_fpfSize - (m_numPagesPerRol - stackFill - m_rolArray[channel].pagesInIndex);
				pthread_mutex_unlock(&m_rolArray[channel].m_pageStackMutex);
				m_emptyStack[subRob] = false;
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: FPF Main for ROL " << channel << ": space for " << fpfFreeCount << " entries available");

				if(fpfFreeCount > 0){
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: Filling FPF up to " << fpfFreeCount << " from " << m_rolArray[channel].m_freeTos << " remaining pages on stack ROL " << channel);
				}
				else{
					continue;
				}

				m_fpfsFull[subRob] = false;

				while ((fpfFreeCount > 0) && (stackFill > 0)){

					MgmtPtr nextFpfEntry = takeFreePageFromStack(channel);
					stackFill--;
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: Updating FPF[" << channel << "] with page 0x" << HEX(nextFpfEntry));

					/*std::stringstream ss;
					ss << "placing page " << std::hex << nextFpfEntry << std::dec << " on fifo for ROL " << channel << " num pages " << m_numPagesPerRol << " " << m_rolArray[4].m_freeTos << " spaces " << fpfFreeCount << " address " << std::hex << ((nextFpfEntry + ((channel - m_numChannelsPerSubRob*subRob)*m_rolArray[channel].numPages))* m_configuration->getPageSize()) << std::dec << std::endl;
					screenPrint(ss.str());*/
					m_Bar0Channel[channel]->BufferManagerFreeFIFO.lower = ((nextFpfEntry + ((channel - m_numChannelsPerSubRob*subRob)*m_numPagesPerRol))* m_configuration->getPageSize());

					//std::stringstream ss;
					//ss << "placing page " << std::hex << nextFpfEntry << " " << ((nextFpfEntry + ((channel - m_numChannelsPerSubRob*subRob)*m_numPagesPerRol))* m_configuration->getPageSize()) << std::dec << " on fifo ";
					//screenPrint(ss.str());

					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: Writing entry 0x" << HEX(nextFpfEntry * channel*m_configuration->getPageSize())<< " to fpfPtr[" << channel << "]");

					m_statistics->m_rolStats[channel].m_pageStat[pagesProvided]++;
					fpfFreeCount --;
				}

				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: Residual FPF " << channel << " count: " << fpfFreeCount);
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: FPF Fill Status = " << getFpfFillStatus(channel));
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::freePageManagerMain: UPF Fill Status = " << getUpfFillStatus(channel));
			}

		}
		if(startup || (m_runMode == runModeROIB)){
			for (unsigned int channel=subRob*m_numChannelsPerSubRob; channel < (subRob + 1)*m_numChannelsPerSubRob; ++channel)
			{
				if (s_runCtlStart == m_rolArray[channel].m_runCtlState){
					toggleBufferManagerXOFF(channel,false);
				}
			}
			startup = false;
		}

	}
	return 0;
}

/***********************************/
void *RobinNP::indexerMain(unsigned int subRob)
/***********************************/
{
	//std::cout << ::syscall(__NR_gettid) << std::endl;
	//Top level process for Indexer Thread
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexerMain: Starting Indexer Thread for SubRob " << subRob);

	m_indexerRunState[subRob] = true;
	unsigned int interrupt = s_UPFInterruptCodes[subRob];

	bool interrupted = false;
	while(m_indexerRunState[subRob]){

		unsigned int readIndex = 0;
		if(!checkCombinedUPFWriteIndex(subRob,readIndex)){
			int ret = ioctl(m_devHandle, WAIT_IRQ,&interrupt);
			if (ret)
			{
				CREATE_ROS_EXCEPTION(ex_robinnp_indexermain1, ROSRobinNPExceptions, INTERRUPTFAIL, "RobinNP::indexerMain: Interrupt " << interrupt << " failure: " << strerror(errno) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_indexermain1);
			}
			interrupted = true;
		}
		else{
			FragInfo theFragment;
			if (processUPFDuplicatorPage(readIndex,subRob,theFragment)){
				processIncomingFragment(&theFragment, theFragment.RolId);
			}
		}

		while(checkCombinedUPFWriteIndex(subRob,readIndex)){
			FragInfo theFragment;
			if (processUPFDuplicatorPage(readIndex,subRob,theFragment)){
				processIncomingFragment(&theFragment, theFragment.RolId);
			}
		}
		if(interrupted){
			m_Bar0Common->InterruptControlRegister.acknowledge = (1 << interrupt);
			interrupted = false;
		}
		signalFreeFPFSlots(subRob);
	}

	std::stringstream ss;
	ss << "RobinNP::indexerMain: Exiting Indexer Thread for SubRob " << subRob;
	screenPrint(ss.str());

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexerMain: Exiting Indexer Thread");
	return 0;
}

/************************/
void RobinNP::startThreads()
/************************/
{

	m_Bar0Common->InterruptMaskRegister.Register[1] = 0x0;
	m_Bar0Common->InterruptMaskRegister.Register[0] = 120;
	std::cout << "RobinNP::startThreads: Interrupt Mask Register[0] = 0x" << std::hex << m_Bar0Common->InterruptMaskRegister.Register[0] << std::dec << std::endl;
	std::cout << "RobinNP::startThreads: Interrupt Mask Register[1] = 0x" << std::hex << m_Bar0Common->InterruptMaskRegister.Register[1] << std::dec << std::endl;
	m_Bar0Common->InterruptControlRegister.acknowledge = 120;

	unsigned int lastSubRob = 99;
	for(unsigned int channel = 0; channel < m_numRols; ++channel){
		if (s_runCtlStart == m_rolArray[channel].m_runCtlState){
			if(m_rolArray[channel].subRobMapping != lastSubRob){
				m_subRobsAvailable++;
				lastSubRob = m_rolArray[channel].subRobMapping;
				m_subRobsActive[lastSubRob] = true;
			}
		}
	}

	std::cout << "RobinNP::startThreads: Starting Threads for " << m_subRobsAvailable << " subRobs" << std::endl;

	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::startThreads: Initiating Threads");
	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){

		if(m_subRobsActive[subRob]){
			ThreadParams *params = new ThreadParams();
			params->robin = this;
			params->subRob = subRob;

			int res = pthread_create(&m_freePageManager_thread[subRob],0,freePageManager,(void*)params);
			if (res)
			{
				CREATE_ROS_EXCEPTION(ex_robinnp_reqstartthr1, ROSRobinNPExceptions, THREAD, "The ROBINNP Free Page Manager Failed to Start for RobinNP " << m_slotNumber << " subRob " << subRob);
				throw (ex_robinnp_reqstartthr1);
			}
			if(m_runMode == runModeROIB){
				res = pthread_create(&m_directDMAManager_thread[subRob],0,directDMAManager,(void*)params);
				if (res)
				{
					CREATE_ROS_EXCEPTION(ex_robinnp_reqstartthr2a, ROSRobinNPExceptions, THREAD, "The ROBINNP Direct DMA Manager Thread Failed to Start for RobinNP " << m_slotNumber << " subRob " << subRob);
					throw (ex_robinnp_reqstartthr2a);
				}
			}
			else{
				res = pthread_create(&m_indexer_thread[subRob],0,indexer,(void*)params);
				if (res)
				{
					CREATE_ROS_EXCEPTION(ex_robinnp_reqstartthr2, ROSRobinNPExceptions, THREAD, "The ROBINNP Indexer Thread Failed to Start for RobinNP " << m_slotNumber << " subRob " << subRob);
					throw (ex_robinnp_reqstartthr2);
				}

				res = pthread_create(&m_requestManager_thread[subRob],0,requestManager,(void*)params);
				if (res)
				{
					CREATE_ROS_EXCEPTION(ex_robinnp_reqstartthr3, ROSRobinNPExceptions, THREAD, "The ROBINNP Request Manager Thread Failed to Start for RobinNP " << m_slotNumber << " subRob " << subRob);
					throw (ex_robinnp_reqstartthr3);
				}
			}
			res = pthread_create(&m_dmaMonitor_thread[subRob],0,dmaMonitor,(void*)params);
			if (res)
			{
				CREATE_ROS_EXCEPTION(ex_robinnp_reqstartthr4, ROSRobinNPExceptions, THREAD, "The ROBINNP DMA Monitor Thread Failed to Start for RobinNP " << m_slotNumber << " subRob " << subRob);
				throw (ex_robinnp_reqstartthr4);
			}

			if(m_runMode == runModeROIB){
				while(!(m_directDMAManagerRunState[subRob] && m_dmaMonitorRunState[subRob] && m_freePageManagerRunState[subRob]));
			}
			else{
				while(!(m_requestManagerRunState[subRob] && m_indexerRunState[subRob] && m_dmaMonitorRunState[subRob] && m_freePageManagerRunState[subRob]));
			}

			delete params;
		}
	}

	std::cout << "RobinNP::startThreads: All Threads Successfully Started" << std::endl;
	m_threadsRunning = true;

}

/***********************/
bool RobinNP::stopThreads()
/***********************/
{

	for(unsigned int subRob = 0; subRob < s_maxSubRobs; ++subRob){

		if(m_subRobsActive[subRob]){

			void *result;
			std::stringstream ss;

			if(m_runMode == runModeROIB){
				m_directDMAManagerRunState[subRob] = false;
				int ret = ioctl(m_devHandle, CANCEL_IRQ_WAIT);
				if (ret)
				{
					std::cout << "ERROR - unable to cancel interrupts" << std::endl;
				}
				(*m_pageQueues)[m_subRobIndex[subRob]]->push(0);
				pthread_join(m_directDMAManager_thread[subRob],&result);
				ss << "RobinNP::stopThreads: Successfully terminated Direct DMA Manager Thread";
				screenPrint(ss.str());
			}
			else{
				m_requestManagerRunState[subRob] = false;
				RequestQueueElement element;
				element.terminate = true;
				m_concurrentRequestQueue[subRob]->push(element);


				pthread_join(m_requestManager_thread[subRob],&result);
				ss << "RobinNP::stopThreads: Successfully terminated Request Manager Thread";

				screenPrint(ss.str());
				m_indexerRunState[subRob] = false;

			}

			m_freePageManagerRunState[subRob] = false;
			m_dmaMonitorRunState[subRob] = false;

			int ret = ioctl(m_devHandle, CANCEL_IRQ_WAIT);
			if (ret)
			{
				std::cout << "ERROR - unable to cancel interrupts" << std::endl;
			}

			pthread_join(m_freePageManager_thread[subRob],&result);
			ss.str(std::string());
			ss << "RobinNP::stopThreads: Successfully terminated Free Page Manager Thread";
			screenPrint(ss.str());
			if(m_runMode != runModeROIB){

				pthread_join(m_indexer_thread[subRob],&result);
				ss.str(std::string());
				ss << "RobinNP::stopThreads: Successfully terminated Indexer Thread";
				screenPrint(ss.str());
			}
			pthread_join(m_dmaMonitor_thread[subRob],&result);
			ss.str(std::string());
			ss << "RobinNP::stopThreads: Successfully terminated DMA Monitor Thread";
			screenPrint(ss.str());
		}
	}

	m_threadsRunning = false;
	return true;
}


/***********************/
bool RobinNP::getThreadState()
/***********************/
{
	return m_threadsRunning;
}

/*************************************************************************/
/*************************************************************************/
/* 						Private Methods								     */
/*************************************************************************/
/*************************************************************************/


/*************************************************************************/
/*                       Startup and Shutdown                            */
/*************************************************************************/

/***************************************/
void RobinNP::initConfigurationParameters()
/***************************************/
{
	RobinNPROLConfig defaultRol;
	for(unsigned int rol = 0; rol < m_numRols; ++rol){
		m_configuration->configureRol(defaultRol,rol);
	}

	configure();
}

/***************************/
int RobinNP::initRol(int rolId)
/***************************/
{
	// initialise ROL management

	int initFailed = 0;

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: for ROL " << rolId);

	// disable ROL first
	disableRol(rolId);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: ROL " << rolId << " disabled");

	m_Bar0Channel[rolId]->ChannelControlRegister.Component.StartDataGen = 0;

	// stop data generator
	m_Bar0Channel[rolId]->DataGeneratorControlRegister.FragmentSize = 0;	// event size zero
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.SelectDataGen = 0;

	// initialise management table
	initFailed += initPageManagement(rolId);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: Init status after initPageManagement (should be 0): " << initFailed);

	// initialise word level buffer manager
	initFailed += initFpgaBufferManager(rolId);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: Init status after initFpgaBufMgr (should be 0): " << initFailed);

	// initialise Data Generator
	initDataGenerator(rolId);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: Init status after initDataGenerator (should be 0): " << initFailed);

	// initialise statistics (all)
	initStatistics(rolId);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initRol: Init status after initStatistics (should be 0): " << initFailed);

	m_Bar0Channel[rolId]->ChannelControlRegister.Component.CRCEnable = 1;

	return initFailed;
}

/**************************************/
int RobinNP::initPageManagement(int rolId)
/**************************************/
{

	// to support modification of the management configuration do all the checking
	// here check valid page size. do this before evaluating numpages etc
	switch (m_configuration->getPageSize()) {
	case s_mgmtMinPageSize:
	case 2 * s_mgmtMinPageSize:
	case 4 * s_mgmtMinPageSize:
	case 8 * s_mgmtMinPageSize:
	case 16 * s_mgmtMinPageSize:
	// is OK
	break;
	default:
		CREATE_ROS_EXCEPTION(ex_robinnp_initPageMgmt, ROSRobinNPExceptions, CONFIGURE, "Invalid RobinNP page size selected:  " << m_configuration->getPageSize() << " for RobinNP " << m_slotNumber << " should be either 256, 512, 1024, 2048 or 4096");
		throw(ex_robinnp_initPageMgmt);
		m_configuration->setPageSize(s_mgmtMinPageSize);
		break;
	}

	// check valid number of pages
	if (s_mgmtMaxNumPages < m_numPagesPerRol) {
		CREATE_ROS_EXCEPTION(ex_robinnp_initPageMgmt1, ROSRobinNPExceptions, CONFIGURE, "Number of pages selected exceeds maximum, selected " << m_numPagesPerRol << " max " << s_mgmtMaxNumPages << " for RobinNP " << m_slotNumber << " setting to Max available");
		warning(ex_robinnp_initPageMgmt1);
		m_numPagesPerRol = calcXmin((unsigned int)s_mgmtMaxNumPages, (unsigned int)(floor((s_mgmtMaxBufSize * m_numSubRobs * 1.0) / (m_numRols * m_configuration->getPageSize()))));  // calculate number of pages from buffer size and page size
		m_configuration->setNumPages(m_numPagesPerRol);

	}

	// check total size of required memory
	if (s_mgmtMaxBufSize * m_numSubRobs < (m_numPagesPerRol * m_configuration->getPageSize() * m_numRols)){
		CREATE_ROS_EXCEPTION(ex_robinnp_initPageMgmt2, ROSRobinNPExceptions, CONFIGURE, "Configuration exceeds memory max (" << s_mgmtMaxBufSize * m_numSubRobs << " requested " << (m_numPagesPerRol * m_configuration->getPageSize() * m_numRols) << " for RobinNP " << m_slotNumber << " setting to max available");
		warning(ex_robinnp_initPageMgmt2);
		//ers warning
		m_numPagesPerRol = (unsigned int) floor((s_mgmtMaxBufSize * m_numSubRobs * 1.0) / (m_numRols * m_configuration->getPageSize()));  // calculate number of pages from buffer size and page size
		m_configuration->setNumPages(m_numPagesPerRol);
	}

	// ----------------- start ROL specific stuff here ---------------
	// hash array: clear all entries. Entry index 0 indicates "not used"
	for (unsigned int i = 0; i < s_numHashLists; i++){
		m_rolArray[rolId].m_hashArrayPtr[i] = 0;
	}
	//std::cout << "num pages " << rolId << " " <<  m_numPagesPerRol << std::endl;
	// free list and stack
	for (unsigned int i = 0; i < m_numPagesPerRol; i++){		// initialise freeList with indices
		m_rolArray[rolId].m_freeList[i] = i;
	}

	// coverity[MISSING_LOCK]
	m_rolArray[rolId].m_freeTos = m_numPagesPerRol - 1;	// top of free stack. Stack is now filled. No need to lock mutex as no threads running, coverity warning deactivated above.
	m_rolArray[rolId].pagesInIndex = 0;
	//std::cout << "num pages on stack " << rolId << " " <<  m_rolArray[rolId].m_freeTos << std::endl;
	// Work back to 1 and reserve index 0 because
	// it is used to mark empty location
	// initialise free entries
	for (unsigned int i = 0; i < m_numPagesPerRol; i++){		// clear all pointers
		m_rolArray[rolId].m_itemListPtr[i].next = 0;
		m_rolArray[rolId].m_itemListPtr[i].prev = 0;
	}

	m_rolArray[rolId].m_usedCounter = 0;				// no pages used so far

	m_rolArray[rolId].m_fragmentComplete = true;     // start new fragment next time
	m_rolArray[rolId].m_ecrJump = 0;					// clear ECR jump indicator

	return 0;

}

/*****************************************/
int RobinNP::initFpgaBufferManager(int rolId)
/*****************************************/
{
	// set page size for buffer manager
	// ROL should be already disabled
	// reset buffer manager via control register

	m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] = bmCtlInitVal | bmCtlReset;	// initialise and turn reset bit on
	// update buffer control, de-assert reset bit
	//Use buffer manager reset bit instead?

	m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] &= ~bmCtlReset;

	// mask size bits (lower 3 bits)
	unsigned int bufCtlVal = m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0];
	bufCtlVal &= ~bmCtlSizeMask;

	// remember : page size is in bytes
	switch (m_configuration->getPageSize()){
	case 256:
		bufCtlVal |= bmCtlSize1k;
		break;
	case 512:
		bufCtlVal |= bmCtlSize2k;
		break;
	case 1024:
		bufCtlVal |= bmCtlSize4k;
		break;
	case 2048:
		bufCtlVal |= bmCtlSize8k;
		break;
	case 4096:
		bufCtlVal |= bmCtlSize16k;
		break;
	default:
		return 1;
		break;
	}

	bufCtlVal |= bmCtlKeepCtl;        // turn bit on to strip off s-link control words
	bufCtlVal |= finish_on_GeneralError;// WPV should now be finish on framing error

	// update buffer control
	m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] = bufCtlVal;
	bufCtlVal = m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0];

	for(unsigned int rol = 0; rol < m_numRols; ++rol){
		if(m_configuration->getMemEmulationState()){
			m_Bar0Channel[rol]->BufferManagerControlRegister.Component.IgnoreMemoryFlowControl = 1;
		}
		else{
			m_Bar0Channel[rol]->BufferManagerControlRegister.Component.IgnoreMemoryFlowControl = 0;
		}
	}

	return 0;
}

/**********************************/
void RobinNP::initDataGenerator(int rolId)
/**********************************/
{
	// check internal data generator usage
	if (m_configuration->getRolConfig(rolId)->getRolDataGen()){
		// this is the default mode, with embedded channel and event id
		//std::cout << "Activating Data Generator for ROL " << rolId << " with a test size of " << m_configuration->getRolConfig(rolId)->getTestSize() << std::endl;
		m_Bar0Channel[rolId]->ChannelControlRegister.Component.SelectDataGen = 1;
		m_Bar0Channel[rolId]->DataGeneratorControlRegister.FragmentSize = m_configuration->getRolConfig(rolId)->getTestSize();
		m_Bar0Channel[rolId]->DataGeneratorControlRegister.FragHeaderRunNumLower16Bits = 1;
		if (m_configuration->getRolConfig(rolId)->getDGEarlyWrap()){
			std::cout << "Enabling Early Wrap Mode for Data Generator" << std::endl;
			m_Bar0Channel[rolId]->ChannelControlRegister.Component.EnableEarlyWrap = 1;
		}
		else{
			m_Bar0Channel[rolId]->ChannelControlRegister.Component.EnableEarlyWrap = 0;
		}
		m_Bar0Channel[rolId]->ChannelControlRegister.Component.StartDataGen = 1;
	} else {
		m_Bar0Channel[rolId]->ChannelControlRegister.Component.SelectDataGen = 0;
	}

}


/**********************************/
void RobinNP::initFragStatusMap()
/**********************************/
{

	// initialise fragment status/error map. Set non-error conditions to 0
	m_fragStatusMap.push_back(std::make_pair(upfWrongRODHdrVersion,fragStatusFormatError));
	m_fragStatusMap.push_back(std::make_pair(upfMissingRODHdrMarker,fragStatusMarkerError));
	m_fragStatusMap.push_back(std::make_pair(upfFramingError,fragStatusEofError));
	m_fragStatusMap.push_back(std::make_pair(upfControlErrorFlag,fragStatusCtlError));
	m_fragStatusMap.push_back(std::make_pair(upfDataErrorFlag,fragStatusDataError));
	m_fragStatusMap.push_back(std::make_pair(upfFragTrailerLengthMismatch,fragStatusSizeError));

	m_fragStatusMapDebug.push_back(std::make_pair(upfWrongRODHdrVersion,"Fragment header format incorrect"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfMissingRODHdrMarker,"Missing ROD header marker"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfFramingError,"Missing end-of-fragment word"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfControlErrorFlag,"Link data error asserted during fragment control word(s)"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfDataErrorFlag,"Link data error asserted during fragment data word(s)"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfFragTrailerLengthMismatch,"Number of words in fragment does not match ROD trailer"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfFragmentTooLong,"Fragment larger than firmware maximum (128k)"));
	m_fragStatusMapDebug.push_back(std::make_pair(upfFragmentTooShort,"Fragment too short to contain L1 ID (< 5 words)"));
	m_fragStatusMapDebug.push_back(std::make_pair(fragStatusSeqError,"Fragment L1ID out of sequence"));
	//m_fragStatusMapDebug.push_back(std::make_pair(upfDataWordTXErr,"upfDataWordTXErr"));
	//m_fragStatusMapDebug.push_back(std::make_pair(upfCtrlWordTXErr,"upfCtrlWordTXErr"));
	//m_fragStatusMapDebug.push_back(std::make_pair(upfLDErr,"upfLDErr"));

	// initialise fragment status/error map. Set non-error conditions to 0
	m_fragErrorCombinations.push_back(std::make_pair(upfWrongRODHdrVersion,fragsFormatError));
	m_fragErrorCombinations.push_back(std::make_pair(upfMissingRODHdrMarker,fragsMarkerError));
	m_fragErrorCombinations.push_back(std::make_pair(upfFramingError,fragsFramingError));
	m_fragErrorCombinations.push_back(std::make_pair(upfControlErrorFlag,fragsCtrlWordError));
	m_fragErrorCombinations.push_back(std::make_pair(upfDataErrorFlag,fragsDataWordError));
	m_fragErrorCombinations.push_back(std::make_pair(upfFragTrailerLengthMismatch,fragsSizeMismatch));
	m_fragErrorCombinations.push_back(std::make_pair(upfFragmentTooLong,fragsLong));
	m_fragErrorCombinations.push_back(std::make_pair(upfFragmentTooShort,fragsShort));
	m_fragErrorCombinations.push_back(std::make_pair(fragStatusSeqError,fragsOutOfSync));

	/*case upfMissingBofErrorFlag:
	m_fragStatusMap[i] = fragStatusBofError; // temporarily disabled
	break;*/
	//
}


/***********************************/
void RobinNP::initStatistics(int rolId)
/***********************************/
{
	m_statistics->m_firmwareVersion = m_designVersion;
	m_statistics->m_fragFormat = s_robFormatVersion;
	m_statistics->m_bufByteSize = m_numPagesPerRol * m_configuration->getPageSize() * sizeof(unsigned int);
	m_statistics->m_numRols = m_numRols;

	pthread_mutex_lock(&m_rolArray[rolId].m_ecrMutex);
	m_statistics->m_rolStats[rolId].reset();
	pthread_mutex_unlock(&m_rolArray[rolId].m_ecrMutex);

	m_statistics->m_rolStats[rolId].m_rolId = rolId;
	m_statistics->m_rolStats[rolId].m_robId = m_configuration->getRolConfig(rolId)->getRobId();

	m_statistics->m_rolStats[rolId].m_fragStatSize = LAST_FRAGSTAT;
	m_statistics->m_rolStats[rolId].m_pageStatSize = LAST_PAGESTAT;
	m_statistics->m_rolStats[rolId].m_mostRecentId = 0xffffffff;

	// clear LDOWN and XOFF counters link control
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroLinkDownCounter = 1;
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroLinkDownCounter = 0;

	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroXoffCounter = 1;
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.ZeroXoffCounter = 0;

	calcPageCount(rolId);

}

/******************************/
void RobinNP::initFifoDuplicators()
/******************************/
{
	unsigned int memorySize = s_4k; //Allocate ring buffer - 4K page
	unsigned int memorySizeUPF = 0;
	if(m_designVersion > 0x1060004){
		memorySizeUPF = 1 << (m_Bar0Common->GlobalControlRegister.FDSourceWidthBits + m_Bar0Common->GlobalControlRegister.FDDestinationLengthElementsLarge); //Allocate according to firmware parameter
	}
	else{
		memorySizeUPF = s_4k;
	}
	//std::cout << "RobinNP::initFifoDuplicators: Using UPF Duplicator Ring Buffer Size of " << memorySizeUPF << " Bytes as per firmware parameters" << std::endl;

	m_writeIndexBase = (volatile unsigned int*)(mmap(0, memorySize, (PROT_WRITE|PROT_READ), MAP_SHARED, m_devHandle, ((m_cardData.writeIndexHandle & 0xffffffff))));
	if(m_writeIndexBase == (unsigned int*)-1)
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_initfifodup4, ROSRobinNPExceptions, MMAP, "A call to mmap Base Write Index returns error:  " << strerror(errno) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_initfifodup4);
	}

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		//std::cout << "Mappings For SubRob " << subRob << " ---------------------------------------" << std::endl;
		//Store FD Register Locations
		/*std::cout << "DMA Done Dup Size " << std::hex << memorySize << std::endl;
		std::cout << "DMA Done Dup Phys Addr (from driver)" << std::hex << m_cardData.dmaDoneDuplicateHandle[subRob] << std::dec << std::endl;
*/
		m_duplicateDMADone[subRob].buffer = (volatile unsigned int*)mmap(0, memorySize, (PROT_WRITE|PROT_READ), MAP_SHARED, m_devHandle, m_cardData.dmaDoneDuplicateHandle[subRob]);
		if(m_duplicateDMADone[subRob].buffer == (unsigned int*)-1)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_initfifodup1, ROSRobinNPExceptions, MMAP, "A call to mmap DMA Done Buffer returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_initfifodup1);
		}
		//std::cout << "DMA Done Dup Virt Addr " << std::hex << (void*)m_duplicateDMADone[subRob].buffer << std::dec << std::endl;

		m_duplicateDMADone[subRob].writeIndexCopy = m_writeIndexBase + 0x2*subRob;

		m_duplicateDMADone[subRob].readIndex = (unsigned int*)&m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.ReadIndexBottom;

		m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationBottom = m_cardData.dmaDoneDuplicateHandle[subRob]& 0xffffffff;
		m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationTop = (m_cardData.dmaDoneDuplicateHandle[subRob]>>32)& 0xffffffff;

		m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationBottom = (volatile unsigned int)((m_cardData.writeIndexHandle + 0x8*subRob)& 0xffffffff);
		m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationTop = (volatile unsigned int)(((m_cardData.writeIndexHandle + 0x8*subRob)>>32)& 0xffffffff);
		/*std::cout << "Read back values written to firmware ------------------" << std::endl;
		std::cout << "DMA Done WI Loc Register lower: " << std::hex << m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationBottom << std::endl;
		std::cout << "DMA Done WI Loc Register upper: " << std::hex << m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationTop << std::endl;

		std::cout << "DMA Done FD Loc Register lower: " << std::hex << m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationBottom << std::endl;
		std::cout << "DMA Done FD Loc Register upper: " << std::hex << m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.RingBufferLocationTop << std::endl;
*/
		m_duplicateDMADone[subRob].readIndexCopy = 0x0;
		*(m_duplicateDMADone[subRob].readIndex) = 0x0;
		*(m_duplicateDMADone[subRob].writeIndexCopy) = 0x0;

		m_Bar0SubRob[subRob]->SubRobControl.Component.DMADoneFIFODuplicatorEnable = 1;

		for(int counter = 0; counter < 256; ++counter){
			*(m_duplicateDMADone[subRob].buffer+4*counter+0) = 0x9999;
			*(m_duplicateDMADone[subRob].buffer+4*counter+1) = 0x8888;
			*(m_duplicateDMADone[subRob].buffer+4*counter+2) = 0x7777;
			*(m_duplicateDMADone[subRob].buffer+4*counter+3) = 0x6666;
		}

		//Store FD Register Locations - only grabbing lowest 32bits.
		//std::cout << "UPF Dup Size " << std::hex << memorySizeUPF << std::endl;
		//std::cout << "UPF Dup Phys Addr (from driver)" << std::hex << m_cardData.upfDuplicateHandle[subRob] << std::dec << std::endl;
		m_duplicateUPF[subRob].buffer = (volatile unsigned int*)mmap(0, memorySizeUPF, (PROT_WRITE|PROT_READ), MAP_SHARED, m_devHandle, m_cardData.upfDuplicateHandle[subRob]& 0xffffffff);
		if(m_duplicateUPF[subRob].buffer == (unsigned int*)-1)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_initfifodup3, ROSRobinNPExceptions, MMAP, "A call to mmap UPF Buffer returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_initfifodup3);
		}
		//std::cout << "UPF Dup Virt Addr " << std::hex << (void*)m_duplicateUPF[subRob].buffer << std::dec << std::endl;
		m_duplicateUPF[subRob].writeIndexCopy = m_writeIndexBase + 0x2*(subRob + 2);

		//Map Read Index on Board
		m_duplicateUPF[subRob].readIndex = (unsigned int*)&m_Bar0SubRob[subRob]->UPFDuplicateReadIndexRegister.lower;

		//Store local memory locations for ring buffer and write index on board
		m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.lower = (volatile unsigned int)(m_cardData.upfDuplicateHandle[subRob]& 0xffffffff);
		m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.upper = (volatile unsigned int)((m_cardData.upfDuplicateHandle[subRob]>>32)& 0xffffffff);

		m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.lower = (volatile unsigned int)((m_cardData.writeIndexHandle + 0x8*(subRob + 2))& 0xffffffff);
		m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.upper = (volatile unsigned int)(((m_cardData.writeIndexHandle + 0x8*(subRob + 2))>>32)& 0xffffffff);
		/*std::cout << "Read back values written to firmware ------------------" << std::endl;
		std::cout << "UPF WI Loc Register lower: " << std::hex << m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.lower << std::endl;
		std::cout << "UPF WI Loc Register upper: " << std::hex << m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.upper << std::endl;

		std::cout << "UPF FD Loc Register lower: " << std::hex << m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.lower << std::endl;
		std::cout << "UPF FD Loc Register upper: " << std::hex << m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.upper << std::endl;
*/

		//Initialise Read and Write indices
		m_duplicateUPF[subRob].readIndexCopy = 0x0;
		*(m_duplicateUPF[subRob].readIndex) = 0x0;
		*(m_duplicateUPF[subRob].writeIndexCopy) = 0x0;

		//Initialise Ring Buffer Memory Space
		for(unsigned int counter = 0; counter < m_upfMaskVal + 1; ++counter){
			*(m_duplicateUPF[subRob].buffer+4*counter+0) = 0x9999;
			*(m_duplicateUPF[subRob].buffer+4*counter+1) = 0x8888;
			*(m_duplicateUPF[subRob].buffer+4*counter+2) = 0x7777;
			*(m_duplicateUPF[subRob].buffer+4*counter+3) = 0x6666;
		}

		//Set Global Enable Bit for Common Duplicator
		m_Bar0SubRob[subRob]->SubRobControl.Component.CommonUPFDuplicatorEnable = 1;
	}
}


/******************************/
void RobinNP::unmapFifoDuplicator()
/******************************/
{
	unsigned int memorySize = s_4k;
	unsigned int memorySizeUPF = 0;
	if(m_designVersion > 0x1060004){
		memorySizeUPF = 1 << (m_Bar0Common->GlobalControlRegister.FDSourceWidthBits + m_Bar0Common->GlobalControlRegister.FDDestinationLengthElementsLarge); //Allocate according to firmware parameter
	}
	else{
		memorySizeUPF = s_4k;
	}

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){

		int ret = munmap((void *)m_duplicateDMADone[subRob].buffer, memorySize);
		if (ret)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_10, ROSRobinNPExceptions, MUNMAP, "A call to munmap subRob " << subRob << " DMA Done Duplicator Buffer returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_10);
		}

		ret = munmap((void *)m_duplicateUPF[subRob].buffer, memorySizeUPF);
		if (ret)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_unmapfifodup1, ROSRobinNPExceptions, MUNMAP, "A call to munmap subRob " << subRob << " UPF Duplicator Buffer returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_unmapfifodup1);
		}

	}

	int ret = munmap((void *)(m_writeIndexBase), memorySize);
	if (ret)
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_10, ROSRobinNPExceptions, MUNMAP, "A call to munmap Base Write Index Copy returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_10);
	}

}

void RobinNP::acquireDevice(){

	// *********************
	// *Open RobinNP device*
	// *********************

	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialising RobinNP ****************************************");

		m_devHandle = open("/dev/robinnp", O_RDWR, 0);
		if (m_devHandle < 0)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_acqdev0a, ROSRobinNPExceptions, OPEN, "RobinNP::acquireDevice: Failed to open /dev/robinnp");
			throw (ex_robinnp_acqdev0a);
		}
		DEBUG_TEXT(DFDB_ROSROBINNP, 20, "RobinNP::acquireDevice: m_dev_handle = " << m_devHandle);

		//Code assumes one card per system
		m_cardData.slot = m_slotNumber;

		int iores = ioctl(m_devHandle,SETLINK,&(m_cardData)); //request hardware information from driver
		if(iores == -1){
			CREATE_ROS_EXCEPTION(ex_robinnp_acqdev0, ROSRobinNPExceptions, INIT, "IOCTL for slot " << m_slotNumber << " returns error: " << strerror(errno));
			throw (ex_robinnp_acqdev0);
		}
		
		m_deviceAcquired = true;
		

		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: Current Implementation for one card per system, m_devHandle = 0x" << HEX(m_devHandle));

		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: upfhandle1 bottom = 0x" << HEX((m_cardData.upfDuplicateHandle[0] & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: upfhandle1 top = 0x" << HEX(((m_cardData.upfDuplicateHandle[0] >> 32 ) & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: upfhandle2 bottom = 0x" << HEX((m_cardData.upfDuplicateHandle[1] & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: upfhandle2 top = 0x" << HEX(((m_cardData.upfDuplicateHandle[1] >> 32 ) & 0xffffffff)));

		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: writeindexhandle base bottom = 0x" << HEX((m_cardData.writeIndexHandle & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: writeindexhandle base top = 0x" << HEX(((m_cardData.writeIndexHandle >> 32 ) & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: dmaDoneDuplicateHandle bottom = 0x" << HEX((m_cardData.dmaDoneDuplicateHandle[0] & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: dmaDoneDuplicateHandle top = 0x" << HEX(((m_cardData.dmaDoneDuplicateHandle[0] >> 32 ) & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: dmaDoneDuplicateHandle bottom = 0x" << HEX((m_cardData.dmaDoneDuplicateHandle[1] & 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: dmaDoneDuplicateHandle top = 0x" << HEX(((m_cardData.dmaDoneDuplicateHandle[1] >> 32 ) & 0xffffffff)));

		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: primaryDmaHandle[0] bottom = 0x" << HEX((m_cardData.primaryDmaHandle[0]& 0xffffffff)));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::acquireDevice: Initialise RobinNP: primaryDmaHandle[0] top = 0x" << HEX(((m_cardData.primaryDmaHandle[0] >> 32 ) & 0xffffffff)));

}

/*************************/
void RobinNP::initDevice()
/*************************/
{
	// **********************************************************
	// *Initialise RobinNP device and assign memory to global pointers*
	// **********************************************************

	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initDevice: Initialising RobinNP ****************************************");

	//Assign virtual addresses to global pointers
	m_bar0Addr = (unsigned long)mmap(0,m_cardData.registerSize,PROT_READ|PROT_WRITE,MAP_SHARED,m_devHandle,m_cardData.registerBaseAddress);
	if(m_bar0Addr == (unsigned long)-1){
		CREATE_ROS_EXCEPTION(ex_robinnp_acqdev1, ROSRobinNPExceptions, MMAP, "Unable to mmap RobinNP base address register 0, error: " << strerror(errno) << ", Device handle = " << m_devHandle << " BAR0 = 0x" << m_cardData.registerBaseAddress << " RobinNP " << m_slotNumber);
		throw (ex_robinnp_acqdev1);
	}
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initDevice: Initialise RobinNP: Base Address Register BAR0 = 0x" << HEX(m_bar0Addr));

	m_Bar0Common = (BAR0CommonStruct*)(m_bar0Addr);
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initDevice: Common mapped to " << HEX((void *)m_Bar0Common));
	m_numChannelsPerSubRob   = m_Bar0Common->DesignID.NumberOfChannelsPerSubRob;
	m_numSubRobs   = m_Bar0Common->DesignID.NumberOfSubRobs;


	if(m_numRols == 0){
		DEBUG_TEXT(DFDB_ROSROBINNP, 5, "RobinNP::initDevice: Setting number of ROLs to default provided by card. Number or ROLs available " << m_cardData.nChannels);
		m_numRols = m_cardData.nChannels;
	}
	else if(m_numRols > m_cardData.nChannels){
		CREATE_ROS_EXCEPTION(ex_robinnp_acqdev2, ROSRobinNPExceptions, ROLCOUNT, "Requested " << m_numRols << " but only " << m_cardData.nChannels << " available on RobinNP " << m_slotNumber);
		throw (ex_robinnp_acqdev2);
	}

	for(unsigned int rol = 0; rol < m_numRols; ++rol){
		m_Bar0Channel[rol] = (BAR0ChannelStruct*)(m_bar0Addr + 0x1000 + (rol * 0x100));
		m_rolArray[rol].subRobMapping = rol / m_numChannelsPerSubRob;
		//std::cout << "Mapped ROL " << rol << " to subRob " << m_rolArray[rol].subRobMapping << std::endl;
	}

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		m_Bar0SubRob[subRob] = (BAR0SubRobStruct*)(m_bar0Addr + 0x4000 + (subRob * 0x200));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initDevice: SubRob " << subRob << " mapped to " << HEX((void *)m_Bar0SubRob[subRob]));
	}

	m_physicalArrayPointer = new unsigned long[m_cardData.nDmaPages];
	m_virtualArrayPointer = new unsigned char*[m_cardData.nDmaPages];

	//Store Locations of DMA Landing Area Pages Allocated by RobinNP Driver, mapping to virtual memory in the process
	//m_pageVector.resize(m_cardData.nDmaPages);
	//m_dataPageVector.resize(m_cardData.nDmaPages);
	//std::cout << "num available pages " << m_cardData.nDmaPages << std::endl;
	for(int pagenumber = 0; pagenumber< m_cardData.nDmaPages; ++pagenumber){
		//std::cout << "try to allocate page " << pagenumber << " size " << m_cardData.dmaPageSize << std::endl;
		m_physicalArrayPointer[pagenumber] = (unsigned long)((m_cardData.primaryDmaHandle[pagenumber])& 0xffffffff);
		m_virtualArrayPointer[pagenumber] = (unsigned char *)mmap(0, m_cardData.dmaPageSize, (PROT_READ|PROT_WRITE), MAP_SHARED, m_devHandle, m_physicalArrayPointer[pagenumber]);
		if (m_virtualArrayPointer[pagenumber] == (unsigned char*)-1)
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_acqdev3, ROSRobinNPExceptions, MMAP, "A call to mmap returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
			throw (ex_robinnp_acqdev3);
		}
		m_pageVector.push_back(std::make_pair(m_physicalArrayPointer[pagenumber],m_virtualArrayPointer[pagenumber]));
		m_dataPageVector.push_back(new DataPage((unsigned int*)m_virtualArrayPointer[pagenumber],(unsigned int*)m_physicalArrayPointer[pagenumber]));
		if(m_physicalArrayPointer[pagenumber] == 0){
			CREATE_ROS_EXCEPTION(ex_robinnp_acqdev4, ROSRobinNPExceptions, NOPAGES, "DMA Physical Address Reported as Zero for RobinNP " << m_slotNumber);
			throw (ex_robinnp_acqdev4);
		}
	}

	//std::cout << "pages mapped successfully" << std::endl;
	int ret = ioctl(m_devHandle, INIT_IRQ);
	if (ret)
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_acqdev5, ROSRobinNPExceptions, INTERRUPTINIT, "Slot " << m_slotNumber << " IOCTL returns error: " << strerror(errno) << " for RobinNP " << m_slotNumber);
		throw (ex_robinnp_acqdev5);
	}

	m_designVersion = (m_Bar0Common->DesignID.DesignVersion << 24) | (m_Bar0Common->DesignID.DesignMajorRevision << 16) | m_Bar0Common->DesignID.DesignMinorRevision;
	std::cout << "Firmware Design ID " << std::hex << m_designVersion << std::dec << std::endl;

	if(m_designVersion > 0x1060004){
		m_upfMaskVal = (1 << m_Bar0Common->GlobalControlRegister.FDDestinationLengthElementsLarge) - 1;
	}
	else{
		m_upfMaskVal = 0xFF;
	}
	std::cout << "RobinNP::initDevice: UPF Mask Value Calculated as " << std::hex << m_upfMaskVal << std::dec << std::endl;

}

/*************************/
void RobinNP::initSequence(){
	/*************************/

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initSequence: Started");
	initFifoDuplicators();

#ifdef FIXED_RUN_NUMBER
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initSequence: Using fixed run number for event lookup");
#else
	DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initSequence: Using external run number for event lookup");
#endif

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initSequence: Initialising large global structures");
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initSequence: Number of ROLs: " << m_numRols);

	// initialise configuration parameters, with each ROL set as default
	initConfigurationParameters();

	for (unsigned int rolId = 0; rolId < m_numRols; ++rolId)
	{
		// setup item list
		m_rolArray[rolId].m_itemListPtr = m_rolArray[rolId].m_itemList;
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initSequence: Initial address of item list (" << rolId << "): 0x" << HEX(m_rolArray[rolId].m_itemListPtr));
		// check caching option for item management structure

		m_rolArray[rolId].m_hashArrayPtr = m_rolArray[rolId].m_hashArray;
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::initSequence: Initial address of hash list (" << rolId << "): 0x" << HEX(m_rolArray[rolId].m_hashArrayPtr));
		// check caching option for item management structure

		// init run control state to inactive
		m_rolArray[rolId].m_runCtlState = (m_configuration->getRolConfig(rolId)->getRolEnabled()) ? s_runCtlStart : s_runCtlStop;

		initRol(rolId);

		// enable  ROLs
		if (s_runCtlStart == m_rolArray[rolId].m_runCtlState){
			enableRol(rolId);	// enable link
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initSequence: ROL " << rolId << " enabled");
		} else {
			disableRol(rolId);
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::initSequence: ROL " << rolId << " not activated");
		}

	}

}

/*************************************************************************/
/*                        Indexer                                        */
/*************************************************************************/

/******************************************************/
bool RobinNP::processUPFDuplicatorPage(int readIndex, unsigned int subRob,FragInfo &fragment)
/******************************************************/
{
	//acquire new fragment page from used page FIFO duplicator.
	//return false if fragment invalid or channel inactive

	unsigned int status = readCombinedUPFDuplicator(readIndex, 0, subRob);

	int channel_number = subRob*m_numChannelsPerSubRob + ((status & upfChannel) >> 16);

	if (upfValid != (status & upfValid)){
		std::stringstream ss;
		ss << "RobinNP::processUPFDuplicatorPage: SubRob " << subRob << " channel " << channel_number << " UPF page Not Valid. Status = 0x" << HEX(status) << ", Host RI = 0x" << HEX(readIndex) << " - FW RI 0x" << HEX(m_Bar0SubRob[subRob]->PrimaryDMADoneFIFODuplicator.ReadIndexBottom) << " - Host WI 0x" << HEX(*(m_duplicateUPF[subRob].writeIndexCopy)) << " - FW WI 0x" << HEX(m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexRegister.lower) << " -- " << HEX(readCombinedUPFDuplicator(readIndex, 0, subRob)) << "  " << HEX(readCombinedUPFDuplicator(readIndex, 1, subRob)) << " " << HEX(readCombinedUPFDuplicator(readIndex, 2, subRob)) << " " << HEX(readCombinedUPFDuplicator(readIndex, 3, subRob))<<  std::endl;
		ss << "UPFD LOC Lower 0x" << HEX(m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.lower) << " UPFD LOC Upper 0x" << HEX(m_Bar0SubRob[subRob]->UPFDuplicateStartLocationRegister.upper) << " WI Host Loc Lower 0x" << HEX(m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.lower) << " WI Host Loc Upper 0x" << HEX(m_Bar0SubRob[subRob]->UPFDuplicateWriteIndexLocationRegister.upper) << std::endl;
		screenPrint(ss.str());

		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: UPF page Not Valid. Status = 0x" << HEX(status) << ", read index = 0x" << HEX(readIndex) << "!");
		addFreePageToStack(channel_number,fragment.upfInfo.pageNum);
		updateCombinedUPFReadIndex(subRob);
		m_statistics->m_rolStats[channel_number].m_pageStat[pagesInvalid]++;
		return false;
	}

	// process activated channels only
	if (s_runCtlStart != m_rolArray[channel_number].m_runCtlState){
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Channel " << HEX(channel_number) << " not active, " << readIndex);
		addFreePageToStack(channel_number,fragment.upfInfo.pageNum);
		updateCombinedUPFReadIndex(subRob);
		m_statistics->m_rolStats[channel_number].m_pageStat[pagesInvalid]++;
		return false;
	}
/*
	std::stringstream ss;
	ss << "Received Event " << fragment.upfInfo.eventId << " rol ROL " << channel_number << " Read Index " << m_duplicateUPF[subRob].readIndexCopy << " write index " << *(m_duplicateUPF[subRob].writeIndexCopy) << std::endl;
	screenPrint(ss.str());
*/
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Read Combined UPF Duplicator register 0 at 0x" << HEX(readCombinedUPFDuplicator(readIndex, 0, subRob)) << ":0x" << HEX(status));

	//static FragInfo fragment;
	fragment.upfInfo.eventId = readCombinedUPFDuplicator(readIndex, 1, subRob);

	// read next FIFO words in one go. This might enable bursting

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Read Combined UPF Duplicator register 1 at 0x" << HEX(readCombinedUPFDuplicator(readIndex, 1, subRob)) << ":0x" << HEX(fragment.upfInfo.eventId));

	unsigned int pageAddress = readCombinedUPFDuplicator(readIndex, 2, subRob);
	//std::cout << std::hex << "Address " << pageAddress << std::endl;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Read Combined UPF Duplicator register 2 at 0x" << HEX(readCombinedUPFDuplicator(readIndex, 2, subRob)) << ":0x" << HEX(pageAddress));

	fragment.upfInfo.runNum = readCombinedUPFDuplicator(readIndex, 3, subRob);
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Read Combined UPF Duplicator register 3 at 0x" << HEX(readCombinedUPFDuplicator(readIndex, 3, subRob)) << ":0x" << HEX(fragment.upfInfo.runNum));

	// get trigger type from pageInfo field
	fragment.trigType = (status & upfTtMask) >> upfTtShift;
	// mask address bits
	pageAddress &= upfAddrMask;    // mask off end marker bits
	// copy status
	fragment.upfInfo.status = status & upfStatusMask;
	fragment.upfInfo.pageLen = (pageAddress & (m_configuration->getPageSize() - 1)); // make size from full address

	// increment length except on last page, if control words suppressed
	// increment page length
	if ((upfEndMarker != (upfEndMask & fragment.upfInfo.status))){
		// increment always
		fragment.upfInfo.pageLen++;
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Corrected page length is 0x" << HEX(fragment.upfInfo.pageLen));
	} else {
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processUPFDuplicatorPage: Page length unchanged: 0x" << HEX(fragment.upfInfo.pageLen));
	}
	fragment.upfInfo.pageNum = (pageAddress >> m_pageSizeOffset) - ((channel_number - subRob*m_numChannelsPerSubRob)* m_numPagesPerRol);
	fragment.RolId = channel_number;
	/*std::stringstream ss;
	ss << "Received Event " << fragment.upfInfo.eventId << " rol ROL " << channel_number << " num pages " << m_rolArray[channel_number].numPages << " stack " << m_rolArray[channel_number].m_freeTos << " indexed " << m_rolArray[channel_number].pagesInIndex << std::endl;
	screenPrint(ss.str());*/
	updateCombinedUPFReadIndex(subRob);
	pthread_mutex_lock(&m_rolArray[channel_number].m_pageStackMutex);

#ifdef SLINKERRBITS_TEST
	std::cout << "status " << HEX(fragment.upfInfo.status) << " " << HEX(status) << std::endl;
	std::cout << "ROL " << channel_number << " ID " << fragment.upfInfo.eventId << std::hex  << " page " << fragment.upfInfo.pageNum << " status " << status << std::dec << std::endl;
#endif
	pthread_mutex_unlock(&m_rolArray[channel_number].m_pageStackMutex);

	return true;
}

/*********************************************************************/
void RobinNP::processIncomingFragment(FragInfo *theFragment, int rolId)
/*********************************************************************/
{
	//Process fragment
	MgmtEntry *entry = 0;
	unsigned int newEcrId;//, newPrimaryId;
	unsigned int oldEcrId; // , oldPrimaryId;
	unsigned int fragmentErrors = 0;  // temporary error accumulator

	int captureId;

	EventTag eventTag;

#ifdef FIXED_RUN_NUMBER
	theFragment->upfInfo.runNum = 0;	// fix run number
#endif

	if (m_rolArray[rolId].m_fragmentComplete == true){			// last fragment completed ?
		//check and store ECR jump
		oldEcrId = (m_statistics->m_rolStats[rolId].m_mostRecentId & upfEcrMask) >> upfEcrShift;

		//newPrimaryId = theFragment->upfInfo.eventId & ~upfEcrMask;
		newEcrId = (theFragment->upfInfo.eventId  & upfEcrMask) >> upfEcrShift;
		m_rolArray[rolId].m_ecrJump = (oldEcrId == newEcrId) ? 0 : 1;
		// check sequence errors
		if (checkSeqError(m_statistics->m_rolStats[rolId].m_mostRecentId, theFragment->upfInfo.eventId)){
			// mark fragment
			fragmentErrors |= fragStatusSeqError;
			// update status
			m_statistics->m_rolStats[rolId].m_fragStat[fragsOutOfSync]++;
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::processIncomingFragment: ROL " << rolId << " fragment synchronisation lost: current id: 0x" << HEX(theFragment->upfInfo.eventId) << ", previous id: 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::processIncomingFragment: Page number 0x" << HEX(theFragment->upfInfo.pageNum) << ", size 0x" << HEX(theFragment->upfInfo.pageLen));
			CREATE_ROS_EXCEPTION(ex_robinnp_procinc, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::processIncomingFragment: ROL " << rolId << " Fragment out of sequence: L1 ID = 0x" << HEX(theFragment->upfInfo.eventId) << ", Most Recent ID 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId) << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_procinc);

		}

		// initialise inhibit mechanism
		m_rolArray[rolId].m_inhibitFragment = false;
		// check for unrecoverable errors. This is already known with the first page of a fragment
		if (theFragment->upfInfo.status & upfErrorMask){
#ifdef SLINKERRBITS_TEST
			for (auto statusPair : m_fragStatusMapDebug) {
				if ((theFragment->upfInfo.status & statusPair.first) == statusPair.first ){
					std::cout << "ERROR " << statusPair.second << " " << HEX(statusPair.first) << " " << HEX(theFragment->upfInfo.status) << std::dec << std::endl;
					std::cout << "-------------------------------------------------------" << std::endl;
				}
			}
#endif
			// sequence errors do no longer qualify a soft error to reject fragments
			if (theFragment->upfInfo.status & upfHardErrorMask){
				m_rolArray[rolId].m_inhibitFragment = true;
				DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::processIncomingFragment: Fragment has unrecoverable error");
			}
		}


		m_rolArray[rolId].m_fragmentComplete = (upfEndMarker == (theFragment->upfInfo.status & upfEndMask)) ? true:false;	// test last page

		if (m_rolArray[rolId].m_inhibitFragment){
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Suppressing first page of erroneous fragment");
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Fragment status is 0x" << HEX(theFragment->upfInfo.status));
			m_statistics->m_rolStats[rolId].m_pageStat[pagesSupressed]++;
			addRejectedPage(theFragment, m_statistics->m_rolStats[rolId].m_mostRecentId, rolId);
		} else {
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Processing new fragment for id 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));

			// set event tag here
			eventTag.eventId = theFragment->upfInfo.eventId;
			eventTag.runNumber = theFragment->upfInfo.runNum;

			// remove previous fragment and update statistics
			if (deleteIndexedFragment(&eventTag, rolId, true)){
				// mark fragment
				fragmentErrors |= fragStatusDuplicate;
				// update status
				m_statistics->m_rolStats[rolId].m_fragStat[fragsReplaced]++;
				// send interrupt
				CREATE_ROS_EXCEPTION(ex_robinnp_processincfrag1, ROSRobinNPExceptions, FRAGDUP,"RobinNP::processIncomingFragment: Fragment for L1ID 0x"
						<< HEX(eventTag.eventId) << " already exists in index for ROL 0x" << HEX(rolId) << " replacing with newer version for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_processincfrag1);
			}
			// now add page
			indexFragmentPage(theFragment,rolId);
			m_statistics->m_rolStats[rolId].m_fragStat[fragsAdded]++;

			// save position of first page of the fragment
			m_rolArray[rolId].m_fragStartPage = theFragment->upfInfo.pageNum;
			// copy to last position
			m_rolArray[rolId].m_fragLastPage = theFragment->upfInfo.pageNum;
			// set initial page count
			m_rolArray[rolId].m_fragPages = 1;

			// get pointer to management entry for the first page
			entry = &m_rolArray[rolId].m_itemListPtr[m_rolArray[rolId].m_fragStartPage];

			// copy status to item
			entry->status = fragmentErrors;
			if (theFragment->upfInfo.status & upfErrorMask){
				for (auto statusElement : m_fragErrorCombinations) {
					if ((theFragment->upfInfo.status & statusElement.first) == statusElement.first){
						entry->status |= statusElement.first;	// copy status and error bits to global
					}
				}
			}

			// initialise total size
			entry->size = theFragment->upfInfo.pageLen;
			// set event id
			entry->info.upfInfo.eventId = theFragment->upfInfo.eventId;
		}
	} else {  // this is not the first page of a fragment
		// check completeness
		m_rolArray[rolId].m_fragmentComplete = (upfEndMarker == (theFragment->upfInfo.status & upfEndMask)) ? true:false;	// test last page

		if (m_rolArray[rolId].m_inhibitFragment){
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Suppressing subsequent page of erroneous fragment");
			m_statistics->m_rolStats[rolId].m_pageStat[pagesSupressed]++;
			addRejectedPage(theFragment, m_statistics->m_rolStats[rolId].m_mostRecentId, rolId);
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Fragment status is 0x" << HEX(theFragment->upfInfo.status));
		} else {
			// increment page count
			m_rolArray[rolId].m_fragPages++;

			// set event tag here
			eventTag.eventId = theFragment->upfInfo.eventId;
			eventTag.runNumber = theFragment->upfInfo.runNum;

			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Processing subsequent fragment for id 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Fragment status is 0x" << HEX(theFragment->upfInfo.status));

			// don't add 0-size pages
			if (theFragment->upfInfo.pageLen != 0){
				// if (m_rolArray[rolId].m_fragPages < bmMaxFragPages) {
				if (m_rolArray[rolId].m_fragPages <= m_configuration->getMaxRxPages()) {	// configurable max size
					// find position of first entry for this id
					// position = findIndexedFragment(theFragment->eventId,rolId);
					// now add page
					indexFragmentPage(theFragment,rolId);
					// update last page pointer
					m_rolArray[rolId].m_fragLastPage = theFragment->upfInfo.pageNum;
				} else {
					DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::processIncomingFragment: Truncating oversize fragment id 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));
					m_statistics->m_rolStats[rolId].m_pageStat[pagesSupressed]++;
					// don't add page to jail, but discard immediately
					// addRejectedPage(theFragment, pciIrqTruncate, m_statistics->m_rolStats[rolId].m_mostRecentId, rolId);
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Releasing truncated page 0x" << HEX(theFragment->upfInfo.pageNum) << " for event 0x" << HEX(theFragment->upfInfo.eventId) << ", channel " << rolId);
					addFreePageToStack(rolId,(MgmtPtr)theFragment->upfInfo.pageNum);
					// update status
					entry = &m_rolArray[rolId].m_itemListPtr[m_rolArray[rolId].m_fragStartPage];
					entry->status |= fragStatusTrunc;
					// set completion flag
					entry = &m_rolArray[rolId].m_itemListPtr[m_rolArray[rolId].m_fragLastPage];
					//  ... on last page
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Setting termination marker for truncated fragment on page 0x" << HEX(m_rolArray[rolId].m_fragLastPage));
					entry->info.upfInfo.status |= upfEndMarker;
				}
			} else {
				// mark currently last (= previous) page as last page of fragment
				entry = &m_rolArray[rolId].m_itemListPtr[m_rolArray[rolId].m_fragLastPage];
				// set end marker on last page
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Setting termination marker on page 0x" << HEX(m_rolArray[rolId].m_fragLastPage));
				entry->info.upfInfo.status |= upfEndMarker;
				// release 0 size page
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Releasing 0-sized page 0x" << HEX(theFragment->upfInfo.pageNum) << " for event 0x" << HEX(theFragment->upfInfo.eventId) << ", channel " << rolId);
				addFreePageToStack(rolId,(MgmtPtr)theFragment->upfInfo.pageNum);

			}
#ifdef SLINKERRBITS_TEST
			if (theFragment->upfInfo.status & upfErrorMask){
				for (auto statusPair : m_fragStatusMapDebug) {
					if ((theFragment->upfInfo.status & statusPair.first) == statusPair.first ){
						std::cout << "ERROR Subsequent page: " << statusPair.second << " " << HEX(statusPair.first) << " " << HEX(theFragment->upfInfo.status) << std::dec << std::endl;
						std::cout << "-------------------------------------------------------" << std::endl;
					}
				}
			}
#endif
			// update fragment status in first page
			entry = &m_rolArray[rolId].m_itemListPtr[m_rolArray[rolId].m_fragStartPage];
			entry->status |= fragmentErrors;
			if (theFragment->upfInfo.status & upfErrorMask){
				for (auto statusElement : m_fragErrorCombinations) {
					if ((theFragment->upfInfo.status & statusElement.first) == statusElement.first){
						entry->status |= statusElement.first;	// update status and error bits to global
					}
				}
			}

			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Accumulated status: 0x" << HEX(entry->status));

			// update total size if not truncated
			if ((entry->status & fragStatusTrunc) == 0){
				entry->size += theFragment->upfInfo.pageLen;
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Accumulated size: 0x" << HEX(entry->size));
			} else {
				DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::processIncomingFragment: Truncated size: 0x" << HEX(entry->size));
			}

			// test ID match of subsequent page
			if (theFragment->upfInfo.eventId != entry->info.upfInfo.eventId){
				CREATE_ROS_EXCEPTION(ex_robinnp_procinc, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::processIncomingFragment: ROL " << rolId << ": Event id mismatch across pages in multipage fragment: page id is 0x" << HEX(theFragment->upfInfo.eventId) << ", fragment id is: 0x" <<	HEX(entry->info.upfInfo.eventId) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_procinc);
				m_statistics->m_rolStats[rolId].m_multipageIndexError++;
			}

		}
	}

	// statistics and error checking in the end
	// update page counter for every page
	m_statistics->m_rolStats[rolId].m_pageStat[pagesReceived]++;
	// per completed fragment only
	if (m_rolArray[rolId].m_fragmentComplete == true){			// fragment completed ?
		// update fragment counter
		m_statistics->m_rolStats[rolId].m_fragStat[fragsReceived]++;

		if (m_rolArray[rolId].m_inhibitFragment){
			m_statistics->m_rolStats[rolId].m_fragStat[fragsCorrupted]++;
			m_statistics->m_rolStats[rolId].m_fragStat[fragsRejected]++;
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Processing of fragment with unrecoverable error completed");
		} else {
			// set completion flag
			entry->isComplete = true;
			entry->isErrorFragment = false;
			entry->toDelete = false;
			// set flag to capture event id
			captureId = 1;
			// check UPF error bits
			bool confirmTxError = false;
			bool confirmSequenceError = false;
			if ((entry->status & upfErrorMask) != 0 || (entry->status & fragStatusSeqError) != 0)  {
				for (auto statusElement : m_fragErrorCombinations) {
					if ((theFragment->upfInfo.status & statusElement.first)
							== statusElement.first) {
						m_statistics->m_rolStats[rolId].m_fragStat[statusElement.second]++;
						confirmTxError = true;
					}
				}
				if((entry->status & fragStatusSeqError) != 0){
					confirmSequenceError = true;
				}
				std::stringstream errors;
				if (confirmTxError || confirmSequenceError) {
					if(confirmTxError){
						entry->status |= fragStatusTxError; // set TX error
						DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Channel " << rolId << " status code 0x" << HEX(entry->status) << " on corrupted fragment 0x" << HEX(entry->info.upfInfo.eventId));
						m_statistics->m_rolStats[rolId].m_fragStat[fragsCorrupted]++;
						m_statistics->m_rolStats[rolId].m_fragStat[fragsTxError]++;
					}
					for (auto statusPair : m_fragStatusMapDebug) {
						if ((entry->status & statusPair.first)
								== statusPair.first) {
							errors << statusPair.second << "; ";
						}
					}

					if ((m_errorsDumped < m_errorDumpLimit
							|| m_errorDumpLimit == -1)
							&& (m_runMode == runModeGen2ROS)) {
						entry->isErrorFragment = true;
						std::string errorDescription = errors.str();
						requestErrorFragmentDMA(entry, rolId, errorDescription);
					} else {
						CREATE_ROS_EXCEPTION(ex_robinnp_procinc,
								ROSRobinNPExceptions, FRAGMENTERROR,
								"RobinNP::processIncomingFragment: ROL " << rolId << " Fragment marked with TX error(s). L1ID = 0x" << HEX(theFragment->upfInfo.eventId) << " for RobinNP " << m_slotNumber << ". Error description: " << errors.str() << " - no fragment dump as max reached");
						ers::warning(ex_robinnp_procinc);
					}

				}

			}

			// check truncation
			if ((entry->status & fragStatusTrunc) != 0) {	// check truncation
				m_statistics->m_rolStats[rolId].m_fragStat[fragsTruncated]++;
				CREATE_ROS_EXCEPTION(ex_robinnp_procinc, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::processIncomingFragment: ROL " << rolId << " Fragment truncated. L1ID = 0x" << HEX(theFragment->upfInfo.eventId) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_procinc);
			}

			// capture ID on kept fragments
			if (1 == captureId) {
				pthread_mutex_lock(&m_rolArray[rolId].m_ecrMutex);
				unsigned int previousId = m_statistics->m_rolStats[rolId].m_mostRecentId; 	// save old MRI
				m_statistics->m_rolStats[rolId].m_mostRecentId = entry->info.upfInfo.eventId;			// capture new id
				// move event log to end of fragment ...
				// log only ECR jumps
				if (1 == m_rolArray[rolId].m_ecrJump){
#ifdef SLINKERRBITS_TEST
					std::cout << "ECR detected " << std::endl;
#endif
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: ECR jump detected");
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: Current id: 0x" << HEX(theFragment->upfInfo.eventId) << ", previous id: 0x" << HEX(previousId));
					
					//if (m_statistics->m_rolStats[rolId].m_eventLog.overflow == 0){
					if((m_statistics->m_rolStats[rolId].m_eventLog.current != 0) || ((m_statistics->m_rolStats[rolId].m_eventLog.current == 0) && previousId != 0xFFFFFFFF)){
						m_statistics->m_rolStats[rolId].m_eventLog.eventArray[m_statistics->m_rolStats[rolId].m_eventLog.current] = previousId;
						m_statistics->m_rolStats[rolId].m_eventLog.current++;
						// advance pointer
						if (m_statistics->m_rolStats[rolId].m_eventLog.logSize <= m_statistics->m_rolStats[rolId].m_eventLog.current){
						  //m_statistics->m_rolStats[rolId].m_eventLog.overflow = 1;     // and set overflow flag
							m_statistics->m_rolStats[rolId].m_eventLog.current = 0;     // reset index for l1Id wrap
						}
					}
						//}

				}
				pthread_mutex_unlock(&m_rolArray[rolId].m_ecrMutex);
			}
		}
	} else {
		if (!m_rolArray[rolId].m_inhibitFragment){
			// reset completion flag
			entry->isComplete = false;
		}
	}

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processIncomingFragment: processIncomingFragment completed");

}

/*****************************************************************/
MgmtPtr RobinNP::findIndexedFragment(EventTag *eventTag, int rolId, unsigned int *lastID, bool doLocking)
/*****************************************************************/
{
	//find the first entry in the item list for an event id.
	//return 0 if not present
	if(lastID != 0){
		*lastID = m_statistics->m_rolStats[rolId].m_mostRecentId;
	}

	// first create the hash key from haskMask and id
	unsigned int fragId = eventTag->eventId;
	unsigned int hashKey = fragId & s_hashMask;

	if(doLocking){
		pthread_mutex_lock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	}

	// then get the head of list pointer from the hash array
	MgmtPtr hashPtr = m_rolArray[rolId].m_hashArrayPtr[hashKey];

	// test if there is a valid entry, indicated by a non-zero value
	if (hashPtr == 0) {
		if(doLocking){
			pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
		}

		return 0;
	}

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::findIndexedFragment: searching for id 0x" << HEX(eventTag->eventId) << ", run# 0x" << HEX(eventTag->runNumber));
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::findIndexedFragment: Initial hashptr: 0x" << HEX(hashPtr));



	// there is at least one entry, so walk entries to find matching id
	while (1){
		// get item
		MgmtEntry *item = &m_rolArray[rolId].m_itemListPtr[hashPtr];
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::findIndexedFragment: Loop hashptr: 0x" << HEX(hashPtr) << ", item address 0x" << HEX(item));
		// check id and run number
		unsigned int id = item->info.upfInfo.eventId;
		if ((id == fragId) && (item->info.upfInfo.runNum == eventTag->runNumber)) {
			break;			// return pointer to first entry with this id
		} else {
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::findIndexedFragment: skipping over id 0x" << HEX(item->info.upfInfo.eventId) << ", run# 0x" << HEX(item->info.upfInfo.runNum));
		}
		hashPtr = item->next;	// go to next item

		if (hashPtr == 0) {
			break;			// not found
		}
	}

	if(doLocking){
		pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	}

	return hashPtr;
}


unsigned int RobinNP::getDebugMemPage(unsigned int eventId, unsigned int rolId){
	unsigned int hashKey = eventId & s_hashMask;
	pthread_mutex_lock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	EventTag eventTag;
	eventTag.eventId = eventId;
	eventTag.runNumber = 0;

	MgmtPtr position = findIndexedFragment(&eventTag,rolId,0,false);
	MgmtEntry *debugItem = &m_rolArray[rolId].m_itemListPtr[position];
	pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	unsigned int memAddress =  (debugItem->info.upfInfo.pageNum + (rolId - m_rolArray[rolId].subRobMapping*m_numChannelsPerSubRob) * m_numPagesPerRol) * m_configuration->getPageSize();
	return memAddress;
}

/******************************************************************************/
void RobinNP::indexFragmentPage(FragInfo *fragment, int rolId)
/******************************************************************************/
{
	unsigned int hashKey = fragment->upfInfo.eventId & s_hashMask;
	pthread_mutex_lock(&m_rolArray[rolId].m_hashMutex[hashKey]);

	EventTag eventTag;
	eventTag.eventId = fragment->upfInfo.eventId;
	eventTag.runNumber = fragment->upfInfo.runNum;

	MgmtPtr position = findIndexedFragment(&eventTag,rolId,0,false);

	//add the entry to the hash list after the given position.
	//Note: new entries are normally added to the head of hash list.
	//check for available space must be done before calling this function

	// new entry is added at position of used page
	MgmtPtr usedPage = (MgmtPtr)fragment->upfInfo.pageNum;
	MgmtEntry *newItem = &m_rolArray[rolId].m_itemListPtr[usedPage];
	MgmtPtr nextPtr;
	MgmtPtr prevPtr;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Adding ROL " << rolId << " Event 0x" << HEX(fragment->upfInfo.eventId) << " at page 0x" << HEX(usedPage));

	// copy new fragment to list
	newItem->info = *fragment;
	// if position is 0, do hashing
	//	bool first = true;
	if (position == 0) {

		nextPtr = m_rolArray[rolId].m_hashArrayPtr[hashKey];	// get next pointer from hashPtr

		if (nextPtr != 0){				// update old head, if present
			m_rolArray[rolId].m_itemListPtr[nextPtr].prev = usedPage;
		}
		m_rolArray[rolId].m_hashArrayPtr[hashKey] = usedPage;	// update hash list
		prevPtr = 0;
	} else {
		MgmtEntry *item;
		// walk to end of list for this id
		while (position != 0){
			// use position pointer to get item at position
			item = &m_rolArray[rolId].m_itemListPtr[position];
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: adding: walk through list. Current position is 0x" << HEX(position));
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: value of pointer to next is 0x" << HEX(item->next));

			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Current item:");
#ifdef DUMP_ITEM
			dumpItem(item);
#endif

			prevPtr = position;				// save current item
			position = item->next;			// go to next item

			if (position != 0){				// check L1ID if next entry valid
				if ((fragment->upfInfo.eventId != m_rolArray[rolId].m_itemListPtr[position].info.upfInfo.eventId) || 	// different id ?
						(fragment->upfInfo.runNum != m_rolArray[rolId].m_itemListPtr[position].info.upfInfo.runNum)) {	// different run number
					m_rolArray[rolId].m_itemListPtr[position].prev = usedPage;				// update next element
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: New event reached");
#ifdef DUMP_ITEM
					dumpItem((MgmtEntry*)&m_rolArray[rolId].m_itemListPtr[position]);
#endif
					break;
				}
			}

		}

		// save position of next element
		nextPtr = position;

		// update previous element
		item->next = usedPage;	// update list

		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Previous item at page 0x" << HEX(item->info.upfInfo.pageNum) << " updated with page 0x" << HEX(usedPage));
	}

	// update new element
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Next pointer is 0x" << HEX(nextPtr));
	newItem->next = nextPtr;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Prev pointer is 0x" << HEX(prevPtr));
	newItem->prev = prevPtr;

	// update statistics for every page
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Updating status information, " << m_statistics->m_rolStats[rolId].m_pageStat[pagesAdded] << " pages added so far");
	m_statistics->m_rolStats[rolId].m_pageStat[pagesAdded]++;
	m_rolArray[rolId].pagesInIndex++;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Status information updated, " << m_statistics->m_rolStats[rolId].m_pageStat[pagesAdded] << " pages added");
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Item successfully added ");
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::indexFragmentPage: Event ID was 0x" << HEX(fragment->upfInfo.eventId));

	pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);

}

/***************************************************************/
bool RobinNP::deleteIndexedFragment(EventTag *eventTag, int rolId, bool isIndexer, bool doLocking)
/***************************************************************/
{

	// first create the hash key from haskMask and id
	unsigned int hashKey = eventTag->eventId & s_hashMask;
	// hashKey = fragId & m_hashMask;

	//find and delete event id.
	//put released entries back to pool
	//return 0 if not present

	MgmtEntry *item;
	unsigned int id, run;

	if(doLocking){
		pthread_mutex_lock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	}

	// then get the head of list pointer from the hash array
	MgmtPtr position = m_rolArray[rolId].m_hashArrayPtr[hashKey];

	if (position == 0){
		if(doLocking){
			pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
		}
		return false;
	}

	// test if there is a valid entry, indicated by a non-zero value
	while (position != 0){
		// get item
		item = &m_rolArray[rolId].m_itemListPtr[position];

		// check id
		id = item->info.upfInfo.eventId;
		run = item->info.upfInfo.runNum;

		// check delete status
		if(item->isErrorFragment){
			if(!isIndexer){
				CREATE_ROS_EXCEPTION(ex_robinnp_delete, ROSRobinNPExceptions, BADDELETE, "RobinNP::deleteIndexedFragment: ROL " << rolId << " attempt to delete captured fragment. L1ID = 0x" << HEX(id) << " for RobinNP " << m_slotNumber << " delete will proceed when capture complete");
				ers::warning(ex_robinnp_delete);
				item->toDelete = true;
				if(doLocking){
					pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
				}
				return false;
			}
		}

		if ((id == eventTag->eventId) && (run == eventTag->runNumber)) {
			break;			// return pointer to first entry with this id
		}

		position = item->next;	// go to next item

		if (position == 0) {
			break;			// not found
		}
	}

	if (position == 0){
		if(doLocking){
			pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
		}
		return false;
	}

	// save head and tail
	MgmtPtr head = item->prev;

	// walk to end of list for this id
	while (1){
		// remove entry: decrement TOS, add entry
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::deleteIndexedFragment: Removing element at position 0x" << HEX(position));
		MgmtPtr nextPosition = item->next;

		addFreePageToStack(rolId,(MgmtPtr)position);
		m_rolArray[rolId].pagesInIndex--;

		// update statistics for every page
		m_statistics->m_rolStats[rolId].m_pageStat[pagesDeleted]++;

		// advance to next entry
		position = nextPosition;
		item = &m_rolArray[rolId].m_itemListPtr[position];

		if (position == 0) {				// last entry ?
			break;
		}
		if ((id != m_rolArray[rolId].m_itemListPtr[position].info.upfInfo.eventId)
				|| (run != m_rolArray[rolId].m_itemListPtr[position].info.upfInfo.runNum)){	// different id or run?
			break;
		}
	}

	// update head and tail pointers
	if (head == 0){
		m_rolArray[rolId].m_hashArrayPtr[hashKey] = position;
	} else {
		m_rolArray[rolId].m_itemListPtr[head].next = position;
	}


	if( position != 0 ){
		item->prev = head;
	}
#ifdef MEMDEBUG
	isDeleted[rolId]->at(eventTag->eventId) = true;
#endif
	// update statistics for fragment
	m_statistics->m_rolStats[rolId].m_fragStat[fragsDeleted]++;

	if(doLocking){
		pthread_mutex_unlock(&m_rolArray[rolId].m_hashMutex[hashKey]);
	}
	return true;
}

/*************************************************************************************************/
bool RobinNP::addRejectedPage(FragInfo *theFragment, unsigned int/* mostRecentId*/, int rolId)
/*************************************************************************************************/
{
	unsigned int rejectedIndex = m_statistics->m_rolStats[rolId].m_savedRejectedPages;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::addRejectedPage: Saving corrupted page[" << rejectedIndex << "] channel " << rolId);
	if (s_mgmtRejectedSize <= rejectedIndex) {
		// release page if no space in jail
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::addRejectedPage: Releasing erroneous page due to insufficient space in jail");
		addFreePageToStack(rolId,(MgmtPtr)theFragment->upfInfo.pageNum);
		return false;	// storage full
	}
	addFreePageToStack(rolId,(MgmtPtr)theFragment->upfInfo.pageNum); //recycle until jail ready
	// save entry
	//m_rolArray[rolId].m_rejectedStore[rejectedIndex].mri = mostRecentId;
	//m_rolArray[rolId].m_rejectedStore[rejectedIndex].info = *theFragment;
	//m_statistics->m_rolStats[rolId].m_savedRejectedPages++;

	CREATE_ROS_EXCEPTION(ex_robinnp_addrejpage, ROSRobinNPExceptions, REJECTEDFRAGMENT, "RobinNP::addRejectedPage: The RobinNP rejected a page as the fragment was too short to extract a L1 ID");
	ers::warning(ex_robinnp_addrejpage);

	return true;
}

/****************************************************************/
bool RobinNP::checkSeqError(unsigned int oldId, unsigned int newId)
/****************************************************************/
{
	// check eventID sequence. Return 0 if OK, else 1
	// check incrementing l1id. Don't consider run number change so far
	if (newId != oldId + 1){
		// AK 28Oct2005: ECR values need not be consecutive. Just check lower bits
		unsigned int newPrimaryId = newId & ~upfEcrMask;
		if (newPrimaryId != 0) return true;
	}
	return false;
}

/*************************************************************************/
/*                        Misc                                           */
/*************************************************************************/

/*********************************/
void RobinNP::disableRol(int rolId)
/*********************************/
{
	// set XOFF bit
	m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] |= bmCtlXoff;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::disableRol: Asserting XOFF to ROL " << rolId);
	// enable discard mode to protect channel from activity on link
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.DiscardModeEnable = 1;
}

/********************************/
void RobinNP::enableRol(int rolId)
/********************************/
{
	// clear XOFF bit
	m_Bar0Channel[rolId]->BufferManagerControlRegister.Register[0] &= ~bmCtlXoff;
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::enableRol: Asserting XON to ROL " << rolId);
	// Disabling discard mode
	m_Bar0Channel[rolId]->ChannelControlRegister.Component.DiscardModeEnable = 0;
}

/************************************/
void RobinNP::calcPageCount(int rolId)
/************************************/
{
	// calculate and update number of free and used pages

	int pagesInUse = m_numPagesPerRol - 1 - (m_rolArray[rolId].m_freeTos + getFPFItems(rolId));
	m_statistics->m_rolStats[rolId].m_pagesInUse = ((pagesInUse < 0) ? 0 : (unsigned int)pagesInUse); //workaround for firmware fifo size reporting limitation

	unsigned int fpfItems = (getFPFItems(rolId) <= s_FIFOFillOffset) ? 0 : getFPFItems(rolId);
	m_statistics->m_rolStats[rolId].m_pagesFree = m_rolArray[rolId].m_freeTos + fpfItems;
	m_statistics->m_rolStats[rolId].m_bufferFull = m_statistics->m_rolStats[rolId].m_pagesFree - s_FIFOFillOffset == 0? 1: 0;

}

/****************************************/
std::string RobinNP::intToString(unsigned int num){
	/****************************************/
	std::stringstream ss;
	ss << num;
	return ss.str();
}

/********************************************************/
unsigned int RobinNP::getFpfFillStatus(unsigned int rolId)
/********************************************************/
{
	if(rolId > m_numRols){
		std::cout << "RobinNP::getFpfFillStatus: Invalid rolNumber: " << rolId << std::endl;
		return -1;
	}
	else{
		return m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.FreeFIFOFill;
	}
}

/********************************************************/
unsigned int RobinNP::getUpfFillStatus(unsigned int rolId)
/********************************************************/
{
	if(rolId > m_numRols){
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::getUpfFillStatus: Invalid rolNumber: " << rolId);
		return -1;
	}
	else{
		return m_Bar0Channel[rolId]->BufferManagerStatusRegister.Component.UsedFIFOFill;
	}
}

/*************************/
void* freePageManager(void *params)
/*************************/
{
	ThreadParams *thread = static_cast<ThreadParams*>(params);
	void* res = (thread->robin)->freePageManagerMain(thread->subRob);
	return res;
}

/*************************/
void* indexer(void *params)
/*************************/
{
	ThreadParams *thread = static_cast<ThreadParams*>(params);
	void* res = (thread->robin)->indexerMain(thread->subRob);
	return res;
}

/*************************/
void* requestManager(void *params)
/*************************/
{
	ThreadParams *thread = static_cast<ThreadParams*>(params);
	void* res = (thread->robin)->requestManagerMain(thread->subRob);
	return res;
}

/*************************/
void* dmaMonitor(void *params)
/*************************/
{
	ThreadParams *thread = static_cast<ThreadParams*>(params);
	void* res = (thread->robin)->dmaMonitorMain(thread->subRob);
	return res;
}

/*************************/
void* directDMAManager(void *params)
/*************************/
{
	ThreadParams *thread = static_cast<ThreadParams*>(params);
	void* res = (thread->robin)->directDMAManagerMain(thread->subRob);
	return res;
}

/*************************************************************************/
/*                        Requests                                       */
/*************************************************************************/

/*****************************************************/
void RobinNP::queueRequest(const RequestQueueElement &element)
/*****************************************************/
{
	unsigned int subRob = element.subRob;

	m_concurrentRequestQueue[subRob]->push(element);

}


/*****************************************************/
void RobinNP::queueActiveDMA(const DMATrackingQueueElement &element, unsigned int subRob)
/*****************************************************/
{
	while (m_DMADoneActiveQueueWriteIndex[subRob] == s_DMAQueueFIFOSize-1 ? m_DMADoneActiveQueueReadIndex[subRob] == 0 : m_DMADoneActiveQueueWriteIndex[subRob] == m_DMADoneActiveQueueReadIndex[subRob] - 1 );

	m_DMADoneActiveQueue[subRob][m_DMADoneActiveQueueWriteIndex[subRob]] = element;
	if ( m_DMADoneActiveQueueWriteIndex[subRob] == s_DMAQueueFIFOSize-1 )
		m_DMADoneActiveQueueWriteIndex[subRob] = 0;
	else
		++m_DMADoneActiveQueueWriteIndex[subRob];

}

/*****************************************************/
void RobinNP::queueActiveCaptureDMA(const DMATrackingQueueElement &element, unsigned int subRob)
/*****************************************************/
{
	while (m_DMADoneActiveCaptureQueueWriteIndex[subRob] == s_captureDMAQueueFIFOSize-1 ? m_DMADoneActiveCaptureQueueReadIndex[subRob] == 0 : m_DMADoneActiveCaptureQueueWriteIndex[subRob] == m_DMADoneActiveCaptureQueueReadIndex[subRob] - 1 );

	m_DMADoneActiveCaptureQueue[subRob][m_DMADoneActiveCaptureQueueWriteIndex[subRob]] = element;
	if ( m_DMADoneActiveCaptureQueueWriteIndex[subRob] == s_captureDMAQueueFIFOSize-1)
		m_DMADoneActiveCaptureQueueWriteIndex[subRob] = 0;
	else
		++m_DMADoneActiveCaptureQueueWriteIndex[subRob];

}

/*************************************************/
void RobinNP::processFragmentRequest(RequestQueueElement &element)
/*************************************************/
{
	bool hasPendingOrLostFragments = false;
	bool requestsDuringDiscardMode = false;
	unsigned int dmasIssued = 0;

	std::vector<unsigned int> tempFragList;
	unsigned int l1Id = 0;
	bool hasDescriptor = false;

	if(element.descriptor != 0){
		hasDescriptor = true;
		l1Id = element.descriptor->level1Id();
	}
	else{
		l1Id = element.l1Id;
		tempFragList.push_back(element.rolId);
		element.channels = tempFragList;
	}

	unsigned int subRob = -1;

	if(!element.channels.empty()){
		subRob = m_rolArray[element.channels[0]].subRobMapping;
	}
	else{
		CREATE_ROS_EXCEPTION(ex_robinnp_procfragreq2a, ROSRobinNPExceptions, BADREQUEST, "RobinNP::processFragmentRequest: The RobinNP received a corrupted fragment request for L1 ID"
				<< HEX(l1Id) << " with no ROLs in the request for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_procfragreq2a);
		return;
	}

	//Hold back the most recent DMA so that at the end of the loop a final DMA can be issued with the DMA done flag, otherwise if
	//the final page is pending no DMA done would be requested
	for(auto currentRol : element.channels){

		if(hasDescriptor){
			std::vector<unsigned int> *header = element.descriptor->headerPage(m_rolArray[currentRol].channelIndex);
			if(header->at(0) == 0){
				initHeader(header,m_configuration->getRolConfig(currentRol)->getRobId());
			}
			if(element.descriptor->status(m_rolArray[currentRol].channelIndex) == NPRequestDescriptor::RequestStatus::NEW_REQUEST){
				element.descriptor->assignPage(m_rolArray[currentRol].channelIndex,getDMAPage(m_rolArray[currentRol].subRobMapping));
			}
		}

		EventTag eventTag;
		MgmtPtr position;
		MgmtEntry *item, responseItem;
		int fragComplete;

		// go ahead
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: Data request for ID 0x" << HEX(l1Id));

		eventTag.eventId = l1Id;
#ifdef FIXED_RUN_NUMBER
		eventTag.runNumber = 0;
#else
		eventTag.runNumber = runNumber;    // need to get run number from somewhere
#endif

		unsigned int lastID = 0;

		position = findIndexedFragment(&eventTag,currentRol,&lastID,true);

		if (position == 0){
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: No fragment id 0x" << HEX(l1Id) << " found");
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: Print ROB (NO DATA) response for ROL " << currentRol);
			responseItem.info.upfInfo.eventId = l1Id;   // set L1ID
			responseItem.info.upfInfo.status = upfEndMarker;    // mark last entry
			responseItem.info.upfInfo.pageLen = 0;      // proper setting of DMA length field
			responseItem.size = 0;              // initialise to 0

			// check discard mode
			if (!m_rolArray[currentRol].linkEnabled) {
				responseItem.status = fragStatusDiscard;
				m_statistics->m_rolStats[currentRol].m_fragStat[fragsDiscarded]++;
				requestsDuringDiscardMode = true;
			} else {
				hasPendingOrLostFragments = true;

				if (isLostFragment(lastID,l1Id)){
					// fragment is lost
					responseItem.status = fragStatusLost; // mark lost
					m_statistics->m_rolStats[currentRol].m_fragStat[fragsNotAvail]++;
					CREATE_ROS_EXCEPTION(ex_robinnp_processrequest1, ROSRobinNPExceptions, FRAGLOST,"RobinNP: Lost fragment detected. The L1ID is 0x"
							<< HEX(l1Id) << ".The last ID is 0x" << HEX(lastID) << ". The Channel ID is 0x" << HEX(currentRol) << " MRE is 0x" << HEX(m_statistics->m_rolStats[currentRol].m_mostRecentId) << " for RobinNP " << m_slotNumber);
					ers::warning(ex_robinnp_processrequest1);
					if(hasDescriptor){
						element.descriptor->status(m_rolArray[currentRol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
					}
				}
				else {
					// fragment is pending
					responseItem.status = fragStatusPending; // mark pending
					m_statistics->m_rolStats[currentRol].m_fragStat[fragsPending]++;
					if(hasDescriptor){
						element.descriptor->status(m_rolArray[currentRol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_PENDING);
					}
				}
			}

			if(hasDescriptor){
				element.virtualAddress = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->virtualAddress();
				element.physicalAddress = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->physicalAddress();
			}
			prepareData(&responseItem,true,&element,currentRol,false);

		} else {
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: Found fragment id 0x" << HEX(l1Id) << " at position 0x" << HEX(position));
			item = &m_rolArray[currentRol].m_itemListPtr[position];
			// check completion flag
			if (item->isComplete == false){
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: Event not complete: Pending response for ROL " << currentRol);
				m_statistics->m_rolStats[currentRol].m_fragStat[fragsPending]++;
				responseItem.info.upfInfo.eventId = l1Id;   // set L1ID
				responseItem.info.upfInfo.runNum = eventTag.runNumber;
				responseItem.info.upfInfo.status = upfEndMarker;    // mark last entry
				responseItem.info.upfInfo.pageLen = 0;      // proper setting of DMA length field
				responseItem.size = 0;              // mark no data in both length fields!!
				responseItem.status = fragStatusPending; // | fragStatusNoData; // mark pending
				CREATE_ROS_EXCEPTION(ex_robinnp_processrequest1b, ROSRobinNPExceptions, FRAGPEND,"RobinNP::processRequest: Fragment 0x" << HEX(l1Id) << " is partially pending (some but not all pages available in index) for Channel ID 0x" << HEX(currentRol) << ", MRE is 0x" << HEX(m_statistics->m_rolStats[currentRol].m_mostRecentId) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_processrequest1b);

				if(hasDescriptor){
					element.virtualAddress = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->virtualAddress();
					element.physicalAddress = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->physicalAddress();
				}
				prepareData(&responseItem,true,&element,currentRol,false);
				if(hasDescriptor){
					element.descriptor->status(m_rolArray[currentRol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_PENDING);
				}
				hasPendingOrLostFragments = true;

			} else {
				m_statistics->m_rolStats[currentRol].m_fragStat[fragsAvail]++;
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::processRequest: Print ROB (1st page) response for ROL " << currentRol);
				// check if fragment is completed
				fragComplete = (upfEndMarker == (item->info.upfInfo.status & upfEndMask));

				DMAInfo dma;
				dma.rolId = currentRol;
				dma.useHeader = true;
				if(hasDescriptor){
					dma.physicalAddressDMA = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->physicalAddress();
				}
				else{
					dma.physicalAddressDMA = element.physicalAddress;
				}
				dma.memoryPage = item;
				m_DMAProcessContainer[subRob][dmasIssued] = dma;
				dmasIssued++;

				if(!fragComplete){
					// send subsequent pages (if any)
					int dmasThisFragment = 1;
					while (1){
						position = item->next;            // go to next item
						// error case: fragment not complete, but no next entry
						bool missingPage = false;
						if (position == 0){                // last entry ?
							m_statistics->m_rolStats[currentRol].m_fragsIncomplete++;
							missingPage = true;
							// terminate transfer
						}

						// error case: fragment not complete, but next entry different L1ID
						if ((l1Id != m_rolArray[currentRol].m_itemListPtr[position].info.upfInfo.eventId) ||     // different id ?
								(eventTag.runNumber != m_rolArray[currentRol].m_itemListPtr[position].info.upfInfo.runNum)){    // or run number
							missingPage = true;
							m_statistics->m_rolStats[currentRol].m_fragsIncomplete++;
							// terminate transfer
						}

						if(missingPage){
							CREATE_ROS_EXCEPTION(ex_robinnp_procfragreq1, ROSRobinNPExceptions, MISSINGPAGE, "RobinNP::processFragmentRequest:  L1ID = 0x" << HEX(l1Id) << ": Pages Missing from Multipage Fragment for RobinNP " << m_slotNumber);
							ers::warning(ex_robinnp_procfragreq1);
							break;
						}

						DMAInfo dma;
						dma.rolId = currentRol;
						dma.useHeader = false;
						if(hasDescriptor){
							dma.physicalAddressDMA = (unsigned long)element.descriptor->dataPage(m_rolArray[currentRol].channelIndex)->physicalAddress() + item->info.upfInfo.pageLen*4*dmasThisFragment; //add length of previous item to indicate start location for next;
						}
						else{
							dma.physicalAddressDMA = element.physicalAddress + item->info.upfInfo.pageLen*4*dmasThisFragment; //add length of previous item to indicate start location for next;
						}
						item = &m_rolArray[currentRol].m_itemListPtr[position];
						dma.memoryPage = item;
						m_DMAProcessContainer[subRob][dmasIssued] = dma;
						dmasIssued++;
						dmasThisFragment++;
						// update completion status
						fragComplete = (upfEndMarker == (item->info.upfInfo.status & upfEndMask));
						if (fragComplete){
							break;      // finished
						}
					}

				}
			}

		}

	}

	for(unsigned int dmaCounter = 0; dmaCounter < dmasIssued; ++dmaCounter){
		unsigned int rol = m_DMAProcessContainer[subRob][dmaCounter].rolId;
		if(hasDescriptor){
			element.virtualAddress = (unsigned long)element.descriptor->dataPage(m_rolArray[rol].channelIndex)->virtualAddress();
		}
		element.physicalAddress = m_DMAProcessContainer[subRob][dmaCounter].physicalAddressDMA;
		if(dmaCounter == dmasIssued - 1){
			prepareData(m_DMAProcessContainer[subRob][dmaCounter].memoryPage,m_DMAProcessContainer[subRob][dmaCounter].useHeader,&element,rol,true,dmasIssued);
		}
		else{
			if(hasDescriptor){
				if(element.descriptor->status(m_rolArray[rol].channelIndex) != NPRequestDescriptor::RequestStatus::REQUEST_CORRUPT){
					element.descriptor->status(m_rolArray[rol].channelIndex,NPRequestDescriptor::RequestStatus::REQUEST_COMPLETE);
				}
			}
			prepareData(m_DMAProcessContainer[subRob][dmaCounter].memoryPage,m_DMAProcessContainer[subRob][dmaCounter].useHeader,&element,rol,false);
		}

	}

	if(dmasIssued == 0){
		if(hasPendingOrLostFragments || requestsDuringDiscardMode){
			if(hasDescriptor){
				m_outputQueue->push(element.descriptor);
			}
			else{
				pthread_mutex_lock(&m_rolArray[element.rolId].m_ticketCondMutex[element.ticket]);
				m_rolArray[element.rolId].m_ticketIndex[element.ticket] = true;
				pthread_cond_signal(&m_rolArray[element.rolId].m_ticketCond[element.ticket]);
				pthread_mutex_unlock(&m_rolArray[element.rolId].m_ticketCondMutex[element.ticket]);
			}
		}
		else
		{
			CREATE_ROS_EXCEPTION(ex_robinnp_procfragreq2, ROSRobinNPExceptions, BADREQUEST, "RobinNP::processFragmentRequest: The RobinNP received a corrupted fragment request for L1 ID"
					<< HEX(l1Id) << " resulting in no DMA's or pending/lost fragments -  number of ROLs in request " << element.channels.size() << " for RobinNP " << m_slotNumber);
			ers::warning(ex_robinnp_procfragreq2);
		}
	}
}

bool RobinNP::clearRequest(const std::vector<unsigned int> *l1IDs, int rolId){

	int missingIds = 0;
	bool allClearsCompleted = true;
	EventTag eventTag;
	unsigned int firstFailedId = 0;
	unsigned int startRol;
	unsigned int endRol;
	if(rolId > -1){
		startRol = rolId;
		endRol = rolId + 1;
	}
	else{
		startRol = 0;
		endRol = m_numRols;
	}

	if (l1IDs->empty()){
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::clearRequest: Invalid clear request without data");
	}else{
		for(unsigned int currentRol = startRol; currentRol < endRol; ++currentRol){

			if(s_runCtlStart == m_rolArray[currentRol].m_runCtlState){

				unsigned int savedID = m_statistics->m_rolStats[currentRol].m_mostRecentId;

				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::clearRequest: Clear request for " << l1IDs->size() << " items");
				// reset missing id counter
				missingIds = 0;
				if (!m_rolArray[currentRol].linkEnabled) {
					DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::clearRequest: Delete request during discard mode");
				} else {
					for(auto l1Id : *l1IDs){

						// go ahead
						DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::clearRequest: Deleting ID 0x" << HEX(l1Id));
						// find and eventually delete the fragment (all relevant pages)
						// create event tag
						eventTag.eventId = l1Id;
#ifdef FIXED_RUN_NUMBER
						eventTag.runNumber = 0;
#else
						eventTag.runNumber = runRumber;    // need to get run number from somewhere
#endif
						if (!deleteIndexedFragment(&eventTag,currentRol)){
							// add id to list of failed events
							if(missingIds == 0){
								firstFailedId = l1Id;
							}
							missingIds++;
						}
					}
					// update number of missing ids
					m_statistics->m_rolStats[currentRol].m_fragStat[fragsMissOnClear] += missingIds;
				}
				signalPagesOnStack(m_rolArray[currentRol].subRobMapping);

				if(missingIds != 0){
					allClearsCompleted = false;
					CREATE_ROS_EXCEPTION(ex_robinnp_clear, ROSRobinNPExceptions, BADDELETE, "RobinNP::clearRequest: The RobinNP could not delete "
							<< missingIds << " events because they were not in its buffer. The first failed delete was for L1ID = 0x"
							<< HEX(firstFailedId) << " for Rol " << currentRol << " Most Recent ID = 0x" << HEX(m_statistics->m_rolStats[currentRol].m_mostRecentId) << " Pre Delete MrID " << HEX(savedID)  << " for RobinNP " << m_slotNumber);
					ers::warning(ex_robinnp_clear);
				}
			}
		}
	}

	return allClearsCompleted;
}

/**************************************************************************************************/
void RobinNP::prepareData(MgmtEntry *item, bool useHeader, RequestQueueElement *responseAddress, int rolId, bool dmaDone, unsigned int numDMAs)
/**************************************************************************************************/
{
	// This function is called once for every page of a fragment<br>
	//Format rob header with information from first page of fragment. If a ROB message has
	//to be distributed across multiple message only the first message contains the ROB header.
	//Subsequent message have the data part only.
	//fragSize is the total size of the fragment (in words)
	//<br>
	//If the SIZE field of *item is 0, format NO DATA response. eventid must be set

	unsigned int subRob = m_rolArray[rolId].subRobMapping;

	DmaDesc dma; // get DMA descriptor

	RobMsgHdr *robHdr = (RobMsgHdr*)((unsigned char*)m_hdrContainer[subRob]); // ROB header
	RodFragment *rodFragment = (RodFragment*)((unsigned char*)m_hdrContainer[subRob] + sizeof(RobMsgHdr)); // ROD fragment

	dma.dataAddr = (item->info.upfInfo.pageNum + (rolId - subRob*m_numChannelsPerSubRob) * m_numPagesPerRol) * m_configuration->getPageSize();

	dma.dataSize = item->info.upfInfo.pageLen;
	dma.hdrInfo = 0;

	unsigned int *hdrOut = (unsigned int*)robHdr;    // default to rob header

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Check if this is the last fragment

	int fragComplete = (upfEndMarker == (item->info.upfInfo.status & upfEndMask));

	if (fragComplete){
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Processing last page of the fragment");
	} else {
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Expecting more pages of the fragment");
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////
	// USE (ROB) HEADER   //////////
	////////////////////////////////
	int noFragment = 0;

	if (useHeader)
	{
		unsigned int fragStat, fragSize, dummyFragSize = 0;

		fragSize = item->size;      // total size of fragment
		fragStat = item->status;  // get accumulated fragment error bits
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Effective fragment status: 0x" << HEX(fragStat));

		// return dummy fragment if discarded, lost or pending
		noFragment = (fragStat & fragStatusLost) || (fragStat & fragStatusPending) \
				|| (fragStat & fragStatusDiscard);
		if (noFragment != 0){
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: handling lost/pending fragment");
			// create dummy fragment
			rodFragment->header.hdrMarker  = s_hdrMarkerRod;//0xee1234ee;
			rodFragment->header.hdrSize  = 9;
			rodFragment->header.eventFormat  = s_rodFormatVersion;
			rodFragment->header.srcId  = m_configuration->getRolConfig(rolId)->getRobId();
			rodFragment->header.runId  = 0;
			rodFragment->header.eventId  = item->info.upfInfo.eventId;
			rodFragment->header.bxId  = 0;
			rodFragment->header.trigType  = 0;
			rodFragment->header.eventType  = 0;
			rodFragment->trailer.status = 0;    // status word = 0
			rodFragment->trailer.numStatusWords = 0;  // number of status elements: 0
			rodFragment->trailer.numDataWords = 0;    // number of data elements: 0

			dummyFragSize = sizeof(RodFragment)/sizeof(unsigned int) - 1;    // adjust fragment size because dummy fragment has no status word

			rodFragment->trailer.statusPosition = 0; // position of status: 0 = preceding data

		} else {
			dummyFragSize = 0;
		}

		robHdr->mrEvntId = m_statistics->m_rolStats[rolId].m_mostRecentId;   // copy most recent id

		if(m_runMode == runModeLegacy){
			robHdr->marker = s_hdrMarkerRob;
			robHdr->hdrSize = sizeof(RobMsgHdr)/sizeof(unsigned int) + 1; //Extra word for DMA alignment
			robHdr->version = s_robFormatVersion;
			robHdr->statCnt = s_robHdrStatusWords + 1;
			robHdr->crc = 0x12121212; // dummy extra status word; //actually contains dummy word (ROB header hack for legacy)
			robHdr->srcId = m_configuration->getRolConfig(rolId)->getRobId();
			robHdr->totalSize = sizeof(RobMsgHdr)/sizeof(unsigned int) + fragSize + dummyFragSize + 1;//Extra word for DMA alignment
			if(m_designVersion >= 0x01050007 && noFragment == 0){
				m_hdrContainer[subRob][9] = s_robHdrCrcType; //actually contains crc (ROB header hack for legacy)
			}
			else{
				m_hdrContainer[subRob][9] = 0; //actually contains crc (ROB header hack for legacy)
			}
		}
		else{
			// current values
			robHdr->totalSize = sizeof(RobMsgHdr)/sizeof(unsigned int) + fragSize + dummyFragSize;
		}

		// check minimum size of fragment: ROD header + trailer (without status word)
		if ((noFragment == 0) && ((sizeof(RodFragment)/sizeof(unsigned int)-1)  > fragSize)) {
			fragStat |= fragStatusShort;
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Status changed to: 0x" << HEX(fragStat));
		}

		// rob status is divided into 2 fields: specific and generic
		unsigned int specificStatus, genericStatus;
		if (fragStat == 0) {
			specificStatus = 0;
			genericStatus = 0;
		} else {
			// copy upper 8 bits of specific status
			specificStatus = fragStat & 0xff000000;
			// map lower 8 bits of specific status

			for (auto statusPair : m_fragStatusMap) {
				if ((fragStat & statusPair.first) == statusPair.first ){
					specificStatus |= statusPair.second;
				}
			}
			// indicate generic error
			if (noFragment != 0){
				genericStatus = fragGenStatusTime;        // timeout if no data
			} else {
				if (fragStat & fragStatusTrunc){
					genericStatus = fragGenStatusBuffer;    // buffer error on truncation
					robHdr->crc = s_robHdrCrcTypeNone;		// overwrite normal CRC type
					DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::prepareData: Truncated fragment: adjusting generic status, crc and size:");
				} else {
					genericStatus = fragGenStatusData;    // else data error
				}
			}
#ifdef FRAG_CHECKING
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::prepareData: Fragment ROL " << rolId << ", event 0x" << HEX(item->info.upfInfo.eventId) << " has errors: 0x" << HEX(fragStat));
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::prepareData: Specific status:: 0x" << HEX(specificStatus));
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::prepareData: Generic status:: 0x" << HEX(genericStatus));
#endif
		}

		robHdr->fragStat = specificStatus | genericStatus; // fragStat;

		// update DMA header length
		dma.hdrInfo += sizeof(RobMsgHdr)/sizeof(unsigned int) + dummyFragSize;

	}
	///////////////////////
	// END OF USE HEADER
	///////////////////////

	// write to response FIFO
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Issuing PCI response");

	// track destination address

	m_rolArray[rolId].m_pciCurrentAddr = responseAddress->physicalAddress;

	// collect status error statistics

	for (u_int bit = 0; bit < 32; bit++){
		if (robHdr->fragStat & (1 << bit)){
			m_statistics->m_rolStats[rolId].m_statusErrors[bit]++;
		}
	}

	sendData(&dma, hdrOut, responseAddress, item->info.upfInfo.eventId, rolId, dmaDone, numDMAs, noFragment);

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: =============================================");
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Completed");
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: =============================================");
}

/**************************************************************************************************************************************************************************/
void RobinNP::sendData(DmaDesc *dmaDescriptor, unsigned int* dataHeader, RequestQueueElement *responseAddress, unsigned int eventID, unsigned int rolId, bool dmaDone, unsigned int numDMAs, bool lostOrPending)
/**************************************************************************************************************************************************************************/
{
	unsigned int subRob =  m_rolArray[rolId].subRobMapping;

	//take DMA descriptor, media header and pointer to header area. Write descriptor, media header and user header data into PCI header FIFO.
	int hdrsize = dmaDescriptor->hdrInfo & s_mediaSizeMask;
	// check empty transfer first
	// this is needed because pages may be empty when S-Link control words are suppressed
	if ((hdrsize == 0) && (dmaDescriptor->dataSize == 0)){
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::sendData: Empty Response suppressed");
		return;
	}

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::sendData: (SubRob# " << subRob << ") (1): DMA descriptor initial address = 0x" << HEX(dmaDescriptor->dataAddr));
	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::sendData: header size : 0x" << HEX(dmaDescriptor->hdrInfo) << ", data size: 0x" << HEX(dmaDescriptor->dataSize));

	unsigned int *pFragmentLocation = (unsigned int *)responseAddress->virtualAddress; //Virtual Address of Response Area

	if ( hdrsize > 0 || dmaDescriptor->dataSize > 0)
	{
		if(dataHeader != 0){
			if(m_runMode == runModeGen2ROS){
				std::vector<unsigned int>* headerPage = responseAddress->descriptor->headerPage(m_rolArray[rolId].channelIndex);
				if( hdrsize > 0){
					headerPage->at(1) = dataHeader[1]; //total fragment size
					headerPage->at(6) = dataHeader[6]; //fragment status
					headerPage->at(7) = dataHeader[7]; // Most Recent ID
					if(lostOrPending || (m_designVersion < 0x01050008)){
						headerPage->at(8) = s_robHdrCrcTypeNone;
					}
					else{
						headerPage->at(8) = s_robHdrCrcType;
					}
#ifdef MEMDEBUG
					headerPage->at(8) = dmaDescriptor->dataAddr;
#endif
				}
			}
			else{
				//Write General Header
				for (int i=0; i < hdrsize + 1 ;i++) { //extra dummy status word for legacy module requires extra space for crc
					pFragmentLocation[i] = dataHeader[i];
				}
			}
		}
		if(lostOrPending){
			unsigned int offset = 0;
			if(m_runMode == runModeLegacy){
				offset = 10;
			}
			pFragmentLocation[offset + 0]  = s_hdrMarkerRod;//0xee1234ee;
			pFragmentLocation[offset + 1]  = 9;
			pFragmentLocation[offset + 2]  = s_rodFormatVersion;
			pFragmentLocation[offset + 3]  = m_configuration->getRolConfig(rolId)->getRobId();
			pFragmentLocation[offset + 4]  = 0;
			pFragmentLocation[offset + 5]  = eventID;
			for(int counter = 6; counter < 13; ++ counter){
				pFragmentLocation[offset + counter] = 0;
			}
		}

		//Initiate DMA if required, writing specific event info to header with other required control words
		if(dmaDescriptor->dataSize > 0){

			__sync_fetch_and_add(&m_numDMAsPending[subRob], 1);

			m_rolArray[rolId].dataVolumeTransferred += (dmaDescriptor->dataSize)*4;

			/*std::stringstream ss;
			//ss << "Dma for Rol ID " << rolId << " for event " << eventID << ", num currently pending " << m_numDMAsPending[subRob] << " for subRob " << subRob << std::endl;
			ss << " Dma for rol ID " << rolId << std::hex << " for address " << dmaDescriptor->dataAddr << " to " << responseAddress->physicalAddress << " with size "  << std::dec << dmaDescriptor->dataSize << " num pending " << m_numDMAsPending[subRob] << std::dec << std::endl;
			screenPrint(ss.str());*/

			if(m_configuration->getMemEmulationState()){
				if( hdrsize > 0){
					//int offsetvalue = (dmaDescriptor->hdrInfo & s_mediaSizeMask) + 5;
					int offsetvalue = 5;
					pFragmentLocation[0]  = s_hdrMarkerRod;//0xee1234ee;
					pFragmentLocation[offsetvalue++] = eventID;
					pFragmentLocation[offsetvalue++] = 0x11111111;
					pFragmentLocation[offsetvalue] = 0xb0b0b0b0;
				}
			}

			//Write DMA landing address to FIFO as this step is the same irrespective of the rest of the configuration
#ifdef MEMDEBUG
			uint64_t temp = ((uint64_t)(eventID & 0xff)) << 56;
			m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = responseAddress->physicalAddress | temp;
#else
			m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = responseAddress->physicalAddress;
#endif
			if((m_runMode == runModeGen2ROS) && dmaDone){
				unsigned int descriptorInfo = (m_DMADoneActiveQueueWriteIndex[subRob] & 0xFFF);

				if(m_configuration->getMemEmulationState()){
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | s_dmaOffsetDmaDone | (descriptorInfo << 16) | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;
				}
				else{
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | s_dmaNoOffsetDmaDone | (descriptorInfo << 16) | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;
				}

				DMATrackingQueueElement element;
				element.descriptor = responseAddress->descriptor;
				element.numDMAs = numDMAs;
				element.rolID = rolId;
				element.index = m_DMADoneActiveQueueWriteIndex[subRob];
				queueActiveDMA(element,subRob);

				/*std::stringstream ss;
				ss << "Transfer size " << dmaDescriptor->dataSize;
				//ss << "Request DMA: Descriptor ID " << responseAddress->descriptor->getId() << " Num DMAs requested " << numDMAs << ", num currently pending " << m_numDMAsPending[subRob] << " for subRob " << subRob << " info " << descriptorInfo << std::endl;
				screenPrint(ss.str());*/
			}
			else if(m_runMode == runModeLegacy)
			{
				if(dmaDone){
					unsigned int descriptorInfo = (responseAddress->ticket & 0xFFF);
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | s_dmaOffsetDmaDone | (descriptorInfo << 16) | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;
					DMATrackingQueueElement element;
					element.ticket = responseAddress->ticket;
					element.rolID = rolId;
					element.virt = responseAddress->virtualAddress;
					element.eventID = eventID;
					element.memAddress = dmaDescriptor->dataAddr;
					element.numDMAs = numDMAs;
					queueActiveDMA(element,subRob);
				}
				else{
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | s_dmaOffsetNoDmaDone | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;
				}
				/*std::stringstream ss;
				ss << "Request DMA: Descriptor ID " << responseAddress->ticket << " Num DMAs requested " << numDMAs << ", num currently pending " << m_numDMAsPending[subRob] << " for subRob " << subRob << " info " << descriptorInfo << std::endl;
				screenPrint(ss.str());*/

			}
			else{

				if(m_configuration->getMemEmulationState()){
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | s_dmaOffsetNoDmaDone | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;

				}
				else{
					m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dmaDescriptor->dataSize)) | ((uint64_t)((dmaDescriptor->dataAddr) * 4)) << 32;
				}
			}
		}
	}

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::sendData Complete");
}

/********************************/
void RobinNP::allocateSharedMem(){
	/********************************/

	umask(0);

        int shm_open_flags = O_RDWR;
        mode_t shm_mode_flags = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

        int fd = ::shm_open (m_sharedMemoryName.c_str(), shm_open_flags,
                             shm_mode_flags);

        /* Since ALMA9 protected_regular is set to 1
           protected_regular
           This protection is similar to protected_fifos, but it avoids writes
           to an attacker-controlled regular file, where a program expected to
           create one.

           When set to "0", writing to regular files is unrestricted.

           When set to "1" don't allow O_CREAT open on regular files that we
           don't own in world writable sticky directories, unless they are
           owned by the owner of the directory.

           When set to "2" it also applies to group writable sticky directories.

           This means for operation across different users, one cannot set
           upfront O_RDRW and O_CREAT. Instead we create the files only if
           it does not exist
        */

        if (fd == -1 && errno == ENOENT) {

          shm_open_flags |= O_CREAT;

          fd = ::shm_open(m_sharedMemoryName.c_str(),
                          shm_open_flags, shm_mode_flags);
        }

        if (-1 == fd){
                CREATE_ROS_EXCEPTION(ex_robinnp_allocsharedmem1, ROSRobinNPExceptions, SHARED, "Error opening shared memory file descriptor for RobinNP " << m_slotNumber << ". Error " << ::strerror(errno));
		throw (ex_robinnp_allocsharedmem1);
	}


        off_t res = lseek (fd, m_memSize, SEEK_SET);
	if(res == -1){
		CREATE_ROS_EXCEPTION(ex_robinnp_allocsharedmem2, ROSRobinNPExceptions, SHARED, "Error " << strerror(errno) << " in lseek of shared memory area for RobinNP " << m_slotNumber);
		throw(ex_robinnp_allocsharedmem2);
	}
	// coverity[SIZEOF_MISMATCH]
	write (fd, &fd, 1); // coverity complained due to write of only 1 byte, but this is safe for our purposes here
	lseek (fd, 0, SEEK_SET);
	//Create the memory mapping.
	m_sharedMemory = static_cast<unsigned char*>(mmap (0, m_memSize, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0));
	close (fd);
	if (m_sharedMemory == reinterpret_cast<unsigned char*>(-1))
	{
		CREATE_ROS_EXCEPTION(ex_robinnp_allocsharedmem3, ROSRobinNPExceptions, SHARED, "Error in MMAP of shared memory area for RobinNP " << m_slotNumber);
		throw (ex_robinnp_allocsharedmem3);
	} else {
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::allocateSharedMem: Shared Memory allocated at " << HEX((void *)m_sharedMemory)<< ", size 0x" << HEX(m_memSize));
	}

}

/**********************************/
void RobinNP::deallocateSharedMem(){
	/**********************************/

	int ret = munmap((void *)m_sharedMemory, m_memSize);
	if (ret){
		CREATE_ROS_EXCEPTION(ex_robinnp_deallocsharedmem1, ROSRobinNPExceptions, UNSHARED, "Error in MUNMAP of shared memory area for RobinNP " << m_slotNumber);
		throw (ex_robinnp_deallocsharedmem1);
	}
}

/**************************************/
RolNP* RobinNP::newRol(u_int number) {
	/**************************************/
	return new RolNP(*this, number);
}

void RobinNP::screenPrint(std::string toPrint){

	pthread_mutex_lock(&m_screenPrintMutex);
	std::cout << toPrint << std::endl;
	pthread_mutex_unlock(&m_screenPrintMutex);


}

unsigned int RobinNP::getParentSubRob(unsigned int rolId){
	return m_rolArray[rolId].subRobMapping;
}

unsigned int RobinNP::getNumChannelsPerSubRob(){
	return m_numChannelsPerSubRob;
}

std::pair<unsigned long, unsigned char*> RobinNP::getMemPage(){
	if(m_pageVector.empty()){
		CREATE_ROS_EXCEPTION(ex_robinnp_regchan1, ROSRobinNPExceptions, NOPAGES, "Insufficient DMA landing pages, "  << m_cardData.nDmaPages << " total for " << m_numRols << " channels for RobinNP " << m_slotNumber);
		throw (ex_robinnp_regchan1);
	}
	std::pair<unsigned long, unsigned char*> page = m_pageVector.back();
	m_pageVector.pop_back();
	if(page.first == 0){
		CREATE_ROS_EXCEPTION(ex_robinnp_regchan2, ROSRobinNPExceptions, NOPAGES, "DMA Physical Address Reported as Zero for RobinNP " << m_slotNumber);
		throw (ex_robinnp_regchan2);
	}
	return page;
}

void RobinNP::recycleRobinNPPage(unsigned int rolId, unsigned int pagenum){
	//std::cout << "Recycling page " << pagenum << " for ROL " << rolId << std::endl;
	addFreePageToStack(rolId,(MgmtPtr)pagenum);
	signalPagesOnStack(m_rolArray[rolId].subRobMapping);
	m_rolArray[rolId].pagesInIndex--;
}

void RobinNP::recycleMemPage(unsigned long physicalAddress, unsigned char* virtualAddress){
	m_pageVector.push_back(std::make_pair(physicalAddress,virtualAddress));
}

void RobinNP::setCollectorQueue(tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue) {
	std::cout << "Setting Output Queue, address " << std::hex << (void*)descriptorQueue << std::dec << std::endl;
	m_outputQueue = descriptorQueue;
}

unsigned int RobinNP::getHeaderSize(){

	return sizeof(RobMsgHdr)/sizeof(unsigned int);
}

void RobinNP::initHeader(std::vector<unsigned int>* headerPage, unsigned int atlasID){
	//std::cout << "Initialise header with ATLAS ID " << atlasID << std::endl;
	headerPage->at(0) = s_hdrMarkerRob;
	headerPage->at(1) = 0; //total fragment size
	headerPage->at(2) = getHeaderSize();
	headerPage->at(3) = s_robFormatVersion;
	headerPage->at(4) = atlasID;
	headerPage->at(5) = s_robHdrStatusWords;
	headerPage->at(6) = 0; //fragment status
	headerPage->at(7) = 0; // Most Recent ID
	headerPage->at(8) = 0; // CRC type
}

void RobinNP::registerChannelIndex(unsigned int rolId,unsigned int channelIndex){
	//std::cout << "Registering channel index " << channelIndex << " for ROL " << rolId << std::endl;
	m_rolArray[rolId].channelIndex = channelIndex;
}

unsigned int RobinNP::getAtlasID(unsigned int rolId){
	return m_configuration->getRolConfig(rolId)->getRobId();
}


void RobinNP::testProbe(){
	//Container for test code called with user command 'publish'
}

void RobinNP::initialiseLegacyControl(unsigned int numTicketsPerRol){
	m_numTicketsPerRol = numTicketsPerRol;

	for(unsigned int rolId = 0; rolId < m_numRols; ++rolId){
		m_rolArray[rolId].m_ticketCond = new pthread_cond_t[numTicketsPerRol];
		m_rolArray[rolId].m_ticketCondMutex = new pthread_mutex_t[numTicketsPerRol];
		m_rolArray[rolId].m_ticketIndex = new unsigned int[numTicketsPerRol];

		for(unsigned int ticket = 0; ticket < m_numTicketsPerRol; ++ticket){
			pthread_mutex_init(&m_rolArray[rolId].m_ticketCondMutex[ticket],NULL);
			pthread_cond_init(&m_rolArray[rolId].m_ticketCond[ticket], NULL);
		}
	}
	m_legacyMode = true;
}

void RobinNP::closeLegacyControl(){
	for(unsigned int rolId = 0; rolId < m_numRols; ++rolId){
		for(unsigned int ticket = 0; ticket < m_numTicketsPerRol; ++ticket){
			pthread_mutex_destroy(&m_rolArray[rolId].m_ticketCondMutex[ticket]);
			pthread_cond_destroy(&m_rolArray[rolId].m_ticketCond[ticket]);
		}
		delete[] m_rolArray[rolId].m_ticketCond;
		delete[] m_rolArray[rolId].m_ticketCondMutex;
		delete[] m_rolArray[rolId].m_ticketIndex;
	}

}

void RobinNP::initialisePageQueues(std::vector<tbb::concurrent_bounded_queue<DataPage*>*>& queues){

	m_queuesReady = true;
	std::cout << "Address of queue vector " << (void*)(&queues) << " " <<  queues.size() << std::endl;
	m_pageQueues = &queues;

	std::vector<tbb::concurrent_bounded_queue<DataPage*>*> tempQueue;

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++ subRob){
		tempQueue.push_back(new tbb::concurrent_bounded_queue<DataPage*>);
	}

	if(m_dataPageVector.empty()){
		CREATE_ROS_EXCEPTION(ex_robinnp_regchan1, ROSRobinNPExceptions, NOPAGES, "Insufficient DMA landing pages, "  << m_cardData.nDmaPages << " total for " << m_numRols << " channels for RobinNP " << m_slotNumber);
		throw (ex_robinnp_regchan1);
	}

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		for(unsigned int counter = 0; counter < s_captureDMAQueueFIFOSize; ++counter){
			m_captureQueue[subRob]->push_back(m_dataPageVector.back());
			m_dataPageVector.pop_back();
		}
	}

	while(!m_dataPageVector.empty()){
		for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){

			tempQueue[subRob]->push(m_dataPageVector.back());
			m_dataPageVector.pop_back();

		}
	}

	m_pageQueues->insert(m_pageQueues->end(),tempQueue.begin(),tempQueue.end());

	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++ subRob){
		std::cout << "Allocated " << (*m_pageQueues)[m_subRobIndex[subRob]]->size() << " for Page Queue for subRob " << subRob << " with global index " << m_subRobIndex[subRob] << std::endl;
	}
}


DataPage* RobinNP::getDMAPage(unsigned int subRob){
	DataPage *temp;
	(*m_pageQueues)[m_subRobIndex[subRob]]->pop(temp);
	return temp;
}

unsigned int RobinNP::getSubRob(unsigned int rolId){
	return m_rolArray[rolId].subRobMapping;
}

void RobinNP::registerSubRobIndex(unsigned int subRob,unsigned int subRobIndex){
	m_subRobIndex[subRob] = subRobIndex;
}

unsigned int RobinNP::getFirmwareVersion(){
	return m_designVersion;
}

bool RobinNP::getLegacyStatus(){
	return m_legacyMode;
}

void RobinNP::resetSubRobChannels(){
	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		//std::cout << "EMPTY " << m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::endl;
		while(m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty != 1){
			m_Bar0Common->MemoryReadInvalidateControlRegister.Bottom = 1 << subRob;
		}
		//std::cout << "EMPTY " << m_Bar0SubRob[subRob]->SubRobStatus.Component.MemoryOutputSecondFIFOEmpty << std::endl;

		m_Bar0SubRob[subRob]->SubRobControl.Component.ChannelReset = 1;
		m_Bar0SubRob[subRob]->SubRobControl.Component.ChannelReset = 0;
	}
}

bool RobinNP::checkCRC(unsigned int *fragmentLocation,RobMsgHdr* robHeader, unsigned int rolId){
	unsigned int *ptr = fragmentLocation;
	unsigned int tfs = robHeader->totalSize;
	//std::cout << "size " << tfs - robHeader->hdrSize - 1 << std::endl;

	unsigned short crc1 = ptr[tfs - robHeader->hdrSize - 1] & 0xffff;
	unsigned short crc2 = (ptr[tfs -robHeader->hdrSize - 1] >> 16) & 0xffff;
	//std::cout << std::hex << crc1 << " " << crc2 << std::endl;
	RodHeader *rodHeader = (RodHeader *)(fragmentLocation);

	unsigned int l1Id = rodHeader->eventId;

	unsigned long asize = tfs - robHeader->hdrSize - 1;

	//bool result = compareCRC(ptr,crc1,crc2,asize,l1Id,rolId);
	return compareCRC(ptr,crc1,crc2,asize,l1Id,rolId);


	/*	while(result){
		std::stringstream ss;
		screenPrint(ss.str());
		unsigned short crc1 = ptr[tfs - robHeader->hdrSize - 1] & 0xffff;
		unsigned short crc2 = (ptr[tfs -robHeader->hdrSize - 1] >> 16) & 0xffff;
		result = compareCRC(ptr,crc1,crc2,asize,l1Id,rolId);
	}*/
	/*	if(result){
				sleep(1);
				std::cout << std::hex << std::endl;
				for(unsigned int location = 0; location < (1040); ++location){
					std::cout << std::setfill('0') << std::setw(8) << *(ptr + location) << " ";
					if((location + 1) % 16 == 0){
						std::cout << std::endl;
					}
				}
				std::cout << std::dec << std::endl;

				unsigned short crc1a = ptr[tfs - robHeader->hdrSize - 1] & 0xffff;
				unsigned short crc2a = (ptr[tfs -robHeader->hdrSize - 1] >> 16) & 0xffff;
					std::cout << std::hex << crc1a << " " << crc2a << std::endl;
				getchar();

			}*/
	//return result;

}

bool RobinNP::checkCRC(unsigned int *fragmentLocation,unsigned int fragmentSize, unsigned int rolId){

	RodHeader *rodHeader = (RodHeader *)fragmentLocation;
	unsigned int *ptr = fragmentLocation;

	unsigned short crc1 = ptr[fragmentSize - 1] & 0xffff;
	unsigned short crc2 = (ptr[fragmentSize - 1] >> 16) & 0xffff;
	unsigned int l1Id = rodHeader->eventId;

	unsigned long asize = fragmentSize - 1;  //The "-1" is for the rob_trailer (aka CRC)

	return compareCRC(ptr,crc1,crc2,asize,l1Id,rolId);
}


bool RobinNP::checkCRC(unsigned int *fragmentLocation,std::vector<unsigned int>* robHeader, unsigned int rolId){

	RodHeader *rodHeader = (RodHeader *)fragmentLocation;
	unsigned int *ptr = fragmentLocation;
	unsigned int tfs = robHeader->at(1);
	unsigned short crc1 = ptr[tfs - robHeader->at(2) - 1] & 0xffff;
	unsigned short crc2 = (ptr[tfs -robHeader->at(2) - 1] >> 16) & 0xffff;
	unsigned int l1Id = rodHeader->eventId;

	unsigned long asize = tfs - robHeader->at(2) - 1;  //The "-1" is for the rob_trailer (aka CRC)

	return compareCRC(ptr,crc1,crc2,asize,l1Id,rolId);
}

bool RobinNP::compareCRC(unsigned int* ptr,unsigned short crc1, unsigned short crc2,unsigned long asize, unsigned int l1Id,unsigned int rolId){
	bool errorcrc = false;
	unsigned short crc_result = crctablefastwithskip16((unsigned short*)(ptr), asize);

	if (crc_result != crc1)
	{
		unsigned int rslot = getIdentity();
		CREATE_ROS_EXCEPTION(ex_robinnp_checkcrc1, ROSRobinNPExceptions, BADCRC, "RobinNP::checkCRC: ERROR in LSW-CRC. RobinNP = " << rslot << " ROL " << rolId << " L1ID = 0x" << HEX(l1Id) << " Computed CRC = 0x" << HEX(crc_result) << " Expected CRC = 0x" << HEX(crc1) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_checkcrc1);
		errorcrc = true;
	}
	/*	if(!errorcrc){
		std::cout << "CRC OK " << std::hex << crc_result << " " << crc1 << std::endl;
	}
	 */
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getFragment: Event-MSW. Expected CRC = "<< HEX(crc2));
	crc_result = crctablefastwithskip16(((unsigned short*)(ptr))+1, asize);
	if (crc_result != crc2)
	{
		unsigned int rslot = getIdentity();
		CREATE_ROS_EXCEPTION(ex_robinnp_checkcrc2, ROSRobinNPExceptions, BADCRC, "RobinNP::checkCRC: ERROR in MSW-CRC. RobinNP = " << rslot << " ROL " << rolId << " L1ID = 0x" << HEX(l1Id) << " Computed CRC = 0x" << HEX(crc_result) << " Expected CRC = 0x" << HEX(crc2) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_checkcrc2);
		errorcrc = true;
	}
	/*	if(!errorcrc){
		std::cout << "CRC OK " << std::hex << crc_result << " " << crc2 << std::endl;
	}*/
	/*errorcrc = true;
		if(errorcrc){
			sleep(1);
			std::cout << std::hex << std::endl;
			for(unsigned int location = 0; location < (240); ++location){
				std::cout << std::setfill('0') << std::setw(8) << *(ptr + location) << " ";
				if((location + 1) % 16 == 0){
					std::cout << std::endl;
				}
			}
			std::cout << std::dec << std::endl;
		}
		errorcrc = false;
	 */
	return errorcrc;
}

/*************************************************/
unsigned short RobinNP::crctablefastwithskip16(unsigned short* p, unsigned long len)
/*************************************************/
{
	unsigned long crc = m_crcinit_direct;
	const unsigned long crcxor = 0x0000;

	while (len--) {
		crc = (crc << 16) ^ m_crctab16[((crc >> (c_order - 16)) & 0xffff) ^ *p]; p+=2;
	}

	crc ^= crcxor;
	crc &= m_crcmask;
	return((unsigned short)crc);
}


/*****************************************************************************/
void RobinNP::initCRCTable()
/*****************************************************************************/
{
	//CRC-16-CCITT - polynomial 0x1021
	unsigned long crc = 0, bit = 0;
	const unsigned long crcinit = 0xffff, polynom = 0x1021;
	m_crc_interval = 0xffffffff;

	//compute constant bit masks for whole CRC and CRC high bit
	m_crcmask = ((((unsigned long) 1 << (c_order - 1)) - 1) << 1) | 1;
	unsigned long crchighbit = (unsigned long) 1 << (c_order - 1);
	//std::cout << "CRC Mask " << std::hex << crcmask << " highbit " << crchighbit << std::dec << std::endl;

	//compute CRC table
	for (unsigned int tableCounter = 0; tableCounter < 65536; ++tableCounter)
	{
		crc = tableCounter;

		for (unsigned int bitcounter = 0; bitcounter < 16; ++bitcounter)
		{
			bit = crc & crchighbit;
			crc <<= 1;
			if (bit)
				crc ^= polynom;
		}

		crc &= m_crcmask;
		m_crctab16[tableCounter] = crc;
	}

	//compute missing initial CRC value
	m_crcinit_direct = crcinit;
	crc = crcinit;
	for (unsigned int bitcounter = 0; bitcounter < c_order; ++bitcounter)
	{
		bit = crc & 1;
		if (bit)
			crc ^= polynom;
		crc >>= 1;
		if (bit)
			crc |= crchighbit;
	}
}

bool* RobinNP::getROLEnableStatus(unsigned int rolId){
	return &m_rolArray[rolId].linkEnabled;
}

void RobinNP::initDriverDataContainer(){
	m_cardData.nDmaPages = 0;
	m_cardData.bufferBaseAddress = 0;
	m_cardData.pciDevice = 0;
	m_cardData.slot = 0;
	m_cardData.registerBaseAddress = 0;
	m_cardData.registerSize = 0;
	m_cardData.bufferBaseAddress = 0;
	m_cardData.bufferSize = 0;
	m_cardData.nChannels = 0;
	m_cardData.nSubRobs = 0;
	for(unsigned int subRob = 0; subRob < MAX_SUBROB; ++subRob){
		m_cardData.upfDuplicateBuffer[subRob] = 0;
		m_cardData.upfDuplicateHandle[subRob] = 0;
		m_cardData.dmaDoneDuplicateBuffer[subRob] = 0;
		m_cardData.dmaDoneDuplicateHandle[subRob] = 0;
	}
	m_cardData.writeIndexBuffer = 0;
	m_cardData.writeIndexHandle = 0;
	m_cardData.nDmaPages = 0;
	m_cardData.dmaPageSize = 0;
	for(unsigned int buffer = 0; buffer < MAXDMABUF; ++buffer)
	{
		m_cardData.primaryDmaBuffer[buffer] = 0;
		m_cardData.primaryDmaHandle[buffer] = 0;
	}
	m_cardData.oscfreq = 0;

}


bool RobinNP::checkID(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId){
	if(*(fragmentLocation + 5) != level1ID){
		CREATE_ROS_EXCEPTION(ex_robinnp_checkid1, ROSRobinNPExceptions, BADID, "RobinNP::checkID: ERROR in L1 ID Match for ROL " << rolId << " Received L1ID = 0x" << HEX(*(fragmentLocation + 5)) << " Expected 0x" << HEX(level1ID) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_checkid1);
		return true;
	}
	return false;
}

int RobinNP::getRolState(unsigned int rolId){
	return m_configuration->getRolConfig(rolId)->getRolEnabled();
}

bool RobinNP::isLostFragment(unsigned int lastID, unsigned int l1Id){
	// check pending vs. lost
	bool result;
	if (lastID==0xffffffff) {
		result=false;
	}
	else if (l1Id > lastID) {
		result=(l1Id > s_fragMaxThreshold && lastID < s_fragMinThreshold);
	}
	else {
		result=!((lastID >= s_fragMaxThreshold) && (l1Id < s_fragMinThreshold));
	}
	return result;

}

/*************************************************************************/
/*                        Configuration                                  */
/*************************************************************************/

void RobinNP::configure(){
	//m_configuration->dumpAll();

	//Calculate FIFO Size Limits and offsets
	std::cout << "Changing Maximum Number of Pages per fragment = " << m_configuration->getMaxRxPages() << std::endl;
	if(s_DMAQueueFIFOSize < m_numChannelsPerSubRob*m_configuration->getMaxRxPages()){
		CREATE_ROS_EXCEPTION(ex12, ROSRobinNPExceptions, RXDMA, "RobinNP::configure: RobinNP " << m_slotNumber << " specified maxRXPages of " << m_configuration->getMaxRxPages() << " too large for current DMA queue size of " <<  s_DMAQueueFIFOSize << " Please ensure (" << m_numChannelsPerSubRob << " * maxRXPages) < Queue Size");
		throw(ex12);
	}
	else{
		m_dmaQueueFIFOLimit = s_DMAQueueFIFOSize - m_numChannelsPerSubRob*m_configuration->getMaxRxPages();
		std::cout << "Modified DMA Queue FIFO Limit (per subRob) = " << m_dmaQueueFIFOLimit << std::endl;
	}

	if(m_configuration->getPageSize() * m_configuration->getMaxRxPages() * 4 > (unsigned int)m_cardData.dmaPageSize){
		CREATE_ROS_EXCEPTION(ex_robinnp_configure, ROSRobinNPExceptions, MAXFRAG, "RobinNP::configure: RobinNP " << m_slotNumber << " largest possible fragment exceeds RobinNP maximum. Ensure Page Size * Max RX Pages <= " << m_cardData.dmaPageSize/4 << " (words). Current values are page size: " << m_configuration->getPageSize() << " and Max Rx Pages " << m_configuration->getMaxRxPages() );
		throw(ex_robinnp_configure);
	}

	std::cout << "Setting Dump File Path to " << m_configuration->getDumpFilePath() << std::endl;
	m_fileDumpPath = m_configuration->getDumpFilePath();

	std::cout << "Setting Dump File Limit to " << m_configuration->getFragDumpLimit() << std::endl;
	m_fileDumpLimit = m_configuration->getFragDumpLimit();
	if(m_fileDumpLimit == -1){
		std::cout << "Current setting will dump out all CRC error events" << std::endl;
	}

	std::cout << "Setting Input Error Dump File Limit to " << m_configuration->getErrorDumpLimit() << std::endl;
	m_errorDumpLimit = m_configuration->getErrorDumpLimit();
	if(m_errorDumpLimit == -1){
		std::cout << "Current setting will dump out all input error events" << std::endl;
	}


	std::cout << "Setting CRC Interval to " << m_configuration->getCRCCheckInterval() << std::endl;
	m_crc_interval = m_configuration->getCRCCheckInterval();

	m_pageSizeOffset = log10(m_configuration->getPageSize())/log10(2);
	std::cout << "Page size offset modified to " << m_pageSizeOffset << " for page size " << m_configuration->getPageSize() << std::endl;

	//Calculate Number Of Pages Per Rol
	if(m_runMode == runModeROIB){
		if(m_configuration->getNumPages() == 0){
			m_numPagesPerRol = (unsigned int)floor((1.0 * m_cardData.nDmaPages - s_captureDMAQueueFIFOSize * s_maxSubRobs) / s_maxRols);
		}
		else{
			if(m_configuration->getNumPages() <= (unsigned int)floor((1.0 * m_cardData.nDmaPages - s_captureDMAQueueFIFOSize * s_maxSubRobs) / s_maxRols)){
				m_numPagesPerRol = m_configuration->getNumPages();
			}
			else{
				CREATE_ROS_EXCEPTION(ex_robinnp_config0, ROSRobinNPExceptions, CONFIGURE, "RobinNP " << m_slotNumber << " requested requested number of pages per ROL for ROIB mode exceeds maximum. Requested " <<  m_configuration->getNumPages() << " Max " << (unsigned int)floor((1.0 * m_cardData.nDmaPages - s_captureDMAQueueFIFOSize * s_maxSubRobs) / s_maxRols));
				throw(ex_robinnp_config0);
			}
		}
	}else{
		m_numPagesPerRol = (unsigned int)(floor((s_mgmtMaxBufSize * m_numSubRobs * 1.0) / (m_numRols * m_configuration->getPageSize()))); // copy value
	}
	m_configuration->setNumPages(m_numPagesPerRol);
	std::cout << "num pages per ROL " <<  m_numPagesPerRol << std::endl;

	unsigned int delayBits = 0;
	switch (m_configuration->getUPFDelaySteps()) {
	case s_mgmtMinDelaySize:
		delayBits = 0;
		break;
	case 2 * s_mgmtMinDelaySize:
	delayBits = 1;
	break;
	case 4 * s_mgmtMinDelaySize:
	delayBits = 2;
	break;
	case 8 * s_mgmtMinDelaySize:
	delayBits = 3;
	break;
	default:
		CREATE_ROS_EXCEPTION(ex_robinnp_config1, ROSRobinNPExceptions, CONFIGURE, "RobinNP " << m_slotNumber << " requested invalid UPF Delay line setting: " << m_configuration->getUPFDelaySteps() << ", must be 512, 1024, 2048 or 4096 steps");
		throw(ex_robinnp_config1);
		break;
	}


	for(unsigned int subRob = 0; subRob < m_numSubRobs; ++subRob){
		m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationWriteTimeout = m_configuration->getMemWriteTimeout();
		m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationReadTimeout = m_configuration->getMemReadTimeout();
		m_Bar0SubRob[subRob]->MemoryControllerControl.Component.InputFIFOFullThreshold = m_configuration->getMemfifoFill();
		std::cout << "SubRob " << subRob << " Memory Arbiter Write Timeout: " << m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationWriteTimeout << " cycles, fifo fill " << m_Bar0SubRob[subRob]->MemoryControllerControl.Component.InputFIFOFullThreshold << std::endl;
		std::cout << "SubRob " << subRob << " Memory Arbiter Read Timeout: " << m_Bar0SubRob[subRob]->MemoryControllerControl.Component.MemoryArbitrationReadTimeout << " cycles, fifo fill " << m_Bar0SubRob[subRob]->MemoryControllerControl.Component.InputFIFOFullThreshold << std::endl;

		//Disable until ready at Point 1
		m_Bar0SubRob[subRob]->SubRobControl.Component.UPFDelayStepsBitMask = delayBits;
		std::cout << "SubRob " << subRob << " UPF Delay Configured to Bit Set: " <<  decimalToBinary(m_Bar0SubRob[subRob]->SubRobControl.Component.UPFDelayStepsBitMask) << std::endl;

		if(m_configuration->getMemEmulationState()){
			std::cout << "Running with Buffer Memory Emulation" << std::endl;
			m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DestinationOffset= 0x48;
			m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.PrimaryDMAFIFOCountIgnore = 1;
			m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 1;
		}
		else{
			std::cout << "Running with Real Buffer Memory" << std::endl;
			m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.PrimaryDMAFIFOCountIgnore = 0;
			m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.MemoryReadIgnore = 0;

			if(m_runMode == runModeLegacy){
				m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DestinationOffset= 0x28;
			}
			else{
				m_Bar0SubRob[subRob]->PrimaryDMAControlRegister.Component.DestinationOffset= 0x0;
			}
		}
	}

	for(unsigned int rol = 0; rol < m_numRols; ++rol){

		if(m_configuration->getRolConfig(rol)->getRolDataGen()){
			//Validate Data Generator Test Size
			if(m_configuration->getRolConfig(rol)->getTestSize() > m_configuration->getPageSize() * s_bmMaxFragPages ){
				//ers warning
				std::cout << "RobinNP::setConfig: Test fragment uses too many pages, please correct pageSize or testsize" << std::endl;
			}
		}

		//Configure ROL Enabled Flags
		if(m_configuration->getRolConfig(rol)->getRolEnabled()){
			// don't enable in discard mode
			if (!m_configuration->getRolConfig(rol)->getDiscardMode()){
				m_rolArray[rol].m_runCtlState =  s_runCtlStart;
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rol << " enabled");
				enableRol(rol);
			} else {
				DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rol << " not enabled due to discard mode");
			}
		} else {
			disableRol(rol);
			m_rolArray[rol].m_runCtlState =  s_runCtlStop;
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rol << " disabled");
		}


		//Apply Discard Mode Configuration
		if(m_configuration->getRolConfig(rol)->getDiscardMode()){
			std::cout << "Activating discard mode for ROL " << rol << std::endl;
			m_rolArray[rol].linkEnabled = false;
		}
		else{
			m_rolArray[rol].linkEnabled = true;
		}
	}

}

void RobinNP::configure(RobinNPConfig& config){

	m_configuration->applyConfig(config);

	configure();

	for(unsigned int rol = 0; rol < m_numRols; ++rol){
		initRol(rol);
	}
}

void RobinNP::setRolEnabled(bool status, unsigned int rolId){
	m_configuration->getRolConfig(rolId)->setRolEnabled(status);
	if (m_configuration->getRolConfig(rolId)->getRolEnabled()){
		m_rolArray[rolId].m_runCtlState =  s_runCtlStop;
		m_rolArray[rolId].linkEnabled = false;
	}
	/*if (m_configuration->getRolConfig(rolId)->getRolEnabled()){
		// don't enable in discard mode
		if (m_rolArray[rolId].linkEnabled){
			m_rolArray[rolId].m_runCtlState =  s_runCtlStart;
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rolId << " enabled");
			enableRol(rolId);
		} else {
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rolId << " not enabled due to discard mode");
		}
	} else {
		disableRol(rolId);
		m_rolArray[rolId].m_runCtlState =  s_runCtlStop;
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::setConfig: ROL " << rolId << " disabled");
	}*/
}

void RobinNP::directFragmentDMA(FragInfo *theFragment,unsigned int rolId){

	int fragmentStatus = checkROIBFragment(theFragment,rolId);

	if(fragmentStatus == -1){
		return;
	}

	unsigned int subRob = m_rolArray[rolId].subRobMapping;
	DataPage* page = getDMAPage(subRob);

	if(page == 0){
		return;
	}
	int dataAddr = (theFragment->upfInfo.pageNum + (rolId - subRob*m_numChannelsPerSubRob) * m_numPagesPerRol) * m_configuration->getPageSize();
	int dataSize = theFragment->upfInfo.pageLen;

	if(dataSize != 0){
		__sync_fetch_and_add(&m_numDMAsPending[subRob], 1);

		//Write DMA landing address to FIFO as this step is the same irrespective of the rest of the configuration

		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[0] = (unsigned long)page->physicalAddress();


		unsigned int descriptorInfo = (m_DMADoneActiveQueueWriteIndex[subRob] & 0xFFF);

		m_Bar0SubRob[subRob]->MemoryReadQueueFIFO.Register[1] = (uint16_t)(4*(dataSize)) | s_dmaNoOffsetDmaDone | (descriptorInfo << 16) | ((uint64_t)((dataAddr) * 4)) << 32;

		DMATrackingQueueElement element;
		element.rolID = rolId;
		element.eventID = theFragment->upfInfo.eventId;
		element.robinnppagenum = theFragment->upfInfo.pageNum;
		element.page = page;
		element.index = m_DMADoneActiveQueueWriteIndex[subRob];
		element.transferSize = dataSize;
		element.memAddress = dataAddr;
		element.fragStatus = fragmentStatus;
		element.virt = (unsigned long)page->virtualAddress();
		queueActiveDMA(element,subRob);
	}

}

void *RobinNP::directDMAManagerMain(unsigned int subRob){


	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::directDMAManagerMain: Starting Direct DMA Manager Thread for SubRob " << subRob);

	m_dmaFull[subRob] = false;
	m_directDMAManagerRunState[subRob] = true;
	unsigned int interrupt = s_UPFInterruptCodes[subRob];
	bool interrupted = false;
	while(m_directDMAManagerRunState[subRob]){

		awaitFreeDMASlots(subRob);

		unsigned int readIndex = 0;
		if(!checkCombinedUPFWriteIndex(subRob,readIndex)){
			int ret = ioctl(m_devHandle, WAIT_IRQ,&interrupt);
			if (ret)
			{
				CREATE_ROS_EXCEPTION(ex_robinnp_indexermain1, ROSRobinNPExceptions, INTERRUPTFAIL, "RobinNP::directDMAManagerMain: Interrupt " << interrupt << " failure: " << strerror(errno) << " for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_indexermain1);
			}
			interrupted = true;
		}
		else{
			FragInfo theFragment;
			if (processUPFDuplicatorPage(readIndex,subRob,theFragment)){
				directFragmentDMA(&theFragment, theFragment.RolId);
			}
		}

		while(checkCombinedUPFWriteIndex(subRob,readIndex) && m_directDMAManagerRunState[subRob]){
			FragInfo theFragment;
			if (processUPFDuplicatorPage(readIndex,subRob,theFragment)){
				directFragmentDMA(&theFragment, theFragment.RolId);
			}
		}
		if(interrupted){
			m_Bar0Common->InterruptControlRegister.acknowledge = (1 << interrupt);
			interrupted = false;
		}
		signalFreeFPFSlots(subRob);
	}

	std::stringstream ss;
	ss << "RobinNP::directDMAManagerMain: Exiting Direct DMA Thread for SubRob " << subRob;
	screenPrint(ss.str());

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::directDMAManagerMain: Exiting Direct DMA Thread");

	return 0;
}

void RobinNP::setupDirectOutputQueue(std::vector<tbb::concurrent_bounded_queue<ROIBOutputElement>*> *directOutputQueue){
	m_directOutputQueue = directOutputQueue;
}

void RobinNP::dumpCRCErrorFragment(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId, unsigned int totalSize,unsigned int memAddress){

	pthread_mutex_lock(&m_fileDumpMutex);

	if(m_filesDumped < m_fileDumpLimit || (m_fileDumpLimit == -1)){
		std::time_t time = std::time(NULL);
		std::stringstream ss;
		ss << m_fileDumpPath << "/ROSCRCErrorDump_RobinNP_" << m_slotNumber << "_ROL_" << rolId << "_dumpindex_" << m_filesDumped << "_" << time << ".log";

		std::string filename = ss.str();
		//std::cout << filename.c_str() << std::endl;
		std::ofstream dumpFile(filename.c_str());
		dumpFile << "File Dump for ROL " << rolId << std::hex << " L1 ID " << level1ID << " memory page address " << memAddress <<std::endl;
		for(unsigned int dataWord = 0; dataWord < totalSize; ++dataWord){
			dumpFile << std::hex << std::setfill('0') << std::setw(8) << *(fragmentLocation + dataWord) << " ";
			if((dataWord + 1) % 8 == 0){
				dumpFile << std::endl;
			}
		}
		dumpFile << std::hex << std::endl;
		dumpFile.close();

		CREATE_ROS_EXCEPTION(ex_robinnp_dumperr, ROSRobinNPExceptions, BADCRC, "RobinNP::dumpCRCErrorFragment: Error Fragment for ROL " << rolId << " L1 ID " << HEX(level1ID) << " for RobinNP " << m_slotNumber << " Dumped to Disc as " << filename.c_str());
		ers::warning(ex_robinnp_dumperr);

		m_filesDumped++;
	}

	pthread_mutex_unlock(&m_fileDumpMutex);

}


void RobinNP::dumpInputErrorFragment(unsigned int *fragmentLocation,unsigned int level1ID, unsigned int rolId, unsigned int totalSize,std::string& errorDescription){

	pthread_mutex_lock(&m_fileDumpMutex);

	std::time_t time = std::time(NULL);
	std::stringstream ss;
	ss << m_fileDumpPath << "/ROSInputErrorDump_RobinNP_" << m_slotNumber << "_ROL_" << rolId << "_dumpindex_" << m_errorsDumped << "_" << time << ".log";

	std::string filename = ss.str();
	//std::cout << filename.c_str() << std::endl;
	std::ofstream dumpFile(filename.c_str());
	dumpFile << "File Dump for ROL " << rolId << std::hex << " L1 ID " << level1ID << ", error description: " << errorDescription.c_str() << "- fragment size 0x" << totalSize << " words" << std::endl;
	for(unsigned int dataWord = 0; dataWord < totalSize; ++dataWord){
		dumpFile << std::hex << std::setfill('0') << std::setw(8) << *(fragmentLocation + dataWord) << " ";
		if((dataWord + 1) % 8 == 0){
			dumpFile << std::endl;
		}
	}
	dumpFile << std::hex << std::endl;
	dumpFile.close();

	CREATE_ROS_EXCEPTION(ex_robinnp_dumpinerr, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::dumpInputErrorFragment: ROL " << rolId << " Fragment marked with TX or sequence error(s).  L1ID = 0x" << HEX(level1ID) << " for RobinNP " << m_slotNumber << ". Error description " <<  errorDescription.c_str() << " - Dumped to Disc as " << filename.c_str());
	ers::warning(ex_robinnp_dumpinerr);
	m_errorsDumped++;

	pthread_mutex_unlock(&m_fileDumpMutex);

}



void RobinNP::resetDumpFileCounter(){
	m_filesDumped = 0;
}

void RobinNP::resetErrorDumpFileCounter(){
	m_errorsDumped = 0;
}


/*********************************************************************/
int RobinNP::checkROIBFragment(FragInfo *theFragment, int rolId)
/*********************************************************************/
{
	//Process ROIB and check for errors fragment
	unsigned int status = 0, finalStatus = 0;

#ifdef FIXED_RUN_NUMBER
	theFragment->upfInfo.runNum = 0;	// fix run number
#endif

	// check sequence errors
	if (checkSeqError(m_statistics->m_rolStats[rolId].m_mostRecentId, theFragment->upfInfo.eventId)){
		// mark fragment
		finalStatus |= fragStatusSeqError;

		if(m_statistics->m_rolStats[rolId].m_mostRecentId == theFragment->upfInfo.eventId){
			finalStatus |= fragStatusDuplicate;
		}
		// update status
		m_statistics->m_rolStats[rolId].m_fragStat[fragsOutOfSync]++;
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::checkROIBFragment: ROL " << rolId << " fragment synchronisation lost: current id: 0x" << HEX(theFragment->upfInfo.eventId) << ", previous id: 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));
		DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::checkROIBFragment: Page number 0x" << HEX(theFragment->upfInfo.pageNum) << ", size 0x" << HEX(theFragment->upfInfo.pageLen));
		CREATE_ROS_EXCEPTION(ex_robinnp_procinc, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::checkROIBFragment: ROL " << rolId << " Fragment out of sequence: L1 ID = 0x" << HEX(theFragment->upfInfo.eventId) << ", Most Recent ID 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId) << " for RobinNP " << m_slotNumber);
		ers::warning(ex_robinnp_procinc);
	}

	m_statistics->m_rolStats[rolId].m_mostRecentId = theFragment->upfInfo.eventId;

	// initialise inhibit mechanism
	m_rolArray[rolId].m_inhibitFragment = false;
	// check for unrecoverable errors. This is already known with the first page of a fragment
	if (theFragment->upfInfo.status & upfErrorMask){

		// sequence errors do no longer qualify a soft error to reject fragments
		if (theFragment->upfInfo.status & upfHardErrorMask){
			m_rolArray[rolId].m_inhibitFragment = true;
			DEBUG_TEXT(DFDB_ROSROBINNP, 6, "RobinNP::checkROIBFragment: Fragment has unrecoverable error");
		}
	}

	// check minimum size of fragment: ROD header + trailer (without status word)
	if (((sizeof(RodFragment)/sizeof(unsigned int)-1)  > theFragment->upfInfo.pageLen)) {
		finalStatus |= fragStatusShort;
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::prepareData: Status changed to: 0x" << HEX(finalStatus));
	}

	if (m_rolArray[rolId].m_inhibitFragment){
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::checkROIBFragment: Suppressing first page of erroneous fragment");
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::checkROIBFragment: Fragment status is 0x" << HEX(theFragment->upfInfo.status));
		m_statistics->m_rolStats[rolId].m_pageStat[pagesSupressed]++;
		m_statistics->m_rolStats[rolId].m_fragStat[fragsCorrupted]++;
		m_statistics->m_rolStats[rolId].m_fragStat[fragsRejected]++;
		addRejectedPage(theFragment, m_statistics->m_rolStats[rolId].m_mostRecentId, rolId);
		finalStatus = -1;
	} else {
		DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::checkROIBFragment: Processing new fragment for id 0x" << HEX(m_statistics->m_rolStats[rolId].m_mostRecentId));

		m_statistics->m_rolStats[rolId].m_fragStat[fragsAdded]++;

		// copy status to item
		bool confirmError = false;
		if (theFragment->upfInfo.status & upfErrorMask){
			for (auto statusElement : m_fragErrorCombinations) {
				if ((theFragment->upfInfo.status & statusElement.first) == statusElement.first){
					status |= statusElement.first;	// copy status and error bits to global
					//std::cout << std::hex << "status " << status << std::dec << std::endl;
					m_statistics->m_rolStats[rolId].m_fragStat[statusElement.second]++;
					confirmError = true;
				}
			}
		}
		if(confirmError){
			finalStatus |= fragStatusTxError;	// set TX error
			DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::checkROIBFragment: Channel " << rolId << " status code 0x" << HEX(status) << " on corrupted fragment 0x" << HEX(theFragment->upfInfo.eventId));
			m_statistics->m_rolStats[rolId].m_fragStat[fragsCorrupted]++;
			m_statistics->m_rolStats[rolId].m_fragStat[fragsTxError]++;
			std::stringstream errors;
			for (auto statusPair : m_fragStatusMapDebug) {
				if ((status & statusPair.first) == statusPair.first ){
					errors << statusPair.second << " ";
				}
			}
			CREATE_ROS_EXCEPTION(ex_robinnp_procinc, ROSRobinNPExceptions, FRAGMENTERROR, "RobinNP::checkROIBFragment: ROL " << rolId << " Fragment marked with TX error(s). L1ID = 0x" << HEX(theFragment->upfInfo.eventId) << " for RobinNP " << m_slotNumber << ". Error description: " << errors.str());
			ers::warning(ex_robinnp_procinc);

			for (auto statusPair : m_fragStatusMap) {
				if ((status & statusPair.first) == statusPair.first ){
					finalStatus |= statusPair.second;
				}
			}
		}
	}

	// update page counter for every page
	m_statistics->m_rolStats[rolId].m_pageStat[pagesReceived]++;

	DEBUG_TEXT(DFDB_ROSROBINNP, 16, "RobinNP::checkROIBFragment: checkROIBFragment completed");
	return finalStatus;
}


void RobinNP::toggleBufferManagerXOFF(unsigned int rolId, bool xoff){
	m_Bar0Channel[rolId]->BufferManagerControlRegister.Component.XOFF = xoff;
}

void RobinNP::requestErrorFragmentDMA(MgmtEntry* entry,unsigned int rolId,std::string& errorDescription){

	unsigned int subRob = m_rolArray[rolId].subRobMapping;
	DataPage* landingPage = m_captureQueue[subRob]->at(m_DMADoneActiveCaptureQueueWriteIndex[subRob]);
	bool fragComplete = (upfEndMarker == (entry->info.upfInfo.status & upfEndMask));
	unsigned int dmasIssued = 0;
	DMAInfo dma;
	dma.rolId = rolId;
	dma.useHeader = true;
	dma.physicalAddressDMA = (unsigned long)landingPage->physicalAddress();
	dma.memoryPage = entry;
	m_captureDMAProcessContainer[subRob][dmasIssued] = dma;
	dmasIssued++;
	unsigned int l1Id = entry->info.upfInfo.eventId;
	MgmtEntry* item = entry;

	if(!fragComplete){
		// send subsequent pages (if any)

		while (1){
			unsigned int position = item->next;            // go to next item
			// error case: fragment not complete, but no next entry
			bool missingPage = false;
			if (position == 0){                // last entry ?
				missingPage = true;
				// terminate transfer
			}

			// error case: fragment not complete, but next entry different L1ID
			if (l1Id != m_rolArray[rolId].m_itemListPtr[position].info.upfInfo.eventId){
				missingPage = true;
				// terminate transfer
			}

			if(missingPage){
				CREATE_ROS_EXCEPTION(ex_robinnp_procfragreq1, ROSRobinNPExceptions, MISSINGPAGE, "RobinNP::requestErrorFragmentDMA:  L1ID = 0x" << HEX(l1Id) << ": Pages Missing from Multipage Fragment for RobinNP " << m_slotNumber);
				ers::warning(ex_robinnp_procfragreq1);
				break;
			}

			DMAInfo dma;
			dma.rolId = rolId;
			dma.useHeader = false;
			dma.physicalAddressDMA = (unsigned long)landingPage->physicalAddress();//pool + dma's this fragment etc, see other method
			item = &m_rolArray[rolId].m_itemListPtr[position];
			dma.memoryPage = item;
			m_captureDMAProcessContainer[subRob][dmasIssued] = dma;
			dmasIssued++;
			// update completion status
			fragComplete = (upfEndMarker == (item->info.upfInfo.status & upfEndMask));
			if (fragComplete){
				break;      // finished
			}

		}
	}
	unsigned int wrapCounter = 0;
	for(unsigned int dmaCounter = 0; dmaCounter < dmasIssued; ++dmaCounter){
		//Request Capture DMA
		awaitFreeCaptureDMASlots(subRob);
		m_Bar0SubRob[subRob]->CaptureDMAQueueFIFO.Register[0] = m_captureDMAProcessContainer[subRob][dmaCounter].physicalAddressDMA + m_captureDMAProcessContainer[subRob][0].memoryPage->info.upfInfo.pageLen*4*dmaCounter; //Physical Address of Response Area
		int dataAddr = (m_captureDMAProcessContainer[subRob][dmaCounter].memoryPage->info.upfInfo.pageNum + (rolId - subRob*m_numChannelsPerSubRob) * m_numPagesPerRol) * m_configuration->getPageSize();
		int dataSize = m_captureDMAProcessContainer[subRob][dmaCounter].memoryPage->info.upfInfo.pageLen;

		if(dmaCounter == dmasIssued - 1 || (dmaCounter > 0 && (dmaCounter % 10 == 0))){
			unsigned int descriptorInfo = 0;
			if(dmaCounter == dmasIssued - 1){
				descriptorInfo = (((m_DMADoneActiveCaptureQueueWriteIndex[subRob]) + s_captureIndexOffset) & 0xFFF);
			}
			else{
				wrapCounter++;
				descriptorInfo = (s_captureCountResetCtrlValue & 0xFFF);
			}
			m_Bar0SubRob[subRob]->CaptureDMAQueueFIFO.Register[1] = (uint16_t)(4*(dataSize)) | s_dmaNoOffsetDmaDone | (descriptorInfo << 16) | ((uint64_t)((dataAddr) * 4)) << 32;
		}
		else{
			m_Bar0SubRob[subRob]->CaptureDMAQueueFIFO.Register[1] = (uint16_t)(4*(dataSize)) | ((uint64_t)((dataAddr) * 4)) << 32;
		}

		__sync_fetch_and_add(&m_numCaptureDMAsPending[subRob], 1);

	}

	DMATrackingQueueElement element;
	element.virt = (unsigned long)landingPage->virtualAddress();
	element.numDMAs = dmasIssued - (wrapCounter * 10);
	element.rolID = rolId;
	element.index = m_DMADoneActiveCaptureQueueWriteIndex[subRob];
	element.transferSize = entry->size;
	element.fragIndexPtr = entry;
	element.eventID = entry->info.upfInfo.eventId;
	element.text = errorDescription;
	queueActiveCaptureDMA(element,subRob);

}



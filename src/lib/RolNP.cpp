/*******************************************/
/* file   Rol.cpp      					   */
/*                     				       */
/* author: William Panduro Vazquez   RHUL  */
/*         (j.panduro.vazquez@cern.ch)     */
/*******************************************/


//C++ headers
#include <iostream>

//TDAQ headers
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_time_stamp/tstamp.h"

//RobinNP headers
#include "ROSRobinNP/RobinNP.h"
#include "ROSRobinNP/RolNP.h"
#include "ROSRobinNP/ROSRobinNPExceptions.h"

using namespace ROS;

const unsigned int ROS::RolNP::c_order = 16;

/*************************************************/
unsigned short RolNP::crctablefast16(unsigned short* p, unsigned long len)
/*************************************************/
{
	unsigned long crc = crcinit_direct;
	const unsigned long crcxor = 0x0000;

	while (len--)
		crc = (crc << 16) ^ crctab16[((crc >> (c_order - 16)) & 0xffff) ^ *p++];

	crc ^= crcxor;
	crc &= crcmask;
	return((unsigned short)crc);
}

/*************************************************/
unsigned short RolNP::crctablefastwithskip16(unsigned short* p, unsigned long len)
/*************************************************/
{
	unsigned long crc = crcinit_direct;
	const unsigned long crcxor = 0x0000;

	while (len--) {
		crc = (crc << 16) ^ crctab16[((crc >> (c_order - 16)) & 0xffff) ^ *p]; p+=2;
	}

	crc ^= crcxor;
	crc &= crcmask;
	return((unsigned short)crc);
}


/*****************************************************************************/
RolNP::RolNP(RobinNP & robinNPBoard, unsigned int rolId) : m_robinNP(robinNPBoard), m_rolId(rolId)
/*****************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::constructor: Called");

	//CRC stuff
	unsigned long crchighbit, bit, crc, i, j;
	const unsigned long crcinit = 0xffff, polynom = 0x1021;
	m_crc_interval = 0xffffffff;

	//compute constant bit masks for whole CRC and CRC high bit
	crcmask = ((((unsigned long) 1 << (c_order - 1)) - 1) << 1) | 1;
	crchighbit = (unsigned long) 1 << (c_order - 1);

	//compute CRC table
	for (i = 0; i < 65536; i++)
	{
		crc = i;

		for (j = 0; j < 16; j++)
		{
			bit = crc & crchighbit;
			crc <<= 1;
			if (bit)
				crc ^= polynom;
		}

		crc &= crcmask;
		crctab16[i] = crc;
	}

	//compute missing initial CRC value
	crcinit_direct = crcinit;
	crc = crcinit;
	for (i = 0; i < c_order; i++)
	{
		bit = crc & 1;
		if (bit)
			crc ^= polynom;
		crc >>= 1;
		if (bit)
			crc |= crchighbit;
	}
}


/*********/
RolNP::~RolNP()
/*********/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::destructor: Called");
}


/************************************************************/
void RolNP::requestFragment(unsigned int eventId, unsigned char *replyAddress, unsigned long physicalAddress, unsigned int ticket)
/************************************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 10 ,"RolNP::requestFragment: Called with eventId = " << eventId << " and replyAddress = 0x" << HEX(replyAddress));

	m_robinNP.requestFragment(m_rolId, replyAddress, physicalAddress, eventId, ticket);

}
/************************************/
unsigned int * RolNP::getFragment(void *responseAddress, unsigned int ticket)
/************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getFragment: Called with responseAddress = 0x" << HEX(responseAddress));

	m_robinNP.receiveDMA(m_rolId,ticket);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getFragment: Done");
	return(static_cast<unsigned int *> (responseAddress));
}

/*************************************************************/
bool RolNP::releaseFragment(const std::vector <unsigned int> *eventIds)
/*************************************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::releaseFragment: Called");
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::releaseFragment: The vector contains " << eventIds->size() << " L1IDs");

	bool result = m_robinNP.clearRequest(eventIds,m_rolId);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::releaseFragment: Done");

	return result;
}

/****************************************************************/
bool RolNP::releaseFragmentAll(const std::vector <unsigned int> *eventIds)
/****************************************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::releaseFragmentAll: Called");
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::releaseFragmentAll: The vector contains " << eventIds->size() << " L1IDs");

	bool result = m_robinNP.clearRequest(eventIds);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::releaseFragmentAll: Done");

	return result;
}


/**************************************/
RobinNPROLStats* RolNP::getStatistics()
/**************************************/
{

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getStatistics: Called");
	return(m_robinNP.getROLStatistics(m_rolId));
}


/*****************************/
int RolNP::getTemperature()
/*****************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getTemperature: Called");

	int rtemp = m_robinNP.requestTempVal();

	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getTemperature: The FPGA core temperature is " << rtemp << " deg. C");

	return(rtemp);
}


/***************************************/
ECRStatisticsBlock RolNP::getECR()
/***************************************/
{
	ECRStatisticsBlock ret;

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getECR: Called");

	unsigned int *sb = m_robinNP.getECR(m_rolId);

	ret.mostRecentId = sb[0];
	ret.overflow = sb[2];
	ret.necrs = 0;
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getECR: most recent ID = 0x" << HEX(ret.mostRecentId));
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getECR: overflow       = " << ret.overflow);
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getECR: # ECRs         = " << sb[1]);

	for(unsigned int loop = 0; loop < sb[1]; loop++)
	{
		DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getECR: ECR # " << loop << " at 0x" << HEX(sb[3 + loop]));
		if (sb[3 + loop] != 0xffffffff)
		{
			ret.ecr[ret.necrs] = sb[3 + loop];
			ret.necrs++;
		}
	}
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getECR: Effective # ECRs         = " << ret.necrs);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getECR: Done");

	delete[] sb; //Clean up response memory

	return(ret);
}


/*******************************************/
void RolNP::setRolEnabled(bool status)
/*******************************************/
{
	m_robinNP.setRolEnabled(status, m_rolId);
}


/***************/
void RolNP::reset()
/***************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::reset: Called");

	m_robinNP.requestEnterDiscardMode(m_rolId);
	m_robinNP.requestLeaveDiscardMode(m_rolId);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::reset: Done");
}


/*************************/
void RolNP::clearStatistics()
/*************************/
{

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::clearStatistics: Called");

	m_robinNP.initStatistics(m_rolId);

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::clearStatistics: Done");
}



/**********************************************/
KeptPage RolNP::getRejectedPage(unsigned int pageno)
/**********************************************/
{
	KeptPage ret;

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::getRejectedPage: Called");

	RejectedEntry *rejected = m_robinNP.requestKeptPage(m_rolId, pageno);

	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: rejected->mri           = 0x" << HEX(rejected->mri));
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: rejected->info.upfInfo.status   = 0x" << HEX(rejected->info.upfInfo.status));
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: rejected->info.upfInfo.eventId  = 0x" << HEX(rejected->info.upfInfo.eventId));
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: rejected->info.upfInfo.pageNum  = 0x" << HEX(rejected->info.upfInfo.pageNum));
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: rejected->info.upfInfo.pageLen  = 0x" << HEX(rejected->info.upfInfo.pageLen));

	//Copy the  data
	ret.rentry = *rejected;
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::getRejectedPage: ret.pagedata_size = 0x" << HEX(ret.pagedata_size));
	for (unsigned int loop = 0; loop < s_mgmtMaxPageSize; loop++){
		ret.pagedata[loop] = 0;//rdata[loop]; - WPV commented this out
	}

	return(ret);
}


/********************************/
void RolNP::clearRejectedPages()
/********************************/
{

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::clearRejectedPages: Called");

	int ret = m_robinNP.requestClearKeptPages(m_rolId);

	if (ret != 0)
	{
		DEBUG_TEXT(DFDB_ROSROBINNP, 5 ,"RolNP::clearRejectedPages: Robin returns invalid response ID");
		CREATE_ROS_EXCEPTION(ex_rolnp_clearrejpages1, ROSRobinNPExceptions, WRONGREPLY, "The ROBIN did not reply with a respAck when trying to clear the special memory for rejected events!");
		throw (ex_rolnp_clearrejpages1);
	}

}


/****************************************/
void RolNP::configureDiscardMode(unsigned int mode)
/****************************************/
{
	//mode = 0 -> Disable discard mode
	//mode = 1 -> Enable discard mode

	DEBUG_TEXT(DFDB_ROSROBINNP, 15 ,"RolNP::configureDiscardMode: Called");
	if (mode == 0)
		m_robinNP.requestLeaveDiscardMode(m_rolId);
	else
		m_robinNP.requestEnterDiscardMode(m_rolId);

}


/****************************************/
void RolNP::setCRCInterval(int crc_interval)
/****************************************/
{
	DEBUG_TEXT(DFDB_ROSROBINNP, 20 ,"RolNP::setCRCInterval: called with interval = " << crc_interval);
	m_crc_interval = crc_interval;
}

/****************************************/
void RolNP::startThreads()
/****************************************/
{
	if(!m_robinNP.getThreadState()) {
		m_robinNP.startThreads();
	}
	else{
		DEBUG_TEXT(DFDB_ROSROBINNP, 6 ,"RolNP::startIndexer: Indexer already started for this RobinNP");
	}
}

/*****************************/
unsigned int RolNP::getRolId(){
	/*****************************/
	return m_rolId;
}

unsigned int RolNP::getRolState(){
	return m_robinNP.getRolState(m_rolId);
}

#include "ROSRobinNP/RobinNPConfig.h"

//RobinNPROLConfig

RobinNPROLConfig::RobinNPROLConfig():
m_testSize(50),
m_rolDataGen(false),
m_rolEnabled(false),
m_robId(0),
m_discardMode(false),
m_default(true),
m_dataGenEarlyWrap(false)
{

}

void RobinNPROLConfig::dump() const{
	std::cout << "-------------------------------------------------" << std::endl;
	std::cout << " ROB ID \t\t\t\t|" << m_robId << std::endl;
	std::cout << " test size (words) \t\t\t|" << m_testSize << std::endl;
	std::cout << " data gen (yes = 1, no = 0) \t\t|" << m_rolDataGen << std::endl;
	std::cout << " dg early wrap (yes = 1, no = 0) \t|" << m_dataGenEarlyWrap << std::endl;
	std::cout << " rol enabled (yes = 1, no = 0) \t\t|" << m_rolEnabled << std::endl;
	std::cout << " discard mode (yes = 1, no = 0) \t|" << m_discardMode << std::endl;
	std::cout << "-------------------------------------------------" << std::endl;

}

//RobinNPConfig

RobinNPConfig::RobinNPConfig():
				m_maxRxPages(1),
				m_pageSize(256),
				m_emulateMem(false),
				m_memWriteTimeout(128),
				m_memReadTimeout(128),
				m_memFifoFill(90),
				m_crcCheckInterval(0),
				m_dumpFilePath("."),
				m_fragDumpLimit(10),
				m_errorDumpLimit(10),
				m_upfDelaySteps(512),
				m_numPages(0)
{
}

void RobinNPConfig::configureRol(RobinNPROLConfig config, unsigned int m_rolId){
	m_rolConfig[m_rolId] = config;
	m_rolConfig[m_rolId].setDefault(false);
}



void RobinNPConfig::applyConfig(const RobinNPConfig &incoming){
	m_maxRxPages = incoming.m_maxRxPages;
	m_pageSize = incoming.m_pageSize;
	m_emulateMem = incoming.m_emulateMem;
	m_memWriteTimeout = incoming.m_memWriteTimeout;
	m_memReadTimeout = incoming.m_memReadTimeout;
	m_memFifoFill = incoming.m_memFifoFill;
	m_crcCheckInterval = incoming.m_crcCheckInterval;
	m_dumpFilePath = incoming.m_dumpFilePath;
	m_fragDumpLimit = incoming.m_fragDumpLimit;
	m_errorDumpLimit = incoming.m_errorDumpLimit;
	m_upfDelaySteps = incoming.m_upfDelaySteps;
	m_numPages = incoming.m_numPages;

	for(unsigned int rol = 0; rol < s_maxRols; ++rol){
		if(!incoming.m_rolConfig[rol].isDefault()){
			m_rolConfig[rol] = incoming.m_rolConfig[rol];
		}
	}

}




void RobinNPConfig::dumpGlobal() const{

		std::cout << "-------------------------------------------------" << std::endl;
		std::cout << "\nPrint Global Parameters:\n" << std::endl;
		std::cout << "Max RX Pages \t\t\t\t|" << m_maxRxPages << std::endl;
		std::cout << "Max Page Size (words) \t\t\t|" << m_pageSize << std::endl;
		std::cout << "Num Pages Per Rol  \t\t\t|" << m_numPages << std::endl;
		std::cout << "Mem Emulation (yes = 1, no = 0)  \t|" << m_emulateMem << std::endl;
		std::cout << "Mem Write Timeout (cycles)  \t\t|" << m_memWriteTimeout << std::endl;
		std::cout << "Mem Read Timeout (cycles)  \t\t|" << m_memReadTimeout << std::endl;
		std::cout << "Mem Fifo Fill (entries)  \t\t|" << m_memFifoFill << std::endl;
		std::cout << "CRC Check Interval (events)  \t\t|" << m_crcCheckInterval << std::endl;
		//std::cout << "Dump File Path \t\t\t\t|" << m_dumpFilePath.c_str() << std::endl;
		std::cout << "Frag Dump Limit \t\t\t|" << m_fragDumpLimit << std::endl;
		std::cout << "Error Dump Limit \t\t\t|" << m_errorDumpLimit << std::endl;
		std::cout << "-------------------------------------------------" << std::endl;

}
void RobinNPConfig::dumpAll() const{
	dumpGlobal();
	for(unsigned int rol = 0; rol < s_maxRols; ++rol){
		dumpRol(rol);
	}
}

void RobinNPConfig::dumpRol(unsigned int rolId) const{
	std::cout << "\nPrint ROL Specific Config: \n" << std::endl;
	std::cout << " ROL " << rolId << std::endl;
		m_rolConfig[rolId].dump();
}

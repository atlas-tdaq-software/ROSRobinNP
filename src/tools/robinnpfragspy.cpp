//Debugging tool which can dump fragments preceding
//a fragment displaying an error to disc

#include <iostream>
#include <signal.h>
#include <getopt.h>

#include "ROSDescriptorNP/DataPage.h"
#include "ROSRobinNP/RobinNPFragSpy.h"


using namespace ROS;

const struct option longopts[] =
{
		{"slot",          		required_argument,  		0, 's'},
		{"generatefrags",		no_argument,  				0, 'g'},
		{"pagesize",   			required_argument,  		0, 'p'},
		{"chanmask",   			required_argument,  		0, 'c'},
		{"buffersize", 			required_argument,  		0, 'b'},
		{"help",            	no_argument,      			0, 'h'},
		{0,0,0,0}
};

bool running = false;
RobinNPFragSpy *robinDirect;

void usage(){

	std::cout << "Valid options are ... " << std::endl;
	std::cout << std::endl;
	std::cout <<  "*****General Options*****" << std::endl;
	std::cout << "-h --help                                 Print this message and exit" << std::endl;
	std::cout << std::endl;
	std::cout <<  "*****Configuration Options*****" << std::endl;
	std::cout << "-s number, --slot=number                  Specify RobinNP to test by slot. Default: 0" << std::endl;
	std::cout << "-c number, --chanmask=number or 0xnumber  Specify binary channel mask (as decimal or hexadecimal) for number of channels to test." << std::endl;
	std::cout << "-b number, --buffersize=number            Specify number of fragments to hold in buffer for dumping in case of error. Default: 100" << std::endl;
	std::cout << "-p number, --pagesize=number              Specify RobinNP memory page size. Default: 4096" << std::endl;
	std::cout << "-g --generatefrags                        Enable RobinNP internal fragment generation. Default: false (expect s-link input)" << std::endl;

}

void intHandler(int) {
	running = false;
	robinDirect->abort();
}

unsigned int getNumChannelsFromMask(unsigned int channelMask){

	unsigned int numChannels = 0;

	for(unsigned int chan = 0; chan < s_maxRols; ++chan){
		if((1 << chan) & channelMask)
		{
			numChannels++;
		}
	}
	return numChannels;
}

unsigned int startGathering(unsigned int channelMask){
	unsigned int totalCounter = 0;
	unsigned int numChannels = getNumChannelsFromMask(channelMask);

	//Control variables
	running = true;
	signal(SIGINT, intHandler);

	//Rate statistics containers
	auto tsStopLast=std::chrono::system_clock::now();
	unsigned int received = 0;
	std::cout << "Starting Gathering" << std::endl;
	while(running){

		ROIBOutputElement receivedFragment;

		if(channelMask & 0x3F){
			//Request next fragment from the RobinNP fragment queue
			receivedFragment = robinDirect->getFragment(0);

			if(receivedFragment.page != 0){
				//Process fragment
				robinDirect->storeFragment(receivedFragment);


				//Estimate fragment rate per ROL
				received++;
				totalCounter++;
			}
			else if(!running){
				break;
			}

		}
		if(channelMask>>6){

			receivedFragment = robinDirect->getFragment(1);


			if(receivedFragment.page != 0){
				//std::cout << "got a fragment" << std::endl;
				//Process fragment
				robinDirect->storeFragment(receivedFragment);

				//Estimate fragment rate per ROL
				received++;
				totalCounter++;
			}
			else if(!running){
				break;
			}

		}

		if(received == 1000000){
			auto tsStop=std::chrono::system_clock::now();
			auto elapsed=tsStop - tsStopLast;
			std::cout << "Rate " <<  received / ((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) / (numChannels * 1000) << " kHz per ROL " << std::endl;
			received = 0;
			tsStopLast = tsStop;
		}

	}
	return totalCounter;

}

int main(int argc, char **argv){

	//Initialise control parameters with default values
	unsigned int slot = 0;
	bool generateFrags = false;
	unsigned int channelMask = 1;
	unsigned int size = 4096;
	unsigned int bufferSize = 100;


	//Parse Command Line Input
	int index;
	int iarg=0;
	std::size_t found = 0;
	std::string candidate;
	std::string candidateSubStr;

	//turn off getopt error message
	opterr=1;

	while(iarg != -1)
	{
		iarg = getopt_long(argc, argv, "s:ghp:c:b:", longopts, &index);

		switch (iarg)
		{

		case 's':
			slot = atoi(optarg);
			break;

		case 'g':
			generateFrags = true;
			break;

		case 'b':
			bufferSize = (atoi(optarg));
			break;

		case 'c':
			candidate.assign(optarg);
			if(candidate.find_first_not_of("0123456789ABCDEFabcdefx") != std::string::npos){
				std::cout << "ERROR: Incorrectly formatted channel map, input must be of either integer form or hexadecmal formatted 0x<value>" << std::endl;
				exit(1);
			}
			else{
				found = candidate.find("0x");
				//handle as hexadecimal if formatted 0x...
				if(found != std::string::npos){
					candidateSubStr = candidate.substr(found + 2);
					channelMask = std::stoul(candidateSubStr,0,16);
				}
				else{
					channelMask = (atoi(optarg));
				}
			}

			break;

		case 'p':
			size = (atoi(optarg));
			break;

		case 'h':
			usage();
			exit(0);

		case '?':
			usage();
			exit(0);

		}
	}

	std::cout << "*********************************************"<< std::endl;
	std::cout << "* RobinFragSpy run settings:                  "<< std::endl;
	std::cout << "*                                            "<< std::endl;
	std::cout << "* slot = " << slot << std::endl;
	std::cout << "* buffer size = " << bufferSize << std::endl;
	std::cout << "* channel mask = " << std::hex << channelMask << std::dec << std::endl;
	std::cout << "* Fragment generation = " << (generateFrags ? "true" : "false") << std::endl;
	std::cout << "* page size = " << size << " words" << std::endl;
	std::cout << "*********************************************"<< std::endl;

	try{

		robinDirect = new RobinNPFragSpy(slot,channelMask,generateFrags,0,size,bufferSize); //RobinNP in slot 0, using all first two channels

		//Start RobinNP threads (and data generator if requested)
		robinDirect->start();

		//Enter main data gathering loop
		unsigned int gathered = startGathering(channelMask);

		//Stop RobinNP threads and reset DMA page queue
		robinDirect->stop();

		delete robinDirect;
		std::cout << "Received " << gathered << " events across all channels" << std::endl;
	}
	catch(ers::Issue& ex){
		std::cout << std::endl;
		std::cout << "RobinNPFragSpy caught exception of type " << ex.get_class_name() << ": " << std::endl;
		std::cout << ex.message() << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
	catch(std::exception &ex){
		std::cout << std::endl;
		std::cout << "RobinNPFragSpy caught std::exception: " << std::endl;
		std::cout << ex.what() << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
	catch(...){
		std::cout << std::endl;
		std::cout << "RobinNPFragSpy caught unhandled exception" << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
}

/*
 *
 * robinnpbist - self test module for RobinNP
 *
 * author: William Panduro Vazquez (j.panduro.vazquez@cern.ch) - 2012
 *
 */

//C++ headers
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

//TDAQ headers
#include "DFDebug/DFDebug.h"

//RobinNP headers
#include "ROSRobinNP/RobinNPBIST.h"
#include "ROSRobinNP/ROSRobinNPExceptions.h"

#include <getopt.h>

using namespace ROS;

#define no_argument 0
#define required_argument 1
#define optional_argument 2

void usage()
{
	std::cout << "Valid options are ..." << std::endl;
	std::cout << std::endl;
	std::cout << "*****General Options*****" << std::endl;
	std::cout << "-h  --help    Print this message and exit" << std::endl;
	std::cout << "-V  --verbose More verbose output" << std::endl;
	std::cout << std::endl;
	std::cout << "*****Configuration Parameters*****" << std::endl;
	std::cout << "-s number, --slot=number                  Specify RobinNP to test by slot.  Default: 0." << std::endl;
	std::cout << "-C number, --channels=number or 0xnumber  Specify binary channel mask (as decimal or hexadecimal) for number of channels to test." << std::endl;
	std::cout << "                                          Example: for channels 0, 3 and 5 specify 101001 = 41 (decimal) or 0x29 (hexademinal)." << std::endl;
	std::cout << "                                          Default: Firmware Specific - normally 4095." << std::endl;
	std::cout << "-S number, --subrob=number                Specify which subRobs to test (0 for 0 only, 1 for 1 only, leave blank for both)." << std::endl;
	std::cout << "                                          Default: All available." << std::endl;
	std::cout << std::endl;
	std::cout << "*****Available Tests:" << std::endl;
	std::cout << "-b,        --design              Basic test on the firmware version report                                            Default: false." << std::endl;
	std::cout << "-g,        --register            Detailed check on control and status registers.                                      Default: false." << std::endl;
	std::cout << "-d,        --dma                 Detailed single DMA test for specified subRob and produce output for each step       Default: false." << std::endl;
	std::cout << "-D,        --dmaspeed            DMA speed test with 1M DMAs for specified subRob                                     Default: false." << std::endl;
	std::cout << "-T,        --dualdmaspeed        Dual DMA speed test with 1M DMAs in parallel across both DMA engines for a 2 subRob  Default: false." << std::endl;
	std::cout << "-p,        --pageflow            Detailed page flow test for specified ROLs                                           Default: false." << std::endl;
	std::cout << "-P,        --pageflowspeed       Page flow speed test with 1M pages for specified ROLs                                Default: false." << std::endl;
	std::cout << "-m,        --pageflowmem         Page flow test using real memory for specified ROLs                                  Default: false." << std::endl;
	std::cout << "-I,        --interrupt           Interrupt testing                                                                    Default: false." << std::endl;
	std::cout << "-L,        --slink               S-link testing (no resets by default)                                                Default: false." << std::endl;
	std::cout << "-U,        --ureset              Add a UReset to --slink tests                                                        Default: false." << std::endl;
	std::cout << "-H,        --hola                Add a HOLA Core reset to --slink tests                                               Default: false." << std::endl;
	std::cout << "-G,        --gtx                 Add a GTX reset to --slink tests                                                     Default: false." << std::endl;
	std::cout << "-F,        --full                Add a full (HOLA Core + GTX + UReset) reset to --slink tests                         Default: false." << std::endl;
	std::cout << "-f number, --osc_newfreq=number  Specify I2C oscillator frequency for s-link testing and run slink test (--slink)" << std::endl;
	std::cout << "-M number, --memory=number       Buffer memory read/write testing. Write an amount of data specified by 'number' " << std::endl;
	std::cout << "                                 (in GB) to the memory and read back to verify integrity." << std::endl;
	std::cout << std::endl;
	std::cout << "*****Memory config syntax (experts only!)." << std::endl;
	std::cout << "-o number, --memcfgwritetimeout=number     Specify memory arbiter write timeout                Default: 128" << std::endl;
	std::cout << "-u number, --memcfgreadtimeout=number     Specify memory arbiter read timeout                Default: 128" << std::endl;
	std::cout << "-c number, --memcfgfifomax=number     Specify max memory input fifo size            Default: 90" << std::endl;
	std::cout << std::endl;
	std::cout << "*****Memory expert test syntax (experts only!)." << std::endl;
	std::cout << "-t,        --memexpert                Use defaults for all of the parameters below" << std::endl;
	std::cout << "-w number, --mem_numwrites=number     Specify how many write loop iterations        Default: 4" << std::endl;
	std::cout << "-W,        --mem_holdwrites           Hold memory writes and send as a burst        Default: false" << std::endl;
	std::cout << "-r number, --mem_numreads=number      Specify how many read loop iterations         Default: 1" << std::endl;
	std::cout << "-y number, --mem_wordsperwrite=number Specify how many words to write per frame     Default: 6" << std::endl;
	std::cout << "-R number, --mem_readlength=number    Specify the length of the read                Default: 10" << std::endl;
	std::cout << "-x,        --mem_reset                Include a reset of memory test                Default: false" << std::endl;
	std::cout << "-i,        --mem_initialise           Initialise Memory with dummy data             Default: false" << std::endl;
	std::cout << "-A number, --mem_startaddress         Start Address for Writes to Memory            Default: 0" << std::endl;
	std::cout << "-a number, --mem_addrstep=number      Specify the address step size for the write   Default: 1" << std::endl;
}

int main(int argc, char **argv){
 
	int slot = 0;
	bool doDesignTest = false;
	bool doDMATest = false;
	bool doDMASpeedTest = false;
	bool doDualDMASpeedTest = false;
	bool doPageFlowTest = false;
	bool doPageFlowSpeedTest = false;
	bool doMemoryTest = false;
	bool doInterruptTests = false;
	bool doPageFlowMemTest = false;
	bool doSLinkTest = false;
	bool doMemExpertTest = false;
	bool doRegisterTest = false;
	float slinkNewFreq = 100;
	bool slinkTestOptions = false;
	unsigned int channelsToTest = 0xFFF;
	unsigned int subRobsToTest = 2;
	bool memTestOptions = false;
	int	memNumWrites = 4;
	bool memHoldWrite = false;
	int memNumReads = 1;
	//bool memBurstMode = false;
	int memWordsPerWrite = 6;
	int memReadLength = 10;
	bool memReset = false;
	int memAddrStep = 0;
	bool memInitialise = false;
	int memStartAddress = 0;
	unsigned int memWriteTimeout = 128;
	unsigned int memReadTimeout = 128;
	unsigned int memFifoFill = 90;
	double memwritetestsize = 1;
	bool doVerbosePrints = false;
	bool doHOLAReset = false;
	bool doGTXReset = false;
	bool doFullLinkReset = false;
	bool doUReset = false;
	bool doLinkFreqChange = false;
	bool doMemoryConfigChange = false;
	bool doCaptureDMATest = false;
	bool DMATestOptions = false;

	bool defaultChans = true;
	bool defaultSubRobs = true;

	const struct option longopts[] =
	{
			{"slot",          		required_argument,  0, 's'},
			{"design",     			no_argument,  		0, 'b'},
			{"dma",        			no_argument,  		0, 'd'},
			{"dmaspeed",        	no_argument,  		0, 'D'},
			{"dualdmaspeed",       	no_argument,  		0, 'T'},
			{"pageflow",  			no_argument,        0, 'p'},
			{"pageflowspeed",  		no_argument,        0, 'P'},
			{"register",	  		no_argument,        0, 'g'},
			{"memory",	  			required_argument,  0, 'M'},
			{"memcfgwritetimeout",	required_argument,  0, 'o'},
			{"memcfgreadtimeout",	required_argument,  0, 'u'},
			{"memcfgfifomax",		required_argument,  0, 'c'},
			{"memexpert",  			no_argument,	    0, 't'},
			{"pageflowmem",	  		no_argument,        0, 'm'},
			{"interrupts",	  		no_argument,        0, 'I'},
			{"slink",	  		    no_argument,        0, 'L'},
			{"hola",	  		    no_argument,        0, 'H'},
			{"ureset",	  		    no_argument,        0, 'U'},
			{"full",	  		    no_argument,        0, 'F'},
			{"gtx",		  		    no_argument,        0, 'G'},
			{"capture",	        	no_argument,  		0, 'N'},
			{"osc_newfreq",	  		required_argument,  0, 'f'},
			{"subrob",  			required_argument,  0, 'S'},
			{"channels",  			required_argument,  0, 'C'},
			{"verbose",  			no_argument,	    0, 'V'},
			{"mem_numwrites",  		required_argument,  0, 'w'},
			{"mem_holdwrites",		required_argument,  0, 'W'},
			{"mem_numreads",  		required_argument,  0, 'r'},
			{"mem_wordsperwrite",	required_argument,  0, 'y'},
			{"mem_readlength",		required_argument,  0, 'R'},
			{"mem_reset",			no_argument,        0, 'x'},
			{"mem_addrstep",		required_argument,  0, 'a'},
			{"mem_initialise",		required_argument,  0, 'i'},
			{"mem_startaddress",	required_argument,  0, 'A'},
			{"help",            	no_argument,        0, 'h'},
			{0,0,0,0},
	};

	int index;
	int iarg=0;
	std::size_t found = 0;
	std::string candidate;
	std::string candidateSubStr;

	//turn off getopt error message
	opterr=1;

	while(iarg != -1)
	{
		//iarg = getopt_long(argc, argv, "s::dDpPMhC::S::w::W::r::b::y::R::xa::TIiA::", longopts, &index);
		iarg = getopt_long(argc, argv, "s:dDpPM:m:hC:S:w:W:r:y:R:xa:TLHbUFGIiA:f:VtgNo:c:", longopts, &index);

		switch (iarg)
		{

		case 's':
			slot = atoi(optarg);
			break;

		case 'b':
			doDesignTest = true;
			break;

		case 'g':
			doRegisterTest = true;
			break;

		case 'd':
			doDMATest = true;
			break;

		case 'D':
			doDMASpeedTest = true;
			break;

		case 'p':
			doPageFlowTest = true;
			break;

		case 'P':
			doPageFlowSpeedTest = true;
			break;

		case 'm':
			doPageFlowMemTest = true;
			break;

		case 'M':
			memwritetestsize = atof(optarg);
			if(memwritetestsize == 0){
				usage();
				exit(0);
			}
			doMemoryTest = true;
			break;

		case 'T':
			doDualDMASpeedTest = true;
			break;

		case 'I':
			doInterruptTests = true;
			break;

		case 'L':
			doSLinkTest = true;
			break;

		case 'H':
			doHOLAReset = true;
			slinkTestOptions = true;
			break;

		case 'G':
			doGTXReset = true;
			slinkTestOptions = true;
			break;

		case 'F':
			doFullLinkReset = true;
			slinkTestOptions = true;
			break;

		case 'U':
			doUReset = true;
			slinkTestOptions = true;
			break;

		case 'f':
			doLinkFreqChange = true;
			slinkNewFreq = atof(optarg);
			if(slinkNewFreq == 0){
				usage();
				exit(0);
			}
			slinkTestOptions = true;
			break;

		case 'C':
			candidate.assign(optarg);
			if(candidate.find_first_not_of("0123456789ABCDEFabcdefx") != std::string::npos){
				std::cout << "ERROR: Incorrectly formatted channel map, input must be of either integer form or hexadecmal formatted 0x<value>" << std::endl;
				exit(1);
			}
			else{
				found = candidate.find("0x");
				//handle as hexadecimal if formatted 0x...
				if(found != std::string::npos){
					candidateSubStr = candidate.substr(found + 2);
					channelsToTest = std::stoul(candidateSubStr,0,16);
				}
				else{
					channelsToTest = atoi(optarg);
				}
			}
			defaultChans = false;

			break;

		case 'S':
			subRobsToTest = atoi(optarg);
			defaultSubRobs = false;
			break;

		case 'w':
			memNumWrites = atoi(optarg);
			memTestOptions = true;
			break;

		case 'W':
			memHoldWrite = true;
			memTestOptions = true;
			break;

		case 'r':
			memNumReads = atoi(optarg);
			memTestOptions = true;
			break;

			/*case 'b':
			memBurstMode = true;
			memTestOptions = true;
			break;*/

		case 'y':
			memWordsPerWrite = atoi(optarg);
			memTestOptions = true;
			break;

		case 'R':
			memReadLength = atoi(optarg);
			memTestOptions = true;
			break;

		case 'x':
			memReset = true;
			memTestOptions = true;
			break;

		case 'a':
			memAddrStep = atoi(optarg);
			memTestOptions = true;
			break;

		case 'i':
			memInitialise = true;
			memTestOptions = true;
			break;

		case 'A':
			memStartAddress = atoi(optarg);
			memTestOptions = true;
			break;

		case 'V':
			doVerbosePrints = true;
			break;

		case 't':
			doMemExpertTest = true;
			break;

		case 'o':
			memWriteTimeout = atoi(optarg);
			doMemoryConfigChange = true;
			break;

		case 'u':
			memReadTimeout = atoi(optarg);
			doMemoryConfigChange = true;
			break;


		case 'c':
			memFifoFill = atoi(optarg);
			doMemoryConfigChange = true;
			break;

		case 'N':
			doCaptureDMATest = true;
			DMATestOptions = true;
			break;

		case 'h':
			usage();
			exit(0);

		case '?':
			usage();
			exit(0);

		}
	}

	DF::GlobalDebugSettings::setup((unsigned int)10,DFDB_ROSROBINNP);


	if(slot < 0){
		std::cout << "ERROR - Specified PCI slot must be >= 0" << std::endl;
		exit(1);
	}

	std::cout << "RobinNPBIST: Running BIST Tests on RobinNP in slot " << slot << std::endl;
	try{

		RobinNPBIST BISTControl((unsigned int)slot,doVerbosePrints);


		unsigned int numRols = BISTControl.getNumRols();
		unsigned int numSubRobs = BISTControl.getNumSubRobs();
		unsigned int rolMask = (1 << numRols) - 1;
		if(doVerbosePrints){
			std::cout << "Setting ROL Mask to " << std::hex << rolMask << std::dec << std::endl;
		}

		bool startupError = false;


		if(defaultChans){
			channelsToTest = rolMask;
			if(doVerbosePrints){
				std::cout << "Setting Number of Channels to Test to FW Default: 0x" << std::hex << channelsToTest << std::dec << std::endl;
			}
		}
		else if((channelsToTest >> numRols) != 0){
			std::cout << "ERROR - invalid number of ROLs to be tested, max allowed " << numRols << std::endl;
			startupError = true;
		}


		if(!startupError && defaultSubRobs){
			subRobsToTest = numSubRobs;
			if(doVerbosePrints){
				std::cout << "Setting Number of SubRobs to Test to FW Default: " << subRobsToTest << std::endl;
			}
		}
		else if(!startupError && (subRobsToTest + 1 > numSubRobs)){
			std::cout << "ERROR - have requested a test of subRob " << subRobsToTest << " (counting from zero), but the RobinNP only supports a maximum SubRob ID of " << numSubRobs - 1 << std::endl;
			startupError = true;
		}

		if(!startupError && channelsToTest == 0){
			std::cout << "ERROR - number of channels to test must be non-zero" << std::endl;
			startupError = true;
		}


		if(startupError){
			std::cout << "Exiting Due to Error(s)" << std::endl;
			exit(1);
		}

		if(doDualDMASpeedTest && numSubRobs < 2){
			std::cout << "WARNING - have requested a Dual DMA Speed Test on a 1 subRob RobinNP - ignored" << std::endl;
			doDualDMASpeedTest = false;
		}

		if(slinkTestOptions && !doSLinkTest){
			std::cout << "WARNING - have specified an s-link test option but not requested s-link test - ignored"<< std::endl;
		}

		if(DMATestOptions && !(doDMATest)){
			std::cout << "WARNING - have specified a DMA test option but not requested DMA test - ignored"<< std::endl;
		}

		if(doVerbosePrints){



			std::cout << "*********************************************"<< std::endl;
			std::cout << "* RobinNPBIST run settings:                  "<< std::endl;
			std::cout << "*                                            "<< std::endl;
			std::cout << "* Running in VERBOSE mode                    "<< std::endl;


			std::cout << "* slot = " << slot << std::endl;
			std::cout << "* register test = " << doRegisterTest << std::endl;
			std::cout << "* dma test = " << doDMATest << std::endl;
			std::cout << "* dma speed test = " << doDMASpeedTest << std::endl;
			std::cout << "* pageflow test = " << doPageFlowTest << std::endl;
			std::cout << "* pageflow speed test = " << doPageFlowSpeedTest <<  std::endl;
			std::cout << "* pageflow memory test = " << doPageFlowMemTest <<  std::endl;
			std::cout << "* interrupt tests = " << doInterruptTests <<  std::endl;
			std::cout << "* s-link test = " << doSLinkTest <<  std::endl;

			if(slinkTestOptions){
				std::cout << "* New I2C oscillator frequency = " << slinkNewFreq <<  std::endl;
				std::cout << "* Full Reset = " << doFullLinkReset <<  std::endl;
				std::cout << "* UReset = " << doUReset <<  std::endl;
				std::cout << "* HOLA Reset = " << doHOLAReset <<  std::endl;
				std::cout << "* GTX Reset = " << doGTXReset <<  std::endl;
			}
			if(doMemoryConfigChange){
				std::cout << "*Memory Config Change:" << std::endl;
				std::cout << "** Arbiter Write Timeout " << memWriteTimeout << std::endl;
				std::cout << "** Arbiter Read Timeout " << memReadTimeout << std::endl;
				std::cout << "** Input Fifo Max Fill " << memFifoFill << std::endl;
			}

			std::cout << "* memory rw test = " << doMemoryTest << ", size = " << memwritetestsize << " GB" << std::endl;

			if(doMemExpertTest){
				std::cout << "** Memory Expert Test Options:" << std::endl;
				std::cout << "**    Num Writes: " << memNumWrites                        << std::endl;
				std::cout << "**    Hold Writes: " <<   memHoldWrite                     << std::endl;
				std::cout << "**    Num Reads: " << memNumReads                                    << std::endl;
				std::cout << "**    Words Per Frame: " << memWordsPerWrite                                   << std::endl;
				std::cout << "**    Read Length: " << memReadLength                                   << std::endl;
				std::cout << "**    Reset: " << memReset                                   << std::endl;
				std::cout << "**    Write Address Step Size: " << memAddrStep                                   << std::endl;
				std::cout << "**    Write Start Address Requested: " << memStartAddress << " - x4 for true location" << std::endl;
				std::cout << "**    Initialise Memory with dummy data: " << memInitialise << std::endl;
			}

			std::cout << "* channel mask to test = " << std::hex << channelsToTest << std::dec <<  std::endl;

			if(defaultSubRobs){
				std::cout << "* testing all subrobs" <<  std::endl;
			}
			else{
				std::cout << "* subrob to test = " << subRobsToTest <<  std::endl;
			}

			std::cout << "*                                           "<< std::endl;
			std::cout << "*********************************************"<< std::endl;

		}


		if(doMemoryConfigChange){
			std::cout << "RobinNPBIST: Change from Default Memory Configuration: Arbiter Write Timeout " << memWriteTimeout << " Read Timeout " << memReadTimeout << " Input Fifo Max Fill " << memFifoFill << std::endl;
		}

		bool overallresult = true;

		//Basic FW Design ID Tests
		if(doDesignTest){
			unsigned int designResult = BISTControl.designTest();
			if(designResult == 1){
				overallresult = false;
				std::cout << "RobinNPBIST: Running Register Test to provide further information on mappings. Will run no further tests beyond this." << std::endl;
			}
			else if(designResult == 2){
				std::cout << "RobinNPBIST: Design Test Completed with Warnings, continuing with tests." << std::endl;
			}
			else if(designResult == 3){
				std::cout << "RobinNPBIST: Design Test Completed with Warnings and Errors, continuing with Register Test to provide further information on mappings. Will run no further tests beyond this." << std::endl;
				overallresult = false;
			}
			std::cout << std::endl;
		}
		overallresult &= BISTControl.printSystemMonitorInfo();

		BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
		//Verify RobinNP Register Map
		if(doRegisterTest){
			overallresult &= BISTControl.registerTest();
		}
		if(!overallresult){
			std::cout << "RobinNPBIST: RobinNP Fails One or More Basic Tests. Further testing will produce unreliable results." << std::endl;
		}

		//Test MSI-X interrupt functionality in single and multi-threaded scenarios
		if(doInterruptTests){

			for(unsigned int interrupt = 0; interrupt < 8; ++interrupt){
				overallresult &= BISTControl.interrupterTest(interrupt);
			}
			overallresult &= BISTControl.interrupterMultiTest(8);

		}

		//Detailed page flow test to detect dataflow blockages
		if(doPageFlowTest){
			for(unsigned int rol = 0; rol < numRols; ++rol){
				if((1 << rol)  & channelsToTest){
					BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
					BISTControl.resetFifoDuplicators();
					overallresult &= BISTControl.pageflowTest(rol);
				}
			}
		}

		if(doPageFlowMemTest){
			for(unsigned int rol = 0; rol < numRols; ++rol){
				if((1 << rol)  & channelsToTest){
					BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
					BISTControl.resetFifoDuplicators();
					overallresult &= BISTControl.pageflowMemTest(rol);
				}
			}
		}

		unsigned int startSubRob;
		unsigned int endSubRob;
		if(defaultSubRobs){
			startSubRob = 0;
			endSubRob = subRobsToTest;
		}
		else{
			startSubRob = subRobsToTest;
			endSubRob = subRobsToTest + 1;
		}


		//Detailed DMA test to detect dataflow blockages
		if(doDMATest){
			for(unsigned int subRob = startSubRob; subRob < endSubRob; ++subRob){
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				BISTControl.resetFifoDuplicators();
				overallresult &= BISTControl.primaryDMATest(subRob,doCaptureDMATest);
			}
		}

		//S-Link Tests
		if(doSLinkTest){
			if((BISTControl.getHardwareVersion() == 1)&& doLinkFreqChange){
				if(slinkNewFreq < 1.0){
					std::cout << "ERROR: Invalid frequency setting (must be greater than 1 Mhz). Requested New Freq: " << slinkNewFreq << std::endl;
					exit(1);
				}

				std::cout << "BIST sLinkTest detected C-RORC hardware, setting up I2C oscillator to " << slinkNewFreq << " MHz " <<  std::endl;
				BISTControl.configureI2CClock(slinkNewFreq);
			}
			else if(doLinkFreqChange){
				std::cout << "WARNING: Attempt to change frequency on PLDA hardware, ignoring " << std::endl;
			}
			for(unsigned int rol = 0; rol < numRols; ++rol){
				if((1 << rol)  & channelsToTest){
					if(doFullLinkReset){
						BISTControl.sLinkTest(rol,true,true,true);
					}
					else{
						BISTControl.sLinkTest(rol,doGTXReset,doHOLAReset,doUReset);
					}
				}
			}
		}

		//Memory Expert test infrastructure - for use by memory developers only
		if(doMemExpertTest){
			for(unsigned int subRob = startSubRob; subRob < endSubRob; ++subRob){
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				overallresult &= BISTControl.memoryExpertTest(subRob,memNumReads,memReadLength,memNumWrites,memHoldWrite,/*memBurstMode,*/memReset,memWordsPerWrite,memAddrStep,memInitialise,memStartAddress);
			}
		}
		else{
			if(memTestOptions){
				std::cout << "WARNING: Memory expert test options specified but test itself not requested, ignoring." << std::endl;
			}
		}

		//Memory R/W test
		if(doMemoryTest){
			for(unsigned int subRob = startSubRob; subRob < endSubRob; ++subRob){
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				overallresult &= BISTControl.memoryReadWriteTest(subRob,memwritetestsize);
			}
		}

		//High rate pageflow benchmarking
		if(doPageFlowSpeedTest){
			for(unsigned int rol = 0; rol < numRols; ++rol){
				if((1 << rol)  & channelsToTest){
					BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
					BISTControl.resetFifoDuplicators();
					overallresult &= BISTControl.pageflowSpeedTest(rol,25,true);
					overallresult &= BISTControl.pageflowSpeedTest(rol,500,true);
					overallresult &= BISTControl.pageflowSpeedTest(rol,950,true);
				}
			}
		}

		//High rate DMA benchmarking
		if(doDMASpeedTest){
			std::cout << "Tests Will Run in FIFO Mode" << std::endl;
			for(unsigned int subRob = startSubRob; subRob < endSubRob; ++subRob){
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				BISTControl.resetFifoDuplicators();
				overallresult &= BISTControl.primaryDMASpeedTest(subRob,25);
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				BISTControl.resetFifoDuplicators();
				overallresult &= BISTControl.primaryDMASpeedTest(subRob,500);
				BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
				BISTControl.resetFifoDuplicators();
				overallresult &= BISTControl.primaryDMASpeedTest(subRob,950);

			}
		}

		//High rate DMA benchmarking for Dual Engines (2 subRob RobinNP)
		if(doDualDMASpeedTest){
			BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
			BISTControl.resetFifoDuplicators();
			overallresult &= BISTControl.dualDMASpeedTest(25);
			BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
			BISTControl.resetFifoDuplicators();
			overallresult &= BISTControl.dualDMASpeedTest(500);
			BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
			BISTControl.resetFifoDuplicators();
			overallresult &= BISTControl.dualDMASpeedTest(950);

		}

		if(overallresult){
			std::cout << "RobinNPBIST: RobinNP Passes BIST" << std::endl;
		}
		else{
			std::cout << "RobinNPBIST: RobinNP Fails BIST" << std::endl;
		}

		std::cout << "RobinNPBIST: RobinNP Testing Complete" << std::endl;
		BISTControl.reset(memWriteTimeout,memReadTimeout,memFifoFill);
	}
	catch(ers::Issue& ex){
		std::cout << std::endl;
		std::cout << "RobinNPBIST caught exception of type " << ex.get_class_name() << ": " << std::endl;
		std::cout << ex.message() << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
	catch(std::exception &ex){
		std::cout << std::endl;
		std::cout << "RobinNPBIST caught std::exception: " << std::endl;
		std::cout << ex.what() << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
	catch(std::string ex){
		std::cout << std::endl;
		std::cout << "RobinNPBIST caught exception of type: " << ex.c_str() << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
	catch(...){
		std::cout << std::endl;
		std::cout << "RobinNPBIST caught unhandled exception" << std::endl;
		std::cout << "Aborting" << std::endl;
		exit(EXIT_FAILURE);
	}
}

//Test Program For RobinNP Indexless ROIB functionality
#include <iostream>
#include <signal.h>

#include "ROSDescriptorNP/DataPage.h"
#include "ROSRobinNP/RobinNPROIB.h"


using namespace ROS;

bool running;
unsigned int channelMask = 0xFFF; //bitwise mask e.g. for 12 channels -> 111111111111

RobinNPROIB robinDirect(0,channelMask,true,0,256,400); //RobinNP in slot 0, using all 12 channels with data generation



void testFragment(ROIBOutputElement &fragment){
	//Example data processing function
	if(fragment.fragmentStatus != 0){
		std::cout << "Fragment status " << std::hex << fragment.fragmentStatus << std::dec << std::endl;
	}

}

void intHandler(int) {
	running = false;
	robinDirect.abort();
}

int main(){

	unsigned int numChannels = 0;

	for(unsigned int chan = 0; chan < s_maxRols; ++chan){
		if((1 << chan) & channelMask)
		{
			numChannels++;
		}
	}

	std::cout << "number of channels " << numChannels << std::endl;

	//Start RobinNP threads (and data generator if requested)
	robinDirect.start();

	//Control variables
	running = true;
	signal(SIGINT, intHandler);

	//Rate statistics containers
	auto tsStopLast=std::chrono::system_clock::now();
	unsigned int received = 0;

	while(running){

		//Request next fragment from the RobinNP fragment queue
		ROIBOutputElement receivedFragment;

		if(channelMask & 0x3F){
			receivedFragment = robinDirect.getFragment(0);

			if(receivedFragment.page != 0){
				//Process fragment
				testFragment(receivedFragment);

				//Recycle fragment DMA landing area to allow new fragments to take its place, ensure all data has been copied/processed
				robinDirect.recyclePage(receivedFragment);

				//Estimate fragment rate per ROL
				received++;
			}
			else if(!running){
				break;
			}
		}
		if((channelMask>>6) != 0){
			receivedFragment = robinDirect.getFragment(1);


			if(receivedFragment.page != 0){
				//Process fragment
				testFragment(receivedFragment);

				//Recycle fragment DMA landing area to allow new fragments to take its place, ensure all data has been copied/processed
				robinDirect.recyclePage(receivedFragment);

				//Estimate fragment rate per ROL
				received++;
			}
			else if(!running){
				break;
			}

		}

		if(received == 1000000){
		  /*for(unsigned int rolId = 0; rolId < s_maxRols; ++rolId){
				if((1 << rolId) & channelMask){
					RobinNPROLStats *rolStats = robinDirect.getRolStatistics(rolId);
					std::cout << "Most Recent ID for ROL " << rolId << " = " << std::hex << rolStats->m_mostRecentId << std::dec << std::endl;
				}
				}*/
			RobinNPROLStats *rolStats = robinDirect.getRolStatistics(0);
			std::cout << "Most Recent ID = " << std::hex << rolStats->m_mostRecentId << std::dec << std::endl;
			auto tsStop=std::chrono::system_clock::now();
			auto elapsed=tsStop - tsStopLast;
			std::cout << "Rate " <<  received / ((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6) / (numChannels * 1000) << " kHz per ROL " << std::endl;
			received = 0;
			tsStopLast = tsStop;
		}

	}

	//Stop RobinNP threads and reset DMA page queue
	robinDirect.stop();
}

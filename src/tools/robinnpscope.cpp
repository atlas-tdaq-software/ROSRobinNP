/************************************************************************/
/*									*/
/* File: robinnpscope.c							*/
/*									*/
/* This is a test program for the RobinNP				*/
/*									*/
/*  3. Sep. 12  MAJO  created						*/
/*									*/
/**************** C 2012 - A nickel program worth a dime ****************/

#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>  
#include <sys/mman.h>
#include <sys/stat.h>        
#include <sys/ioctl.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "io_rcc/io_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobinNP/RobinNP.h"
#include "ROSRobinNP/RobinNPRegisters.h"
#include "ROSRobinNP/RobinNPRegistersCommon.h"
#include "ROSRobinNP/RobinNPMonitoring.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "ROSRobinNP/RobinNPConfig.h"
#include "ROSRobinNP/RobinNPStats.h"

using namespace ROS;

// Types
typedef struct L1idRange
{
	u_int first;
	u_int last;
} L1idRange;

//Prototypes
int mainhelp(void);
int reg_dump(void);
int setdebug(void);
void scbench(void);
void usage(void);
void dump_ram(void);
void pci_peek(void);
void dump_config(u_int rol);
void dump_status(u_int rol);
void dump_event_list(u_int rol);
void benchmark_cs(void);
void benchmark_irq(void);
void check_offsets(void);


//Globals
u_long bar0_virtaddr, bar1_virtaddr; 
u_int verbose = 0, smem_ok = 1, mode_benchmark = 0;
RobinNPMonitoring *monitoringBlock;


//Definitions
#define BUFFER_RAM_SIZE (1024 * 1024)   //Just the first MB for now


/**************/
void usage(void)
/**************/
{
	std::cout << "Valid options are ..." << std::endl;
	std::cout << "-m <robin>: Define the number of the Robin (0..N-1) Default = 0" << std::endl;
	std::cout << "-l <rol>:   Define the number of the ROL (0, 1, .. 11) Default = 0" << std::endl;
	std::cout << "-s:         Print the statistics block of the selected ROL" << std::endl;
	std::cout << "-c:         Print the configuration parameters of the selected ROL" << std::endl;
	std::cout << "-e:         Print the L1ID of all events that are currently in the buffer of the selected ROL" << std::endl;
	std::cout << "-b:         Run single cycle benchmark" << std::endl;
	std::cout << "            Remember to execute </etc/init.d/cpuspeed stop> before you run this test" << std::endl;
	std::cout << "-D:         Enable debugging" << std::endl;
	std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
	int fd, fun = 1, c, robin_nr = 0, mode_stat = 0, mode_cfg = 0, rol_nr = 0, mode_events = 0;
	unsigned int size, handle, idata;
	IO_ErrorCode_t ret;
	char smem_file[1000];

	while ((c = getopt(argc, argv, "bl:m:hsceD")) != -1)
		switch (c)
		{
		case 'm': robin_nr = atoi(optarg);                             break;
		case 'b': mode_benchmark = 1;                                  break;
		case 'l': rol_nr = atoi(optarg);                               break;
		case 'h': usage(); exit(0);                                    break;
		case 's': mode_stat = 1;                                       break;
		case 'c': mode_cfg = 1;                                        break;
		case 'e': mode_events = 1;                                     break;
		case 'D': {DF::GlobalDebugSettings::setup(20, DFDB_ROSROBINNP); verbose = 1;} break;

		default:
			std::cout << "Invalid option " << c << std::endl;
			std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
			usage();
			exit (-1);
		}

	printf("\n\n\nThis is RobinNPscope\n");

	sprintf(smem_file, "/sharedMemRobinNP_%d", robin_nr);
	if(verbose){
		printf("=========================================================================\n");
		printf("Reading data from file /dev/shm%s.\n", smem_file);
	}

	//open shared memory area
	fd = shm_open(smem_file, O_RDONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (fd == -1)
	{
		perror("Error is ");
		printf("Failed to open the shared memory area. Check if the RobinNPs are managed by an active process\n");
		smem_ok = 0;
	}
	else{

		size = lseek(fd, 0, SEEK_END);
		lseek(fd, 0, SEEK_SET);

		if(size == (unsigned int)-1){
			perror("Error is ");
			printf("Failed seek when mapping shared memory area.\n");
			smem_ok = 0;
		}
		else{

			//map into virtual memory and cast to a RobinNPMonitoring object
			monitoringBlock = (RobinNPMonitoring *) mmap(0, size, PROT_READ , MAP_SHARED, fd, 0);
			if (monitoringBlock == NULL)
			{
				perror("Error is ");
				printf("Failed to map the shared memory area.\n");
				smem_ok = 0;
			}
		}
	}
	if (smem_ok == 0)
	{
		printf("\nDue to the problem with the shared memory area it makes no sense to continue.\n");
		printf("Have a nice day.\n");
		exit(1);
	}

	ret = IO_Open();
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	ret = IO_PCIDeviceLink(0x10dc, 0x0187, robin_nr + 1, &handle);
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	//Read BAR0
	ret = IO_PCIConfigReadUInt(handle, 0x10, &idata);
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	if(verbose){
		printf("BAR0 is: 0x%08x\n", idata);
	}
	idata &= 0xfffff000;

	//Map the PCI MEM space at BAR0 into the local virtual address space
	ret = IO_PCIMemMap(idata, 1024 * 64, &bar0_virtaddr);
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	//Read BAR1
	ret = IO_PCIConfigReadUInt(handle, 0x14, &idata);
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	if(verbose){
		printf("BAR1 is: 0x%08x\n", idata);
	}
	idata &= 0xfffff000;

	//Map the PCI MEM space at BAR1 into the local virtual address space
	ret = IO_PCIMemMap(idata, BUFFER_RAM_SIZE, &bar1_virtaddr);
	if (ret != IO_RCC_SUCCESS)
	{
		rcc_error_print(stdout, ret);
		exit(-1);
	}

	if (mode_benchmark)
		scbench();

	if (mode_stat)
		dump_status(rol_nr);

	if (mode_cfg)
		dump_config(rol_nr);

	if (mode_events)
		dump_event_list(rol_nr);

	if (argc == 1)
	{
		while (fun != 0)
		{
			printf("\n");
			printf("Select an option:\n");
			printf("   1 Help                                                        \n");
			printf("=== Stand alone (io_rcc based) ==================================\n");
			printf("   2 Dump and decode registers       3 Dump buffer memory        \n");
			printf("   4 Set debug parameters            5 Peek & poke a PCI register\n");
			printf("   6 Reset a ROL                                                 \n");
			printf("=== test_robin based ============================================\n");
			printf("   8 Dump configuration parameters   9 Dump status parameters    \n");
			printf("  10 Dump list of events                                         \n");
			printf("=== driver based ================================================\n");
			printf("  12 Benchmark context switch       13 Benchmark IRQ             \n");
			printf("=== Without H/W= ================================================\n");
			printf("  14 Check offsets                                               \n");
			printf("=================================================================\n");
			printf("   0 Exit                                                        \n");
			printf("Your choice ");
			fun = getdecd(fun);
			if (fun == 1) mainhelp();
			if (fun == 2) reg_dump();
			if (fun == 3) dump_ram();
			if (fun == 4) setdebug();
			if (fun == 5) pci_peek();
			if (fun == 8) dump_config(99);
			if (fun == 9) dump_status(99);
			if (fun == 10) dump_event_list(99);
			if (fun == 12) benchmark_cs();
			if (fun == 13) benchmark_irq();
			if (fun == 14) check_offsets();
		}
	}

	ret = IO_PCIMemUnmap(bar1_virtaddr, BUFFER_RAM_SIZE);
	if (ret != IO_RCC_SUCCESS)
		rcc_error_print(stdout, ret);

	ret = IO_PCIMemUnmap(bar0_virtaddr, 1024 * 64);
	if (ret != IO_RCC_SUCCESS)
		rcc_error_print(stdout, ret);

	ret = IO_PCIDeviceUnlink(handle);
	if (ret != IO_RCC_SUCCESS)
		rcc_error_print(stdout, ret);

	ret = IO_Close();
	if (ret != IO_RCC_SUCCESS)
		rcc_error_print(stdout, ret);

	if (smem_ok)
		close(fd);

	return(0);
}

/**********************/
void benchmark_irq(void)
/**********************/
{
	u_int loop, ret;
	int iret, m_dev_handle;
	tstamp ts1, ts2;
	double delta, dmin = 99, dmax = 0, dmean = 0;
	BAR0CommonStruct *bar0_ptr;

	printf("Remember to execute </etc/init.d/cpuspeed stop> before you run this test         \n");

	ret = ts_open(0, TS_DUMMY);
	if (ret)
	{
		rcc_error_print(stdout, ret);
		return;
	}

	m_dev_handle = ::open("/dev/robinnp", O_RDWR, 0);
	if (m_dev_handle < 0)
	{
		printf("Error from call to ::open\n");
		return;
	}


	bar0_ptr = (BAR0CommonStruct *)bar0_virtaddr;

	for (loop = 0; loop < 1000; loop++)
	{
		ts_clock(&ts1);

		bar0_ptr->InterruptTestRegister.Trigger = 1;  //Trigger S/W interrupt

		//Wait for the interrupt
		int ret = ioctl(m_dev_handle, WAIT_IRQ);
		if (ret)
		{
			printf("Error from call to ioctl\n");
			return;
		}

		bar0_ptr->InterruptControlRegister.acknowledge = 0xFF;

		ts_clock(&ts2);
		delta = ts_duration(ts1, ts2);

		if (delta < dmin)
			dmin = delta;
		if (delta > dmax)
			dmax = delta;
		dmean += delta;
	}
	bar0_ptr->InterruptTestRegister.Trigger = 0;  //Trigger S/W interrupt
	bar0_ptr->InterruptControlRegister.acknowledge = 0xFF;

	printf("Minimum latency = %e seconds\n", dmin);
	printf("Maximum latency = %e seconds\n", dmax);
	printf("Average latency = %e seconds\n", dmean / 1000.0);

	iret = ::close(m_dev_handle);
	if (iret < 0)
	{
		printf("Error from call to ::close\n");
		return;
	}

	ret = ts_close(TS_DUMMY);
}

/*********************/
void benchmark_cs(void)
/*********************/
{
	int iret;
	u_int loop, ret;
	int testdata = 0, m_dev_handle;
	tstamp ts1, ts2;
	double delta, dmin = 99, dmax = 0, dmean = 0;

	printf("Remember to execute </etc/init.d/cpuspeed stop> before you run this test         \n");

	ret = ts_open(0, TS_DUMMY);
	if (ret)
	{
		rcc_error_print(stdout, ret);
		return;
	}

	m_dev_handle = ::open("/dev/robinnp", O_RDWR, 0);
	if (m_dev_handle < 0)
	{
		printf("Error from call to ::open\n");
		return;
	}

	for (loop = 0; loop < 10; loop++)
	{
		ts_clock(&ts1);
		ret = ioctl(m_dev_handle, CSTEST, &testdata);
		if (ret)
		{
			printf("Error from call to ioctl\n");
			iret = ::close(m_dev_handle);
			if (iret < 0)
			{
				printf("Error from call to ::close\n");
			}
			return;
		}
		ts_clock(&ts2);
		delta = ts_duration(ts1, ts2);
		printf("loop = %d, testdata = %d, Context Switch latency = %e seconds\n", loop, testdata, delta);
	}


	for (loop = 0; loop < 1000; loop++)
	{
		ts_clock(&ts1);
		ret = ioctl(m_dev_handle, CSTEST, &testdata);
		if (ret)
		{
			printf("Error from call to ioctl\n");
			iret = ::close(m_dev_handle);
			if (iret < 0)
			{
				printf("Error from call to ::close\n");
			}
			return;
		}
		ts_clock(&ts2);
		delta = ts_duration(ts1, ts2);

		if (delta < dmin)
			dmin = delta;
		if (delta > dmax)
			dmax = delta;
		dmean += delta;
	}

	printf("Minimum latency = %e seconds\n", dmin);
	printf("Maximum latency = %e seconds\n", dmax);
	printf("Average latency = %e seconds\n", dmean / 1000.0);

	iret = ::close(m_dev_handle);
	if (iret < 0)
	{
		printf("Error from call to ::close\n");
		return;
	}

	ret = ts_close(TS_DUMMY);
}


/*****************************/
void dump_event_list(u_int rol)
/*****************************/
{
	u_int entry, rol_nr = 0, *HashList;
	u_int lastid = 0, firstflag = 1, thisid = 0, totnum = 0;
	MgmtEntry *ItemList, *item;
	std::vector <L1idRange> pairs;
	L1idRange pair;

	printf("\n=========================================================================\n");
	if (rol == 99)
	{
		std::cout << "NOTE: ROL numbers are 0..N-1 " << std::endl;
		std::cout << "Enter the number of the ROL: " << std::endl;
		rol_nr = getdecd(rol_nr);
	}
	else
		rol_nr = rol;

	if (verbose)
		std::cout << "s_numHashLists is " << s_numHashLists << std::endl;


	HashList = monitoringBlock->getHashArrayPtr(rol_nr);
	ItemList = monitoringBlock->getItemListPtr(rol_nr);
	std::vector <u_int> l1id_list;
	l1id_list.clear();

	for (unsigned int loop = 0; loop < s_numHashLists; loop++)
	{
		entry = HashList[loop];
		//if (entry)
		//std::cout << "entry " << loop << " is " << entry << std::endl;
		if (entry)
		{
			item = &ItemList[entry];
			if (verbose)
				std::cout << "L1ID is " << item->info.upfInfo.eventId  << " in page " << item->info.upfInfo.pageNum << " with length " << item->info.upfInfo.pageLen << std::endl;
			l1id_list.push_back(item->info.upfInfo.eventId);

			while (item->next != 0)
			{
				item = &ItemList[item->next];
				if (verbose)
					std::cout << "L1ID is " << item->info.upfInfo.eventId  << std::endl;
				l1id_list.push_back(item->info.upfInfo.eventId);
			}
		}
	}

	pairs.clear();

	if (l1id_list.size() == 0)
		std::cout << "There are currently no events in the memory of this channel" << std::endl;
	else if (l1id_list.size() == 1)
		std::cout << "The memory of this channel contains one event with L1ID = " << l1id_list[0] << std::endl;
	else
	{
		sort(l1id_list.begin(), l1id_list.end(), std::less<int>());

		for (std::vector<u_int>::iterator it = l1id_list.begin(); it != l1id_list.end(); it++)
		{
			thisid = *it;
			if (firstflag)
			{
				pair.first = *it;
				lastid = *it;
				firstflag = 0;
				if (verbose)
					std::cout << "L1ID list: first pair.first = " << pair.first << std::endl;
			}
			else
			{
				if (*it == (lastid + 1))
				{
					lastid = *it;
					if (verbose)
						std::cout << "L1ID list: continuing with = " << *it << std::endl;
				}
				else
				{
					pair.last = lastid;
					if (verbose)
						std::cout << "L1ID list: pair.last = " << pair.last << std::endl;
					pairs.push_back(pair);
					lastid = *it;
					pair.first = *it;
					if (verbose)
						std::cout << "L1ID list: pair.first = " << pair.first << std::endl;
				}
			}
		}
		pair.last = thisid;
		pairs.push_back(pair);

		for (std::vector<L1idRange>::const_iterator it2 = pairs.begin(); it2 != pairs.end(); it2++)
		{
			pair = *it2;
			if (pair.first == pair.last)
			{
				std::cout << "L1ID = 0x" << HEX(pair.first) << std::endl;
				if (pair.first != 0xffffffff)
					totnum++;
			}
			else
			{
				std::cout << "First L1ID = 0x" << HEX(pair.first) << "  Last L1ID = 0x" << HEX(pair.last) << "  ( " << pair.last - pair.first + 1<< " events)" << std::endl;
				totnum += pair.last - pair.first + 1;
			}
		}
		std::cout << "There are currently " << totnum << " events in memory" << std::endl;
	}
}


/*************************/
void dump_config(u_int rol)
/*************************/
{
	u_int rol_nr = 0;

	printf("\n=========================================================================\n");
	if (rol == 99)
	{
		std::cout << "NOTE: ROL numbers are 0..N-1 " << std::endl;
		std::cout << "Enter the number of the ROL: " << std::endl;
		rol_nr = getdecd(rol_nr);
	}
	else{
		rol_nr = rol;
	}

	RobinNPConfig *config = monitoringBlock->getConfigPtr();

	config->dumpGlobal();
	if(rol < s_maxRols){
		config->dumpRol(rol);
	}
	else{
		std::cout << "ROL " << rol << " larger than maximum number (" << s_maxRols << ")" << std::endl;
	}
}


/*************************/
void dump_status(u_int rol)
/*************************/
{
	u_int rol_nr = 0;

	printf("\n=========================================================================\n");
	if (rol == 99)
	{
		std::cout << "Enter the number of the ROL: " << std::endl;
		rol_nr = getdecd(rol_nr);
	}
	else
		rol_nr = rol;

	RobinNPStats *statblock = monitoringBlock->getStatsPtr();

	statblock->dump();//Get General Card Stats
	if(rol < s_maxRols){
		statblock->m_rolStats[rol].dump(); // Get ROL-Specific Stats
	}
	else{
		std::cout << "ROL " << rol << " larger than maximum number (" << s_maxRols << ")" << std::endl;
	}
}


/*****************/
void dump_ram(void)
/*****************/
{
	volatile u_int *pci_ptr;

	printf("\n=========================================================================\n");
	pci_ptr = (u_int *)bar1_virtaddr;

	printf("Dumping first 5 words of the buffer memory\n");
	printf("Word 1 = 0x%08x\n", pci_ptr[0]);
	printf("Word 2 = 0x%08x\n", pci_ptr[1]);
	printf("Word 3 = 0x%08x\n", pci_ptr[2]);
	printf("Word 4 = 0x%08x\n", pci_ptr[3]);
	printf("Word 5 = 0x%08x\n", pci_ptr[4]);
}

/****************/
void scbench(void)
/****************/
{
	u_int ncycles = 1000, loop, loop2, ret;
	float delta;
	tstamp ts1, ts2;
	volatile u_int *pci_ptr, pci_reg;

	printf("\n=========================================================================\n");
	ret = ts_open(0, TS_DUMMY);
	if (ret)
	{
		rcc_error_print(stdout, ret);
		return;
	}

	pci_ptr = (unsigned int *)(bar1_virtaddr + 0x8);
	printf("Dumping first word\n");
	printf("Word 1 = 0x%08x\n", pci_ptr[0]);

	for(loop2 = 0; loop2 < 3; loop2++)
	{
		ts_clock(&ts1);
		for(loop = 0; loop < ncycles; loop++)
			pci_reg = pci_ptr[0];
		ts_clock(&ts2);
		delta = ts_duration(ts1, ts2);
		printf("%d. Duration of %d PCIe read cycles = %f microseconds\n", loop2 + 1, ncycles, delta * 1000000.0);
	}

	for(loop2 = 0; loop2 < 3; loop2++)
	{
		ts_clock(&ts1);
		for(loop = 0; loop < ncycles; loop++)
			pci_ptr[0] = pci_reg;
		pci_reg = pci_ptr[0];
		ts_clock(&ts2);
		delta = ts_duration(ts1, ts2);
		printf("%d. Duration of %d PCIe write cycles = %f microseconds\n", loop2 + 1,  ncycles, delta * 1000000.0);
	}

	ret = ts_close(TS_DUMMY);
	if (ret)
		rcc_error_print(stdout, ret);

}


/****************/
int setdebug(void)
/****************/
{
	u_int dblevel = 0, dbpackage = DFDB_IORCC;

	printf("\n=========================================================================\n");

	printf("Enter the debug level: ");
	dblevel = getdecd(dblevel);
	printf("Enter the debug package: ");
	dbpackage = getdecd(dbpackage);
	DF::GlobalDebugSettings::setup(dblevel, dbpackage);
	return(0);
}


/****************/
int mainhelp(void)
/****************/
{
	printf("\n=========================================================================\n");
	printf("Contact markus.joos@cern.ch if you need help\n");
	printf("=========================================================================\n\n");
	return(0);
}


/*****************/
void pci_peek(void)
/*****************/
{
	volatile u_int *pci_ptr;
	u_int regdata;
	volatile u_long *lpci_ptr;
	u_long lregdata;
	static u_int bar = 0, regsize = 0, offset = 0;

	printf("\n=========================================================================\n");
	printf("Select the transfer size (0=32bit, 1=64bit): ");
	regsize = getdecd(regsize);

	printf("Select the BAR (0 / 1): ");
	bar = getdecd(bar);

	printf("Select the offset of the register: ");
	offset = gethexd(offset);

	if(regsize == 0)
	{
		if (bar == 0)
			pci_ptr = (volatile u_int *) (bar0_virtaddr + offset);
		else
			pci_ptr = (volatile u_int *) (bar1_virtaddr + offset);

		regdata = *pci_ptr;
		printf("The register contains 0x%08x\n", regdata);
		printf("Enter data to write: ");
		regdata = gethexd(regdata);
		*pci_ptr = regdata;
		printf("Data written\n");
		regdata = *pci_ptr;
		printf("On reading back register contains 0x%08x\n", regdata);
	}
	else
	{
		if (bar == 0)
			lpci_ptr = (volatile u_long *) (bar0_virtaddr + offset);
		else
			lpci_ptr = (volatile u_long *) (bar1_virtaddr + offset);

		lregdata = *lpci_ptr;
		printf("The register contains 0x%016lx\n", lregdata);
		printf("Enter data to write: ");
		lregdata = gethexd(lregdata);
		*lpci_ptr = lregdata;
		printf("Data written\n");
		lregdata = *lpci_ptr;
		printf("On reading back register contains 0x%016lx\n", lregdata);
	}

	printf("=========================================================================\n\n");
}


/****************/
int reg_dump(void)
/****************/
{
	u_int value, loop, nsub;//, nchan;

	BAR0CommonStruct *bar0_ptr;
	BAR0ChannelStruct *bar0_cptr;
	BAR0SubRobStruct *bar0_sptr;

	printf("\n=========================================================================\n");

	printf("|------------------|\n");
	printf("| Global registers |\n");
	printf("|------------------|\n");

	bar0_ptr = (BAR0CommonStruct *)bar0_virtaddr;

	printf("Decoding DesignID:\n");
	printf("DesignMinorRevision                          : %d\n", bar0_ptr->DesignID.DesignMinorRevision);
	printf("RobinExpress                                 : %s\n", bar0_ptr->DesignID.RobinExpress?"Yes":"No");
	printf("Author                                       : ");
	if (bar0_ptr->DesignID.Author == 1)      printf("B. Green\n");
	else if (bar0_ptr->DesignID.Author == 2) printf("G. Kieft\n");
	else if (bar0_ptr->DesignID.Author == 3) printf("A. Kugel\n");
	else if (bar0_ptr->DesignID.Author == 4) printf("M. Mueller\n");
	else if (bar0_ptr->DesignID.Author == 5) printf("Krause\n");
	else if (bar0_ptr->DesignID.Author == 6) printf("N. Schroer\n");
	else if (bar0_ptr->DesignID.Author == 7) printf("A. Borga\n");
	else                                     printf("Unknown\n");
	printf("DesignMajorRevision                          : %d\n", bar0_ptr->DesignID.DesignMajorRevision);
	printf("DesignVersion                                : %d\n", bar0_ptr->DesignID.DesignVersion);
	printf("NumberOfChannels                             : %d\n", bar0_ptr->DesignID.NumberOfChannels);
	printf("NumberOfChannelsPerSubRob                    : %d\n", bar0_ptr->DesignID.NumberOfChannelsPerSubRob);
	printf("NumberOfSubRobs                              : %d\n", bar0_ptr->DesignID.NumberOfSubRobs);

	nsub  = bar0_ptr->DesignID.NumberOfSubRobs;
	//nchan = bar0_ptr->DesignID.NumberOfChannels;

	printf("\nDecoding GlobalControlRegister:\n");
	printf("GlobalReset                                  : %d\n", bar0_ptr->GlobalControlRegister.GlobalReset);
	printf("InterrupterReset                             : %d\n", bar0_ptr->GlobalControlRegister.InterrupterReset);

	printf("\nDecoding InterruptStatusReg:\n");
	printf("MSIAllowed                                   : %s\n", bar0_ptr->InterruptStatusRegister.Component.MSIAllowed?"Yes":"No");
	printf("NumberOfMSIsRequested                        : %d\n", bar0_ptr->InterruptStatusRegister.Component.NumberOfMSIsRequested);
	printf("NumberOfMSIsAllocated                        : %d\n", bar0_ptr->InterruptStatusRegister.Component.NumberOfMSIsAllocated);
	printf("SixtyFourBitCapable                          : %d\n", bar0_ptr->InterruptStatusRegister.Component.SixtyFourBitCapable);
	printf("PerVectorMaskingCapable                      : %d\n", bar0_ptr->InterruptStatusRegister.Component.PerVectorMaskingCapable);
	printf("InIdleState                                  : %d\n", bar0_ptr->InterruptStatusRegister.Component.InIdleState);
	printf("InWaitingForACKState                         : %d\n", bar0_ptr->InterruptStatusRegister.Component.InWaitingForACKState);
	printf("InWaitingForEnableResetState                 : %d\n", bar0_ptr->InterruptStatusRegister.Component.InWaitingForEnableResetState);
	printf("filler                                       : %d\n", bar0_ptr->InterruptStatusRegister.Component.filler);
	printf("InterruptCount                               : %d\n", bar0_ptr->InterruptStatusRegister.Component.InterruptCount);

	printf("\nDecoding InterruptMaskReg:\n");
	printf("PrimaryDMADoneNotEmpty                       : %s\n", bar0_ptr->InterruptMaskRegister.Component.PrimaryDMADoneNotEmpty?"Set":"Not set");
	printf("PrimaryDMADoneDuplicateNotEmpty              : %s\n", bar0_ptr->InterruptMaskRegister.Component.PrimaryDMADoneDuplicateNotEmpty?"Set":"Not set");
	printf("FreePageFIFONotEmpty                         : %s\n", bar0_ptr->InterruptMaskRegister.Component.CommonUsedPageFIFONotEmpty?"Set":"Not set");
	printf("FreePageFIFODuplicateNotEmpty                : %s\n", bar0_ptr->InterruptMaskRegister.Component.CommonUsedPageFIFODuplicateNotEmpty?"Set":"Not set");

	printf("\nDecoding InterruptControlReg:\n");
	printf("acknowledge                                  : 0x%08x\n", bar0_ptr->InterruptControlRegister.acknowledge);

	printf("\nDecoding InterruptTestReg:\n");
	printf("Bottom                                       : 0x%08x\n", bar0_ptr->InterruptTestRegister.Bottom);

	printf("\nDecoding InterruptMaskReg:\n");
	printf("PrimaryDMADoneNotEmpty                       : %s\n", bar0_ptr->InterruptPendingRegister.Component.PrimaryDMADoneNotEmpty?"Set":"Not set");
	printf("PrimaryDMADoneDuplicateNotEmpty              : %s\n", bar0_ptr->InterruptPendingRegister.Component.PrimaryDMADoneDuplicateNotEmpty?"Set":"Not set");
	printf("FreePageFIFONotEmpty                         : %s\n", bar0_ptr->InterruptPendingRegister.Component.CommonUsedPageFIFONotEmpty?"Set":"Not set");
	printf("FreePageFIFODuplicateNotEmpty                : %s\n", bar0_ptr->InterruptPendingRegister.Component.CommonUsedPageFIFODuplicateNotEmpty?"Set":"Not set");


	printf("|------------------|\n");
	printf("| SubRob registers |\n");
	printf("|------------------|\n");


	for (loop = 0; loop < nsub; loop++)
	{
		bar0_sptr = (BAR0SubRobStruct *)(bar0_virtaddr + 0x4000 + 0x200 * loop);

		printf(" ---> SubRob %d  <---\n", loop);

		printf("\nDecoding MemoryReadQueueFIFO:\n");
		printf("...to be done...\n");

		printf("PCI Address                              : 0x%016lx\n", (((u_long)bar0_sptr->MemoryReadQueueFIFO.Component.pciAddressUpper & 0x3fffffff) << 32) + bar0_sptr->MemoryReadQueueFIFO.Component.pciAddressLower);
		printf("Enable termination word                  : %s\n", (bar0_sptr->MemoryReadQueueFIFO.Component.pciAddressUpper & 0x40000000)?"Set":"Not set");
		printf("Enable offset                            : %s\n", (bar0_sptr->MemoryReadQueueFIFO.Component.pciAddressUpper & 0x80000000)?"Set":"Not set");
		printf("Transfer Size                            : 0x%08x\n", bar0_sptr->MemoryReadQueueFIFO.Component.transferSize);
		printf("Local address                            : 0x%08x\n", bar0_sptr->MemoryReadQueueFIFO.Component.localAddress);

		printf("\nDecoding CaptureDMAQueueFIFO:\n");
		printf("...to be done...\n");

		printf("\nDecoding PrimaryDMAStatusRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding PrimaryDMAControlRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding PrimaryDMADoneFIFO:\n");
		printf("...to be done...\n");

		printf("\nDecoding SubRobStatus:\n");
		printf("...to be done...\n");

		printf("\nDecoding PrimaryDMADoneFIFODuplicator:\n");
		printf("...to be done...\n");

		printf("\nDecoding SubRobControl:\n");
		printf("...to be done...\n");

		printf("\nDecoding CommonUPFBottom:\n");
		printf("...to be done...\n");

		printf("\nDecoding CommonUPFTop:\n");
		printf("...to be done...\n");

		printf("\nDecoding CommonUPFBottomInvalidate:\n");
		printf("...to be done...\n");

		printf("\nDecoding CommonUPFTopInvalidate:\n");
		printf("...to be done...\n");

		printf("\nDecoding MemoryOutputFIFOBottom:\n");
		printf("...to be done...\n");

		printf("\nDecoding MemoryOutputFIFOTop:\n");
		printf("...to be done...\n");

		printf("\nDecoding MemoryOutputFIFOBottomInvalidate:\n");
		printf("...to be done...\n");

		printf("\nDecoding MemoryOutputFIFOTopInvalidate:\n");
		printf("...to be done...\n");

		printf("\nDecoding UPFDuplicateStartLocationRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding UPFDuplicateWriteIndexRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding UPFDuplicateWriteIndexLocationRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding UPFDuplicateReadIndexRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding PipelineDMAQueueFIFO:\n");
		printf("...to be done...\n");

		printf("\nDecoding PipelineDMAStatusRegister:\n");

		printf("\nDecoding PipelineDMADoneFIFO:\n");
		printf("...to be done...\n");

		printf("\nDecoding MemoryControllerStatus:\n");
		printf("...to be done...\n");
	}

	printf("|-------------------|\n");
	printf("| Channel registers |\n");
	printf("|-------------------|\n");

	for (loop = 0; loop < nsub; loop++)
	{
		printf(" ---> Channel %d  <---\n", loop);

		bar0_cptr = (BAR0ChannelStruct *)(bar0_virtaddr + 0x1000 + 0x100 * loop);

		printf("\nDecoding BufferManagerControlRegister:\n");

		printf("BufferPageSize		  : ");
		value = bar0_cptr->BufferManagerControlRegister.Component.BufferPageSize;
		if (value == 0) printf("1024 bytes\n");
		else if (value == 1) printf("2048 bytes\n");
		else if (value == 2) printf("4096 bytes\n");
		else if (value == 3) printf("8192 bytes\n");
		else if (value == 4) printf("16384 bytes\n");
		else if (value == 5) printf("32768 bytes\n");
		else if (value == 6) printf("65536 bytes\n");
		else if (value == 7) printf("8192 bytes\n");
		else printf("\n");
		printf("ResetBit			  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.ResetBit?"Yes":"No");
		printf("FinishFormatVersionError 	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishFormatVersionError?"Yes":"No");
		printf("FinishFragSizeError      	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishFragSizeError?"Yes":"No");
		printf("FinishHdrMarkerError     	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishHdrMarkerError?"Yes":"No");
		printf("FinishCNTR              	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishCNTR?"Yes":"No");
		printf("FinishBOF                	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishBOF?"Yes":"No");
		printf("FinishEOF                	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishEOF?"Yes":"No");
		printf("FinishGeneralError        	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishGeneralError?"Yes":"No");
		printf("FinishTriggerType        	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishTriggerType?"Yes":"No");
		printf("FinishRunNumber          	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishRunNumber?"Yes":"No");
		printf("FinishMissingBOF       	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishMissingBOF?"Yes":"No");
		printf("FinishMissingEOF      	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishMissingEOF?"Yes":"No");
		printf("FinishIncomplHeader     	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishIncomplHeader?"Yes":"No");
		printf("FinishNoHeader           	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishNoHeader?"Yes":"No");
		printf("FinishCTLError         	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishCTLError?"Yes":"No");
		printf("FinishDataError         	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.FinishDataError?"Yes":"No");
		printf("SuppressCNTLWrdStorage	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.SuppressCNTLWrdStorage?"Yes":"No");
		printf("XOFF                    	  : %s\n", bar0_cptr->BufferManagerControlRegister.Component.XOFF?"Yes":"No");

		printf("\nDecoding BufferManagerStatusRegister:\n");
		printf("...to be done...\n");

		printf("\nDecoding BufferManagerUsedFIFOBottom:\n");
		printf("...to be done...\n");

		printf("\nDecoding BufferManagerUsedFIFOTop:\n");
		printf("...to be done...\n");

		printf("\nDecoding BufferManagerUsedFIFOTopInvalidate:\n");
		printf("...to be done...\n");

		printf("\nDecoding HOLABypassStatus:\n");
		printf("...to be done...\n");

		printf("\nDecoding HOLABypassRX:\n");
		printf("...to be done...\n");

		printf("\nDecoding HOLABypassTXWrite:\n");
		printf("...to be done...\n");

		printf("\nDecoding HOLABypassTXControl:\n");
		printf("...to be done...\n");

		printf("\nDecoding ChannelControlRegister:\n");
		printf("GTX_Reset 	                        : %s\n", bar0_cptr->ChannelControlRegister.Component.GTX_Reset?"Yes":"No");
		printf("HOLA_Reset			        : %s\n", bar0_cptr->ChannelControlRegister.Component.HOLA_Reset?"Yes":"No");
		printf("BufferManagerReset 		        : %s\n", bar0_cptr->ChannelControlRegister.Component.BufferManagerReset?"Yes":"No");
		printf("CRCEnable 		        : %s\n", bar0_cptr->ChannelControlRegister.Component.CRCEnable?"Yes":"No");

		printf("\nDecoding ChannelStatusRegister:\n");

		printf("ChannelNumber			: %d\n", bar0_cptr->ChannelStatusRegister.Component.ChannelNumber);
		printf("ROLHandler_intUXOFF			: %s\n", bar0_cptr->ChannelStatusRegister.Component.HOLA_UXOFF_n?"Yes":"No");
		printf("ROLHandler_LDCEmulationEnabled	: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROLHandler_LDCEmulationEnabled?"Yes":"No");
		printf("ROL_LinkDataError			: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROL_LinkDataError?"Yes":"No");
		printf("ROL_LinkUp				: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROL_LinkUp?"Yes":"No");
		printf("ROL_FlowControl			: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROL_FlowControl?"Yes":"No");
		printf("ROL_Activity			: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROL_Activity?"Yes":"No");
		printf("ROL_Test				: %s\n", bar0_cptr->ChannelStatusRegister.Component.ROL_Test?"Yes":"No");
		printf("GTX_ResetDone			: %s\n", bar0_cptr->ChannelStatusRegister.Component.GTX_ResetDone?"Yes":"No");
	}



	/*
  printf("\nDecoding UPFDuplicateStartLocationRegStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateStartLocationRegister.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateStartLocationRegister.upper);

  printf("\nDecoding UPFDuplicateWriteIndexRegStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateWriteIndexRegister.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateWriteIndexRegister.upper);

  printf("\nDecoding UPFDuplicateWriteIndexLocationRegStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateWriteIndexLocationRegister.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateWriteIndexLocationRegister.upper);

  printf("\nDecoding UPFDuplicateReadIndexRegStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateReadIndexRegister.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->UPFDuplicateReadIndexRegister.upper);

  printf("\nDecoding PrimaryDMAStatusRegStruct:\n");
  printf("DMAStatus                                    : ");

  if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 0)       printf("(idle state): Last transfer ended successfully\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 1)  printf("(idle state): Last transfer was stopped by backend\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 2)  printf("(idle state): Last transfer ended because of CPL timeout\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 3)  printf("(idle state): Last transfer ended because of CPL UR error\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 4)  printf("(idle state): Last transfer ended because of CPL CA error\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 8)  printf("(busy state): Channel is busy processing: DMA channel computes next action\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 9)  printf("(busy state): Requesting transfer: DMA channel attempts to send a request to the PCI Express Link\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 10) printf("Waiting for Completion: Waiting for Completion(s): DMA channel waits for reception of Completion(s) that correspond to sent requests\n");
  else if (bar0_s0ptr->PrimaryDMAStatusRegister.Component.Status == 11) printf("Waiting for backend to provide/accept data: DMA is in FIFO mode and DMA FIFO COUNT: DMAN_FIFOCNT is not large enough to allow data transfer\n");
  else                                                                           printf("Undefined value\n");
  printf("FillError                           : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFOFillError);
  printf("PrimaryDMADoneFIFOFill                       : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.DoneFIFOFill);
  printf("PrimaryDMADoneFIFOEmpty                      : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.DoneFIFOEmpty);
  printf("PrimaryDMADoneFIFOFull                       : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.DoneFIFOFull);
  printf("PrimaryDMAQueueFIFODataAvailable             : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFODataAvailable);
  printf("PrimaryDMAFill                      : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFOFill);
  printf("PrimaryDMAQueueFIFOFull                      : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFOFull);
  printf("PrimaryDMAStateIsIdle                        : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.StateIsIdle);
  printf("PrimaryDMAStateIsWaitingForStart             : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.StateIsWaitingForStart);
  printf("PrimaryDMAStateIsWaitingForEnd               : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.StateIsWaitingForEnd);
  printf("PrimaryDMAFill0                     : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFOFill0);
  printf("PrimaryDMAFill1                     : %d\n", bar0_s0ptr->PrimaryDMAStatusRegister.Component.QueueFIFOFill1);


  printf("\nDecoding PrimaryDMADoneFIFOStruct:\n");
  printf("pciAddressLower                              : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.pciAddressLower);
  printf("pciAddressUpper                              : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.pciAddressUpper);
  printf("transferSize                                 : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.transferSize);
  printf("localAddress                                 : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.localAddress);
  printf("transferSizeInvalidate                       : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.transferSizeInvalidate);
  printf("localAddressInvalidate                       : %d\n", bar0_s0ptr->PrimaryDMADoneFIFO.localAddressInvalidate);

  printf("\nDecoding PrimaryDMADoneFIFODuplicatorStruct:\n");
  printf("PrimaryDMADoneFIFODuplicatorReadIndexBottom   : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.ReadIndexBottom);
  printf("PrimaryDMADoneFIFODuplicatorReadIndexTop      : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.ReadIndexTop);
  printf("PrimaryDMADoneFIFODuplicatorWriteIndexBottom  : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.WriteIndexBottom);
  printf("PrimaryDMADoneFIFODuplicatorWriteIndexTop     : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.WriteIndexTop);
  printf("PrimaryDMADoneFIFODuplicatorLocationRegBottom : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.RingBufferLocationBottom);
  printf("PrimaryDMADoneFIFODuplicatorLocationRegTop    : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.RingBufferLocationTop);
  printf("PrimaryDMADoneFIFODuplicatorTimeRegBottom     : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.TimeBottom);
  printf("PrimaryDMADoneFIFODuplicatorTimeRegTop        : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.TimeTop);
  printf("PrimaryDMADoneFIFODuplicatorWILocationBottom  : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationBottom);
  printf("PrimaryDMADoneFIFODuplicatorWILocationTop     : 0x%08x\n", bar0_s0ptr->PrimaryDMADoneFIFODuplicator.WriteIndexCopyLocationTop);

  printf("\nDecoding CommonUPFBottomStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->CommonUPFBottom.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->CommonUPFBottom.upper);

  printf("\nDecoding CommonUPFTopStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->CommonUPFTop.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->CommonUPFTop.upper);

  printf("\nDecoding CommonUPFBottomInvalidateStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->CommonUPFBottomInvalidate.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->CommonUPFBottomInvalidate.upper);

  printf("\nDecoding CommonUPFTopInvalidateStruct:\n");
  printf("lower                                        : 0x%08x\n", bar0_s0ptr->CommonUPFTopInvalidate.lower);
  printf("upper                                        : 0x%08x\n", bar0_s0ptr->CommonUPFTopInvalidate.upper);
	 */

	/*
  printf("\nDecoding MACConfigRegStruct:\n");
  printf(" : %d\n", bar0_ptr->MACConfigRegister.);
  printf("\nDecoding MACHeaderFIFOStatusStruct:\n");
  printf(" : %d\n", bar0_ptr->MACHeaderStatusFIFO.);
  printf("\nDecoding MACHeaderFIFOStruct:\n");
  printf(" : %d\n", bar0_ptr->MACHeaderFIFO.);
  printf("\nDecoding HOLABypassStatusStruct:\n");
  printf(" : %d\n", bar0_ptr->HOLABypassStatusRegister.);
  printf("\nDecoding PCIDPMStatusRegStruct:\n");
  printf(" : %d\n", bar0_ptr->PCIDPMStatusRegister.);
  printf("\nDecoding HOLABypassRXStruct:\n");
  printf(" : %d\n", bar0_ptr->HOLABypassRX.);
  printf("\nDecoding MACRXDesriptorStruct:\n");
  printf(" : %d\n", bar0_ptr->MACRXDescriptor.);
  printf("\nDecoding HOLABypassTXWriteStruct:\n");
  printf(" : %d\n", bar0_ptr->HOLABypassTXWrite.);
  printf("\nDecoding HOLABypassTXControlStruct:\n");
  printf(" : %d\n", bar0_ptr->HOLABypassTXControl.);
  printf("\nDecoding EtMisDuplicateStartLocationRegStruct:\n");
  printf(" : %d\n", bar0_ptr->EtMisDuplicateStartLocationRegister.);
  printf("\nDecoding EtMisDuplicateWriteIndexRegStruct:\n");
  printf(" : %d\n", bar0_ptr->EtMisDuplicateWriteIndexRegister.);
  printf("\nDecoding EtMisDuplicateWriteIndexLocationRegStruct:\n");
  printf(" : %d\n", bar0_ptr->EtMisDuplicateWriteIndexLocationRegister.);
  printf("\nDecoding EtMisDuplicateReadIndexRegStruct:\n");
  printf(" : %d\n", bar0_ptr->EtMisDuplicateReadIndexRegister.);
  //write only? printf("\nDecoding PrimaryDMAQueueFIFOStruct:\n");
  //write only? printf(" : %d\n", bar0_ptr->CaptureDMAQueueFIFO.);
	 */
	/*
  printf("bar0_virtaddr size is    %ld\n", sizeof(bar0_virtaddr));
  printf("bar0_virtaddr      is at 0x%16lx\n", bar0_virtaddr);
  printf("bar0_ptr           is at 0x%16llx\n", (long long unsigned int)bar0_ptr);
  printf("bar0_ptr->DesignID is at 0x%16llx\n", (long long unsigned int)&bar0_ptr->DesignID);

  pci_ptr = (unsigned int *)(bar0_virtaddr + 8);
  printf("pci_ptr            is at 0x%16llx\n", (long long unsigned int)pci_ptr);

  printf("Dumping first 2 words\n");
  printf("Word 1 = 0x%08x\n", pci_ptr[0]);  
  printf("Word 2 = 0x%08x\n", pci_ptr[1]);  
	 */
	/*
  printf("\nChannel specific registers of channel #1:\n");
  printf("=========================================\n");

	 */
	printf("=========================================================================\n\n");
	return(0);
}


void check_offsets(void)
{
	BAR0SubRobStruct *bar0_sptr;
	BAR0CommonStruct *bar0_gptr;
	BAR0ChannelStruct *bar0_cptr;

	printf("\n=========================================================================\n");

	printf("|------------------|\n");
	printf("| Global registers |\n");
	printf("|------------------|\n");

	bar0_gptr = (BAR0CommonStruct *)0;

	printf("DesignID                 is at offset 0x%016lx\n", (u_long)&bar0_gptr->DesignID);
	printf("GlobalControlRegister    is at offset 0x%016lx\n", (u_long)&bar0_gptr->GlobalControlRegister);
	printf("InterruptStatusRegister  is at offset 0x%016lx\n", (u_long)&bar0_gptr->InterruptStatusRegister);
	printf("InterruptMaskRegister    is at offset 0x%016lx\n", (u_long)&bar0_gptr->InterruptMaskRegister);
	printf("InterruptControlRegister is at offset 0x%016lx\n", (u_long)&bar0_gptr->InterruptControlRegister);
	printf("InterruptTestRegister    is at offset 0x%016lx\n", (u_long)&bar0_gptr->InterruptTestRegister);
	printf("InterruptPendingRegister is at offset 0x%016lx\n", (u_long)&bar0_gptr->InterruptPendingRegister);

	printf("|------------------|\n");
	printf("| SubRob registers |\n");
	printf("|------------------|\n");

	bar0_sptr = (BAR0SubRobStruct *)0;

	printf("MemoryReadQueueFIFO                    is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryReadQueueFIFO);
	printf("CaptureDMAQueueFIFO                    is at offset 0x%016lx\n", (u_long)&bar0_sptr->CaptureDMAQueueFIFO);
	printf("PrimaryDMAStatusRegister               is at offset 0x%016lx\n", (u_long)&bar0_sptr->PrimaryDMAStatusRegister);
	printf("PrimaryDMAControlRegister              is at offset 0x%016lx\n", (u_long)&bar0_sptr->PrimaryDMAControlRegister);
	printf("PrimaryDMADoneFIFO                     is at offset 0x%016lx\n", (u_long)&bar0_sptr->PrimaryDMADoneFIFO);
	printf("SubRobStatus                           is at offset 0x%016lx\n", (u_long)&bar0_sptr->SubRobStatus);
	printf("PrimaryDMADoneFIFODuplicator           is at offset 0x%016lx\n", (u_long)&bar0_sptr->PrimaryDMADoneFIFODuplicator);
	printf("SubRobControl                          is at offset 0x%016lx\n", (u_long)&bar0_sptr->SubRobControl);
	printf("CommonUPFBottom                        is at offset 0x%016lx\n", (u_long)&bar0_sptr->CommonUPFBottom);
	printf("CommonUPFTop                           is at offset 0x%016lx\n", (u_long)&bar0_sptr->CommonUPFTop);
	printf("CommonUPFBottomInvalidate              is at offset 0x%016lx\n", (u_long)&bar0_sptr->CommonUPFBottomInvalidate);
	printf("CommonUPFTopInvalidate                 is at offset 0x%016lx\n", (u_long)&bar0_sptr->CommonUPFTopInvalidate);
	//printf("MemoryOutputFIFOBottom                 is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryOutputSecondFIFOBottom);
	//printf("MemoryOutputFIFOTop                    is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryOutputSecondFIFOTop);
	//printf("MemoryOutputFIFOBottomInvalidate       is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryOutputSecondFIFOBottomInvalidate);
	//printf("MemoryOutputFIFOTopInvalidate          is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryOutputSecondFIFOTopInvalidate);
	printf("UPFDuplicateStartLocationRegister      is at offset 0x%016lx\n", (u_long)&bar0_sptr->UPFDuplicateStartLocationRegister);
	printf("UPFDuplicateWriteIndexRegister         is at offset 0x%016lx\n", (u_long)&bar0_sptr->UPFDuplicateWriteIndexRegister);
	printf("UPFDuplicateWriteIndexLocationRegister is at offset 0x%016lx\n", (u_long)&bar0_sptr->UPFDuplicateWriteIndexLocationRegister);
	printf("UPFDuplicateReadIndexRegister          is at offset 0x%016lx\n", (u_long)&bar0_sptr->UPFDuplicateReadIndexRegister);

	printf("MemoryControllerStatus                 is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryControllerStatus);
	printf("MemoryControllerControl                is at offset 0x%016lx\n", (u_long)&bar0_sptr->MemoryControllerControl);

	printf("|-------------------|\n");
	printf("| Channel registers |\n");
	printf("|-------------------|\n");

	bar0_cptr = (BAR0ChannelStruct *)0;

	printf("BufferManagerControlRegister       is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerControlRegister);
	printf("BufferManagerStatusRegister        is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerStatusRegister);
	printf("BufferManagerUsedFIFOBottom        is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerUsedFIFOBottom);
	printf("BufferManagerUsedFIFOTop           is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerUsedFIFOTop);
	printf("BufferManagerUsedFIFOTopInvalidate is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerUsedFIFOTopInvalidate);
	printf("ROLHandlerLDCEMU                   is at offset 0x%016lx\n", (u_long)&bar0_cptr->DataGeneratorControlRegister);
	printf("BufferManagerFreeFIFO              is at offset 0x%016lx\n", (u_long)&bar0_cptr->BufferManagerFreeFIFO);
	printf("HOLABypassStatus                   is at offset 0x%016lx\n", (u_long)&bar0_cptr->HOLABypassStatus);
	printf("HOLABypassRX                       is at offset 0x%016lx\n", (u_long)&bar0_cptr->HOLABypassRX);
	printf("HOLABypassTXWrite                  is at offset 0x%016lx\n", (u_long)&bar0_cptr->HOLABypassTXWrite);
	printf("HOLABypassTXControl                is at offset 0x%016lx\n", (u_long)&bar0_cptr->HOLABypassTXControl);
	printf("ChannelControlRegister             is at offset 0x%016lx\n", (u_long)&bar0_cptr->ChannelControlRegister);
	printf("ChannelStatusRegister              is at offset 0x%016lx\n", (u_long)&bar0_cptr->ChannelStatusRegister);

	printf("=========================================================================\n\n");
}
